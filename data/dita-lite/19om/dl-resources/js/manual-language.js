﻿//-------------------------------------------------
// LANGUAGE
//-------------------------------------------------
!function (_DL) {
    const __ = _DL;

    let $W, $D, $H, $B, $HB, $H1, $H2, $BTN1, $BTN2, $M, $N, $FC, $F, $T;
    let _W, _D, _H, _B, _HB, _H1, _H2, _BTN1, _BTN2, _M, _N, _FC, _F, _T;
    let TOUCH, TOUCH_CLICK, TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_ENTER, TOUCH_LEAVE;

    let $LANG;
    let _LANG;

    __.language = {
        Init: function () {
            //* this.$.Init();
            //* this.add.Init();
        },
        $: {
            Init: function () {
                // $W = $(_W = window);
                // $D = $(_D = document);
                // $H = $(_H = 'html');
                $B = $(_B = 'body');
                // $HB = $(_HB = _H + ',' + _B);
                // $H1 = $(_H1 = 'header[role="global"]');
                // $H2 = $(_H2 = 'header[role="manual"]');
                // $M = $(_M = 'main[role="main_contents"]');
                // $N = $(_N = 'nav[role="side_toc"]');
                // $FC = $(_FC = 'nav[role="footer_control"]');
                // $F = $(_F = 'footer');
                // $T = $(_T = '#page_top a');

                $BTN2 = $(_BTN2 = '.header_btn.language a');
                $LANG = $(_LANG = '.header_btn.language ul');

            }
        },
        is: {
            show: false
        },
        set: {
            Init: function () { }
        },
        add: {
            Init: function () {
                this.click.Show();
            },
            click: {
                Show: function () {
                    $BTN2.on('click', function (E) {
                        E.preventDefault();
                        __.language.action.ShowHide();
                    });
                }
            }
        },
        action: {
            Init: function () { },
            Set: function (_target) {
                __.location.language = _target;
            },
            ShowHide: function () {

                if ($BTN2.hasClass('open')) {
                    $LANG.stop().animate({ right: '-100%' }, 0, 'swing');
                    $LANG.removeClass('open');
                    $BTN2.removeClass('open');

                    $B.removeClass('language_open');

                    __.language.is.show = false;
                } else {
                    $LANG.stop().animate({ right: '0' }, 0, 'swing');
                    $LANG.addClass('open');
                    $BTN2.addClass('open');

                    $B.addClass('language_open');

                    __.language.is.show = true;

                }
            },
        }

    };

    window.DL = __;
}(window.DL || {});

$(function () {
    DL.language.Init();
});