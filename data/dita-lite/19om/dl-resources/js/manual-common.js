//-------------------------------------------------
// common
//-------------------------------------------------
!function (_DL) {
    const __ = _DL;

    const G_HEADER_H = 60
        , M_HEADER_H = 80
        , FOOTER_H = 50
        , MARGIN = 20
        , WIN_BREAK1 = 1366
        , WIN_BREAK2 = 1024
        , WIN_BREAK3 = 600
        , DURATION = 300
        ;

    const LANGUAGES = ['ar-eg', 'be-by', 'bg-bg', 'bs-ba', 'ca-es', 'cs-cz', 'da-dk', 'de-ch', 'de-de', 'el-gr', 'en-ca', 'en-gb', 'en-us', 'es-419', 'es-es', 'et-ee', 'fi-fi', 'fr-be', 'fr-ca', 'fr-ch', 'fr-fr', 'he-il', 'hi-in', 'hr-hr', 'hu-hu', 'id-id', 'is-is', 'it-ch', 'it-it', 'ja-jp', 'kk-kz', 'ko-kr', 'lt-lt', 'lv-lv', 'mk-mk', 'ms-my', 'nl-be', 'nl-nl', 'no-no', 'pl-pl', 'pt-br', 'pt-pt', 'ro-ro', 'ru-ru', 'sk-sk', 'sl-si', 'sr-latn-me', 'sr-latn-rs', 'sr-rs', 'sr-sp', 'sv-se', 'th-th', 'tr-tr', 'uk-ua', 'ur-pk', 'vi-vn', 'zh-cn', 'zh-hans', 'zh-hant', 'zh-tw'];

    const WINDOW_MODE = 1; // pub:0, dev:1;

    let $W, $D, $H, $B, $HB, $H1, $H2, $BTN1, $BTN2, $M, $N, $FC, $F, $T;
    let _W, _D, _H, _B, _HB, _H1, _H2, _BTN1, _BTN2, _M, _N, _FC, _F, _T;
    let TOUCH, TOUCH_CLICK, TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_ENTER, TOUCH_LEAVE;

    __.LOG = function () { };


    // -------------------------------------------------------
    // Init
    // -------------------------------------------------------
    __.Init = function(){

        this.util.Init();
        this.$.Init();
        this.location.Init();
        this.window.Init();
        this.document.Init();

        __.LOG('// -- DL start -- //');
    };

    // -------------------------------------------------------
    // util
    // -------------------------------------------------------
    __.util = {
        Init: function () {
            this.set.Log();
        },
        set: {
            History: function (uri) {
                history.replaceState(null, null, uri);
            },
            Log: function () {
                __.LOG = (function () {
                    return !!__.window.mode ? console.log : function () { };
                })();
            }
        },
        get: {

        },
        change: {
            Query2obj: function (query) {
                let result = {};
                query.substr(1).split('&').every(function (V) {
                    const tmp = V.split('=');
                    return result[tmp[0]] = tmp[1];
                });
                return result;
            },
            Obj2query: function (_obj) {
                const obj = __.util.object.Assign(__.location.query,_obj);
                let result = [];
                Object.keys(obj).every(function (V) {
                    return obj[V] ? result.push(V + '=' + obj[V]) : !obj[V];
                });
                return '?' + result.join('&');
            }
        },
        object: {
            Assign: function () {
                const arg = arguments;
                const base = arg[0];
                Array.prototype.forEach.call(arg, function (O, I) {
                    if (!!I) {
                        for (let key in O) {
                            if (Object.prototype.hasOwnProperty.call(O, key)) {
                                base[key] = O[key];
                            }
                        }
                    }
                });
                return base;

            }
        }
    };

    // -------------------------------------------------------
    // $
    // -------------------------------------------------------
    __.$ = {
        Init: function () {
            $W = $(_W = window);
            $D = $(_D = document);
            $H = $(_H = 'html');
            $B = $(_B = 'body');
            $HB = $(_HB = _H + ',' + _B);
            $H1 = $(_H1 = 'header[role="global"]');
            $H2 = $(_H2 = 'header[role="manual"]');

            $BTN1 = $(_BTN1 = '.header_btn.toc a');
            $BTN2 = $(_BTN2 = '.header_btn.language a');

            $M = $(_M = 'main[role="main_contents"]');
            $N = $(_N = 'nav[role="side_toc"]');
            $FC = $(_FC = 'nav[role="footer_control"]');
            $F = $(_F = 'footer');
            $T = $(_T = '#page_top a');
        }
    };

    // -------------------------------------------------------
    // location
    // -------------------------------------------------------
    __.location = {
        Init: function () {
        },
        _query: '',
        get hash() {
            return location.hash;
        },
        get query() {
            return this._query = __.util.change.Query2obj(location.search);
        },
        set query(obj) {
            __.util.set.History(this.hash + __.util.change.Obj2query(obj));
        },
        pathname: location.pathname,
        get language() {
            const pathname = this.pathname;
            let result = '';
            LANGUAGES.some(function (lang) {
                const _lang = lang.split('-')[0];
                if (pathname.indexOf('/' + lang + '/') >= 0) {
                    return result = lang;
                } else if (pathname.indexOf('/' + _lang + '/') >= 0) {
                    return result = _lang;
                }
                return false;
            });
            return result;
        },
        set language(_target) {
            const pathname = this.pathname;
            const target = '/' + _target + '/';

            let old = '';
            LANGUAGES.some(function (lang) {
                const _lang = lang.split('-')[0];
                if (pathname.indexOf(old = '/' + lang + '/') >= 0) {
                    return location.assign(pathname.replace(old, target));
                } else if (pathname.indexOf(old = '/' + _lang + '/') >= 0) {
                    return location.assign(pathname.replace(old, target));
                }
                return false;
            });
            throw new Error('No-Language in pathname');
        },
        extend: {
            Init: function () {
            }
        }
    };

    // -------------------------------------------------------
    // window
    // -------------------------------------------------------
    __.window = {
        Init: function () {
            this.get.Init();
            this.set.Init();

            this.add.Init();
        },

        width: 0,
        height: 0,

        device: '',
        isPC: false,
        isSP: false,
        isTB: false,

        ontouch: false,
        userAgent: '',
        fixed: false,
        scrollTop: 0,
        scrollBottom: 0,

        _mode: WINDOW_MODE,
        get mode() {
            return this._mode;
        },
        set mode(mode) {
            this._mode = mode;
            __.util.set.Log();
        },
        Mode: function () {
            this._mode = (this._mode + 1) % 2;
        },


        add: {
            Init: function () {
                this.Load();
                this.Resize();
                this.Scroll();
            },
            Load: function () {
                if (__.location.hash) {
                    const $target = $(__.location.hash);
                    if (!!$target.length) {
                        setTimeout(function () {

                            const position = $target.offset().top - (G_HEADER_H + 10);
                            $HB.stop().animate({ scrollTop: position });
                            $target.addClass('anchortarget');

                            __.LOG(position);
                        }, 300);

                    }
                }
            },
            Resize: function () {
                $W.on('resize', function () {
                    __.window.get.Init();
                    __.window.set.Init();

                    __.document.get.Init();
                    __.document.set.Init();
                });
            },
            Scroll: function () {
                $W.on('scroll', function (E) {
                    __.window.get.Scroll();
                    __.window.set.Scroll();

                });
            }
        },
        get: {
            Init: function () {
                this.Width();
                this.Height();
                this.Scroll();
                this.Device();
                this.UserAgent();
                this.is.Init();
            },
            Width: function () {
                __.window.width = $W.width();
            },


            Height: function () {
                __.window.height = window.innerHeight || $W.height();
            },
            Scroll: function () {
                __.window.scrollTop = $W.scrollTop();
            },
            Device: function () {

                // window break
                const w = __.window.width;

                if (w > WIN_BREAK1) {
                    __.window.device = 'pc2';
                } else if (w > WIN_BREAK2) {
                    __.window.device = 'pc1';
                } else if (w > WIN_BREAK3) {
                    __.window.device = 'tb';
                } else {
                    __.window.device = 'sp';
                }

                // TOUCH
                const touch = __.window.ontouch = 'ontouchend' in document;
                TOUCH = [
                    TOUCH_CLICK = touch ? 'touchstart' : 'click',
                    TOUCH_START = touch ? 'touchstart' : 'mousedown',
                    TOUCH_MOVE = touch ? 'touchmove' : 'mousemove',
                    TOUCH_END = touch ? 'touchend' : 'mouseup',
                    TOUCH_ENTER = touch ? 'touchstart' : 'mouseenter',
                    TOUCH_LEAVE = touch ? 'touchend' : 'mouseleave'
                ];
            },
            UserAgent: function () {
                const _ua = navigator.userAgent.toLowerCase();

                const uaList = [
                    ['iphone', 'iphone', true],
                    ['ipad', 'ipad', true],
                    ['ipad', 'macintosh', 'ontouchend' in document],
                    ['ipod', 'ipod', true],
                    ['android', 'android', true],
                    ['ie', 'msie|trident', true],
                    ['edge', 'edge', true],
                    ['chrome', 'chrome', true],
                    ['firefox', 'firefox', true],
                    ['safari', 'safari', true],
                    ['opera', 'opera', true],
                    ['else', '.', true]
                ];

                let ua = {};
                uaList.some(function(V){
                    const reg = new RegExp(V[1], 'ig');
                    return ua[V[0]] = V[2] ? !!_ua.match(reg) || false : false;
                });

                __.window.userAgent = ua;
            },
            is: {
                Init: function () {
                    this.PC();
                    this.SP();
                    this.TB();
                },
                PC: function () {
                    return __.window.isPC = __.window.device === 'pc2' || __.window.device === 'pc1';
                },
                SP: function () {
                    return __.window.isSP = __.window.device === 'sp';
                },
                TB: function () {
                    return __.window.isTB = __.window.device === 'tb';
                }
            }
        },
        set: {
            Init: function () {
            },
            Scroll: function () {
                const winHeight = __.window.height;
                const docHeight = __.document.height;
                const scrollTop = __.window.scrollTop;
                const scrollBottom = __.window.scrollBottom = docHeight - (scrollTop + winHeight);

                // set.Fixed
                __.document.set.Fixed();

                // set.fixed.PageTop
                if ((__.window.isSP || __.window.isTB) && scrollBottom < FOOTER_H) {
                    __.document.set.fixed.PageTop();
                } else {
                    __.document.set.fixed.PageTop('cancel');
                }

            }
        }
    };

    // -------------------------------------------------------
    // document
    // -------------------------------------------------------
    __.document = {
        Init: function () {
            this.get.Init();
            this.set.Init();
            this.add.Init();
        },

        height: 0,

        // Objects
        fc: {},
        toc: {
            height: 0
        },
        imagemap: false,

        get: {

            Init: function () {
                this.Height();
            },
            Height: function () {
                __.document.height = $D.height();
                __.document.fc.height = $FC.outerHeight();
            }
        },
        set: {

            Init: function () {
                this.Fixed(true);
                this.Height();
                this.body.Type();
                this.imagemap.Responsive();
            },
            Height: function () {

                // Body
                $B.outerHeight(__.window.height + (!!__.window.isPC? 0: FOOTER_H));

                // Toc
                let fix;
                if (__.window.isPC) {
                    fix = G_HEADER_H + (!__.window.fixed ? M_HEADER_H : 0) + FOOTER_H + MARGIN + 10;
                    __.document.toc.height = __.window.height - fix + (!__.window.fixed ? __.window.scrollTop : 0);
                } else {
                    fix = G_HEADER_H;
                    __.document.toc.height = __.window.height - fix;
                }

                $N.find('.side_toc').height(__.document.toc.height);
            },
            body: {
                Type: function () {
                    $B.removeClass('pc2 pc1 tb sp');
                    $B.addClass(__.window.device);
                },
            },
            imagemap: {
                Responsive: function () {
                    if (!__.document.imagemap) {
                        __.document.imagemap = true;
                        $('img[usemap]').rwdImageMaps();
                    }
                }
            },
            Fixed: function (_scroll) {

                const scrollTop = __.window.scrollTop;

                if (scrollTop > M_HEADER_H) {
                    $('*[role]').removeClass('top_area');
                    $('*[role]').addClass('fixed');

                    __.window.fixed = true;
                } else {
                    if ($('*[role]').hasClass('fixed')) {
                        $('*[role]').addClass('top_area');
                    }
                    $('*[role]').removeClass('fixed');
                    __.window.fixed = false;

                    __.document.set.Height();
                }

                if (!!_scroll) {
                    $('*[role]').addClass('loaded');
                } else {
                    $('*[role]').removeClass('loaded');
                }
                if (scrollTop <= 0) {
                    $('*[role]').addClass('reached');
                } else {
                    $('*[role]').removeClass('reached');
                }
            },
            fixed: {
                PageTop: function (_cancel) {
                    if (_cancel) {
                        $FC.removeClass('bottom');

                    } else {
                        $FC.addClass('bottom');

                    }
                }
            }
        },
        add: {

            Init: function () {
                this.click.footer.PageTop();
                this.click.Ankers();
                this.click.Cause();
            },
            click: {
                header: {
                },
                footer: {
                    PageTop: function () {
                        $T.on('click', function () {
                            $(_B+','+_H).animate({ scrollTop: 0 }, DURATION, 'swing');
                            return false;
                        });
                    }
                },
                Ankers: function () {
                    $('a[href^="#"]:not([role])').on('click', function (E) {
                        const hash = this.hash;

                        if (hash === '') {
                            return false;
                        }

                        const $target = $(hash);
                        const posY = $target.offset().top - (G_HEADER_H + 10);

                        $('body,html').animate({ scrollTop: posY }, DURATION, 'swing');
                        $('.anchortarget').removeClass('anchortarget');
                        $target.addClass('anchortarget');

                        return false;
                    });
                },
                Cause: function () {
                    $D.on('click', '.cause', function () {
                        const $cause = $(this);
                        const $remedy = $cause.next('.remedy');

                        if ($cause.hasClass('close')) {
                            $remedy.stop().slideDown(DURATION);
                            $cause.removeClass('close');
                        } else {
                            $remedy.stop().slideUp(DURATION);
                            $cause.addClass('close');
                        }
                    });
                }
            }
        }
    };

    window.DL = __;
}(window.DL || {});

$(function () { DL.Init(); });