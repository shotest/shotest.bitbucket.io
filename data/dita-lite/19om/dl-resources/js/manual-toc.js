﻿//-------------------------------------------------
// TOC
//-------------------------------------------------
!function (_DL) {
    const __ = _DL;

    const DURATION = 100;

    let $W, $D, $H, $B, $HB, $H1, $H2, $BTN1, $BTN2, $M, $N, $FC, $F, $T;
    let _W, _D, _H, _B, _HB, _H1, _H2, _BTN1, _BTN2, _M, _N, _FC, _F, _T;
    let TOUCH, TOUCH_CLICK, TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_ENTER, TOUCH_LEAVE;

    let $LISTS, $ITEMS, $INNERS, $LINKS, $BTNS;
    let _LISTS, _ITEMS, _INNERS, _LINKS, _BTNS;


    __.toc = {
        Init: function () {

            // init
            this.$.Init();
            this.action.Init();

            // set
            this.set.Init();

            // add
            this.add.Init();
        },

        scrollTop: 0,

        $: {
            Init: function () {
                $W = $(_W = window);
                $D = $(_D = document);
                $H = $(_H = 'html');
                $B = $(_B = 'body');
                $HB = $(_HB = _H + ',' + _B);
                $H1 = $(_H1 = 'header[role="global"]');
                $H2 = $(_H2 = 'header[role="manual"]');
                $M = $(_M = 'main[role="main_contents"]');
                $N = $(_N = 'nav[role="side_toc"]');
                $FC = $(_FC = 'nav[role="footer_control"]');
                $F = $(_F = 'footer');
                $T = $(_T = '#page_top a');

                $LISTS = $(_LISTS = '.toc_list');
                $ITEMS = $(_ITEMS = '.toc_list_item');
                $INNERS = $(_INNERS = '.toc_list_inner');
                $LINKS = $(_LINKS = '.toc_list_a');
                $BTNS = $(_BTNS = '.toc_list_btn');

                $BTN1 = $(_BTN1 = '.header_btn.toc a');


            }
        },
        is: {
            show: false
        },
        set: {
            Init: function () {
                this.Scrollbar();
            },
            Scrollbar: function () {
                if (!__.window.get.is.SP()) {
                    new SimpleBar($('.side_toc_inner')[0]);
                }
            },
            scrollBack: {
                top: 0,
                Lock: function () {
                    $HB.each(function (I, _E) {
                        const st = $(_E).scrollTop();
                        if (st !== 0) {
                            __.toc.scrollTop = st;
                        }
                    });
                    $B.css({ position: 'fixed' });
                },
                Release: function () {
                    $B.css({ position: 'inherit' });
                    $HB.scrollTop(__.toc.scrollTop);
                }
            }
        },
        add: {
            Init: function () {
                this.scroll.Inner();
                this.click.Show();
                this.click.Open();
                this.click.Hide();
            },
            scroll: {
                Inner: function () {
                    $D.on('touchmove scroll','.side_toc *', function (_event) {
                        _event.stopPropagation();
                    });
                }
            },
            click: {
                Show: function () {
                    $BTN1.on('click', function (E) {
                        E.preventDefault();
                        __.toc.action.ShowHide();
                    });
                },
                Open: function () {
                    $BTNS.on('click', function () {
                        const $inner = $(this).parent('.toc_list_a').next('.toc_list_inner');
                        const $parent = $(this).closest('li');
                        
                        __.toc.action.OpenClose($parent);
                    
                        return false;
                    });
                },
                Hide: function () {

                    $('.side_toc_back,.side_toc_inner').on('click', function (_event) {
                        const target = _event.target;

                        __.LOG($(target));
                        if (
                            $(target).hasClass('side_toc_back') ||
                            $(target).hasClass('side_toc_inner')
                        ) {
                            __.toc.action.Show();
                        }
                    });
                }
            }
        },
        action: {
            Init: function () {
                this.open.Actibate();
            },
            Refresh: function () {
                __.toc.set.Init();
            },
            ShowHide: function () {

                if ($BTN1.hasClass('open')) {
                    $N.stop().animate({ right: '-100%' }, 0, 'swing');
                    $N.removeClass('open');
                    $BTN1.removeClass('open');

                    $B.removeClass('toc_open');
                    //$HB.scrollTop($B.attr('data-toc_open'));
                    __.toc.set.scrollBack.Release();

                    __.toc.is.show = false;
                } else {
                    $N.stop().animate({ right: '0' }, 0, 'swing');
                    $N.addClass('open');
                    $BTN1.addClass('open');

                    //$B.attr('data-toc_open', __.window.scrollTop);
                    $B.addClass('toc_open');

                    __.toc.set.scrollBack.Lock();
                    __.toc.is.show = true;

                }
            },
            OpenClose: function ($T) {
                if ($T.hasClass('open')) {
                    __.toc.action.close.Target($T);
                } else {
                    __.toc.action.open.Target($T);
                }
                return;
            },
            open: {
                Init: function () {

                },
                Actibate: function () {
                    const $active = $ITEMS.filter('.active');
                    $active.parents(_ITEMS).addClass('active open');

                    const $opened = $(_ITEMS).filter('.open');

                    __.toc.action.open.Target($opened);

                    return false;
                },
                Target: function ($T) {

                    if ($T.length > 1) {
                        $T.each(function (I,E) {

                            const $inner = $(E).children('.toc_list_inner');

                            $inner.slideDown(DURATION);
                            $(E).addClass('open');
                        });
                    } else {

                        const $inner = $T.children('.toc_list_inner');

                        $inner.slideDown(DURATION);
                        $T.addClass('open');
                    }

                    return;
                }
            },
            close: {

                Target: function ($T) {

                    if ($T.length > 1) {
                        $T.each(function (I, E) {

                            const $inner = $(E).children('.toc_list_inner');

                            $inner.slideUp(DURATION);
                            $(E).removeClass('open');
                        });
                    } else {

                        const $inner = $T.children('.toc_list_inner');

                        $inner.slideUp(DURATION);
                        $T.removeClass('open');
                    }

                    return;
                }
            }
        }
    };

    window.DL = __;
}(window.DL || {});

$(function () {
    DL.toc.Init();
});