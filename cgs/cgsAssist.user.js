// ==UserScript==
// @name         cgsAssist
// @namespace    http://tampermonkey.net/
// @version      0.2.0
// @description  cgs assist tool
// @author       ShoInoue
// @match        http://cgs/itp/*
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @require http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js
// @require http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js
// ==/UserScript==

var CGS;
!function(CGS,$) {
    'use strict';

	var _D = document;
	
	CGS.message = {
		config: {
			no: '設定なし'
		}
	};
	
	CGS.util = {
		frameName: function(){
			return location.href.replace(/http:\/\/cgs\/itp\/|\.asp.*/g,'');
		},
		logFrameName: function(){
			console.log('// '+CGS.util.frameName()+' -->');
		},
		escape: function(STR){
            return STR.replace(/([\\*+.?!={}()\[\]^$|\/])/g,'\\$1');
        },
		num: {
			fix: function(NUM,DIGIT){
				return ('00000000'+NUM).slice(DIGIT*-1);
			},
		},
		get: {
			storage: function(KEY){
				if(CGS.util.check.storage){
					return localStorage.getItem(KEY);
				}
			}
		},
		set: {
			storage: function(KEY,DATA){
				if(CGS.util.check.storage){
					localStorage.setItem(KEY, DATA);
				}
				return;
			}
		},
		remove: {
			storage: function(KEY){
				if(CGS.util.check.storage){
					localStorage.removeItem(KEY);
				}
				return;
			}
		},
		check: {
			storage: function(){
				if((('localStorage' in window) && (window.localStorage !== null)) || localStorage.length==0) {
					return true;
				}else{
					throw new Error('No localStorage...');
					return false;
				}
			}
		}

	};
	
	CGS.init = function(){
		CGS.info.frame = CGS.util.frameName();
		
		// フレームごとにwindow.onloadからinit発生するため処理分岐
		switch(CGS.util.frameName()){
			
			// 全体（ページ自体）
			case 'default':
				CGS.util.logFrameName();
				CGS.info.location.init();
				break;
				
			// グローバル左メニュー
			case 'menu':
				CGS.util.logFrameName();
				break;
					
			// 右フレーム初期画面
			case 'blank':
				CGS.leafAction.reset.config();
				break;
				
			// 【日報】作業日報入力
			case 'workreport_lict_sel':
				CGS.util.logFrameName();
				break;
			
			// 【日報】作業日報の作成
			case 'workreport_lict_input':
				CGS.util.logFrameName();
				break;

			// 【リーフ】受注リーフ作成メニュー
			case 'input_leaf_sel':
				CGS.util.logFrameName();
				CGS.leafAction.reset.config();
				break;
				
			// 【リーフ】受注リーフ作成
			case 'input_leaf':
				CGS.util.logFrameName();
				CGS.leafAction.set.config();
				CGS.leafAction.set.remarks();
				break;
				
			// 【リーフ】作成済みリーフ表示画面
			case 'show_leaf':
				CGS.util.logFrameName();
				CGS.leafAction.set.config();
				CGS.leafAction.add.event();
				break;
				
			// 謎の不可視オブジェクト達 => なにもしない
			case 'blank1':
				return;
				break;
				
			default:
				CGS.util.logFrameName();
				break;
		}
		
		window.top.CGS = CGS;
	};
	
	CGS.info = {
		frame: '',
		copyLeaf: '',
		project: '',
		leaf: {},
		location: {
			init: function(){
				for(var k in CGS.info.location){
					if(k==='init'){
						continue;
					}
					CGS.info.location[k] = window.top.location[k];
				}
				
				(function(){
					var result = {};
					var sch1 = decodeURIComponent(_D.location.search).replace(/^\?/,'').split('&');
					var sch2;
					if(sch1[0]===''){
						CGS.info.location.query = null;
						return;
					}

					for(var i=0,l=sch1.length;i<l;i++){
						sch2 = sch1[i].split('=');
						result[sch2[0]] = sch2[1];

					}
					CGS.info.location.query = result;
				})();
			},
			href:      '',
			origin:    '',
			protocol:  '',
			host:      '',
			hostname:  '',
			port:      '',
			pathname:  '',
			search:    '',
			hash:      '',
			query:     {}
		}
	};
	
	// リーフ系アクション
	CGS.leafAction = {
		reset: {
			config: function(){
				
				CGS.info.leaf = {};
				CGS.info.copyLeaf = '';
				CGS.info.project = '';
				
				//CGS.util.set.storage('leaf','');
				CGS.util.set.storage('copyLeaf','');
				CGS.util.set.storage('project','');
				
				console.log('--> localStorage reset');
				return;
			}
		},
		add: {
			event: function(){
				
				var $btn = $('input[value="流用作成"]');
				$btn.on('click',function(){
					
					var leafKbn = CGS.info.leaf.LeafKbn
					CGS.info.copyLeaf = leafKbn;
					CGS.util.set.storage('copyLeaf',leafKbn);
					
					var project = '----------------------------------------\n管理リーフNo：'
					+ CGS.info.leaf.LeafNo.replace(/(\d{3})(\d{4})/,'$1-$2')
					+ '\n管理リーフ名：'+CGS.info.leaf.projectName
					+ '\n----------------------------------------\n'
					+ CGS.info.leaf.remarks;
					
					CGS.info.project = project;
					CGS.util.set.storage('project',project);
					
				});

			},
		},
		change: {
			func: {
			}
		},
		set: {
			config: function(){
				var nameList = ['LeafKbn','LeafNo','LeafType','Hinsyu','LeafDisp','LeafStatus','NyuukouDate','NouhinDate','standard_size','expand_size','KindCD','Gyomu','Mode','History','NouhinMeisai','NouhinMeisai_Shanai','UnLockFLG1'];
				var value;
				var result = {};

				for(var item of nameList){
					value = $('input[name="'+item+'"]').val() || '';
					
					if(item == 'LeafKbn' && value == ''){
						value = CGS.leafAction.get.leafKbn();
					}
					
					result[item] = value;
				}
				if(CGS.info.frame == 'show_leaf'){
					result.projectName = CGS.leafAction.get.projectName();
					result.remarks = CGS.leafAction.get.remarks();
				}
				
				//CGS.util.set.storage('leaf',JSON.stringify(result));
				CGS.info.leaf = result;
			},
			copyData: function(){
				
			},
			remarks: function(){
				console.dir(localStorage);
				var $bikou = $('textarea[name="Bikou"]');
				if(
					CGS.util.get.storage('copyLeaf') == 3
					//&& CGS.info.leaf.LeafKbn == 3 ◆一時的に【管理リーフ(3)⇒管理リーフ(3)】作成時で設定
					&& CGS.info.leaf.LeafKbn == 2 //◆【管理リーフ(3)⇒社内リーフ(2)】作成時で設定
				){
					$bikou.val(CGS.util.get.storage('project'));
					console.log('--> $bikou value set complete');
				}
			}
		},
		get: {
			leafKbn: function(){
				var result = CGS.leafAction.get.tdNext('受注区分');
				return (result == '管理'? 3: result == '社内'? 2: result == '受注'? 1: '');
			},
			projectName: function(){
				return CGS.leafAction.get.tdNext('品　　名');
			},
			remarks: function(){
				return CGS.leafAction.get.tdBottom('備　　　　考');
			},
			tdBottom: function(TARGET){
				var result = '';
				
				for(var tableNum = 1;tableNum<$('table').length+1;tableNum++){
					
					
					var $trs = $('table').eq(tableNum).find('tr');
					for(var n=0,l1=$trs.length;n<l1;n++){

						var $ths = $trs.eq(n).find('th');

						for(var m=0,l2=$ths.length;m<l2;m++){
							if($ths.eq(m).html().match(TARGET)){
								console.log($trs.eq(n+1).find('th,td').eq(m).html().replace(/&nbsp;/g,''));

								return $trs.eq(n+1).find('th,td').eq(m).html().replace(/&nbsp;/g,'');

							}
						}

					}
				}
			},
			tdNext: function(TARGET){
				var result = '';
				var $trs = $('table').eq(1).find('tr');
				
				for(var n=0,l1=$trs.length;n<l1;n++){
					
					var $ths = $trs.eq(n).find('th');

					for(var m=0,l2=$ths.length;m<l2;m++){
						if($ths.eq(m).html() == TARGET){
							return $ths.eq(m).next('td').html().replace(/&nbsp;/g,'');
						}
					}
					
				}
			},
			config: function(){
				return JSON.parse(localStorage.getItem('leaf'));
			}
		}
	};
	
	CGS.workreport = {
		time: {}
	};
	
	


	


}((CGS||(CGS={})),jQuery);

CGS.init();
