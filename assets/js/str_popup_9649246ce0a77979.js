(function(creative_key) {
  var a = document.querySelectorAll('.link--document a');
  a.forEach(function(_a){
    _a.classList.add('satori__popup_button');
    
    _a.addEventListener('click',function(_event){
      if(!!SatoriCreative){
        _event.preventDefault();
        if(!SatoriCreative.display){
          SatoriCreative.showWidget();
        }else{
          $("#satori__popup_close").click();
        }
        return false;
      }
    });
  });
  
  var c, s;
  c = document.createElement("div");
  c.id = "satori__creative_container";
  s = document.createElement("script");
  s.id = "-_-satori_creative-_-";
  s.src = "//delivery.satr.jp/js/creative_set.js";
  s.setAttribute("data-key", creative_key);
  s.setAttribute("data-immediately", "true");
  c.appendChild(s);
  document.body.appendChild(c);
 })("9649246ce0a77979"); 