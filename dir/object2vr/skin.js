// Garden Gnome Software - Skin
// Object2VR 3.1.7/10775
// Filename: shotest.ggsk
// Generated 月 7 22 16:07:34 2019

function object2vrSkin(player,base) {
	var me=this;
	var flag=false;
	var nodeMarker=new Array();
	var activeNodeMarker=new Array();
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=new Array();
	this.elementMouseOver=new Array();
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	for(i=0;i<prefixes.length;i++) {
		if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
			cssPrefix='-' + prefixes[i].toLowerCase() + '-';
			domTransition=prefixes[i] + 'Transition';
			domTransform=prefixes[i] + 'Transform';
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=new Array();
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=new Array();
		var stack=new Array();
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		this._container_1=document.createElement('div');
		this._container_1.ggId="Container 1";
		this._container_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_1.ggVisible=true;
		this._container_1.className='ggskin ggskin_container';
		this._container_1.ggType='container';
		this._container_1.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-60 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -60px;';
		hs+='width: 308px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_1.setAttribute('style',hs);
		this._default_loading_bar=document.createElement('div');
		this._default_loading_bar.ggId="default loading bar";
		this._default_loading_bar.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._default_loading_bar.ggVisible=true;
		this._default_loading_bar.className='ggskin ggskin_container';
		this._default_loading_bar.ggType='container';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 308px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._default_loading_bar.setAttribute('style',hs);
		this._loading_text=document.createElement('div');
		this._loading_text__text=document.createElement('div');
		this._loading_text.className='ggskin ggskin_textdiv';
		this._loading_text.ggTextDiv=this._loading_text__text;
		this._loading_text.ggId="loading text";
		this._loading_text.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_text.ggVisible=true;
		this._loading_text.className='ggskin ggskin_text';
		this._loading_text.ggType='text';
		this._loading_text.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-40 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -40px;';
		hs+='width: 332px;';
		hs+='height: 23px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loading_text.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 332px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ff9696;';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._loading_text__text.setAttribute('style',hs);
		this._loading_text.ggUpdateText=function() {
			var hs="<b>Loading... "+(me.player.getPercentLoaded()*100.0).toFixed(0)+"% ("+(me.player.getPercentLoaded()*100000.0).toFixed(0)+" \/ "+(100000.0).toFixed(0)+")<\/b>";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
			}
		}
		this._loading_text.ggUpdateText();
		this._loading_text.appendChild(this._loading_text__text);
		this._default_loading_bar.appendChild(this._loading_text);
		this._loading_bar=document.createElement('div');
		this._loading_bar.ggId="loading bar";
		this._loading_bar.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_bar.ggVisible=true;
		this._loading_bar.className='ggskin ggskin_rectangle';
		this._loading_bar.ggType='rectangle';
		this._loading_bar.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-10 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -10px;';
		hs+='width: 980px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 0% 50%;';
		hs+='visibility: inherit;';
		hs+='background: #ff9696;';
		hs+='border: 0px solid #000080;';
		this._loading_bar.setAttribute('style',hs);
		this._default_loading_bar.appendChild(this._loading_bar);
		this._container_1.appendChild(this._default_loading_bar);
		this.divSkin.appendChild(this._container_1);
		this._container_3=document.createElement('div');
		this._container_3.ggId="Container 3";
		this._container_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_3.ggVisible=true;
		this._container_3.className='ggskin ggskin_container';
		this._container_3.ggType='container';
		this._container_3.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-200 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -200px;';
		hs+='top:  -50px;';
		hs+='width: 200px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_3.setAttribute('style',hs);
		this._container_2=document.createElement('div');
		this._container_2.ggId="Container 2";
		this._container_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_2.ggVisible=true;
		this._container_2.className='ggskin ggskin_container';
		this._container_2.ggType='container';
		this._container_2.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-200 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -200px;';
		hs+='top:  -50px;';
		hs+='width: 200px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_2.setAttribute('style',hs);
		this._svg_1=document.createElement('div');
		this._svg_1.ggId="Svg 1";
		this._svg_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_1.ggVisible=true;
		this._svg_1.className='ggskin ggskin_svg';
		this._svg_1.ggType='svg';
		this._svg_1.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-130 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -130px;';
		hs+='top:  -50px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._svg_1.setAttribute('style',hs);
		this._svg_1__img=document.createElement('img');
		this._svg_1__img.className='ggskin ggskin_svg';
		this._svg_1__img.setAttribute('src',basePath + 'images/svg_1.svg');
		this._svg_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._svg_1__img['ondragstart']=function() { return false; };
		this._svg_1.appendChild(this._svg_1__img);
		this._svg_1.onclick=function () {
			me.player.changePanLog(1,true);
		}
		this._svg_1.onmouseover=function () {
			me._svg_1__img.src=basePath + 'images/svg_1__o.svg';
			me._svg_1.ggIsOver=true;
		}
		this._svg_1.onmouseout=function () {
			me._svg_1__img.src=basePath + 'images/svg_1.svg';
			me._svg_1.ggIsOver=false;
		}
		this._svg_1.onmousedown=function () {
			me._svg_1__img.src=basePath + 'images/svg_1__a.svg';
		}
		this._svg_1.onmouseup=function () {
			if (me._svg_1.ggIsOver) {
				me._svg_1__img.src=basePath + 'images/svg_1__o.svg';
			} else {
				me._svg_1__img.src=basePath + 'images/svg_1.svg';
			}
		}
		this._container_2.appendChild(this._svg_1);
		this._svg_3on=document.createElement('div');
		this._svg_3on.ggId="Svg 3on";
		this._svg_3on.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_3on.ggVisible=true;
		this._svg_3on.className='ggskin ggskin_svg';
		this._svg_3on.ggType='svg';
		this._svg_3on.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-90 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -90px;';
		hs+='top:  -50px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._svg_3on.setAttribute('style',hs);
		this._svg_3on__img=document.createElement('img');
		this._svg_3on__img.className='ggskin ggskin_svg';
		this._svg_3on__img.setAttribute('src',basePath + 'images/svg_3on.svg');
		this._svg_3on__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._svg_3on__img['ondragstart']=function() { return false; };
		this._svg_3on.appendChild(this._svg_3on__img);
		this._svg_3on.onclick=function () {
			me.player.stopAutorotate();
			me._svg_3on.style[domTransition]='none';
			me._svg_3on.style.visibility='hidden';
			me._svg_3on.ggVisible=false;
			me._svg_3off.style[domTransition]='none';
			me._svg_3off.style.visibility='inherit';
			me._svg_3off.ggVisible=true;
		}
		this._svg_3on.onmouseover=function () {
			me._svg_3on__img.src=basePath + 'images/svg_3on__o.svg';
			me._svg_3on.ggIsOver=true;
		}
		this._svg_3on.onmouseout=function () {
			me._svg_3on__img.src=basePath + 'images/svg_3on.svg';
			me._svg_3on.ggIsOver=false;
		}
		this._svg_3on.onmousedown=function () {
			me._svg_3on__img.src=basePath + 'images/svg_3on__a.svg';
		}
		this._svg_3on.onmouseup=function () {
			if (me._svg_3on.ggIsOver) {
				me._svg_3on__img.src=basePath + 'images/svg_3on__o.svg';
			} else {
				me._svg_3on__img.src=basePath + 'images/svg_3on.svg';
			}
		}
		this._container_2.appendChild(this._svg_3on);
		this._svg_3off=document.createElement('div');
		this._svg_3off.ggId="Svg 3off";
		this._svg_3off.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_3off.ggVisible=true;
		this._svg_3off.className='ggskin ggskin_svg';
		this._svg_3off.ggType='svg';
		this._svg_3off.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-90 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -90px;';
		hs+='top:  -50px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._svg_3off.setAttribute('style',hs);
		this._svg_3off__img=document.createElement('img');
		this._svg_3off__img.className='ggskin ggskin_svg';
		this._svg_3off__img.setAttribute('src',basePath + 'images/svg_3off.svg');
		this._svg_3off__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._svg_3off__img['ondragstart']=function() { return false; };
		this._svg_3off.appendChild(this._svg_3off__img);
		this._svg_3off.onclick=function () {
			me.player.startAutorotate("0.1");
			me._svg_3on.style[domTransition]='none';
			me._svg_3on.style.visibility='inherit';
			me._svg_3on.ggVisible=true;
			me._svg_3off.style[domTransition]='none';
			me._svg_3off.style.visibility='hidden';
			me._svg_3off.ggVisible=false;
		}
		this._svg_3off.onmouseover=function () {
			me._svg_3off__img.src=basePath + 'images/svg_3off__o.svg';
			me._svg_3off.ggIsOver=true;
		}
		this._svg_3off.onmouseout=function () {
			me._svg_3off__img.src=basePath + 'images/svg_3off.svg';
			me._svg_3off.ggIsOver=false;
		}
		this._svg_3off.onmousedown=function () {
			me._svg_3off__img.src=basePath + 'images/svg_3off__a.svg';
		}
		this._svg_3off.onmouseup=function () {
			if (me._svg_3off.ggIsOver) {
				me._svg_3off__img.src=basePath + 'images/svg_3off__o.svg';
			} else {
				me._svg_3off__img.src=basePath + 'images/svg_3off.svg';
			}
		}
		this._container_2.appendChild(this._svg_3off);
		this._svg_2=document.createElement('div');
		this._svg_2.ggId="Svg 2";
		this._svg_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_2.ggVisible=true;
		this._svg_2.className='ggskin ggskin_svg';
		this._svg_2.ggType='svg';
		this._svg_2.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-50 + w) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-50 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -50px;';
		hs+='top:  -50px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._svg_2.setAttribute('style',hs);
		this._svg_2__img=document.createElement('img');
		this._svg_2__img.className='ggskin ggskin_svg';
		this._svg_2__img.setAttribute('src',basePath + 'images/svg_2.svg');
		this._svg_2__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._svg_2__img['ondragstart']=function() { return false; };
		this._svg_2.appendChild(this._svg_2__img);
		this._svg_2.onclick=function () {
			me.player.changePanLog(-1,true);
		}
		this._svg_2.onmouseover=function () {
			me._svg_2__img.src=basePath + 'images/svg_2__o.svg';
			me._svg_2.ggIsOver=true;
		}
		this._svg_2.onmouseout=function () {
			me._svg_2__img.src=basePath + 'images/svg_2.svg';
			me._svg_2.ggIsOver=false;
		}
		this._svg_2.onmousedown=function () {
			me._svg_2__img.src=basePath + 'images/svg_2__a.svg';
		}
		this._svg_2.onmouseup=function () {
			if (me._svg_2.ggIsOver) {
				me._svg_2__img.src=basePath + 'images/svg_2__o.svg';
			} else {
				me._svg_2__img.src=basePath + 'images/svg_2.svg';
			}
		}
		this._container_2.appendChild(this._svg_2);
		this._container_3.appendChild(this._container_2);
		this.divSkin.appendChild(this._container_3);
		this._container_4=document.createElement('div');
		this._container_4.ggId="Container 4";
		this._container_4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_4.ggVisible=true;
		this._container_4.className='ggskin ggskin_container';
		this._container_4.ggType='container';
		hs ='position:absolute;';
		hs+='left: 10px;';
		hs+='top:  10px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_4.setAttribute('style',hs);
		this._fullscreen=document.createElement('div');
		this._fullscreen.ggId="fullscreen";
		this._fullscreen.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._fullscreen.ggVisible=true;
		this._fullscreen.className='ggskin ggskin_svg';
		this._fullscreen.ggType='svg';
		hs ='position:absolute;';
		hs+='left: -10px;';
		hs+='top:  -10px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._fullscreen.setAttribute('style',hs);
		this._fullscreen__img=document.createElement('img');
		this._fullscreen__img.className='ggskin ggskin_svg';
		this._fullscreen__img.setAttribute('src',basePath + 'images/fullscreen.svg');
		this._fullscreen__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._fullscreen__img['ondragstart']=function() { return false; };
		this._fullscreen.appendChild(this._fullscreen__img);
		this._fullscreen.onclick=function () {
			me.player.toggleFullscreen();
		}
		this._fullscreen.onmouseover=function () {
			me._fullscreen__img.src=basePath + 'images/fullscreen__o.svg';
			me._fullscreen.ggIsOver=true;
		}
		this._fullscreen.onmouseout=function () {
			me._fullscreen__img.src=basePath + 'images/fullscreen.svg';
			me._fullscreen.ggIsOver=false;
		}
		this._fullscreen.onmousedown=function () {
			me._fullscreen__img.src=basePath + 'images/fullscreen__a.svg';
		}
		this._fullscreen.onmouseup=function () {
			if (me._fullscreen.ggIsOver) {
				me._fullscreen__img.src=basePath + 'images/fullscreen__o.svg';
			} else {
				me._fullscreen__img.src=basePath + 'images/fullscreen.svg';
			}
		}
		this._container_4.appendChild(this._fullscreen);
		this._restorescreen=document.createElement('div');
		this._restorescreen.ggId="restorescreen";
		this._restorescreen.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._restorescreen.ggVisible=true;
		this._restorescreen.className='ggskin ggskin_svg';
		this._restorescreen.ggType='svg';
		hs ='position:absolute;';
		hs+='left: -10px;';
		hs+='top:  -10px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._restorescreen.setAttribute('style',hs);
		this._restorescreen__img=document.createElement('img');
		this._restorescreen__img.className='ggskin ggskin_svg';
		this._restorescreen__img.setAttribute('src',basePath + 'images/restorescreen.svg');
		this._restorescreen__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._restorescreen__img['ondragstart']=function() { return false; };
		this._restorescreen.appendChild(this._restorescreen__img);
		this._restorescreen.onclick=function () {
			me.player.toggleFullscreen();
		}
		this._restorescreen.onmouseover=function () {
			me._restorescreen__img.src=basePath + 'images/restorescreen__o.svg';
			me._restorescreen.ggIsOver=true;
		}
		this._restorescreen.onmouseout=function () {
			me._restorescreen__img.src=basePath + 'images/restorescreen.svg';
			me._restorescreen.ggIsOver=false;
		}
		this._restorescreen.onmousedown=function () {
			me._restorescreen__img.src=basePath + 'images/restorescreen__a.svg';
		}
		this._restorescreen.onmouseup=function () {
			if (me._restorescreen.ggIsOver) {
				me._restorescreen__img.src=basePath + 'images/restorescreen__o.svg';
			} else {
				me._restorescreen__img.src=basePath + 'images/restorescreen.svg';
			}
		}
		this._container_4.appendChild(this._restorescreen);
		this.divSkin.appendChild(this._container_4);
		this._container_5=document.createElement('div');
		this._container_5.ggId="Container 5";
		this._container_5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_5.ggVisible=true;
		this._container_5.className='ggskin ggskin_container';
		this._container_5.ggType='container';
		this._container_5.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-60 + w) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -60px;';
		hs+='top:  10px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_5.setAttribute('style',hs);
		this._close=document.createElement('div');
		this._close.ggId="close";
		this._close.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._close.ggVisible=true;
		this._close.className='ggskin ggskin_svg';
		this._close.ggType='svg';
		this._close.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-40 + w) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -40px;';
		hs+='top:  -10px;';
		hs+='width: 50px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._close.setAttribute('style',hs);
		this._close__img=document.createElement('img');
		this._close__img.className='ggskin ggskin_svg';
		this._close__img.setAttribute('src',basePath + 'images/close.svg');
		this._close__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
		this._close__img['ondragstart']=function() { return false; };
		this._close.appendChild(this._close__img);
		this._close.onclick=function () {
			me.player.toggleFullscreen();
		}
		this._close.onmouseover=function () {
			me._close__img.src=basePath + 'images/close__o.svg';
			me._close.ggIsOver=true;
		}
		this._close.onmouseout=function () {
			me._close__img.src=basePath + 'images/close.svg';
			me._close.ggIsOver=false;
		}
		this._close.onmousedown=function () {
			me._close__img.src=basePath + 'images/close__a.svg';
		}
		this._close.onmouseup=function () {
			if (me._close.ggIsOver) {
				me._close__img.src=basePath + 'images/close__o.svg';
			} else {
				me._close__img.src=basePath + 'images/close.svg';
			}
		}
		this._container_5.appendChild(this._close);
		this.divSkin.appendChild(this._container_5);
		this._tooltip=document.createElement('div');
		this._tooltip.ggId="tooltip";
		this._tooltip.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._tooltip.ggVisible=true;
		this._tooltip.className='ggskin ggskin_container';
		this._tooltip.ggType='container';
		hs ='position:absolute;';
		hs+='left: 22px;';
		hs+='top:  67px;';
		hs+='width: 201px;';
		hs+='height: 112px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._tooltip.setAttribute('style',hs);
		this._rectangle_1=document.createElement('div');
		this._rectangle_1.ggId="Rectangle 1";
		this._rectangle_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._rectangle_1.ggVisible=true;
		this._rectangle_1.className='ggskin ggskin_rectangle';
		this._rectangle_1.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: -12px;';
		hs+='top:  -12px;';
		hs+='width: 194px;';
		hs+='height: 86px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='background: #ffffff;';
		hs+='background: rgba(255,255,255,0.392157);';
		hs+='border: 0px solid #000000;';
		hs+='border-radius: 10px;';
		hs+=cssPrefix + 'border-radius: 10px;';
		this._rectangle_1.setAttribute('style',hs);
		this._tooltip.appendChild(this._rectangle_1);
		this._tooltiptext=document.createElement('div');
		this._tooltiptext__text=document.createElement('div');
		this._tooltiptext.className='ggskin ggskin_textdiv';
		this._tooltiptext.ggTextDiv=this._tooltiptext__text;
		this._tooltiptext.ggId="tooltip-text";
		this._tooltiptext.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._tooltiptext.ggVisible=true;
		this._tooltiptext.className='ggskin ggskin_text';
		this._tooltiptext.ggType='text';
		hs ='position:absolute;';
		hs+='left: -2px;';
		hs+='top:  -1px;';
		hs+='width: 179px;';
		hs+='height: 65px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._tooltiptext.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 179px;';
		hs+='height: 65px;';
		hs+='border: 0px solid #000000;';
		hs+='color: #780000;';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._tooltiptext__text.setAttribute('style',hs);
		this._tooltiptext.ggTextDiv.innerHTML="";
		this._tooltiptext.appendChild(this._tooltiptext__text);
		this._tooltip.appendChild(this._tooltiptext);
		this.divSkin.appendChild(this._tooltip);
		this._external_1=document.createElement('div');
		this._external_1.ggId="External 1";
		this._external_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._external_1.ggVisible=true;
		this._external_1.className='ggskin ggskin_external';
		this._external_1.ggType='external';
		this._external_1.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-175 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: 10px;';
		hs+='top:  -175px;';
		hs+='width: 208px;';
		hs+='height: 55px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._external_1.setAttribute('style',hs);
		this._external_1__img=document.createElement('img');
		this._external_1__img.className='ggskin ggskin_external';
		this._external_1__img.setAttribute('src','http://d2fu6qsc3xtnne.cloudfront.net/images/xtr/xtr_logo.png');
		this._external_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._external_1__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._external_1__img);
		this._external_1.appendChild(this._external_1__img);
		this.divSkin.appendChild(this._external_1);
		me._container_3.style[domTransition]='none';
		me._container_3.style.visibility='hidden';
		me._container_3.ggVisible=false;
		me._container_4.style[domTransition]='none';
		me._container_4.style.visibility='hidden';
		me._container_4.ggVisible=false;
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
			me._container_1.style[domTransition]='none';
			me._container_1.style.visibility='hidden';
			me._container_1.ggVisible=false;
			me._container_3.style[domTransition]='none';
			me._container_3.style.visibility='inherit';
			me._container_3.ggVisible=true;
			me._container_4.style[domTransition]='none';
			me._container_4.style.visibility='inherit';
			me._container_4.ggVisible=true;
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
			me._fullscreen.style[domTransition]='none';
			me._fullscreen.style.visibility='hidden';
			me._fullscreen.ggVisible=false;
			me._restorescreen.style[domTransition]='none';
			me._restorescreen.style.visibility='inherit';
			me._restorescreen.ggVisible=true;
		}
		this.divSkin.ggExitFullscreen=function() {
			me._fullscreen.style[domTransition]='none';
			me._fullscreen.style.visibility='inherit';
			me._fullscreen.ggVisible=true;
			me._restorescreen.style[domTransition]='none';
			me._restorescreen.style.visibility='hidden';
			me._restorescreen.ggVisible=false;
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.changeActiveNode=function(id) {
		var newMarker=new Array();
		var i,j;
		var tags=me.player.userdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId==id) && (id!='')) match=true;
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
			}
		}
		activeNodeMarker=newMarker;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		this._loading_text.ggUpdateText();
		var hs='';
		if (me._loading_bar.ggParameter) {
			hs+=parameterToTransform(me._loading_bar.ggParameter) + ' ';
		}
		hs+='scale(' + (1 * me.player.getPercentLoaded() + 0) + ',1.0) ';
		me._loading_bar.style[domTransform]=hs;
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		this.elementMouseDown=new Array();
		this.elementMouseOver=new Array();
		this.__div=document.createElement('div');
		this.__div.setAttribute('style','position:absolute; left:0px;top:0px;visibility: inherit;');
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		if (hotspot.skinid=='hotspoter') {
			this.__div=document.createElement('div');
			this.__div.ggId="hotspoter";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 500px;';
			hs+='top:  300px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='cursor: pointer;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._hotspoterplus=document.createElement('div');
			this._hotspoterplus.ggId="hotspoter-plus";
			this._hotspoterplus.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspoterplus.ggVisible=true;
			this._hotspoterplus.className='ggskin ggskin_svg';
			this._hotspoterplus.ggType='svg';
			hs ='position:absolute;';
			hs+='left: -25px;';
			hs+='top:  -24px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='opacity: 0.5;';
			hs+='visibility: inherit;';
			this._hotspoterplus.setAttribute('style',hs);
			this._hotspoterplus__img=document.createElement('img');
			this._hotspoterplus__img.className='ggskin ggskin_svg';
			this._hotspoterplus__img.setAttribute('src',basePath + 'images/hotspoterplus.svg');
			this._hotspoterplus__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
			this._hotspoterplus__img['ondragstart']=function() { return false; };
			this._hotspoterplus.appendChild(this._hotspoterplus__img);
			this._hotspoterplus.onclick=function () {
				me.skin._tooltiptext.ggText="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
				me.skin._tooltiptext.ggTextDiv.innerHTML=me.skin._tooltiptext.ggText;
				if (me.skin._tooltiptext.ggUpdateText) {
					me.skin._tooltiptext.ggUpdateText=function() {
						var hs="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
						if (hs!=me.skin._tooltiptext.ggText) {
							me.skin._tooltiptext.ggText=hs;
							me.skin._tooltiptext.ggTextDiv.innerHTML=hs;
						}
					}
				}
			}
			this._hotspoterplus.onmouseover=function () {
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspoterplus.style.opacity='1';
				me._hotspoterplus.style.visibility=me._hotspoterplus.ggVisible?'inherit':'hidden';
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspoterplus.ggParameter.sx=1.5;me._hotspoterplus.ggParameter.sy=1.5;
				me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 1000ms ease-out 0ms';
				}
				me._hotspoterplus.ggParameter.a="360";
				me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._container_6.style[domTransition]='none';
				} else {
					me._container_6.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._container_6.style.opacity='1';
				me._container_6.style.visibility=me._container_6.ggVisible?'inherit':'hidden';
			}
			this._hotspoterplus.onmouseout=function () {
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspoterplus.style.opacity='0.5';
				me._hotspoterplus.style.visibility=me._hotspoterplus.ggVisible?'inherit':'hidden';
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspoterplus.ggParameter.sx=1;me._hotspoterplus.ggParameter.sy=1;
				me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._hotspoterplus.style[domTransition]='none';
				} else {
					me._hotspoterplus.style[domTransition]='all 1000ms ease-out 0ms';
				}
				me._hotspoterplus.ggParameter.a="0";
				me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._container_6.style[domTransition]='none';
				} else {
					me._container_6.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._container_6.style.opacity='0';
				me._container_6.style.visibility='hidden';
			}
			this.__div.appendChild(this._hotspoterplus);
			this._container_6=document.createElement('div');
			this._container_6.ggId="Container 6";
			this._container_6.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._container_6.ggVisible=true;
			this._container_6.className='ggskin ggskin_container';
			this._container_6.ggType='container';
			hs ='position:absolute;';
			hs+='left: -70px;';
			hs+='top:  42px;';
			hs+='width: 139px;';
			hs+='height: 18px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='opacity: 0;';
			hs+='visibility: hidden;';
			this._container_6.setAttribute('style',hs);
			this._rectangle_2=document.createElement('div');
			this._rectangle_2.ggId="Rectangle 2";
			this._rectangle_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_2.ggVisible=true;
			this._rectangle_2.className='ggskin ggskin_rectangle';
			this._rectangle_2.ggType='rectangle';
			hs ='position:absolute;';
			hs+='left: -10px;';
			hs+='top:  -6px;';
			hs+='width: 160px;';
			hs+='height: 29px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='background: #ffffff;';
			hs+='background: rgba(255,255,255,0.588235);';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 10px;';
			hs+=cssPrefix + 'border-radius: 10px;';
			this._rectangle_2.setAttribute('style',hs);
			this._container_6.appendChild(this._rectangle_2);
			this._text_1=document.createElement('div');
			this._text_1__text=document.createElement('div');
			this._text_1.className='ggskin ggskin_textdiv';
			this._text_1.ggTextDiv=this._text_1__text;
			this._text_1.ggId="Text 1";
			this._text_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_1.ggVisible=true;
			this._text_1.className='ggskin ggskin_text';
			this._text_1.ggType='text';
			hs ='position:absolute;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: 137px;';
			hs+='height: 18px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this._text_1.setAttribute('style',hs);
			hs ='position:absolute;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: 137px;';
			hs+='height: 18px;';
			hs+='border: 0px solid #000000;';
			hs+='color: #323232;';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 0px 1px 0px 1px;';
			hs+='overflow: hidden;';
			this._text_1__text.setAttribute('style',hs);
			this._text_1.ggTextDiv.innerHTML="<b>"+me.hotspot.title+"<\/b>";
			this._text_1.appendChild(this._text_1__text);
			this._container_6.appendChild(this._text_1);
			this.__div.appendChild(this._container_6);
			this.ggUse3d=true;
			this.gg3dDistance=500;
		} else
		{
			this.__div=document.createElement('div');
			this.__div.ggId="hotspot_pano";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 192px;';
			hs+='top:  350px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._hotspot_panoplus=document.createElement('div');
			this._hotspot_panoplus.ggId="hotspot_pano-plus";
			this._hotspot_panoplus.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspot_panoplus.ggVisible=true;
			this._hotspot_panoplus.className='ggskin ggskin_svg';
			this._hotspot_panoplus.ggType='svg';
			hs ='position:absolute;';
			hs+='left: -24px;';
			hs+='top:  -25px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='opacity: 0.5;';
			hs+='visibility: inherit;';
			this._hotspot_panoplus.setAttribute('style',hs);
			this._hotspot_panoplus__img=document.createElement('img');
			this._hotspot_panoplus__img.className='ggskin ggskin_svg';
			this._hotspot_panoplus__img.setAttribute('src',basePath + 'images/hotspot_panoplus.svg');
			this._hotspot_panoplus__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 50px;height: 50px;-webkit-user-drag:none;');
			this._hotspot_panoplus__img['ondragstart']=function() { return false; };
			this._hotspot_panoplus.appendChild(this._hotspot_panoplus__img);
			this._hotspot_panoplus.onclick=function () {
				me.skin._tooltiptext.ggText="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
				me.skin._tooltiptext.ggTextDiv.innerHTML=me.skin._tooltiptext.ggText;
				if (me.skin._tooltiptext.ggUpdateText) {
					me.skin._tooltiptext.ggUpdateText=function() {
						var hs="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
						if (hs!=me.skin._tooltiptext.ggText) {
							me.skin._tooltiptext.ggText=hs;
							me.skin._tooltiptext.ggTextDiv.innerHTML=hs;
						}
					}
				}
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
			}
			this._hotspot_panoplus.onmouseover=function () {
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspot_panoplus.style.opacity='1';
				me._hotspot_panoplus.style.visibility=me._hotspot_panoplus.ggVisible?'inherit':'hidden';
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspot_panoplus.ggParameter.sx=1.5;me._hotspot_panoplus.ggParameter.sy=1.5;
				me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 1000ms ease-out 0ms';
				}
				me._hotspot_panoplus.ggParameter.a="360";
				me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			}
			this._hotspot_panoplus.onmouseout=function () {
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspot_panoplus.style.opacity='0.5';
				me._hotspot_panoplus.style.visibility=me._hotspot_panoplus.ggVisible?'inherit':'hidden';
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
				}
				me._hotspot_panoplus.ggParameter.sx=1;me._hotspot_panoplus.ggParameter.sy=1;
				me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
				if (me.player.transitionsDisabled) {
					me._hotspot_panoplus.style[domTransition]='none';
				} else {
					me._hotspot_panoplus.style[domTransition]='all 1000ms ease-out 0ms';
				}
				me._hotspot_panoplus.ggParameter.a="0";
				me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			}
			this.__div.appendChild(this._hotspot_panoplus);
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	this.addSkin();
};