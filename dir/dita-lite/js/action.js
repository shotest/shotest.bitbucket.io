!function(__){
  const _ = __ || {};
  
  const C_ON = 'on';
  const C_HOVER = 'hover';
  
  const TOUCH = 'ontouchstart' in window;
  
  
  /* **********************************************************
    Init
    ********************************************************** */
  _.Init = function(){
    _.Log = _.config.mode === 'dev'? function(arg){console.log(arg)}: function(){};
    
    _.doc.Init();
    
    _.tooltip.Init();
    _.d3.Init();
  };
  
  /* **********************************************************
    util
    ********************************************************** */
  _.util = {
    array: {
      Unique: function(arr){
        return arr.filter(function(V,I,AR){
          return AR.indexOf(V) === I;
        }).sort(function(A,B){
          return A < B? -1: 1;
        });
      }
    },
    num: {
      ZeroFill: function(num,digital){
        return ('000000' + num).slice(-1 * digital);
      },
      Percent: function(num,decimal = 0){
        return (num * 100).toFixed(decimal);
      }
    },
    $: {
      On: function(target){
        const $target = $(target);
        if($target.hasClass(C_ON)){
          $target.removeClass(C_ON);
        }else{
          $target.addClass(C_ON);
        }
      }
    }
  };
  /* **********************************************************
  config
  ********************************************************** */
  _.config = {
    mode: 'dev',
    nums: 0,
    show: {
      result: false
    }
  };
  /* **********************************************************
  window
  ********************************************************** */
  _.window = {
    Init: function(){},
    
  };
  /* **********************************************************
  doc
  ********************************************************** */
  _.doc = {
    Init: function(){
      this.add.key.Common();
      this.add.hover.Card();
      this.add.click.Li();
      this.add.click.Submit();
      this.add.click.PageTop();
      this.add.click.Cancel();
      this.add.click.result.Back();
    },
    add: {
      key: {
        Common: function(){
          $(window).on({
            'keydown': function(E){
              return false;
            },
            'keyup': function(E){
              E.preventDefault();
              E.stopPropagation();
              switch(E.keyCode){
                case 13:
                  _.action.Submit();
                  break;
                case 27:
                  _.action.close.Result();
                  break;
                default: 
                  _.Log(E.keyCode);
                  break;
              }
              return false;
            }
          });
        }
      },
      hover: {
        Card: function(){
          $('.card').on({
          'mouseover':function(){
            $('.cards').addClass(C_HOVER);
            $(this).addClass(C_HOVER);
          },
          'mouseout':function(){
            $('.cards').removeClass(C_HOVER);
            $(this).removeClass(C_HOVER);
          }
          });
        }
      },
      click: {
        Li: function(){
          $('.hearing li').on('click',function(){
            //* const $this = $(this);
            //* if($this.hasClass(C_ON)){
            //*   $this.removeClass(C_ON);
            //* }else{
            //*   $this.addClass(C_ON);
            //* }
            _.util.$.On(this);
          });
        },
        Submit: function(){
          $('#btn-result').on('click',function(E){
            E.preventDefault();
            _.action.Submit();
          });
        },
        PageTop: function(){
          $('#btn-pagetop').on('click',function(E){
            E.preventDefault();
            $('html,body').animate({scrollTop:0});
          });
        },
        Cancel: function(){
          if(TOUCH){
            $('#btn-cancel').on({
              'touchstart': function(E){
                E.preventDefault();
                $('.hearing li').removeClass(C_ON);
              },
              'touchend': function(E){
                E.preventDefault();
                $('.hearing li').removeClass(C_ON);
              }
            });
          } else {
            $('#btn-cancel').on('click',function(E){
              E.preventDefault();
              $('.hearing li').removeClass(C_ON);
            });
          }
        },
        result: {
          Back: function(){
            $('.result .back').on('click',function(){
              _.action.toggle.Result();
            });
          }
        }
      }
    }
    
  };
  /* **********************************************************
  action
  ********************************************************** */
  _.action = {
    Init: function(){},
    get: {
      Choose: function(){
        const $lis = $('.hearing li.on');
        
        let nums = [];
        $lis.each(function(){
          const num = $(this).data('num');
          if(num < 100){
          nums.push($(this).data('num'));
          }
        });
        
        return nums;
      }
    },
    toggle: {
      Result: function(){
        if(_.config.show.result){
        _.Log('toggle:close');
          _.action.close.Result();
        }else{
        _.Log('toggle:show');
          _.action.show.Result();
        }
      }
    },
    show: {
      Result: function(){
        _.action.build.result.Init();
        
        $('.result').addClass(C_ON);
        _.config.show.result = true;
      }
    },
    close: {
      Result: function(){
        $('.result').removeClass(C_ON);
        _.config.show.result = false;
      }
    },
    Submit: function(){
      _.config.nums = _.action.get.Choose();
      _.action.toggle.Result();
      $('.result_contents').animate({scrollTop:0});
    },
    build: {
      Init: function(){
      },
      result: {
        Init: function(){
          this.Data();
          this.AfterData();
          this.Dita();
          this.Request();
          this.Service();
        },
        Data: function(){
        
          _.config.service_num = _.util.array.Unique(
            _.config.nums.map(function(V){
              return _.PULL_LIST[V];
            }).flat()
          );
          _.config.service = _.config.service_num.map(function(V){
            return _.SERVICES[V];
          });
          
          _.Log(_.config.service);
          
          $('.card').removeClass(C_ON);
          _.config.service_num.forEach(function(V,I){
            const $target = $('.card[data-num="'+_.util.num.ZeroFill(V,2)+'"]');
            $target.addClass(C_ON);
          });
        },
        AfterData: function(){
          $.each($('.cards-ctt'),function(N,target){
            const $target = $(target);
            const on_num = $target.find('.card.on').length;
            $target.attr('data-cards-on',on_num);
            $target.prev().show();
            if(on_num === 0){
              $target.prev().hide();
            }
          });
          
            $('.service').show();
          
          const all_ctts = $('.cards-ctt').length;
          const active_ctts = $('.cards-ctt[data-cards-on="0"]').length;
            
          if(all_ctts === active_ctts){
            $('.service').hide();
          }
        },
        Dita: function(){
          const ditas = _.config.service.filter(function(V){
            return V[1] == 'd';
          });
          const per_num = ditas.length / 7;
          
          $('.dita .characteristic').removeClass(C_ON);
          ditas.forEach(function(V,I){
            $('.characteristic[data-num="'+_.util.num.ZeroFill(V[2],2)+'"]').addClass(C_ON);
          });
          if(per_num === 0){
            $('.characteristic[data-num="99"]').addClass(C_ON);
          }
          
          _.d3.build.Proccess('dita',per_num);
          
        },
        Request: function(){
          this.Request1();
          this.Request2();
        },
        Request1: function(){
          const negas = _.config.service.filter(function(V){
            return V[1].indexOf('nega1') > -1;
          });
          const per_num = negas.length / 3;
          
          $('.request1 .characteristic').removeClass(C_ON);
          negas.forEach(function(V,I){
            $('.characteristic[data-num="'+_.util.num.ZeroFill(V[2],2)+'"]').addClass(C_ON);
          });
          
          $('.request1').show();
          $('.request').removeClass('no-request1');
          if(per_num === 0){
            $('.request1').hide();
            $('.request').addClass('no-request1');
          }
          
          _.d3.build.Proccess('request1',per_num);
          
        },
        Request2: function(){
          const negas = _.config.service.filter(function(V){
            return V[1].indexOf('nega2') > -1;
          });
          const per_num = negas.length / 5;
          
          $('.request2 .characteristic').removeClass(C_ON);
          negas.forEach(function(V,I){
            $('.characteristic[data-num="'+_.util.num.ZeroFill(V[2],2)+'"]').addClass(C_ON);
          });
          
            $('.request2').show();
          $('.request').removeClass('no-request2');
          if(per_num === 0){
            $('.request2').hide();
            $('.request').addClass('no-request2');
          }
          
          _.d3.build.Proccess('request2',per_num);
          
        },
        Service: function(){}
      }
    }
  };
  
  /* **********************************************************
  tooltip
  ********************************************************** */
  _.tooltip = {
    Init: function(){
      tippy('.card',{
        allowHTML: true,
        interactive: true,
        offset: [0,15],
        // interactiveBorder: 15,
        appendTo: 'parent',
        
        content: function(ref){
          const $tooltip = $(ref).find('.tooltip');
          if(!!$tooltip.length){
            return $tooltip[0].innerHTML;
          }
        }
      });
      
      /* $('.tooltip').tooltipster({
        theme: 'Shadow',
        contentAsHTML: true,
        content: function(){
          _.Log(this)
          return $(this).find('.tooltip-content').html();
        },
        functionInit: function(){
          _.Log(this)
          _.Log($(this._$origin[0]).find('.tooltip-content').html())
          return $(this._$origin[0]).find('.tooltip-content').html();
        },
      }); */
    },
    
  };
  
  /* **********************************************************
  d3
  ********************************************************** */
  _.d3 = {
    Init: function(){
      
      this.radius = Math.min(this.width,this.height) / 2 - 10;
      this.build.Init();
    },
    
    pi: Math.PI * 2,
    width: 250,
    height: 250,
    radius: 0,
    border: 25,
    color: '#263b74',
    duration: 1000,
    per_tag: '<tspan class="small">%</tspan>',
    
    percent: {},
    angle: {},
    
    Arc: function(arg){
      return d3.arc()
            .outerRadius(_.d3.radius)
            .innerRadius(_.d3.radius - _.d3.border)
            (arg)
            ;
    },
    
    tween: {
      Angle: function(_key){
        const ip = d3.interpolate(
          {startAngle: 0, endAngle: _.d3.pi * 0},
          {startAngle: _.d3.pi * 0, endAngle: _.d3.angle[_key]}
        );
        return function(t){
          return _.d3.Arc(ip(t));
        }
      },
      NumPer: function(){
        const ip = d3.interpolate(0,100);
        return function(t){
          return ip(t);
        }
      }
    },
    
    update: {
      Graph: function(_key,d3targets){
        
        d3targets.forEach(function(d3e){
          switch(d3e.node().nodeName){
            case 'path':
              d3e
                .transition()
                .ease(d3.easeExpOut) 
                .duration(_.d3.duration)
                .attrTween('d',function(){return _.d3.tween.Angle(_key)})
                ;
                break;
            case 'text':
              d3e
                .transition()
                .ease(d3.easeExpOut) 
                .duration(_.d3.duration)
                .tween('text',function(d){
                  const _this = this;
                  const ip = d3.interpolateRound(0,_.d3.percent[_key]);
                  return function(t){
                    d3.select(_this).html(ip(t) + _.d3.per_tag)
                  }
                })
                break;
            default:
              console.log('default');
              
          }
        });
        
      }
    },
    build: {
      Init: function(){
      },
      Proccess: function(_key,_value){
          _.d3.percent[_key] = _.util.num.Percent(_value);
          _.d3.angle[_key] = _.d3.pi * _value
          //_.d3.build.Dita();
          _.d3.build.Graph(_key);
          _.d3.build.Graph(_key);
      },
      Graph: function(_key,_options = {}){
        const 
          selector = '.' + _key,
          w = _options.width || _.d3.width,
          h = _options.height || _.d3.height,
          r = _options.radius || _.d3.radius,
          c = _options.color || _.d3.color,
          Arc = _.d3.Arc
          ;
          
        d3.select(selector + ' svg').remove()
          
        const svg = d3.select(selector + ' .chart')
          .append('svg')
          .attr('width', w)
          .attr('height',h)
          ;
          
        const g = svg
          .append('g')
          .classed('arc',true)
          .attr('transform','translate(' + w / 2 + ',' + h / 2 + ')')
          ;
        
        const arc_back = g.append('path')
          .classed('back',true)
          .attr('fill','#fff')
          .attr('fill-opacity','0.2')
          .attr('d',function(){ 
            return Arc({
                startAngle: 0,
                endAngle: _.d3.pi
              })
            })
          ;
        
        const arc_per = g.append('path')
          .classed('percent',true)
          .attr('fill','#fff')
          .attr('fill-opacity','0.5')
          ;
          
            
        const num_per = g
          .append('g')
          .classed('text',true)
          .attr('transform','translate(0,10)')
          .append('text')
          .attr('fill','#fff')
          .attr('font-size','30px')
          .attr('text-anchor','middle')
          .html(0 + _.d3.per_tag)
          ;
        
        // update                           
        _.d3.update.Graph(_key,[arc_per,num_per]);
      },
      Dita: function(){
      }
    }
  };
  
  
  window._ = _;
}(window._);

$(_.Init);