const http = require('http');
const server = http.createServer();

const {config} = require('./node/config');
const {mimetype} = require('./node/mimetype');
const fs = require('fs');
const path = require('path');

// basic認証
const basic_realm = "/";
const basic_name = "simple-nodejs_member";

let content;
let basic_users = ["QWxhZGRpbjpvcGVuIHNlc2FtZQ=="];

server.on('request',(REQ,RES)=>{
	const {mime,encode,format} = mimetype(REQ.url);
  
	let reqUrl = REQ.url.split(/\?/);
  let reqUrlResolve = path.resolve(REQ.url);
  let referer = REQ.headers.referer;
	let url = (__dirname + reqUrl[0])
      .replace(/\//g,'\\')
      .replace(/(\\[^.\\]+)(\\)?$/,'$1\\index.html')
      .replace(/(\d{2,})$/,'$1\\index.html');
      
    if(reqUrl[0].match(/\/[^\/.]+$/)){
      RES.writeHead(302, {
        'Location': reqUrl[0] + '/'
      });
      RES.end();
    }
    
	addUser("syo_inoue", "31128311");
	addUser("itp", "itp");
	addUser("--", "--");addUser("++", "++");
	
	if (reqUrl[0].startsWith(basic_realm)) {
		var auth = REQ.headers["authorization"]||"";
	    if (!auth.startsWith("Basic ") || !isUser(auth.substring(6))) {
	      RES.statusCode = 401;
	      RES.setHeader('WWW-Authenticate', 'Basic realm="' + basic_name + '"');
	      RES.end();
	    }
	}
	if (!RES.finished) {
	
  	fs.readFile(url,encode, (ERR,DATA)=>{
  		const {mime,encode,format} = mimetype(url);
  		
  		if(ERR){
  			console.log(ERR);
  			RES.writeHead(404,{'Content-Type':'text/plain'});
  			RES.write('Page Not Found...');
  			return RES.end();
  		}
  		
  		content = DATA;
  		
  		RES.writeHead(200,{'Content-Type':mime});
  		RES.end(content,encode);
  		
  	});
	}
});
function isBin(_url){
	return getType(_url) == types['.png'];
}
function getType(_url) {
	return types[_url.match(/\.[^.]+$/gim)[0]] || 'text/plain';
}

function isUser(_auth) {
  for (var l=0; l<basic_users.length; l++) {
    if (basic_users[l] == _auth) {
      return true;
    }
  }
  return false;
}
function addUser(_id, _pw) {
  var auth = (new Buffer((_id + ':' + _pw).toString(), 'binary')).toString('base64');
  if (!isUser(auth)) {
    basic_users.push(auth);
  }
}

server.listen(config.port);
console.log('==============================================')
console.log('Server Start... '+config.port)
console.log('==============================================')
