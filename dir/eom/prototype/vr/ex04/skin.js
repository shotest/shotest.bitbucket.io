// Garden Gnome Software - Skin
// Object2VR 3.1.7/10775
// Filename: tyt_eom.ggsk
// Generated 火 9 17 18:34:14 2019

function object2vrSkin(player,base) {
	var me=this;
	var flag=false;
	var nodeMarker=new Array();
	var activeNodeMarker=new Array();
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=new Array();
	this.elementMouseOver=new Array();
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	for(i=0;i<prefixes.length;i++) {
		if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
			cssPrefix='-' + prefixes[i].toLowerCase() + '-';
			domTransition=prefixes[i] + 'Transition';
			domTransform=prefixes[i] + 'Transform';
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=new Array();
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=new Array();
		var stack=new Array();
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		this._toolbar=document.createElement('div');
		this._toolbar.ggId="toolbar";
		this._toolbar.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._toolbar.ggVisible=true;
		this._toolbar.className='ggskin ggskin_rectangle';
		this._toolbar.ggType='rectangle';
		this._toolbar.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-162 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-41 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -162px;';
		hs+='top:  -41px;';
		hs+='width: 323px;';
		hs+='height: 36px;';
		hs+=cssPrefix + 'transform-origin: 50% 100%;';
		hs+='visibility: inherit;';
		hs+='background: #000000;';
		hs+='background: rgba(0,0,0,0.00392157);';
		hs+='border: 0px solid #000000;';
		this._toolbar.setAttribute('style',hs);
		this._left=document.createElement('div');
		this._left.ggId="left";
		this._left.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._left.ggVisible=true;
		this._left.className='ggskin ggskin_button';
		this._left.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._left.setAttribute('style',hs);
		this._left__img=document.createElement('img');
		this._left__img.className='ggskin ggskin_button';
		this._left__img.setAttribute('src',basePath + 'images/left.png');
		this._left__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._left__img.className='ggskin ggskin_button';
		this._left__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._left__img);
		this._left.appendChild(this._left__img);
		this._left.onclick=function () {
			me.player.changePanLog(1,true);
		}
		this._toolbar.appendChild(this._left);
		this._right=document.createElement('div');
		this._right.ggId="right";
		this._right.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._right.ggVisible=true;
		this._right.className='ggskin ggskin_button';
		this._right.ggType='button';
		hs ='position:absolute;';
		hs+='left: 35px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._right.setAttribute('style',hs);
		this._right__img=document.createElement('img');
		this._right__img.className='ggskin ggskin_button';
		this._right__img.setAttribute('src',basePath + 'images/right.png');
		this._right__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._right__img.className='ggskin ggskin_button';
		this._right__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._right__img);
		this._right.appendChild(this._right__img);
		this._right.onclick=function () {
			me.player.changePanLog(-1,true);
		}
		this._toolbar.appendChild(this._right);
		this._up=document.createElement('div');
		this._up.ggId="up";
		this._up.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._up.ggVisible=true;
		this._up.className='ggskin ggskin_button';
		this._up.ggType='button';
		hs ='position:absolute;';
		hs+='left: 70px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._up.setAttribute('style',hs);
		this._up__img=document.createElement('img');
		this._up__img.className='ggskin ggskin_button';
		this._up__img.setAttribute('src',basePath + 'images/up.png');
		this._up__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._up__img.className='ggskin ggskin_button';
		this._up__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._up__img);
		this._up.appendChild(this._up__img);
		this._up.onclick=function () {
			me.player.changeTiltLog(1,true);
		}
		this._toolbar.appendChild(this._up);
		this._down=document.createElement('div');
		this._down.ggId="down";
		this._down.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._down.ggVisible=true;
		this._down.className='ggskin ggskin_button';
		this._down.ggType='button';
		hs ='position:absolute;';
		hs+='left: 105px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._down.setAttribute('style',hs);
		this._down__img=document.createElement('img');
		this._down__img.className='ggskin ggskin_button';
		this._down__img.setAttribute('src',basePath + 'images/down.png');
		this._down__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._down__img.className='ggskin ggskin_button';
		this._down__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._down__img);
		this._down.appendChild(this._down__img);
		this._down.onclick=function () {
			me.player.changeTiltLog(-1,true);
		}
		this._toolbar.appendChild(this._down);
		this._zoom_in=document.createElement('div');
		this._zoom_in.ggId="zoom in";
		this._zoom_in.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._zoom_in.ggVisible=true;
		this._zoom_in.className='ggskin ggskin_button';
		this._zoom_in.ggType='button';
		hs ='position:absolute;';
		hs+='left: 140px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._zoom_in.setAttribute('style',hs);
		this._zoom_in__img=document.createElement('img');
		this._zoom_in__img.className='ggskin ggskin_button';
		this._zoom_in__img.setAttribute('src',basePath + 'images/zoom_in.png');
		this._zoom_in__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoom_in__img.className='ggskin ggskin_button';
		this._zoom_in__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoom_in__img);
		this._zoom_in.appendChild(this._zoom_in__img);
		this._zoom_in.onclick=function () {
			me.player.changeFovLog(-1,true);
		}
		this._toolbar.appendChild(this._zoom_in);
		this._zoom_out=document.createElement('div');
		this._zoom_out.ggId="zoom out";
		this._zoom_out.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._zoom_out.ggVisible=true;
		this._zoom_out.className='ggskin ggskin_button';
		this._zoom_out.ggType='button';
		hs ='position:absolute;';
		hs+='left: 175px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._zoom_out.setAttribute('style',hs);
		this._zoom_out__img=document.createElement('img');
		this._zoom_out__img.className='ggskin ggskin_button';
		this._zoom_out__img.setAttribute('src',basePath + 'images/zoom_out.png');
		this._zoom_out__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoom_out__img.className='ggskin ggskin_button';
		this._zoom_out__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoom_out__img);
		this._zoom_out.appendChild(this._zoom_out__img);
		this._zoom_out.onclick=function () {
			me.player.changeFovLog(1,true);
		}
		this._toolbar.appendChild(this._zoom_out);
		this._auto_rotate=document.createElement('div');
		this._auto_rotate.ggId="auto rotate";
		this._auto_rotate.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._auto_rotate.ggVisible=true;
		this._auto_rotate.className='ggskin ggskin_button';
		this._auto_rotate.ggType='button';
		hs ='position:absolute;';
		hs+='left: 210px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._auto_rotate.setAttribute('style',hs);
		this._auto_rotate__img=document.createElement('img');
		this._auto_rotate__img.className='ggskin ggskin_button';
		this._auto_rotate__img.setAttribute('src',basePath + 'images/auto_rotate.png');
		this._auto_rotate__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._auto_rotate__img.className='ggskin ggskin_button';
		this._auto_rotate__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._auto_rotate__img);
		this._auto_rotate.appendChild(this._auto_rotate__img);
		this._auto_rotate.onclick=function () {
			me.player.toggleAutorotate();
		}
		this._toolbar.appendChild(this._auto_rotate);
		this._info=document.createElement('div');
		this._info.ggId="info";
		this._info.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._info.ggVisible=true;
		this._info.className='ggskin ggskin_button';
		this._info.ggType='button';
		hs ='position:absolute;';
		hs+='left: 245px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._info.setAttribute('style',hs);
		this._info__img=document.createElement('img');
		this._info__img.className='ggskin ggskin_button';
		this._info__img.setAttribute('src',basePath + 'images/info.png');
		this._info__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._info__img.className='ggskin ggskin_button';
		this._info__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._info__img);
		this._info.appendChild(this._info__img);
		this._info.onclick=function () {
			alert(me.player.userdata.information);
		}
		this._toolbar.appendChild(this._info);
		this._hide_hotspot=document.createElement('div');
		this._hide_hotspot.ggId="hide hotspot";
		this._hide_hotspot.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._hide_hotspot.ggVisible=true;
		this._hide_hotspot.className='ggskin ggskin_image';
		this._hide_hotspot.ggType='image';
		hs ='position:absolute;';
		hs+='left: 280px;';
		hs+='top:  0px;';
		hs+='width: 38px;';
		hs+='height: 38px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._hide_hotspot.setAttribute('style',hs);
		this._hide_hotspot__img=document.createElement('img');
		this._hide_hotspot__img.className='ggskin ggskin_image';
		this._hide_hotspot__img.setAttribute('src',basePath + 'images/hide_hotspot.png');
		this._hide_hotspot__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._hide_hotspot__img.className='ggskin ggskin_image';
		this._hide_hotspot__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._hide_hotspot__img);
		this._hide_hotspot.appendChild(this._hide_hotspot__img);
		this._hide_hotspot.onclick=function () {
			ToggleHotspot();
		}
		this._toolbar.appendChild(this._hide_hotspot);
		this.divSkin.appendChild(this._toolbar);
		this._loading=document.createElement('div');
		this._loading.ggId="Loading";
		this._loading.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading.ggVisible=true;
		this._loading.className='ggskin ggskin_container';
		this._loading.ggType='container';
		this._loading.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
				this.style.left=Math.floor(-105 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
				this.style.top=Math.floor(-28 + h/2) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -105px;';
		hs+='top:  -28px;';
		hs+='width: 222px;';
		hs+='height: 57px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loading.setAttribute('style',hs);
		this._rectangle_1=document.createElement('div');
		this._rectangle_1.ggId="Rectangle 1";
		this._rectangle_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._rectangle_1.ggVisible=true;
		this._rectangle_1.className='ggskin ggskin_rectangle';
		this._rectangle_1.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: -1px;';
		hs+='top:  -3px;';
		hs+='width: 220px;';
		hs+='height: 55px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='background: #000000;';
		hs+='background: rgba(0,0,0,0.588235);';
		hs+='border: 2px solid #ffffff;';
		hs+='border-radius: 5px;';
		hs+=cssPrefix + 'border-radius: 5px;';
		this._rectangle_1.setAttribute('style',hs);
		this._loading.appendChild(this._rectangle_1);
		this._loading_bar=document.createElement('div');
		this._loading_bar.ggId="loading bar";
		this._loading_bar.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_bar.ggVisible=true;
		this._loading_bar.className='ggskin ggskin_rectangle';
		this._loading_bar.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: 1px;';
		hs+='top:  36px;';
		hs+='width: 220px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 0% 50%;';
		hs+='visibility: inherit;';
		hs+='background: #ffffff;';
		hs+='border: 0px solid #000000;';
		this._loading_bar.setAttribute('style',hs);
		this._loading.appendChild(this._loading_bar);
		this._loading_text=document.createElement('div');
		this._loading_text__text=document.createElement('div');
		this._loading_text.className='ggskin ggskin_textdiv';
		this._loading_text.ggTextDiv=this._loading_text__text;
		this._loading_text.ggId="loading text";
		this._loading_text.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_text.ggVisible=true;
		this._loading_text.className='ggskin ggskin_text';
		this._loading_text.ggType='text';
		hs ='position:absolute;';
		hs+='left: 10px;';
		hs+='top:  11px;';
		hs+='width: 198px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loading_text.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 198px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._loading_text__text.setAttribute('style',hs);
		this._loading_text.ggUpdateText=function() {
			var hs="<b>Loading... "+(me.player.getPercentLoaded()*100.0).toFixed(0)+"%<\/b>";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
			}
		}
		this._loading_text.ggUpdateText();
		this._loading_text.appendChild(this._loading_text__text);
		this._loading.appendChild(this._loading_text);
		this._loading_close=document.createElement('div');
		this._loading_close.ggId="loading close";
		this._loading_close.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_close.ggVisible=true;
		this._loading_close.className='ggskin ggskin_image';
		this._loading_close.ggType='image';
		hs ='position:absolute;';
		hs+='left: 203px;';
		hs+='top:  2px;';
		hs+='width: 13px;';
		hs+='height: 13px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._loading_close.setAttribute('style',hs);
		this._loading_close__img=document.createElement('img');
		this._loading_close__img.className='ggskin ggskin_image';
		this._loading_close__img.setAttribute('src',basePath + 'images/loading_close.png');
		this._loading_close__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._loading_close__img.className='ggskin ggskin_image';
		this._loading_close__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._loading_close__img);
		this._loading_close.appendChild(this._loading_close__img);
		this._loading_close.onclick=function () {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		}
		this._loading.appendChild(this._loading_close);
		this.divSkin.appendChild(this._loading);
		this._hotspot=document.createElement('div');
		this._hotspot.ggId="hotspot";
		this._hotspot.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._hotspot.ggVisible=true;
		this._hotspot.className='ggskin ggskin_container';
		this._hotspot.ggType='container';
		hs ='position:absolute;';
		hs+='left: 12px;';
		hs+='top:  10px;';
		hs+='width: 268px;';
		hs+='height: 73px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._hotspot.setAttribute('style',hs);
		this.divSkin.appendChild(this._hotspot);
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.changeActiveNode=function(id) {
		var newMarker=new Array();
		var i,j;
		var tags=me.player.userdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId==id) && (id!='')) match=true;
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
			}
		}
		activeNodeMarker=newMarker;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		var hs='';
		if (me._loading_bar.ggParameter) {
			hs+=parameterToTransform(me._loading_bar.ggParameter) + ' ';
		}
		hs+='scale(' + (1 * me.player.getPercentLoaded() + 0) + ',1.0) ';
		me._loading_bar.style[domTransform]=hs;
		this._loading_text.ggUpdateText();
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		this.elementMouseDown=new Array();
		this.elementMouseOver=new Array();
		this.__div=document.createElement('div');
		this.__div.setAttribute('style','position:absolute; left:0px;top:0px;visibility: inherit;');
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		if (hotspot.skinid=='hotspot-script-p2') {
			this.__div=document.createElement('div');
			this.__div.ggId="hotspot-script-p2";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 325px;';
			hs+='top:  25px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='cursor: pointer;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				Hotspot(me.hotspot.title,me.hotspot.url);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._rectangle_2=document.createElement('div');
			this._rectangle_2.ggId="Rectangle 2";
			this._rectangle_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_2.ggVisible=true;
			this._rectangle_2.className='ggskin ggskin_rectangle';
			this._rectangle_2.ggType='rectangle';
			hs ='position:absolute;';
			hs+='left: -36px;';
			hs+='top:  -26px;';
			hs+='width: 66px;';
			hs+='height: 46px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='background: #0064cc;';
			hs+='background: rgba(0,100,204,0.196078);';
			hs+='border: 4px solid #0064cc;';
			hs+='border-radius: 10px;';
			hs+=cssPrefix + 'border-radius: 10px;';
			this._rectangle_2.setAttribute('style',hs);
			this.__div.appendChild(this._rectangle_2);
		} else
		if (hotspot.skinid=='hotspot-script-p') {
			this.__div=document.createElement('div');
			this.__div.ggId="hotspot-script-p";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 214px;';
			hs+='top:  25px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				Hotspot(me.hotspot.title,me.hotspot.url);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._hotspotscriptpimage=document.createElement('div');
			this._hotspotscriptpimage.ggId="hotspot-script-p-image";
			this._hotspotscriptpimage.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotscriptpimage.ggVisible=true;
			this._hotspotscriptpimage.className='ggskin ggskin_image';
			this._hotspotscriptpimage.ggType='image';
			hs ='position:absolute;';
			hs+='left: -24px;';
			hs+='top:  -25px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this._hotspotscriptpimage.setAttribute('style',hs);
			this._hotspotscriptpimage__img=document.createElement('img');
			this._hotspotscriptpimage__img.className='ggskin ggskin_image';
			this._hotspotscriptpimage__img.setAttribute('src',basePath + 'images/hotspotscriptpimage.png');
			this._hotspotscriptpimage__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
			this._hotspotscriptpimage__img.className='ggskin ggskin_image';
			this._hotspotscriptpimage__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._hotspotscriptpimage__img);
			this._hotspotscriptpimage.appendChild(this._hotspotscriptpimage__img);
			this.__div.appendChild(this._hotspotscriptpimage);
			this._hotspotscriptpimageon=document.createElement('div');
			this._hotspotscriptpimageon.ggId="hotspot-script-p-image-on";
			this._hotspotscriptpimageon.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotscriptpimageon.ggVisible=false;
			this._hotspotscriptpimageon.className='ggskin ggskin_button';
			this._hotspotscriptpimageon.ggType='button';
			hs ='position:absolute;';
			hs+='left: -24px;';
			hs+='top:  -25px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: hidden;';
			this._hotspotscriptpimageon.setAttribute('style',hs);
			this._hotspotscriptpimageon__img=document.createElement('img');
			this._hotspotscriptpimageon__img.className='ggskin ggskin_button';
			this._hotspotscriptpimageon__img.setAttribute('src',basePath + 'images/hotspotscriptpimageon.png');
			this._hotspotscriptpimageon__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
			this._hotspotscriptpimageon__img.className='ggskin ggskin_button';
			this._hotspotscriptpimageon__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._hotspotscriptpimageon__img);
			this._hotspotscriptpimageon.appendChild(this._hotspotscriptpimageon__img);
			this.__div.appendChild(this._hotspotscriptpimageon);
		} else
		{
			this.__div=document.createElement('div');
			this.__div.ggId="hotspot-popup";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 83px;';
			hs+='top:  24px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='cursor: pointer;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				HideHotspot(this);
				flag=me._hotspotpopupcontainer.ggOpacitiyActive;
				if (me.player.transitionsDisabled) {
					me._hotspotpopupcontainer.style[domTransition]='none';
				} else {
					me._hotspotpopupcontainer.style[domTransition]='all 500ms ease-out 0ms';
				}
				if (flag) {
					me._hotspotpopupcontainer.style.opacity='0';
					me._hotspotpopupcontainer.style.visibility='hidden';
				} else {
					me._hotspotpopupcontainer.style.opacity='1';
					me._hotspotpopupcontainer.style.visibility=me._hotspotpopupcontainer.ggVisible?'inherit':'hidden';
				}
				me._hotspotpopupcontainer.ggOpacitiyActive=!flag;
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me._hotspotpopuptext.ggText=me.hotspot.title;
				me._hotspotpopuptext.ggTextDiv.innerHTML=me._hotspotpopuptext.ggText;
				if (me._hotspotpopuptext.ggUpdateText) {
					me._hotspotpopuptext.ggUpdateText=function() {
						var hs=me.hotspot.title;
						if (hs!=me._hotspotpopuptext.ggText) {
							me._hotspotpopuptext.ggText=hs;
							me._hotspotpopuptext.ggTextDiv.innerHTML=hs;
						}
					}
				}
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._hotspotpopupcontainer=document.createElement('div');
			this._hotspotpopupcontainer.ggId="hotspot-popup-container";
			this._hotspotpopupcontainer.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotpopupcontainer.ggVisible=true;
			this._hotspotpopupcontainer.className='ggskin ggskin_container';
			this._hotspotpopupcontainer.ggType='container';
			hs ='position:absolute;';
			hs+='left: -73px;';
			hs+='top:  23px;';
			hs+='width: 150px;';
			hs+='height: 40px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='opacity: 0;';
			hs+='visibility: hidden;';
			hs+='cursor: pointer;';
			this._hotspotpopupcontainer.setAttribute('style',hs);
			this._hotspotpopupcontainer.onclick=function () {
				me.player.openUrl("..\/..\/html\/vh"+me.hotspot.url+".html","_parent");
				flag=me._hotspotpopuprectangle.ggScaleActive;
				if (me.player.transitionsDisabled) {
					me._hotspotpopuprectangle.style[domTransition]='none';
				} else {
					me._hotspotpopuprectangle.style[domTransition]='all 500ms ease-out 0ms';
				}
				if (flag) {
					me._hotspotpopuprectangle.ggParameter.sx=1;me._hotspotpopuprectangle.ggParameter.sy=1;
					me._hotspotpopuprectangle.style[domTransform]=parameterToTransform(me._hotspotpopuprectangle.ggParameter);
				} else {
					me._hotspotpopuprectangle.ggParameter.sx=1.1;me._hotspotpopuprectangle.ggParameter.sy=1.1;
					me._hotspotpopuprectangle.style[domTransform]=parameterToTransform(me._hotspotpopuprectangle.ggParameter);
				}
				me._hotspotpopuprectangle.ggScaleActive=!flag;
			}
			this._hotspotpopuprectangle=document.createElement('div');
			this._hotspotpopuprectangle.ggId="hotspot-popup-rectangle";
			this._hotspotpopuprectangle.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotpopuprectangle.ggVisible=true;
			this._hotspotpopuprectangle.className='ggskin ggskin_rectangle';
			this._hotspotpopuprectangle.ggType='rectangle';
			hs ='position:absolute;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: 150px;';
			hs+='height: 40px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='background: #ffffff;';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			this._hotspotpopuprectangle.setAttribute('style',hs);
			this._hotspotpopupcontainer.appendChild(this._hotspotpopuprectangle);
			this._hotspotpopuptext=document.createElement('div');
			this._hotspotpopuptext__text=document.createElement('div');
			this._hotspotpopuptext.className='ggskin ggskin_textdiv';
			this._hotspotpopuptext.ggTextDiv=this._hotspotpopuptext__text;
			this._hotspotpopuptext.ggId="hotspot-popup-text";
			this._hotspotpopuptext.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotpopuptext.ggVisible=true;
			this._hotspotpopuptext.className='ggskin ggskin_text';
			this._hotspotpopuptext.ggType='text';
			hs ='position:absolute;';
			hs+='left: 5px;';
			hs+='top:  5px;';
			hs+='width: 138px;';
			hs+='height: 28px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='cursor: pointer;';
			this._hotspotpopuptext.setAttribute('style',hs);
			hs ='position:absolute;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: 138px;';
			hs+='height: auto;';
			hs+='border: 0px solid #000000;';
			hs+='color: #000000;';
			hs+='text-align: center;';
			hs+='white-space: pre-wrap;';
			hs+='padding: 0px 1px 0px 1px;';
			hs+='overflow: hidden;';
			this._hotspotpopuptext__text.setAttribute('style',hs);
			this._hotspotpopuptext.ggTextDiv.innerHTML="";
			this._hotspotpopuptext.appendChild(this._hotspotpopuptext__text);
			this._hotspotpopupcontainer.appendChild(this._hotspotpopuptext);
			this.__div.appendChild(this._hotspotpopupcontainer);
			this._hotspotpopupimage=document.createElement('div');
			this._hotspotpopupimage.ggId="hotspot-popup-image";
			this._hotspotpopupimage.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotpopupimage.ggVisible=true;
			this._hotspotpopupimage.className='ggskin ggskin_button';
			this._hotspotpopupimage.ggType='button';
			hs ='position:absolute;';
			hs+='left: -24px;';
			hs+='top:  -24px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this._hotspotpopupimage.setAttribute('style',hs);
			this._hotspotpopupimage__img=document.createElement('img');
			this._hotspotpopupimage__img.className='ggskin ggskin_button';
			this._hotspotpopupimage__img.setAttribute('src',basePath + 'images/hotspotpopupimage.png');
			this._hotspotpopupimage__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
			this._hotspotpopupimage__img.className='ggskin ggskin_button';
			this._hotspotpopupimage__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._hotspotpopupimage__img);
			this._hotspotpopupimage.appendChild(this._hotspotpopupimage__img);
			this.__div.appendChild(this._hotspotpopupimage);
			this._hotspotpopupimageon=document.createElement('div');
			this._hotspotpopupimageon.ggId="hotspot-popup-image-on";
			this._hotspotpopupimageon.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspotpopupimageon.ggVisible=false;
			this._hotspotpopupimageon.className='ggskin ggskin_button';
			this._hotspotpopupimageon.ggType='button';
			hs ='position:absolute;';
			hs+='left: -24px;';
			hs+='top:  -24px;';
			hs+='width: 50px;';
			hs+='height: 50px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: hidden;';
			this._hotspotpopupimageon.setAttribute('style',hs);
			this._hotspotpopupimageon__img=document.createElement('img');
			this._hotspotpopupimageon__img.className='ggskin ggskin_button';
			this._hotspotpopupimageon__img.setAttribute('src',basePath + 'images/hotspotpopupimageon.png');
			this._hotspotpopupimageon__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
			this._hotspotpopupimageon__img.className='ggskin ggskin_button';
			this._hotspotpopupimageon__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._hotspotpopupimageon__img);
			this._hotspotpopupimageon.appendChild(this._hotspotpopupimageon__img);
			this.__div.appendChild(this._hotspotpopupimageon);
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	this.addSkin();
};