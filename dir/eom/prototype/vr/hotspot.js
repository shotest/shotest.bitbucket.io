'use strict';

window.VR = {};

VR.app = 'pano' in window? 'pano': 'obj';
VR.isMobile = parent.Eom.IsMobile;
VR.hotspots = {};
VR.userAction = VR.isMobile
  ? ['touchend','touchstart','touchend']
  : ['click','mouseenter','mouseout'];

// hotspotモーダル起動
function Hotspot(_TITLE,_ARR){

  const $container = $('#container');
  const $overlay =   $('#hotspot-overlay');
  const $popup =     $('#hotspot-popup');
  const $back =      $('#hotspot-back');
  const $title =     $('#title');
  const $links =     $('#links');
  $links.empty();
  
  const title = _TITLE || '***';
  const arr = Array.isArray(_ARR)?_ARR:JSON.parse(_ARR||[]);
  
  $title.html(title);
  
  const $ul = $('<ul id="links-list"></ul>')
  
  for(let i=0,len=arr.length;i<len;i++){
    
    const $li = $('<li>').append(
      $('<a></a>',{
        html: arr[i][0],
        href: '../../html/vh'+arr[i][1]+'.html',
        target: '_parent'
      })
    );
    
    $ul.append($li);
  }
  
  $links.append($ul);
  
  $overlay.fadeIn('fast');
  $container.toggleClass('popup');
  
  $back.on('click',function(){
    $overlay.fadeOut('fast');
  });
  
  HideHotspot();
  
}

// hotspot表示切替ボタン
function ToggleHotspot(){
  const $hotspots = $('.ggskin_hotspot');
  $hotspots.each(function(I,E){
    if($(E).is(':visible')){
      $(E).hide();
    }else{
      $(E).show();
    }
  })
}

// hotspotの他のポップアップを隠す
function HideHotspot(_this){
  const D = document;
  const __ = function(S,A,B){return A? (B||D).querySelectorAll(S):(B||D).querySelector(S);};
  
  TextFix(_this);
}
function TextFix(_this){
  const D = document;
  const __ = function(S,A,B){return A? (B||D).querySelectorAll(S):(B||D).querySelector(S);};
  
  const container = __('.ggskin_container',null,_this);

  window.textDiv = [];
  
  const hotspots = __('.ggskin_hotspot .ggskin_container','all');
  hotspots.forEach(function(E){
    
    if(!_this || !+container.style.opacity){
      E.style.visibility = 'hidden';
      E.style.opacity = 0;
      E.ggOpacitiyActive = false;
    }
      
    const rect = __('.ggskin_rectangle',null,E);
    const text = __('.ggskin_text',null,E);
    const textDiv = __('div',null,text);
    const textHeight = textDiv.offsetHeight;
    
    rect.style.height = (textHeight + 10) + 'px';
    
    textDiv.style.position = 'relative';
    textDiv.style.display = 'table-cell';
    textDiv.style.verticalAlign = 'middle';
    textDiv.style.height = textHeight + 'px';
  })
}

// hotspot押下時のアクション設定
function ActiveHotspot(_this){
  
  const $btns = $('.ggskin_hotspot');
  
  let action = {};
  action[VR.userAction[0]] = function(){
    if($(this).hasClass('on')){
      $btns.removeClass('on');
    }else{
      $btns.removeClass('on');
      $(this).toggleClass('on');
    }
  };
  action[VR.userAction[1]] = function(){
    $(this).addClass('over');
  };
  action[VR.userAction[2]] = function(){
    $(this).removeClass('over');
    
    if(VR.isMobile){
      if($(this).hasClass('on')){
        $btns.removeClass('on');
      }else{
        $btns.removeClass('on');
        $(this).toggleClass('on');
      }
    }
  };
  
  $btns.on(action);
}

// モーダル系アクションの設定
function ModalAction(){
  const $overlay = $('#hotspot-overlay');
  const $close = $('#close');
  
  $close.on('click',function(){
    $overlay.hide();
  })
  
}
function SetLandscape(){
  if(VR.isMobile){
    setTimeout(function(){
      if(isLandscape()){
        $('#hotspot-overlay').addClass('landscape');
      }else{
        $('#hotspot-overlay').removeClass('landscape');
      }
    },300);
  }
}
function isLandscape () {
  const 
    w = window.innerWidth,
    h = window.innerHeight;
  return h < w;
}
function SetPanoControl(){
  if(!VR.app){return;}
  
  VR.hotspotIds = VR.control.getPointHotspotIds();
  VR.hotspots = [];
  VR.hotspotIds.forEach(function(V){
    const hotspot = VR.control.getHotspot(V);
    VR.hotspots.push(hotspot);
  });
  
  PanoControlHotspotFar();
  
  VR.control.on('positionchanged',function(){
    //console.log('VR.fov: '+VR.fov + ' / pano.getFov: '+pano.getFov());
    if(VR.fov > 40 && VR.control.getFov() <= 40){
      PanoControlHotspotNear();
    }else if(VR.fov < 40 && VR.control.getFov() >= 40){
      PanoControlHotspotFar();
    }
    VR.fov = VR.control.getFov();
  });

}
function PanoControlHotspotNear(){
  VR.hotspots.forEach(function(V){
    const id = V.div.ggId;
    if(id == 'hotspot-script-p2'){
      //V.div.style.display = 'none';
      $(V.div).hide();
    }
    if(id == 'hotspot-popup'){
      //V.div.style.display = 'inherit';
      $(V.div).show();
    }
  });
}
function PanoControlHotspotFar(){
  VR.hotspots.forEach(function(V){
    const id = V.div.ggId;
    if(id == 'hotspot-script-p2'){
      //V.div.style.display = 'inherit';
      $(V.div).show();
    }
    if(id == 'hotspot-popup'){
      //V.div.style.display = 'none';
      $(V.div).hide();
    }
  });
}
$(function(){
  console.log('--------------------');
  console.log('isMobile: '+VR.isMobile);
  console.log('--------------------');
  VR.control = window[VR.app];
  
  ActiveHotspot();
  ModalAction();
  
  //SetPanoControl();
  
  SetLandscape();
  $(window).on('orientationchange',SetLandscape);
  
  console.log(VR);
});

