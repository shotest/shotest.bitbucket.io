'use strict';

window.VR = {};

VR.app = 'pano2vrPlayer' in window? 'pano': 'obj';
VR.isMobile = parent.Eom.IsMobile;
VR.hotspots = {};
VR.userAction = VR.isMobile
  ? ['touchend','touchstart','touchend']
  : ['click','mouseenter','mouseout'];
VR.winWidth = $(window).width();
VR.winHeight = window.height;
VR.popup = false;
VR.hideAll = false;

// ------------------------------------------------------------
// hotspotモーダル起動
// ------------------------------------------------------------
function Hotspot(_TITLE,_ARR){

  const $container = $('#container');
  const $overlay =   $('#hotspot-overlay');
  const $popup =     $('#hotspot-overlay #hotspot-popup');
  const $back =      $('#hotspot-overlay #hotspot-back');
  const $title =     $('#hotspot-overlay #title');
  const $links =     $('#hotspot-overlay #links');
  $links.empty();
  
  const title = _TITLE || '***';
  const arr = Array.isArray(_ARR)?_ARR:JSON.parse(_ARR||[]);
  
  $title.html(title);
  
  const $ul = $('<ul id="links-list"></ul>')
  
  for(let i=0,len=arr.length;i<len;i++){
    
    const $li = $('<li>');
    const $div = $('<div class="modal-div">');
    
    $div.append(
      $('<div>',{
        class: 'modal-img'
      }).append(
      $('<img>',{
        src: '../_img/'+arr[i][1]+'.png'
      })
    )
    );
    $div.append(
      $('<div>',{
        class: 'modal-a'
      }).append(
      $('<a>',{
        html: arr[i][0],
        href: '../../html/vh'+arr[i][1]+'.html',
        target: '_parent'
      })
    )
    );
    
    
    $ul.append($li.append($div));
  }
  
  $links.append($ul);
  
  $overlay.fadeIn('fast');
  $container.toggleClass('popup');
  
  $back.on('click',function(){
    $overlay.fadeOut('fast');
  });
  
  //HideHotspot();
  
}

// ------------------------------------------------------------
// hotspot表示切替ボタン
// ------------------------------------------------------------
function ToggleHotspot(){
  const $hotspots = $('.ggskin_hotspot');
  $hotspots.each(function(I,E){
    if($(E).is(':visible')){
      $(E).hide();
    }else{
      $(E).show();
    }
  })
}

// ------------------------------------------------------------
// hotspotポップアップを表示する
// ------------------------------------------------------------
function ShowHotspot(_this){
  
  
  
}
// ------------------------------------------------------------
// hotspotの他のポップアップを隠す
// ------------------------------------------------------------
function HideHotspot(_this){
  //TextFix(_this);
  
  const width = VR.winWidth;
  const widthFlg = width/2;
  const $hs = $(_this);
  const $hsc = $hs.find('.ggskin_container');
  const $pu = $hs.find('#hotspot-popup');
  
  const hLeft = +$hs.position().left;
  const cLeft = +$hsc.position().left;
  const cWidth = $hsc.find('#hotspot-popup').width();
  
  if(width - hLeft < cWidth){
    const value = ((cWidth - (width - hLeft + 10)) * -1) + 'px';
    $pu.css('left',value);
  }else{
    $pu.css('left',0);
  }
  
  HideElse(_this);
  
  return false;
}

function HideElse(_this,scr){
  const target = _this;
  const $container = $('.ggskin_container',target)[0];
  const hotspots = $('.ggskin_hotspot .ggskin_container');
  
  if(VR.hideAll){
    VR.hideAll = false;
    
    return false;
  }else{
    hotspots.each(function(I,E){
      if(!_this || !+$container.style.opacity){
        E.style.visibility = 'hidden';
        E.style.opacity = 0;
        E.ggOpacitiyActive = false;
        
        if(scr){E.parentNode.classList.remove('on');}
      }
    })
    console.log('hide all');
    VR.hideAll = true;
  }

}

function ScreenClick(){
  if(VR.app == 'obj'){
  $('#container').on('click touchend',function(){
    HideElse(null,'screen');
    VR.hideAll = false;
    
  });
  }
}
  
//function TextFix(_this){
//  
//  const $container = $('.ggskin_container',_this)[0];
//
//  const hotspots = $('.ggskin_hotspot .ggskin_container');
//  hotspots.each(function(I,E){
//    
//    if(!_this || !+$container.style.opacity){
//      E.style.visibility = 'hidden';
//      E.style.opacity = 0;
//      E.ggOpacitiyActive = false;
//    }
//      
//    const $rect = $('.ggskin_rectangle',E);
//    const $text = $('.ggskin_text',E);
//    const $textDiv = $('div',$text)[0];
//    //const textHeight = $textDiv.offsetHeight;
//    
//    //$rect.height((textHeight + 10) + 'px');
//    
//    //$textDiv.style.position = 'relative';
//    //$textDiv.style.display = 'table-cell';
//    //$textDiv.style.verticalAlign = 'middle';
//    //$textDiv.style.height = textHeight + 'px';
//  })
//}

// ------------------------------------------------------------
// hotspot押下時のアクション設定
// ------------------------------------------------------------
function ActiveHotspot(_this){
  
  const $btns = $('.ggskin_hotspot');
  
  let action = {};
  action[VR.userAction[0]] = function(){
    if($(this).hasClass('on')){
      $btns.removeClass('on');
    }else{
      $btns.removeClass('on');
      $(this).toggleClass('on');
    }
  };
  action[VR.userAction[1]] = function(){
    $(this).addClass('over');
  };
  action[VR.userAction[2]] = function(){
    $(this).removeClass('over');
    
    if(VR.isMobile){
      if($(this).hasClass('on')){
        $btns.removeClass('on');
      }else{
        $btns.removeClass('on');
        $(this).toggleClass('on');
      }
    }
  };
  
  $btns.on(action);
  
  $btns.each(function(I,E){
    E.ontouchend = E.onclick;
  });
  
}

// ------------------------------------------------------------
// モーダル系アクションの設定
// ------------------------------------------------------------
function ModalAction(){
  const $overlay = $('#hotspot-overlay');
  const $close = $('#close');
  
  $close.on('click',function(){
    $overlay.hide();
  })
  
}
function SetLandscape(){
  if(VR.isMobile){
    setTimeout(function(){
      if(isLandscape()){
        $('#hotspot-overlay').addClass('landscape');
      }else{
        $('#hotspot-overlay').removeClass('landscape');
      }
    },300);
  }
}
function isLandscape () {
  const 
    w = window.innerWidth,
    h = window.innerHeight;
  return h < w;
}
function SetPanoControl(){
  if(!VR.app){return;}
  
  VR.hotspotIds = VR.control.getPointHotspotIds();
  VR.hotspots = [];
  VR.hotspotIds.forEach(function(V){
    const hotspot = VR.control.getHotspot(V);
    VR.hotspots.push(hotspot);
  });
  
  PanoControlHotspotFar();
  
  VR.control.on('positionchanged',function(){
    if(VR.fov > 40 && VR.control.getFov() <= 40){
      PanoControlHotspotNear();
    }else if(VR.fov < 40 && VR.control.getFov() >= 40){
      PanoControlHotspotFar();
    }
    VR.fov = VR.control.getFov();
  });

}
function PanoControlHotspotNear(){
  VR.hotspots.forEach(function(V){
    const id = V.div.ggId;
    if(id == 'hotspot-script-p2'){
      $(V.div).hide();
    }
    if(id == 'hotspot-popup'){
      $(V.div).show();
    }
  });
}
function PanoControlHotspotFar(){
  VR.hotspots.forEach(function(V){
    const id = V.div.ggId;
    if(id == 'hotspot-script-p2'){
      $(V.div).show();
    }
    if(id == 'hotspot-popup'){
      $(V.div).hide();
    }
  });
}

// ------------------------------------------------------------
// hotspot-svg画像設定
// ------------------------------------------------------------
function HotspotSvg(){
  var svg = '<svg class="hotspotpopupsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50" viewBox="0 0 50 50"><g style="isolation: isolate"><g id="fill"><g><g><path d="M25,38A13,13,0,1,1,38,25,13.0149,13.0149,0,0,1,25,38Zm0-23A10,10,0,1,0,35,25,10.01145,10.01145,0,0,0,25,15Z"/><g><path d="M37.94934,23.5c.02539.49677.05066.99371.05066,1.5s-.02527,1.00323-.05066,1.5H43v-3Z"/><path d="M25,38c-.50629,0-1.00323-.02527-1.5-.05066V43h3V37.94934C26.00323,37.97473,25.50629,38,25,38Z"/><path d="M25,12c.50629,0,1.00323.02527,1.5.05066V7h-3v5.05066C23.99677,12.02527,24.49371,12,25,12Z"/><path d="M12,25c0-.50629.02527-1.00323.05066-1.5H7v3h5.05066C12.02527,26.00323,12,25.50629,12,25Z"/></g></g><circle cx="25" cy="25" r="10" style="opacity: 0.3"/></g></g></g></svg>';
  var svgs = $('img.ggskin_svg[src*="hotspotpopupsvg"]');
  svgs.each(function(I,V){
    var p = V.parentElement;
    
    $(V).remove();
    $(p).append(svg);
    
  });
}

// ------------------------------------------------------------
// SetImageUrl
// ------------------------------------------------------------
function SetImageUrl(){
  const $hotspots = $('.ggskin_hotspot');
  $hotspots.each(function(I,E){
    if(E.ggId == 'hotspot-popup'){
      const $hs = $(E);
      const $hsc = $hs.find('.ggskin_container');
      const $hst = $hsc.find('.ggskin_text div');
      const hst = [$hst.get(0).textContent,$hst.get(1).textContent];
      
      $hsc.html($('<div id="hotspot-popup"><div id="links"><ul id="links-list"><li><div class="modal-div"><div class="modal-img"><img src="../_img/ch02se010402.png"></div><div class="modal-a"><a href="'+hst[1]+'" target="_parent">'+hst[0]+'</a></div></div></li></ul></div></div>'))    
    }
  })
}


// ------------------------------------------------------------
// $(function(){})();
// ------------------------------------------------------------
$(function(){
  console.log('--------------------');
  console.log('isMobile: '+VR.isMobile);
  console.log('--------------------');
  VR.control = window[VR.app];
  
  ActiveHotspot();
  ModalAction();
  HotspotSvg();
  SetImageUrl();
  ScreenClick();
  
  //SetPanoControl();
  
  SetLandscape();
  $(window).on('orientationchange',SetLandscape);
  
  $('body').addClass(VR.isMobile? 'sp': 'pc');
  
  console.log(VR);
});

