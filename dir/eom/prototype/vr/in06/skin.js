// Garden Gnome Software - Skin
// Pano2VR 6.0.6/17336
// Filename: tyt_eom2.ggsk
// Generated 2019-09-30T19:45:26

function pano2vrSkin(player,base) {
	var me=this;
	var skin=this;
	var flag=false;
	var hotspotTemplates={};
	var skinKeyPressed = 0;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	var hs,el,els,elo,ela,elHorScrollFg,elHorScrollBg,elVertScrollFg,elVertScrollBg,elCornerBg;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	this.callNodeChange=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggNodeChange) {
				e.ggNodeChange();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	player.addListener('configloaded', function() { me.callNodeChange(me.divSkin); });
	player.addListener('changenode', function() { me.ggUserdata=player.userdata; me.callNodeChange(me.divSkin); });
	
	var parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		el=me._toolbar=document.createElement('div');
		el.ggId="toolbar";
		el.ggDx=-0.5;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.00392157);';
		hs+='border : 0px solid #000000;';
		hs+='bottom : -95px;';
		hs+='cursor : default;';
		hs+='height : 36px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 323px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 100%';
		me._toolbar.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._toolbar.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._left=document.createElement('div');
		els=me._left__img=document.createElement('img');
		els.className='ggskin ggskin_left';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAFEElEQVRYhe2YzWsUdxjHv89vdjeTTl5MNF3HFyqNJSkyTQ6pl8ZCUFtao9DDeEjxJF4kN/MHrOcmAfHkwUOoteIWpEhfFhvW0mBBmIUQEmPapkl3ZE1tSHY3O7szszNPD01CUjXRnWih+IU5zOFhPjzfZ57f8/yAV/qfiIIEx2IxjI+PC8dxBABEIhH/wIEDfiwWCwwWChJsmqbU1NT0WrlcbgCAUCi0YJqmBcALCiZVGqjrulAUpc7zvFYieh/AW57nlSVJWjp+/Lh9+/btlwum6zoOHjwo1dTU1AHoAPBJe3u7XigU3nFdN8LMjwqFwl+1tbX+zMxMxWDieQMURZHC4XAdgLcBHDty5MjHZ8+ebTt9+nQLgDeJqImZpWQyWTEU8Jw1puu6iEQitQ'+
			'BaAXx4+PDh7pMnT+4vFot88+bNNBH9zsyPiMjr6up68WC6rkNRFGkZqgPAsRUoALh8+fLk9PT0V0SUIKIpVVWdS5cu8QsHW2NfK/6x7yNd1/cDQDwevz82NpYAkPB9f9J13XwsFgsE9UxgT7JvBer69eu/Dg8PfwfgGwD3XNfNFwqFwK0C2KDBbmbfMtQXABIA7i0sLOTi8bi/FVDABhnbzL7lTCWYedJ13fxWQj0V7L+yb63WWbmZfdeuXZtNJpNfElFCCDERiURynudVXOjMzNXV1Wyaptfa2rrujF2XsZ07d0qO49Qx82P2XblyJTsyMvILAJOZJd/33ygWi0Ez5du2XWhqanqYy+UKAFbLYS0Yua4rM3MzER3t6upatW9oaIjv3LkDADsAdAJoZ2YPQNC24AghZhzH+ba6unommUzaK4151UpmpjNnzkSFEB90'+
			'dnb2njp16t2AH91UiURi4caNGyMAPgNgqKpaXOmBqxk7f/48iMgFsDg2NvZbJpPRVFWVAWB4eBiWZYGIfCGEC8AnosBNdGpqqszMEhER/lXv6156e3urXNfdA+CQoiif9vX1daqqKqfTaQwODvqWZRUA/AFgkZndLYArAZgkos8VRbnX09NT6ujoeBxM13Xatm1bGMDrRHS0rq7uRF9f39FoNKqYpsmDg4MZy7ISzPwzEZnMXAoI5jFzLhwOz4TD4fyFCxdWf6Z189jExASam5t9WZZLRJSzbbtkGEZI07R9u3fvDmuaJqdSqXnbtqcBjIdCoftElCaiB5U8ADIAHlVVVRUbGhr8tcPlE48kXdepvr5eFkLsAdBZW1vbc+7cuU5VVeW5ublCf3//D7lc7msAtyRJ+hOAG41GK0pZd3c3r9i3VhudlU+1NZPJlAYGBk'+
			'by+fxVACO+75vZbLYUj8cD/xAreupovZGtqqrKmqbtSqVSkm3bJSJalGU539zc7E9MTLxYsLVwVVVVFhEt2rZdNAyjXtO0Xaqqym1tbXsNwwjZtl0EMCfLcmmr4DZdRpbhPFmW8wDmHMexUqmU1NbWtjcajSqapu0zDKPecRyLiHKyLFsnTpwov5QtaTNbGxsbdxiGIQkhSgDmHcdZunv3bqAx6JnXt41sbWlpURobG3eMjo7aAB54nvegv7/fGxoaqhjsuda3eDzO2Wy2xMyzzHwrn89fHRgY+H52djabTqftiimeoIruLlZaCRG9wczvEdEhAGDmHwH8JMty+uLFi+UgYBVdEaypOUsIMQ/ABDAmhBhl5ofRaNQJWvyBb3vm5+dDS0tLYQCoqalxt2/fXt6K256t0mNjyyu90rL+Bo7cpsqmHxukAAAAAElFTkSu'+
			'QmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="left";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._left.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._left.onclick=function (e) {
			player.changePanLog(1,true);
		}
		me._left.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._left);
		el=me._right=document.createElement('div');
		els=me._right__img=document.createElement('img');
		els.className='ggskin ggskin_right';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAE/0lEQVRYhe2Y32tTZxjHv897kpxkJ7a2a1zT2MaZutR1obAUiq4watxPFSyY9GLovNIL7/IXHP8BofVOvfLCQTMYDJyTMaxQL6RNGF5U6TAdksKiLLVNk5zk9LzPLmwl2s7VnIyB+IUD5+Z9zofn877vec8B3uYNCTWjiK7rSKfTiqIoZFkWR6NRS9d1WzUVu1DxeJyEEC6v1/uux+Pxtba2Og3DMIPBoJybm/t/wHRdhxDCZZqmT0r5CRENSSl3MnNBVVUjFAo1DGcLrKenRwHQwczDRPRtJBL5Mp/P7wFgENGKqqrlUChkNQJnC+z48eNKsVjsIKKhoaGhI2fPnu31+/27MpmMA0CViJ663e5iI52zBRYMBsnhcLiIaGexWAxGIpHd4XBY6+rq6k'+
			'yn004ABoC82+1+ba22wEZHR7lWq9WklIVqtWpkMhllYGCge9++fS1+vz+QyWTaiKjciFZbYFNTUwgGg1JVVYOIVqrVqpFOpx2RSGRPOBzW/H6/r1GttreLubk5hEIhqapqmYieVqvVSjqdbo1EIl12tNoGq4Oz3G53EUC+VquV7WptClgdnHS73S9rfb8RrU0Dq4fb0GoYhpHJZNrXtb7zOlpfeFeeOXOGXC6XICKFmRt+jxIR1tbWXFJKPzOPeb3eE8lksj8QCCizs7Mrly9fvgvgOwDTUsrc8vKykUqluL7GCx0bGRlRy+Vyh2mau5nZb1nWrkYvZu4A0Aags1ar9czMzHT09/er+/fvV+u1CiGWPB7P6rFjx+TU1NRzFsfGza1btyiVSrUAiAohhqSUHUS2Dx8KEfmY2Vcul5ULFy4gmUxicHCwhYhGLl26xABM'+
			'KeXKo0eP/gJgbQK7ffs2ra2teYnoo0gkcuLcuXN9dqlelWg02nbq1KlPr169+oSIftM0bZmZrY1mOLYYI/L5vLxx40YJTTivMbNgZkVK6SAiGhgYQHd3NwzDsO7cuZMD8JiZK6Zp8vnz55+Pe/7g2dlZunLlil9K+RmAz5jZT/ZdCgBeAJ0AOg4cOKCePn0alUqFJyYmHmSz2Z+J6DoRzTidztWLFy/KTWAAKJlMuldXVzuZ+T0hhNvmylQA7GDmKICR4eHhgZMnT2qGYVjj4+Pz2Wx2EsBNAPeXlpZWUqmUrB9fr5IdDkdVCLEopfyTiESjDRNCKJZltTBzHwDfoUOHAmNjY5phGNbExMRcNpv9FcBNZn5gmmbxZShgizmk6zra29tRKBQaolpYWCCPx9MipfwQwBexWOybRCIRqtN3E8B1ALO1Wq1YKpWsVCq1qU'+
			'5TPkYAIB6PQ9M0xeVy7QAwCOBILBY7mkgkererrz5brcqGomma4nQ6WwD0AThy+PDhr+LxeO/r6Gs6WDweF+ud6sMzfUfj8Xjvur75dajrAO6bplkslUrWqyvaBNN1HblcTlEUZQczb9K3DrVtffWxdbrw+XzC6/V6AYQBjMZisa8TicQHdfp+AfDjhr5r165tCwp4tgE2nL179wrLstqY+eODBw9+nkgkwpVKhcfHx+cfPnxYr29lO/rq07TJf+/evfLCwkJpcnLyj2w2+z0a0FcfWyq7urqgaZqUUpqmabqmp6efFAqFu0KInxrRVx9bHevv75e5XK6kKMo8M/8AYBcRPQbw+3ZX3z+lWX97aHFx0WWapuJ0Oq1AIFDTdZ3/feR/DLZFLVtQb/NG5W9DUQ1sS3v6XgAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="right";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 35px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._right.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._right.onclick=function (e) {
			player.changePanLog(-1,true);
		}
		me._right.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._right);
		el=me._up=document.createElement('div');
		els=me._up__img=document.createElement('img');
		els.className='ggskin ggskin_up';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAFIklEQVRYhe2Y32tTZxjHv8+bXydEu3ahs0m3MhKtUZGKhmywCet24ZiNboPzJyheNRfeS26Ftn+BF5ViwRy2MZ1zYkOMlco2E0HQ/opdR+tqO2zaJE2bc3LOswtTMdpfSbrtpt/Lw/s+fM7zed+XlxfYSWWhf6EOb3fBiiPLMpqamkyGYUiFQmG3EEKVJGlpZmZGVRSlJkBTLZMDgYCJiOqKxWKrEOJjAI3FYrEgSVLO6/UaT548+e/BZFkWu3btqgNwAMCXbrf722w220JEOoBZSZJWaoGrGEyWZQQCAVMJyg/gG6/X+/X58+ePtbS0fJhIJN4lojwRZWw2W97r9erVwFUMFggETBaLpY6IDpSgvgiFQj673S7cbrfN5XI1JpNJM4ACES1IkpStpn'+
			'MVga3qK0Gd8Hg8p0OhkE+SpFd13G63ze12NyUSCQuAFVSpdUtga+nzeDynOzs7fXa7XfT29uLIkSOIRqOwWCzw+Xx2l8vVnEwmG6rVKrYyyOFwmCwWy+pCP1nS12q32+ny5cv6/fv3dQBYXl5GT08Ppqam4Pf7686cOfMRgA4Ax4UQ79fX11tkWd7SEbUpmCzLwmq17iYiH17qO9HZ2XlQkiRTX19ffmhoaA7AQmk45/P5Yk9Pz/KzZ88Mv99fd/bs2XYAJwF8CuC9rcKtq3Izff39/XODg4MPADwEoAaDwQ/Hx8f10dHROU3THiYSibm2tjZna2trXTVa1+3YRvoikUgqHo/fBKAQUQzANAAQURHAXwBu5nK577q6ugZmZmZWqtG6Zsc22n2RSOTpwMDADSK6JoT4DYAG4GAwGDyWSqXU4eHhSQDXATxSVXUhkUi8'+
			'c/jwYff+/fsdlezWMrBwOIy9e/eaHA7HmvoikUgqGo1eIaIfdV1/CCDNzA1EdDQYDPpTqZQ2Ojo6TURRIcQIM0+pqppPJpOmtra2D/bt2/eWVkmS8qdOnSreuXNnfZWPHz8WZrPZAaAVwFcej+eVPkVRRqPR6E8AbjHziK7rWZvNxkRr2mAAmhDibwD3MpnM9a6urtuva2Xmk0T0CRE1vXjxwvxmgTIwj8cjdF1vYOajXq/381AodLCkLzUwMHATwA0Aw5qmZZaWlvS1iFZDRCyEUA3DmAYwmMlkrnV3d99bhTt37txnzHwcgLdQKFhisVjZH667+GdnZ9V0Oq1dvXp1PBqNXgEQAfAgnU5nent7dUVRNuICAOzZsweLi4srzPwnM9/OZrP93d3dv8zOzubGxsbyG80ta+HExIThdDrTuq4nc7lcSzgcnmDmP4QQtw'+
			'zDGNE0LasoirEp0WtRFIVlWdbq6+vn8FIrX7hwYR4AmDkO4KkkSVp7e3vZ/a2sY4cOHTKKxeKSEGIMwA8A+ojoe2xR30ZwCwsLGjNPCyHiAPoB9AshBgE8dzqdxTfnlIGFw2FcunRJd7lci8z8SFXVGDM/crlci1vVtxFcc3PzstVqndI0bUjTtCGr1TrV3Ny8HA6H3xr/1m4oATIA9bVP23KPLwEUAax2ft26a4JtJ0y1tbd0u/g/sgNWaXbAKs0OWKWpCYyZmYgMAOrExMTy/Pz8CoAVItKZazsGawJzOp0MYAnA5MWLF6N37979FcAIgKzD4dA7Ojqqptvo5N80Y2NjemNj4/NCofAzM/+Ol69HWbPZPKnruub3+6uuXROYz+czMpnMkqqqk0Q0BYCYWbdYLFpDQ0NF16M3sy0Pd7FYDPF4nACgo6ODa+nUTqrN'+
			'PwOJjBpWGxFvAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="up";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 70px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._up.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._up.onclick=function (e) {
			player.changeTiltLog(1,true);
		}
		me._up.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._up);
		el=me._down=document.createElement('div');
		els=me._down__img=document.createElement('img');
		els.className='ggskin ggskin_down';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAFM0lEQVRYhe2YzU8bRxiHf++O7TU4rI0Tx1loa0E+jGtZVRpaRAUiiaGlKW5zWZM2BxCqciknkj/A55ySXLlzwJyi9pRUEFWtVMn2gSiUL5tKoYoqKKbQtbGXnemhTWTApLHdKlLF77ar1bvPvs/O7MwCR6ks9G8UmZ6epkePHhEADAwMiPb2dgAQrw3sxo0bpCiKLZvNKkTkICIyTbPgcrmyFoulcPv2bV5tbakWMJfLJem67maMdUiSdI2IPrdYLH26rp/inFtisVjVtWsCKxaLzDRNNxG9HwqFhrxe73Ui6gPg1XWdud3u1wO2u7tLRCRzzj2jo6NtnZ2dPs65CsAuhJA2NjaqflVqAgMAIQSIXtyfiIiEEDUPqprB/qscgVWaI7BKcwRWaSzlTs'+
			'ZiMSSTScYYI9M0xYULF8xaPi+ldZ88eSIVi0UJAGw2Gw8Gg7xc7QNgmqbR+vq6tbm5udE0TQdjTF9fX89qmmbE4/GaVgyrq6vM4/HU7+7uNgKAxWLJrq6u5gCY+6/dozIWi8Hr9VoNw/CYptlNRFdN0+w2DMPjcrmsmqZVPaNrmiYxxhoMwwgAuALgimEYAcZYQywWO1B3D1gmk2GFQsHNOf+AiIY6Ojq+UhTlSyHEh0Tkczqd9krhNE3D8PAwa2xsVIQQ7QCudXZ2jh47dmyEiK5yzs89e/bMdvHixT1194C1tLQQgHoiejMUCvlHRkZab9682aMoyqcAuiVJeqPSzjkcDma1WhUAAQCf9Pb2XhkeHg6Ojo6+DaCViDxCCDY9PX14x1ZWVgSAHICnjx8/TicSiS1VVe23bt3qUxQlAqALwMlXhVtZWSGbzdZARG0A'+
			'PgqHwwPRaNSfz+fF5OTkz0S0AmCNiMxLly4dDtba2mrKsrxBRD8AmBwfH3+YTCazXq/XMTY21t/Q0PAFEfU918r54QtUzjmz2+0KgHYA0XA4fD0ajZ7Z2dkx7927N5/JZKYA3CeiRVVVizMzM3sGFis9mJmZgc/n47Is7xDRFoBCMpm0qqra7Pf7HaFQqCmVSrFCobBDRJuc87wQ4gQRvRuJRNqXl5eN+fn5XwB8T0TbQogzAK729vZ+HI1Gz/0NNZfJZB4AuC+EmC8Wi9t37tw58IQHJth4PC42NzcNzvkqgO8AfD0+Pv7jfq1CiG4iUomoft8DSkTUIIR4oU/TNH8+nxd3795dTKfT3wL4BsBPhmFs6bp+YKo40LHnmZubw+nTp0273b4N4FciyqVSKUtTU9Ops2fPKqFQqCWVSrkLhYIDgFcI0RaJRNqWlpbMhY'+
			'WF3wHkiOi9cDj8WYm+hUwmMwngPoDZbDa7NTExwefm5sohlAcrgeN2u/1QrYlE4mSxWPQR0VuRSMSzuLgoFhYWCEDj5cuXzw8ODraU02cYxvbExMRLd1Av/Vb+g1bb2NhYsL6+/jwAHwBwzi0ATnR1db0zODjoq1TfK3VsX+fKavX7/fXBYFBOJBLW/v5+LC0t0fHjxy1DQ0O2avRVBFYCV1ZrIBCQg8EgnE4n6urq0NPTg2r1VQxWCifLco6INgHspFKpRlVVPYFAQAYAp9OJfD4v/u7UA/ylb9YwjG1d181X6VTFYCVwZbU2NTXV1aqvarASuD1aE4mExe12n5yamlpOp9MPiagqfTWBlcLJspyTJCkrhNBnZ2e3NzY2UkT0EFXqK01NO2ZN08jtdlslSXKZpuklohxjbG1tbe2PeDxe9Z8eoMY1fzweF6qqGoVC'+
			'4Tdd1xdzudxTxljNUEf5X+VPmdXdk44JJ0kAAAAASUVORK5CYII=';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="down";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 105px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._down.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._down.onclick=function (e) {
			player.changeTiltLog(-1,true);
		}
		me._down.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._down);
		el=me._zoom_in=document.createElement('div');
		els=me._zoom_in__img=document.createElement('img');
		els.className='ggskin ggskin_zoom_in';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAKfklEQVRYhe2Y/U+b1xXHv+fexy+AMdiJmxjyCjRxKURxIKFrutQZVfEklCzSqJpMkbpOqlQp+aH9C/wPRKqqVmp+qNRGapSOsqaFJRNFGX2bWhKSlpgEBvEoXQh2jQ12jF/vPfshhkEpJJnW33okS36uzr3385zn3HPuOcAv8nBC/8ukQCBATqcTsVhszfnt7e3c3NwMAPyzgfl8PrJYLLR161aLEKKsUCiUCSGM1fSZmYkoJ4SYSyaTmUQiUeju7n5gwAcCO3nypMhms2ZmthHRFgCPAtgCoHSVNRiAAjALIAjgOwAzhUIhtWXLFh0IBO4LeF+wjo4O4XK5bFrraq31TgD7ATxeU1OzvampqcLj8Zh+PCcajeqRkZHM5cuXf7h79+63AL4hoiEhxI'+
			'jWOh6LxfKdnZ1rwq0K1tHRgbKyMmk2m8sBNAFoZeYWn8/X6Pf71zmdTnG/lwKAgYGBdE9PTygcDt8E0E1E/1BK3Z6bm8usBbcq2AsvvCBNJpOdiDwAOmpra1uPHz++0+12WwBgeHh4PhgMhoeHh1U4HK4EUAkgB2CmsbEx4/V6K+rr69c5HA4JAL29vTNdXV39AC4A+JyZv5udnV3Vcj8J1tHRIRwOhx3AYwDaWltb/3Do0KHtVqtVjo+P586cORMKh8MjAK4DsAD4FYDdAOIAvgRwE4AbQL3f72/0+/2OkpISmpiYSLz55puXEonEx8z8CYDIanDyxwMnT54UJSUlNq11M4Dftba2/v65556rMwxDnD9//od33333y1Qq9TERfSiE6GfmKBFtPn36dIvdbg8Hg8GPAfxFCPENM/9rfHxcXbt2zeTxeCo2b95c1tjY'+
			'uG1wcLAin8+niGi2srIy9fXXX6sfcyzzE5/PR9ls1qy1rgbQWltb23ro0KHtAPD++++HLl68eBHAn4UQF7TWVwuFwjSABDPniktoZs4w80w+nx8H8DkRfRAOh7tOnTr12dTUVM7tdluff/75fczsB+DN5/MVPp9vhb8uG7BYLMTMNq31TmZuOX78+E6r1SrPnz//w6VLl74A8CGATwzDGJNSJkwmk8K90MAAQEQAwESkrVZrtrS0NEpEXwH4KJlM9pw6depGLBbTzc3N9paWln3M3KyUqq6url7x5ZaBbd261UJEWwHs9/l8jW632zI+Pp67ePHiFQB/B3DZarVOv/HGG+m33npLS7liPSwAulwufu211wqhUCgupRwF8Pndu3e/fO+99yYA4MiRI5sA7GNmr91utx87dmwZy+JDIBAgIUQZgDoAj/v9/nUAcObMmR'+
			'CAz4QQV81mc+z1119f4Q9rSW9vLyulsoZhfEdEnwaDwauDg4NzDofDdPDgwToi2qWUWu9yuYxAILASzOl0olAolAHYUlNTs93pdIrh4eH5cDg8QkQDSqnJQqGQfRioBamqqlKZTCbBzNcBXOnr67sNAA0NDesA1DDzuvn5ecPpdC7OWcx1sViMhBAGEZU2NTVVAEAwGAwDuE5EE8ycIyLTiRMnlu5pJiITM0sAYGYCYBCROR6PW06cOMEAEI1GQUQgoiiAYCgUuhmLxTwNDQ2lRFQGwCKEoOKlgJeBLQgz00KaCQaDBQAWZt4ipXQopVip/35JIjJprWuJyAkAWmsLgCqttSeTyaSYeTE+Ff3RzMxOAGJsbKzQ0tJi9nq91qtXrxpSSuF2u1da7KckEok4ADzBzC7ci+rLAiEzyyLUYwCQy+UqcS+XbiyGkBX6AJwA'+
			'dkajUQEASqkSInIUCgVLX19falWLAYDdbi9Np9MAUOnxePa+8sorB9Z6gQVpa2urbGtrOwBgVf0LFy5kP/roI73w+ZVSDmbeBqCivr5+FoBeFSybzebtdjsA5GKxWKy/vz9NRKoYp5ZagJRSlnw+X9nW1lZ569atXCgUiplMpuRq+rdu3ZIAKnAvlRERZQEkAeRmZmYWLbwqWPFvNBKJfHHu3LkrzJz+CVUJoArAU21tbQdCoVDsgw8+uATgCoAV+kRkMHMVgP1CiCcAmKWUc0T0vWEYyfLycr0WGEejUbVp0yY0NjZmh4aGRgD0MPMMEemlViAis1LKQ0RuAAfMZnOiCNUlhEgudX5mhta6hIh2M/P2HTt2CACIRCJprfW8UipvsVgW114GprVmIYQaGRnJ7t69G16vt+L69etuIYQ1n8+nrVZr1uVyLW4Wj8ctxd'+
			'O3mCsBpIUQyY0bN85hifOPjo4Ku91eAmB9eXn5hrq6OiMej6upqakcEamlpx1YEmDb29tZCJEDMHv58uUfAKC+vn4dgHqttUdKWWEymUQgEOCFn1KKi1ZZmiuxMLagBwAOh8PEzJuZeVdzc/OjABAMBpMAYsycKisrU+3t7bwCrLm5GUKIOQDBVCr17cDAQNrhcEi/398I4LdEtCeTydifffbZh6qsmpqa6Pbt2yatdRURPWOz2Z5ua2vbCAB9fX0TAK5IKadisVi+WFEtBwPAyWQyw8yTzPxNT09PCAD8fr9jw4YNe5l5n9Z6a21tbclLL70kl+a11SQQCNCePXtMQginUqqemZ88fPjwTofDYQwODs5OT0/fYOag1npucnJSL527LKMnEokCEUWJaCgcDt/s7e2dKSkpoZdffrmuvLz8SQC/VkrtNAyjYnJy0ijG'+
			'uhXCzBgdHRWRSMQipdyktW4mIp/P5/MeOHDAnk6n+ezZs2MArkkpbyUSiUx/f/+yYLwMrLu7mwuFQkoIMQKgu6urq39iYiLhdrutr7766tM2m+2PAP6klGo1DKPOMIxy3ItHy3IlM5fY7fZHCoXCLmZ+nohe9vl8R48ePVoFAG+//fZIKpXqIaJPpZRT586dW2Yt4Ceu1ocPH0YymcwRUQJAbmhoyNzY2LitqqrKsnfv3kfC4bArEok4AdiJyAygGoBXSlk7OjqajEQi3xNRjogaAPzGZrP5Ozo69rS3t68HgM7OztGvvvrqb7hXMf3TMIzUwMDA/cH6+/tRU1OjzWZzmohms9lsenBwsMLpdK6vra21trS0OKqqqqrj8XhNPB7fBOBxAJ7R0dHKSCQimLlUCFFjs9me2r9//9Mvvvjiox6PpySdTvPp06dHilB/BT'+
			'CUy+WS8Xhc3bhxY4U7rFVXUmVlpQnAI0KIZ5jZ39LSsu/IkSObHA6HCQBisZgeGxsrRKNRUcx9LIQo7NixQ9TV1S3GyMHBwdmzZ8+OpVKpnmJ1dDMejyc6OztXWOq+YEvhhBCbAXiZuRlAy8GDB2sbGhrWNTQ0lK42Nx6Pq2AwmOzr65uYnp6+AeAaEX0KYDybzSbeeeedNW/CD9IiILfbbc7n8xVKqWoAXgC7ANQAsHm9XovWulQpVUlEWSnlXDgczty5cycHYAbAFWYOSilvSSmnhBCZ6elp1dnZuea+D9PtEdXV1dJut9uVUuuZeZ0QwszMBhE5mXlb8cD8m5lTuFfKpaSUU1rruUQikSmevgfq+Dx0f+zYsWPC5XIZ8/PzhpSStNYC967GFcyc01onpZR5pRTKyspULBbLT05O6h/Hqf87GAAEAgEsNO6mpqZw'+
			'584dklKK4iVA7969G8Cyxt0v8rPLfwD09il8PlvDyQAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="zoom in";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 140px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._zoom_in.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._zoom_in.onclick=function (e) {
			player.changeFovLog(-1,true);
		}
		me._zoom_in.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._zoom_in);
		el=me._zoom_out=document.createElement('div');
		els=me._zoom_out__img=document.createElement('img');
		els.className='ggskin ggskin_zoom_out';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAJsUlEQVRYhe2Y3VPTVxrHv885PxJCSCCB1AKKCIgsBRckSsfu7MallewOo+3M0rHu2On0olf1D+j0Jv+AF33xop3pTPWiY4fp1FbW3bVZx77tKhJpISiuiJRdkWBICCHk9ZxnL4CsqFCx07t+Z343mfOc+eQ55zzn+xzgF21M9DhBPp+PnE4nIpHIuvHd3d3sdrsBgH82MI/HQ2azmbZu3WoWQlhzuZxVCGGsNZ6ZmYgyQohYPB5Pzc/P586cOfPIgI8EdvToUZFOp03MXExE1QC2A6gGULTGHAxAAZgDEATwA4DZXC6XqK6u1j6f70cBfxSsp6dHuFyuYq11ldZ6B4BnADxVW1u7rb29vaSxsbHg/phwOKxHR0dTly9fvruwsPA9gO+IaEgIMaq1jk'+
			'YikWxvb++6cGuC9fT0wGq1SpPJZAPQDqCTmTs8Hk+L1+stczqd4sf+FAD09/cn+/r6xkOh0DUAZ4jon0qp27FYLLUe3Jpgr7zyiiwoKLATUSOAnrq6us4jR47sqKioMAPAyMjI4sjIyHQwGFwIhUK5e2N37txZ1Nra6mxqaipzOBwSAM6dOzf7ySefXABwFsDXzPzD3Nzcmpl7KFhPT49wOBx2AL8C0NXZ2fnnAwcObCssLJRjY2OZkydPjodCoVEAgwBmsbSf/j8pkZWZawE85fV6W7xer8NisdDExMT88ePHz8/Pz3/OzF8AmFkLTt7/w9GjR4XFYinWWrsBPN/Z2fmnF198sd4wDHH69Om7J06c+DaRSHxORJ8KIfzMPEhEw0QUXPmYeYSIrgG4NTY2pgYHBwsaGxtLtmzZYm1paakJBAIl2Ww2QURzpaWliUuX'+
			'Lql1wTweD5WWlpoBVDPz83V1dV0vv/xyg2EY4uOPPx7/4osvzgP4ixDiK631aDQajVgslsXKysq0zWbLf5OTk0khRExKOUNE0YWFhflAIKCbm5u3VFZWmp1OZ3kgEAARRZk5VFxcnJyYmFiVtVV1yGw2EzMXa613MHPHkSNHdhQWFsrTp0/fPX/+/DcAPgVw2TCMCIB0b2+vfthWAMAejyfd2toaTqVSF5n5Tjwejxw7dsz15ptv7nS73fahoaE9ly5dmlBK3ayqqooCWDXXqox1dnYWEtF2AH/0eDz79+7dax8bG8ucOHHiWwB/AfB1YWHh9DvvvJMZGBhY97hPTEzg4sWL2ul0psrKymLMnMhkMuWhUMjV0dHhqKmpKfb7/YqZ71oslomGhob08PBwfs78kff5fCSEsAKox9KmLQOAkydPjgP4SghxxWQyRd5+++'+
			'0H9sN6OnfuHCul0oZh/EBEXwaDwSuBQCDmcDgK9u3bV09EO5VS5S6Xy/D5fPm4PJjT6UQul7MCqK6trd3mdDrFyMjIYigUGiWifqXUZC6XS28EakWVlZUqlUrNM/MwgAG/338bAJqbm8sA1DJz2eLiouF0OvMx+T0WiURICGEQUVF7e3sJAASDwRCAYSKaYOYMERW8/vrrGwYLh8MgIhBRGEBwfHz8WiQSaWxubi4iIisAsxCClk0BrwJbETPTyjUTDAZzAMzMXC2ldCilWKkNrWReUkoAMDGzE4C4ceNGrqOjw9TW1lZ45coVQ0opKioq8uPXdAcAMDMz4wDwNDO7AGTwGPblXjGzBOAEsCMcDgsAUEpZiMiRy+XMfr8/gbUytqJkMgkApQDa3nvvvd/+FKB7dfbs2fRnn32mlyGhlHIwcw2Akqampjksl401wSwW'+
			'C7CUpeiFCxemiUgRPZavzIuZ6ebNmxJACQAzACKiNIA4gMzs7Gx+RdZdSgBhAN+cOnVqgJmTP4lqicJg5koAzwghngZgklLGiOg/hmHEbTZbvsg+DIzD4bDavHkzWlpa0kNDQ6MA+ph5loj042aNmaG1thBRKzNva2hoEAAwMzOT1FovKqWyZrM5P34VmNaahRBqdHQ03draira2tpLh4eEKIURhNptNFhYWpl0u12MdgOvXrwu73W4BUG6z2TbV19cb0WhUTU1NZYhI3X/a82Dd3d38/vvvZwDMXb58+e6hQ4eqm5qaygA0aa0bpZR3CwoKwj6fL4cNyufzkcPhKMjlclsA7HS73dsBIBgMxgFEmDlhtVpVd3c3r1T/fOV3u90QQsQABBOJxPf9/f1Jh8MhvV5vC4A/ENGuVCpl379//4bWsr29nW7fvl2gta4kom'+
			'eLi4t/19XV9SQA+P3+CQADUsqpSCSSXe6oVoMB4Hg8nmLmSWb+rq+vbxwAvF6vY9OmTbuZeY/WemtdXZ3ltddek/fea+tlateuXQVCCKdSqomZ9x48eHCHw+EwAoHA3PT09FVmDmqtY5OTk2u7i5qaGrbZbIqIxMLCQq3ZbN7U2NhY1NjYaA8EApzJZNLMnDAMIzU7O5sdHBxcc795PB5RVFRkJqIqrfWvicjj8Xie6+7uLk8mk/zWW28FM5nMP6SU/4rFYnf7+vpWgT2wLK+++qo0m83lSqkuAAfeeOON52pqauxTU1OZY8eOXV1YWPiWiL5k5mFmniKijMlkWgWYTCallNIOoJqInmXmZzweT+tLL71UCQDvvvvutWAweArAXw3DuHr8+PFF3HerPGCtDx48iHg8niGieQCZoaEhU0tLS01lZaV59+7dT4RCIdfM'+
			'zIwTgH254bUrpcqVUk+sfFLKrUS0C8Dvi4uLvT09Pbu6u7vLAaC3t/f6xYsX/4aljunfhmEk+vv7HzCcazUjVFJSUiiE2AzgNzab7fChQ4f2uN1uOwAEAoGY3++/fevWrSAzx3BPM7LUgJPFZrNtdrvdDV1dXU86HA4jmUzyBx98MDo8PPx3LJnOgUwmE08kEqq3t/cBhvX6SiotLS0A8IQQ4llm9nZ0dOx54YUXNjscjgIAiEQiemxsLDs7O7uqCG3fvt1UX1+fL0WBQGDuo48+upFIJPqWu6Nr0Wh0fh1rvn4nvgInhNgCoI2Z3QA69u3bV9fc3FzW3NxctFZsNBpVwWAw7vf7J6anp68CGCSiLwGMpdPp+Q8//HBd//QoTwRUUVFhymazJUqpKgBtAHYCqAVQ3NbWZtZaFymlSokoLaWMhUKh1J07dzJY6jkHmD'+
			'kopbwppZwSQqSmp6cfunwbAluRx+MRVVVV0m6325VS5cxcJoQwMbNBRE5mrlk+MP9l5gQAzcwJKeWU1jo2Pz+fOnXqlMYjeroN38iHDx8WLpfLWFxcNKSUpLUWWLLGJcyc0VrHpZRZpRSsVquKRCLZyclJfeHChQ3dsY/7cIeVh7upqSncuXOHpJRi2QTo1tZWAKse7n7Rz67/AToDpst/DIy8AAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="zoom out";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 175px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._zoom_out.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._zoom_out.onclick=function (e) {
			player.changeFovLog(1,true);
		}
		me._zoom_out.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._zoom_out);
		el=me._auto_rotate=document.createElement('div');
		els=me._auto_rotate__img=document.createElement('img');
		els.className='ggskin ggskin_auto_rotate';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAI90lEQVRYhe2XS0xbZxbH/+e714/Lxdfg8rAhLsQmHvKiQBOiQkqARFE6TKWoU49oqkbZNaq66bI7t7tuUNtR1ceiihRVs0CjaReddDRNQtokGkISAopMhvBIGMCuIXYw+IXv/c4s+pgweUwy7arKf/2dc386/3PP9x3gsX4lol8yWSQSwZUrVwQAIaVkj8cjjx07xv9PLuWXggqHw2Bm1eVyuTRN8xqGoTmdTrO+vt6MRqOPnE/8UmC6rqtSynLTNJuJ6KCUcm+xWNxYWVnpiEQij+zMz65YOBxGW1ubarfbvQA6ALzY1NT0+0QiEWBmklLeTKfTGcMw5I0bNx4678+qWDgchmEYNpvN5gGwC0BvT0/Pvtdff33T4cOHtxFRgIgMTdPE6dOnHyn3zw'+
			'LTdV0VQpQT0VPM3Nvd3b23r6/vyXw+L8+fPz/DzDeEECu5XE52d3c/Um71YQ4dPHiQpJTCZrPR4uKi7Onp4dnZWcVms3mZeRcz93Z1dfX29fVV5XI5vP/++9MzMzNDQohx0zSZiJybN28ml8tlrqys8ODg4P/8Ux/YY5FIhAKBgOJ2u0t1Xa8oKSkp83g8JjNLy7IMInoGwO96enr2vfTSSzXZbBb9/f3mzZs3UwAyAHQABhFpzOxwu92Wx+OR7e3t8sKFCw+Euy/Y1q1byePx2Gw22xPMvB1AG4AGRVFuSSnzUkofgIM9PT3P9fX11QNAOp1GIBCgZ599trS6unqD1+ttLBQKDaurq3UAnmRmmxDCAlDwer3FiYmJ+8LdZWUkEkEikVAsy3Izs19K2crMz3m93kA2m72ZTqevMfMifhjOdXV19jvjQ6EQAXAEg0EH'+
			'AA8A//z8fNuZM2eSg4ODV6WUF6SU39TU1IwdPXo06XA48u+9995dgHdVLBQKKcVi0U1EOwHsc7lcB44cObLn0KFDG2dmZpbi8fgZRVHmmTlPRDQyMqL6/f5ar9erA8A777yDzz77rDg1NbX83XffLVuWJYLBoGP79u367t27/ZlMpmpubq6MiMiyrCwzr168eNF8INjzzz9PhmE4hBBBIvpDMBj87RtvvLEjEAjoqVTKPHny5MTq6uoZZl6QUmaJKA0gNzw8jJqamkBdXZ1zx44dGB8fz01NTU1dv379ytDQUHx4eLio63ppQ0ODo7m5uaK8vNw/OjpaIoRICyHiPp8v89+2rgPr6OhQFEWpYObduq6/+Oabbz7lcrnUs2fPJvv7+8+trKx8C2C4rKws+cILL6xdunQpK4RYJKLFixcvqn6/v9bv9+s7d+5UJyYmbq'+
			'dSqTFm/nsmkxkfGRlZikaj+rZt24xQKFTqcDgqotFoFsC/DMOIM7OMxWI/saybY16vVzBzOYDQoUOHNmmaRidOnFg5fvz4twD+AuAEgAXTNIvd3d3IZDKmlDLFzKNE9OWHH3745cjISELTNNHR0VEFQCOiSQBfAfjT9PT0V2+//fZEPp+X+/fvr/D5fI3MvNGyLOfTTz99fyu3bt1qB7ABQMerr77ankwm5QcffDAF4M9CiFMAphOJRLaurk4ODg4iGo2ioaFBKopSUBRlGUB2eHiYs9ls+RdffPFPZh5i5n+oqrqgqmpCSlksFouV2Wx2Q1NTk8bM1tWrV6cBRAuFQmZ0dPQnO9dVzOl02onIs3nzZj8AjI2NrQFISiknpJQLPp8v+/nnn8tIJPJTzMDAAI4dO2aura3FiegbAMdPnTr1R2Y+RkR/tdvt8erq6lxl'+
			'ZeWyZVlTzDx76dKlFQAwDMMDICCEeMLtdqvM/2mzdeNCSknMrIyPj2cBwOPxKADsROQSQjjj8fgqAAv3UCaTMTVNSwkhrpimOSuEKKiqmpqdnS1Eo1E0NzerQgidiEoqKyttAJBKpbIA0sycLxaL/NZbb927xwqFwhoRJQEkk8mk1dTUZKuqqvICaGXmkJTS8/LLL9u6urruumMHBgZQXV1txmKx9OLi4mwsFouPjY3ldV0XwWCwJJvN+oioBcBv9u3b5waA27dv3wZwQ0qZSqfT5p1OrOuxtrY2YmYVQOXKysrm1tbWslAoZFy+fNmztrbmAVBqt9utiooKbm5ulu3t7dTZ2SmCwSB5PB7Mzc2JlpYWoSiK0HVdraio0DRN8xNRM4C9AA52dna2HjhwwB2LxfKffvrpeSI6LYSYMAyjODQ0dG+wmpoaGIbBAOzz8/'+
			'P1Pp/P19jY6NyzZ09FMpmsnp+f9wMoB1CqKEqJZVllhULBrSiKraysjNxut57L5aqZeQMR+YQQm5i5G8D+0tLSrqNHjzbt3bvXncvl+JNPPplIpVKniOi8aZqJjz76SN7Jsg5MVVWura2ViqIUmdm4fPmyYbPZ3I2NjVpra6t7y5YtXrvdHrh169aThUKhAcB2IgoJISqEEG4hxCYiaieiLgC7iOiZQCCwv7e3t/WVV16pq6mpcczPz5sff/zxwszMzGkiOqkoyrVz585lFxcX17XGXU/eSCRCiUTCYZpmA4BnmHlXQ0PDrnA4XL9x48bSH89NTk6ac3NzxUKhUJicnFxQFCXNzLby8vJqt9tdtmHDBnttba3q8XgEAOTzeXn27Nn4wMDAGIBRAH8joquapqXefffdu66ke77Fw+EwVVVVaZZlVUkp6/H9y2K71+vd'+
			'1NnZWbtly5Yqn89nv1fsncrlcnJ8fHx5amoq8fXXX88CGAEwSkTXFEWZYeaVpaUlc2Bg4K7Y+y4JR44cISmlarPZnKqqegHUMXM9EYWYOQjA3dLSojGz48cqCSHMH6q3HI/Hi7FYLA9gFsB1ALNEdI2ZY1LK9NLSUqG5uXndTHwosDvPHD58WGiaZrcsy8HMFUKIJ4QQzh/+YDeAAL6/MbIApololpmLACwpZUpKuaSq6iqA/MTEhDU4OCgf8L2HBgPwvb1OpxOqqqqapqmWZQkAZJqmQwjhBuAioqJlWctCiLSiKLJYLMIwDHNubs70+/2yv7//oZffR973urq68NprryEajVI6ncb4+Di1traKVColSktLeWFhQRKRDAaDAIA9e/bwoy4ij/VYj/VYvwb9G5ErGt7Q8vxJAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="auto rotate";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 210px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._auto_rotate.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._auto_rotate.onclick=function (e) {
			player.toggleAutorotate();
		}
		me._auto_rotate.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._auto_rotate);
		el=me._info=document.createElement('div');
		els=me._info__img=document.createElement('img');
		els.className='ggskin ggskin_info';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAKe0lEQVRYhe2Y329U1drHv8/ae37vactAmZkOtpR2Di0FGhxqiwNlxBxN4JRXE0uMidFENMRoYvwLeuuFJvSCJmpMTIwxqRdvUo4BeYHWHt56tBQQBtoOLS3Q33Smw+zd2TOz937OhbSHWtSq55y8F+/3bk/Ws+az1vo+z3r2Bv6Piv5IcCwWozfffBPXr19f8fv+/fvx1FNP8X8UbAmmq6uLHA6HTQhhZ2YhhCAAyOfzrChKYX5+Ph8IBKyWlhb+PZBrAmtra0M8HheKoshOp9PDzF4ATsMwgkQUIiIFgADAAAxmniGiu0KIDACVmdV0Op2rqakx2tra1gQmrWVQdXW1VFxcrAghQpZl7WbmKIBGIcR+AAcURYnW1NQ0KYrSkEql6oUQVURUyczVzF'+
			'xKRLLb7TZ0Xc+53W5rbGzsV//zF3esra2NZmdnZcMwSoQQ1ZZlNTHzvkAgUN3c3LwhHA57Kioqin4at7CwkL9161ZmaGhIO3/+/AiAa0TUx8x9AOa8Xq/+/vvvm78L7NixY8KyLLcQ4jHLspoA7PH7/U2HDh36U2Njo2Np3NTUVH5xcRFjY2Oyx+Ph0tLSQigUsjudTgEAyWTSOnXq1L2enp6rAM4QUT8R3dB1fVbTNKOzs/O3gb311luuQqFQxcxPAWg5ePBg3bPPPhtwOp0ilUoVent7p3t6eqZVVV0EsJGZK4hIBTAKoLBz584NTU1NgUgksg4ARkdH9U8//fTG9PT034noawB/E0Kk/H7/I333SI+1tLQIt9u9EcBeAP919OjRfQcOHFhfKBTw1VdfTXd0dHybSCTOFwqFcwBGmFl57rnnwqlUakbTtHMAzs3M'+
			'zAxdvHjx3vfffy+Vl5eXVFVVOffs2bNxaGhofSqVcgCYliQplUgk8oODg6uydhXY22+/Lbxer2JZVgOAv7zxxht/3r17d5Gu62Z7e/vQd9999wWAL4UQZ5j5MoAsEYXffffdxjt37kzfuXPnDICTRHSZiK6rqjp74cIFq6yszF9eXu6ORCLrBgYG1muapjPzpMvlSg0MDBg/5RAPP8RiMcrlcnbLskIAmvfs2dMYiURKdF232tvbb4yOjp5m5r8y899VVR2TJCkNQAdgAkBlZaUEwCAiNZfLzRYKhUFm/h9m7vrwww/P9/f3Z1wuFx07diwEoJ6Za4UQxbFYTPwimMPhIGZWLMvaqihK44svvhgGgOPHj4+MjIycY+aviCiezWZTly5dyjOzBcAAoI+Ojmbv3buXBZDP5/OWz+cz5+bmsqZp3iaiXmb+60cffdQfj8'+
			'e1UCgkHzlyZAeAnQCCoVBo1cmtAKuoqHAQUQWA6DPPPFPvdDqlU6dOZUZHRy8AOMnMF1Op1P3PPvvMjMfjcLlcLITQiOjWe++9d/L06dN9QohZu91ubN68GV1dXZzJZPRcLjdOROcAnP7kk0+GdV23otFo0Ov1NjLzrqKioqKXXnpJPBKsra2NhBAeANUA6pqbm9dls1k+c+bMbQADAEY0Tct0dnZaSzGyLIOZGUDhwZHmmNkAgEAgAADo7Ozk8fHxgmmacwCuqKp6o6enJ+10OkVzc3MVEe00TXNDaWmp/HB2LoP5fD4YhuEBUN7U1BR2uVwUj8dVVVUTzHwVwD1VVVeYNJPJEDO7mLn8yJEjsUgksgvAhnw+Lz18sXd3d7OmaToRjQD44cKFC3cBYPfu3RsBbGHm9YuLi7LP51sNlkwmSQghE5F769atCgBcvXp1'+
			'FsAlSZJu6rq+2NXVtSKtZVkmIrIzc8nTTz/9WE1NTYCZPXa7fZWZv/jiC4uIZojo0uzs7I2JiQmzrKzM7vV6vUTkkCSJksnkcl1dNQEzU3l5uQwA165dUwHMA1iMRqOPvEKYGUQr6vTPFW32eDx5Zk4xczqRSOQBIBwO2wHIAGhycvKfi/6ZSQAAqqqaAExmtqampn5p6JqUy+UYP5YWM5vNMgAws4OIvLIs24uKirSlsat2DABKS0uLlwL/3TJN02FZltcwDMf4+PgyzyPB5ubm0i6X6w91t2uVJElZIURKkiTd7/cvZ/wjwZakKMq/HO7H6gIsLZyI8sysmaZZeHic+GkQEVm3b98uAMCOHTvcROQRQtjxB98PAMBms0kPaqU7HA7bACCRSOTwo4+5qOifrd0y2LZt2/gB/f3h4eH7AFBfX7+embdYlrVxbGzMHo'+
			'vFfjdcLBYTmqYVAagoKyurCIVC8tTUVD6TySwQUVaSJGvbtm3Lvl4GO3HiBJhZAzDe19eXyGazXFtb6/N6vduZudZms63btGnTmlrxn6q1tZU2b95sB1DOzPXRaDQMAP39/TNENGKa5nw2mzVOnDixHLMM1t3dzaZpasw8BuDmN998k3Y6neLw4cO7APwFwJNutzvw6quvyq2trWuGevnll8W6des8NpvtT8x8KBAIHNi7d29A13Wzu7v7tmVZQwDuGYZhdHd3rwYDACIyAdxj5mtff/31oK7rVnNzs3f79u3NRHQIQKPNZvO5XC5bXV3dLx7r5OQkWlpayG63ewBUEdHTRPTnV155ZZvT6RS9vb1zmUzmKjOPSJKU03V9RfwKMNM0TVmW54moX1XVvs8//3wcAF5//fXHKisr9zPzIQC7nE5naUNDgzOXy9FSlj0s'+
			'wzCoUCjIfr9fEUJUMfMBAAePHj1av2XLFufExITx5Zdffk9EvUKIhKIo+c7OzhUTrfDM5cuXEQwGDUVRNCLCxMREsc/nK9uyZYsSiURKhoaGAul0eh0AryRJsiRJzMzrAeyKRqP1MzMzqR9++OGiEOKuLMsBImogosNEdPC1116LPPHEE25d183jx4/HVVX9bwA9uVxusqOjY1UHu8rMw8PDiEajFjPnAZhXrlxxBIPBTRUVFc59+/Z5HQ5H4MaNG2UAggAUAH4A2xOJxKbh4WFtcXFxCoAbwJMAnqmsrGx+5513ttbU1DhTqZTZ0dFx/e7du2cBnLLZbGMLCwv69evXf73nB4CDBw9a6XRaB5AGkBsYGCjOZDLF1dXV7pqaGs/evXsDxcXFlclkskJV1c1EVJVOp9drmiY9gN3a2NgYe+GFF3Y8//zzAa/XK928eb'+
			'PwwQcfxGdnZ88+6ISvFgoFtbq62nrY9Ev6WQO3trZSSUmJTQhRysz7ADwZDAYfP3z4cO3jjz++3Dglk0men59nZhYAmIiscDi8vOCFhYXc6dOnJ8+ePXuZiHoB/C8RDSWTyfsPN51rBluC8/v9NtM0N1iWVcHMdQAag8Hg1oaGhrLa2tqyYDDocLlcK5JocnIyPzMzc//KlSszfX19gwCuAbgihLgmSdJMLpfTPv7449/3Jr6ktrY2DA4Oyh6PxyHLcgl+7DgrAZQD2A5gPVZbQgUwBmCUiG4CuGVZ1rRhGBlVVY26ujrr1z6u/KYrpqWlRYRCIZtpmk7TNL2SJAWIyIOHyg4RsWVZeWZOMvO8LMuaYRj5b7/91ozH42tupX7z3ReJRCgSiSCVSkmhUEjWNE2SpJUbZpom22w2Y25uzggGg9ze3v7v+T72c+rv78fJ'+
			'kydXzeHz+TiZTGKt38L+X/8K/QPKjAWQgLV0UQAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="info";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 245px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._info.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info.onclick=function (e) {
			alert('$ui');
		}
		me._info.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._info);
		el=me._hide_hotspot=document.createElement('div');
		els=me._hide_hotspot__img=document.createElement('img');
		els.className='ggskin ggskin_hide_hotspot';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAKt0lEQVRYhe2Yf2xTV5bHv+e+Zzt2bPNiQwwJpSQOA3HopLuEDdPpNOCEzU7jVppqHWZbIbQo5Z9W6qj/t3L/BalSUf8oalVVbHeJiDqFTatOOqRO0AbBhGY0AyGQdShVTH44HTvEfrHj53fP/oGdTSihlFlp/5kjvb/u1T0fnfO955z7gL/ZjzP6vzgkGo3S4OAgAUAoFOKmpiYA4P83sCNHjpDb7bamUik3EZUTEZmmuaRpWkpV1aWjR4/KlfsjkQhGR0dFPp8XAGC1WmVDQ4OMRCLfO1v8NWCapgld1z2KojQLIX5NRP+iqup+Xdc3SinVex3G43Flw4YN5T6fr8rn81Vt2LChPB6PK/c7+68Cy+fzimmaHiL6hzfffPM3ra2t/0pE+wH4dF1XPB'+
			'7P8t5wOCwURXEZhlEP4FkAzxqGUa8oiisSiXwvc+qPhWloaIDf7xeNjY0imUzamNlJRL7q6mqfy+VyAaiSUrqtVqvt6tWr+XA4LG02m3A4HC5mbgLwbGNjY3BiYiKfyWS2SCnPTE9P/2nv3r35gYGBZV3+KLBIJELxeNyiqmr57OxsBRF5APiZ2QsAzCyY2UVEjxuG8RchxLTT6VywWCw2Zt5ORB1tbW2/7Ozs3H7t2rWFd9555yYRbWBmJRqNguh/A/fQYOFwmBKJhE1V1Spm/gmABmauJaJKAPVFMBVAFYAOZv4JM1+zWq3jAMqJ6OlgMBjq7Oysy2az3NvbO0lE3zDzHBGZ+/btW+XvoW7lwYMHhd1udwCoARAC8POamprtwWCwuq6uzubxeFZpNRaLFS5dujQ/MjIymclkJgAowWDw7w4cOLAVAN59992xK1eu'+
			'nCaiPgBjmzZtuhOJRFaVlx8EC4fDVFFRUQ7ADyDocrn++fDhwz8NBAJOAMhms3z79u1Cab/dbhfV1dVKae3jjz9OOhwO8dJLL1UAwOnTp2+cO3fuCyI6zczXDcNY+Oijj8x7/T4QLBKJUCKRsBUKhW0A2rZt2/arrq6uPZqmWZLJJPf19U0PDAxMMnOOiEo1y1JZWekLhUJbmpubbSvP6+7ujkej0f8EcNY0zT+YppnWdd3s6en5nu81NdbQ0ICi0KsAhFwuV6irq2u3pmmWy5cvp0+dOnVD1/XfA/gjESUBSAAKAE8ikdj/4Ycftum6/lgwGFQA4OLFi2Y0Go0DGAMwvrCwsNDT0yPX8r9mHfP7/UJV1fKi0H9++PDhn5ag3n///QuZTOZTAGeJaFBRlEuqqn4N4Doz3wFgb2lpcZSgFhcXsWfPHqWmpsYLwAfA6X'+
			'a7H1hD11xsbGwUUsoKZm7YuXNnfSAQcCaTST516tQNAOeJ6HfMPMrMf5mfn89KKQWALQCeCgaDT7344ouVAHDy5Mmlzz//3ACA559/fiOAemZ+3Ol0WqLR6JpSWpXKI0eOkNVqFUSkJJNJGwAvgNrm5uZNANDX1zet6/rviaifmcdfe+01/dixY8LlcrmklE0AOtra2kKdnZ01ANDd3f3t0NDQDQAb9u/fvzMQCLg2bty4dWZm5rFcLuc8c+YMv/LKK7KsrEwmEgnT7/dzqY2tipjb7bbm8/nKbDa7zTCMADNvA1BZV1dny2azXBT6HwF8AyB77NgxYbFY3Lhbx0rFsw64e/uKQv8EwPDg4GASAAKBgAbgMWauz+VyDfl8vn5hYaHK4/HYJyYmliO4HLFoNEo9PT1uALuEEM0AKonIC6De4/GIWCxmAMgyc1JRlMX5'+
			'+XkUW9AOAO2tra2hcDhcgor19/d/AeALIcSslHLd5ORkCoBP0zQvgKcBbJJSLhFRFsDlpaWl/6qtrZ0CUFgFNjg4SIVCwUlEO994442Xq6urffdJPRORNE1TuN3uUu/raG1tDa2IVKy/v//fAfSpqnp9aWnJoSjKvBAiBwDt7e1ae3v7MwCeAYDjx49PXbt2DVLK0e+++27mvhormhgaGkq7XC5XsfepHR0dy/uYWQHgxl2hd7S1tf2yFKmenp4bxUj1MfN1h8ORLhQKbgDrpJQOABgfH+di9E0i4rm5OQAQtLJRrgQLhUL8wQcfLEkpb/f3918CUA3ACaCqo6Ojym63CwAWIvIwcwMzP9XW1na/9H0OYGx+fj6taRqIqAzAOrvdbgeAiYmJ7NmzZycBzDGzQUQ6gG+JSPd6vd+fLpqamvD666+nMpnMeWa+QUQuAF'+
			'uJ6NlYLBaqq6tTKysrfYlE4h+JqCwYDD5Vun0r0wdgTNf1hdraWqTTaScz1wGoefLJJ91FsGki+oyZB4QQupSyQEQJq9U6Mz4+vtyaVt5KVlV1SQhxm4j+pKrqsBBiBEDs0qVL88WobgHQ1tLSsv/AgQMlqBv9/f2fldKXSqXSmzdvxsLCgt00zS0AfuHz+Rp37dq1LpVKGVeuXIkDGFUUZRjAsBDiayK6ZbFY9B07dix3glUaO3r0qIxEInmPx4MLFy7kNU2bZuZrIyMjky+88IK3ubnZpuv65lJF7+7u/jYajX4J4AtVVa87HI60pmlIp9NOKeUWZm4G8LNDhw7VAcDw8PAdAOMAYkKI+bGxsfzevXvR0tLCDz32hMNhcjqd5RaLpRHAb5qamva9/PLL3tL6yZMnly5cuDDAzJ8S0WXTNBOqqpoAHFLK7QB+AeBn'+
			'XV1dTbt373ZMTU3l33rrrXMATiuKci4ej0/39vau2SvXbOI2m01YLBYbgHIAisPhWE774uIi7Ha7YOZKZt4NYJMQYo6ZzeI0W+/z+RoPHTpU5/f7y3K5nHnixIkRIvoSwNeKoiQfBLVmxMLhsKioqHAzcwDAP7W2th4sDXkXL1409+zZowBAKpUyBgcH5ycnJ3UhRIaIZFlZmauxsdGza9eudQAwNTWVP3HixMjMzMzvAPwWwDdCiMX33nvv4cEikQji8biiKMp9i2dxnorX1tauf+6553yBQMC11sGpVMoYHh6+88knn/yBiL5k5q+I6L+TyeRST0/PDz6GV4GFw2Hh9XqdzLyDmX/9xBNPtL/66qsB4G7xPHfuXD8RjTGzD0D9xo0btwYCAU3TNG97e7s2Pj7OExMT2VgsNn316tU47gp9iIi+ZuZbqVRKfxgo4B'+
			'6N1dbWimQyWUFEf+/3+4NdXV3bgdW9j4jGmdnJzI/Pzs5unpmZ2QLg6fb29mdisZhx9uzZSSL6jIhGcff23VQUJVkoFB4qUvcFW2mzs7P5VCplnD9//uZXX331HwD6TNMcS6VSC263WzidzolcLudk5noi2oS7vc8EkJBSDlit1mEhxPytW7eMHxL6D4LdvHlTer3elGmaI5lMZkskErnJzN8IIfqklNdN00wXx2EZjUbNM2fOcC6X06WUSwBARMzMBSGETkR6Op3O9/b2PtLPlVXzWENDgywUCroQYhzApwD+jYh+C2DMMIwFXdeXW8a+ffu4UCjIIkj2+PHjU0NDQ/NElAFQMAxDbt269VGYANwTseL0aEYikTu3b9/+s2EYisViMauqqvL3vvsAYP369XJ2djYlpbw8OjoK3H2M3AKQWLdundnS0vLIv6Ie9Hxb'+
			'uXZfBwcPHiSPx2NfXFxcD0ADQMy8aLfbZywWi/7222//aG2V7EEvFV7x3df8fj8nk8ms2+2eslqt161W65imabfm5uZ0t9v9yFB/s0ex/wHNHC01LQz7vwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hide hotspot";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 38px;';
		hs+='left : 280px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hide_hotspot.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._hide_hotspot.onclick=function (e) {
			ToggleHotspot()
		}
		me._hide_hotspot.ggUpdatePosition=function (useTransition) {
		}
		me._toolbar.appendChild(me._hide_hotspot);
		me.divSkin.appendChild(me._toolbar);
		el=me._loading=document.createElement('div');
		el.ggId="Loading";
		el.ggDx=6;
		el.ggDy=0.5;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 57px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 222px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._loading.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._loading.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._rectangle_1=document.createElement('div');
		el.ggId="Rectangle 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='background : rgba(0,0,0,0.588235);';
		hs+='border : 2px solid #ffffff;';
		hs+='cursor : default;';
		hs+='height : 55px;';
		hs+='left : -1px;';
		hs+='position : absolute;';
		hs+='top : -3px;';
		hs+='visibility : inherit;';
		hs+='width : 220px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._rectangle_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._rectangle_1.ggUpdatePosition=function (useTransition) {
		}
		me._loading.appendChild(me._rectangle_1);
		el=me._loading_bar=document.createElement('div');
		el.ggId="loading bar";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : #ffffff;';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 5px;';
		hs+='left : 1px;';
		hs+='position : absolute;';
		hs+='top : 36px;';
		hs+='visibility : inherit;';
		hs+='width : 220px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='0% 50%';
		me._loading_bar.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._loading_bar.ggUpdatePosition=function (useTransition) {
		}
		me._loading.appendChild(me._loading_bar);
		el=me._loading_text=document.createElement('div');
		els=me._loading_text__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="loading text";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 10px;';
		hs+='position : absolute;';
		hs+='top : 11px;';
		hs+='visibility : inherit;';
		hs+='width : 200px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 200px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._loading_text.ggUpdateText=function() {
			var hs="<b>Loading... "+(player.getPercentLoaded()*100.0).toFixed(0)+"%<\/b>";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._loading_text.ggUpdateText();
		el.appendChild(els);
		me._loading_text.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._loading_text.ggUpdatePosition=function (useTransition) {
		}
		me._loading.appendChild(me._loading_text);
		el=me._loading_close=document.createElement('div');
		els=me._loading_close__img=document.createElement('img');
		els.className='ggskin ggskin_loading_close';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAAXUlEQVQokb3RyQ2AMBBD0REVUMqUROcpgRI+lyAhy45yYo6xXhan6vcBLuAGejubi2g4wTtDUSsUYG/h4BoEaMFhXFfVGXc1p+gbbDkr0K4cRanyL7SVj/C5MdueB+cnAYSoJSEgAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="loading close";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 13px;';
		hs+='left : 203px;';
		hs+='position : absolute;';
		hs+='top : 2px;';
		hs+='visibility : inherit;';
		hs+='width : 13px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._loading_close.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._loading_close.onclick=function (e) {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		}
		me._loading_close.ggUpdatePosition=function (useTransition) {
		}
		me._loading.appendChild(me._loading_close);
		me.divSkin.appendChild(me._loading);
		el=me._hotspot=document.createElement('div');
		el.ggId="hotspot";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 73px;';
		hs+='left : 12px;';
		hs+='position : absolute;';
		hs+='top : 10px;';
		hs+='visibility : inherit;';
		hs+='width : 268px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspot.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._hotspot.ggUpdatePosition=function (useTransition) {
		}
		me.divSkin.appendChild(me._hotspot);
		player.addListener('sizechanged', function() {
			me.updateSize(me.divSkin);
		});
		player.addListener('imagesready', function() {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		});
	};
	this.hotspotProxyClick=function(id, url) {
	}
	this.hotspotProxyDoubleClick=function(id, url) {
	}
	me.hotspotProxyOver=function(id, url) {
	}
	me.hotspotProxyOut=function(id, url) {
	}
	player.addListener('changenode', function() {
		me.ggUserdata=player.userdata;
	});
	me.skinTimerEvent=function() {
		me.ggCurrentTime=new Date().getTime();
		var hs='';
		if (me._loading_bar.ggParameter) {
			hs+=parameterToTransform(me._loading_bar.ggParameter) + ' ';
		}
		hs+='scale(' + (1 * player.getPercentLoaded() + 0) + ',1.0) ';
		me._loading_bar.style[domTransform]=hs;
		me._loading_text.ggUpdateText();
	};
	player.addListener('timer', me.skinTimerEvent);
	function SkinHotspotClass_hotspotscriptp2(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspotscriptp2=document.createElement('div');
		el.ggId="hotspot-script-p2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : 325px;';
		hs+='position : absolute;';
		hs+='top : 25px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptp2.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me._hotspotscriptp2.onclick=function (e) {
			Hotspot(me.hotspot.title,me.hotspot.url)
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp2.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp2.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp2.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp2.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspotscriptp2image=document.createElement('div');
		els=me._hotspotscriptp2image__img=document.createElement('img');
		els.className='ggskin ggskin_hotspotscriptp2image';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAACKElEQVRoge2Zv07rMBSHPxAbS3gDulOpSBkYi3iAwng3huzsLLcsd+/ugY0R+gCIjgyRqMTde9/gZul+Bzu3Thqn9UlKS+VPiuS6x8f+9fjPcQqBQCAQ2AIHG/WepEPgp/n0gIqHm+rqcFOOv5ogZNfYGyHtL/YkvQUGQB+ISt9mwAQYAy+oOGur2/aE6B3qjuXBu8iAUVs7WXMhSXoKPAM9oYcpcIOK/zQZRjMhSdoD3liOwgx4B36X6s+AC6BTqs+AS1Q8lQ5FLkRH4oOiiBmggM8VrbtAQlFQBpxLI9Nk13qmKOIVuGe1CIzNvWmTExmfImRC9MK218QrMALmHl7mpo0tpmd8e+MvJEkj9O6Uk08nKcr4yLlzGdYhicg1xSml8ItEmTnFHyIyZ5'+
			'EXEiEDqzxjvTWxik+KURm4DF1IhPSt8rugvQvbV99l5OKo9tvqhWdPq/I50QTbV1TZd00WUC9kcSnaBlV9D13Ge5P9rorIQ0XdV0Wpqm8n/ilKkv5lsU6ezNMGP8wDkKHiE5/Gkqk1scoXgvYubF8Tl5ELiZCxVe6gE8CmdCkmkGOXoQtZ9lucXjN0Aig93Y+BXyyEeE8rkO9aI6vcQafkUsrp/MhlWEeT+8gHyxmwT951jBZxZdVNUfG5ZDhNzpEb9GUo5wo9RdZZM11ja4vIjE8R4ar7n714+WDz7V8HlVn3BZ2KH9vsNvytsGsEIbvG3ggJBAKBwFb4B/tSh2Ca4i4KAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot-script-p2-image";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptp2image.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotscriptp2image.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotscriptp2.appendChild(me._hotspotscriptp2image);
		el=me._hotspotscriptp2imageon=document.createElement('div');
		els=me._hotspotscriptp2imageon__img=document.createElement('img');
		els.className='ggskin ggskin_hotspotscriptp2imageon';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAC0UlEQVRoge2ZT07bQBTGv5oYTILUWEhEXUSpIix2KOxgg+gmuyjlAEi9gMURCkdAuUAlDkCDd9kUsWGJxQ4FISIvUJAqG6kkJoGoCyZ0OvWYeJwQGs1vZY/fvDdf3vx5dgCJRCKRjIF3o3ReqtR3AHwlt7uWaeyMKpYyKsevjRTy1pgYIUNf7KVK/QuAMoANAGnmsQfgCEAVwHfLNLxhxR2aELJDbePfwfPwAOwNayeLLaRUqX8EcACgIOjCBrBpmcZVnHHEElKq1AsAfoDJgqYqTT05dT4/l2jQ7T9/PeTc1uOS3+1lGFcegE+WadiiYxEWQjJxCkqEpirNxYWZWlafbnA7AnDcTu7i5r7ICPIArIhmJs6udQBKhJ5MnK3lU/sviQCArD7dWMun9v'+
			'Vk4oxqThOfQggJIQv7eU3oycTZaj51qKmKP6gPTVX81XzqkBFTIL4jE1lIqVJP42l36g+oWcjO1kSCA0AhO1vTVKVJNW1zjUMQychnUFNqcWGmFiUTLJqq+IsLM/QPkSZnUSREhJSpQTQHWRMvkdWnG0xWylxjDiJCNvoXenLqXKB/IIyvDZ4dj0TYQ87Ce55W7DkRh/m5ROP6tvscIyh2WBUQKgR/XorGQVDsHZ7xxFS/L2VkN6DttbIUFJtL5BKlVKm7IOvkw3v1uJBNHkf1EYTttNavb7vr5NazTEOP0l9kah31L9zW45JA/0AYX0c8Ox4iQqr9C7/byzhuJyfg4y8ct5NjCsgq15hDZCGWaXzDU6UKACBVrBbVTx+/29Mubu6LVJNHYkRCdNfaowaSsZ12Mcw4DNtps+X8Htc4hDjvI6dgKmBSAA5Ud/ndnmY7'+
			'7aLbelimmm3LNFZExhPnHNkENcXc1sPyyeXd1iBrxnE7uZPLuy1GhEd8CiFfdftMxMcHmv/+cxDLoB/oRLbYMOTfCm8NKeStMTFCJBKJRDIWfgP/gwJ6YRVfSAAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot-script-p2-image-on";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptp2imageon.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotscriptp2imageon.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotscriptp2.appendChild(me._hotspotscriptp2imageon);
		me.__div = me._hotspotscriptp2;
	};
	function SkinHotspotClass_hotspotscriptp(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspotscriptp=document.createElement('div');
		el.ggId="hotspot-script-p";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='height : 0px;';
		hs+='left : 214px;';
		hs+='position : absolute;';
		hs+='top : 25px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptp.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me._hotspotscriptp.onclick=function (e) {
			Hotspot(me.hotspot.title,me.hotspot.url)
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotscriptp.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspotscriptpimage=document.createElement('div');
		els=me._hotspotscriptpimage__img=document.createElement('img');
		els.className='ggskin ggskin_hotspotscriptpimage';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAACKElEQVRoge2Zv07rMBSHPxAbS3gDulOpSBkYi3iAwng3huzsLLcsd+/ugY0R+gCIjgyRqMTde9/gZul+Bzu3Thqn9UlKS+VPiuS6x8f+9fjPcQqBQCAQ2AIHG/WepEPgp/n0gIqHm+rqcFOOv5ogZNfYGyHtL/YkvQUGQB+ISt9mwAQYAy+oOGur2/aE6B3qjuXBu8iAUVs7WXMhSXoKPAM9oYcpcIOK/zQZRjMhSdoD3liOwgx4B36X6s+AC6BTqs+AS1Q8lQ5FLkRH4oOiiBmggM8VrbtAQlFQBpxLI9Nk13qmKOIVuGe1CIzNvWmTExmfImRC9MK218QrMALmHl7mpo0tpmd8e+MvJEkj9O6Uk08nKcr4yLlzGdYhicg1xSml8ItEmTnFHyIyZ5'+
			'EXEiEDqzxjvTWxik+KURm4DF1IhPSt8rugvQvbV99l5OKo9tvqhWdPq/I50QTbV1TZd00WUC9kcSnaBlV9D13Ge5P9rorIQ0XdV0Wpqm8n/ilKkv5lsU6ezNMGP8wDkKHiE5/Gkqk1scoXgvYubF8Tl5ELiZCxVe6gE8CmdCkmkGOXoQtZ9lucXjN0Aig93Y+BXyyEeE8rkO9aI6vcQafkUsrp/MhlWEeT+8gHyxmwT951jBZxZdVNUfG5ZDhNzpEb9GUo5wo9RdZZM11ja4vIjE8R4ar7n714+WDz7V8HlVn3BZ2KH9vsNvytsGsEIbvG3ggJBAKBwFb4B/tSh2Ca4i4KAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot-script-p-image";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -24px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptpimage.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotscriptpimage.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotscriptp.appendChild(me._hotspotscriptpimage);
		el=me._hotspotscriptpimageon=document.createElement('div');
		els=me._hotspotscriptpimageon__img=document.createElement('img');
		els.className='ggskin ggskin_hotspotscriptpimageon';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAC0UlEQVRoge2ZT07bQBTGv5oYTILUWEhEXUSpIix2KOxgg+gmuyjlAEi9gMURCkdAuUAlDkCDd9kUsWGJxQ4FISIvUJAqG6kkJoGoCyZ0OvWYeJwQGs1vZY/fvDdf3vx5dgCJRCKRjIF3o3ReqtR3AHwlt7uWaeyMKpYyKsevjRTy1pgYIUNf7KVK/QuAMoANAGnmsQfgCEAVwHfLNLxhxR2aELJDbePfwfPwAOwNayeLLaRUqX8EcACgIOjCBrBpmcZVnHHEElKq1AsAfoDJgqYqTT05dT4/l2jQ7T9/PeTc1uOS3+1lGFcegE+WadiiYxEWQjJxCkqEpirNxYWZWlafbnA7AnDcTu7i5r7ICPIArIhmJs6udQBKhJ5MnK3lU/sviQCArD7dWMun9v'+
			'Vk4oxqThOfQggJIQv7eU3oycTZaj51qKmKP6gPTVX81XzqkBFTIL4jE1lIqVJP42l36g+oWcjO1kSCA0AhO1vTVKVJNW1zjUMQychnUFNqcWGmFiUTLJqq+IsLM/QPkSZnUSREhJSpQTQHWRMvkdWnG0xWylxjDiJCNvoXenLqXKB/IIyvDZ4dj0TYQ87Ce55W7DkRh/m5ROP6tvscIyh2WBUQKgR/XorGQVDsHZ7xxFS/L2VkN6DttbIUFJtL5BKlVKm7IOvkw3v1uJBNHkf1EYTttNavb7vr5NazTEOP0l9kah31L9zW45JA/0AYX0c8Ox4iQqr9C7/byzhuJyfg4y8ct5NjCsgq15hDZCGWaXzDU6UKACBVrBbVTx+/29Mubu6LVJNHYkRCdNfaowaSsZ12Mcw4DNtps+X8Htc4hDjvI6dgKmBSAA5Ud/ndnmY7'+
			'7aLbelimmm3LNFZExhPnHNkENcXc1sPyyeXd1iBrxnE7uZPLuy1GhEd8CiFfdftMxMcHmv/+cxDLoB/oRLbYMOTfCm8NKeStMTFCJBKJRDIWfgP/gwJ6YRVfSAAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot-script-p-image-on";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -24px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotscriptpimageon.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotscriptpimageon.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotscriptp.appendChild(me._hotspotscriptpimageon);
		me.__div = me._hotspotscriptp;
	};
	function SkinHotspotClass__hotspotpopup(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me.__hotspotpopup=document.createElement('div');
		el.ggId="__hotspot-popup";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : 286px;';
		hs+='position : absolute;';
		hs+='top : 25px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me.__hotspotpopup.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me.__hotspotpopup.onclick=function (e) {
			HideHotspot(this)
			var flag=skin._hotspotpopupcontainer.ggOpacitiyActive;
			if (player.transitionsDisabled) {
				skin._hotspotpopupcontainer.style[domTransition]='none';
			} else {
				skin._hotspotpopupcontainer.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				skin._hotspotpopupcontainer.style.opacity='0';
				skin._hotspotpopupcontainer.style.visibility='hidden';
			} else {
				skin._hotspotpopupcontainer.style.opacity='1';
				skin._hotspotpopupcontainer.style.visibility=skin._hotspotpopupcontainer.ggVisible?'inherit':'hidden';
			}
			skin._hotspotpopupcontainer.ggOpacitiyActive=!flag;
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me.__hotspotpopup.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me.__hotspotpopup.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin._hotspotpopuptext.ggText=me.hotspot.title;
			skin._hotspotpopuptext.ggTextDiv.innerHTML=skin._hotspotpopuptext.ggText;
			if (skin._hotspotpopuptext.ggUpdateText) {
				skin._hotspotpopuptext.ggUpdateText=function() {
					var hs=me.hotspot.title;
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (skin._hotspotpopuptext.ggUpdatePosition) {
				skin._hotspotpopuptext.ggUpdatePosition();
			}
			skin._hotspotpopuptext.ggTextDiv.scrollTop = 0;
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me.__hotspotpopup.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me.__hotspotpopup.ggUpdatePosition=function (useTransition) {
		}
		el=me.__hotspotpopupcontainer=document.createElement('div');
		el.ggId="__hotspot-popup-container";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 40px;';
		hs+='left : -73px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : 23px;';
		hs+='visibility : hidden;';
		hs+='width : 150px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me.__hotspotpopupcontainer.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me.__hotspotpopupcontainer.onclick=function (e) {
			player.openUrl("..\/..\/html\/vh"+me.hotspot.url+".html","_parent");
		}
		me.__hotspotpopupcontainer.ggUpdatePosition=function (useTransition) {
		}
		el=me.__hotspotpopuprectangle=document.createElement('div');
		el.ggId="__hotspot-popup-rectangle";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='background : #ffffff;';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 40px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 150px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me.__hotspotpopuprectangle.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me.__hotspotpopuprectangle.ggUpdatePosition=function (useTransition) {
		}
		me.__hotspotpopupcontainer.appendChild(me.__hotspotpopuprectangle);
		el=me.__hotspotpopuptext=document.createElement('div');
		els=me.__hotspotpopuptext__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="__hotspot-popup-text";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 28px;';
		hs+='left : 5px;';
		hs+='position : absolute;';
		hs+='top : 5px;';
		hs+='visibility : inherit;';
		hs+='width : 140px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 140px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: center;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="undefined";
		el.appendChild(els);
		me.__hotspotpopuptext.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me.__hotspotpopuptext.ggUpdatePosition=function (useTransition) {
		}
		me.__hotspotpopupcontainer.appendChild(me.__hotspotpopuptext);
		me.__hotspotpopup.appendChild(me.__hotspotpopupcontainer);
		el=me.__hotspotpopupsvg=document.createElement('div');
		els=me.__hotspotpopupsvg__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		me.__hotspotpopupsvg__img.setAttribute('src',basePath + 'images/_hotspotpopupsvg.svg');
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;_hotspotpopupsvg;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me.__hotspotpopupsvg__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		me.__hotspotpopupsvg__imgo.setAttribute('src',basePath + 'images/_hotspotpopupsvg__o.svg');
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;_hotspotpopupsvg;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me.__hotspotpopupsvg__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXRhcmdldDItYzAwPC90aXRsZT4NCiAgPGcgc3R5bGU9Imlzb2xhdGlvbjogaXNvbGF0ZSI+DQogICAgPGcgaWQ9ImZpbGwiPg0KICAgICAgPGc+DQogICAgICAgIDxnPg0KICAgICAgICAgIDxnPg0KICAgICAgICAgICAgPHBhdGggZD0iTTI1LDM4QTEzLDEzLDAsMSwxLDM4LDI1LDEzLjAxNDksMTMuMDE0OSwwLDAsMSwyNSwzOFptMC0yM0ExMC'+
			'wxMCwwLDEsMCwzNSwyNSwxMC4wMTE0NSwxMC4wMTE0NSwwLDAsMCwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICA8Zz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTM3Ljk0OTM0LDIzLjVjLjAyNTM5LjQ5Njc3LjA1MDY2Ljk5MzcxLjA1MDY2LDEuNXMtLjAyNTI3LDEuMDAzMjMtLjA1MDY2LDEuNUg0M3YtM1oiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICAgIDxwYXRoIGQ9Ik0yNSwzOGMtLjUwNjI5LDAtMS4wMDMyMy0uMDI1MjctMS41LS4wNTA2NlY0M2gzVjM3Ljk0OTM0QzI2LjAwMzIzLDM3Ljk3NDczLDI1LjUwNjI5LDM4LDI1LDM4'+
			'WiIgc3R5bGU9ImZpbGw6ICMwMDY0Y2MiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTI1LDEyYy41MDYyOSwwLDEuMDAzMjMuMDI1MjcsMS41LjA1MDY2VjdoLTN2NS4wNTA2NkMyMy45OTY3NywxMi4wMjUyNywyNC40OTM3MSwxMiwyNSwxMloiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMiwyNWMwLS41MDYyOS4wMjUyNy0xLjAwMzIzLjA1MDY2LTEuNUg3djNoNS4wNTA2NkMxMi4wMjUyNywyNi4wMDMyMywxMiwyNS41MDYyOSwxMiwyNVoiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICA8L2c+DQogICAgICAgICAgPC9nPg'+
			'0KICAgICAgICAgIDxjaXJjbGUgY3g9IjI1IiBjeT0iMjUiIHI9IjEwIiBzdHlsZT0iZmlsbDogIzAwNjRjYztvcGFjaXR5OiAwLjMwMDAwMDAwMDAwMDAwMDA0Ii8+DQogICAgICAgIDwvZz4NCiAgICAgICAgPGcgc3R5bGU9Im9wYWNpdHk6IDAuMzAwMDAwMDAwMDAwMDAwMDQ7bWl4LWJsZW5kLW1vZGU6IHNjcmVlbiI+DQogICAgICAgICAgPGc+DQogICAgICAgICAgICA8cGF0aCBkPSJNMjUsMzhBMTMsMTMsMCwxLDEsMzgsMjUsMTMuMDE0OSwxMy4wMTQ5LDAsMCwxLDI1LDM4Wm0wLTIzQTEwLDEwLDAsMSwwLDM1LDI1LDEwLjAxMTQ1LDEwLjAxMTQ1LDAsMCwwLDI1LDE1WiIgc3R5bGU9ImZp'+
			'bGw6ICNmZmYiLz4NCiAgICAgICAgICAgIDxnPg0KICAgICAgICAgICAgICA8cGF0aCBkPSJNMzcuOTQ5MzQsMjMuNWMuMDI1MzkuNDk2NzcuMDUwNjYuOTkzNzEuMDUwNjYsMS41cy0uMDI1MjcsMS4wMDMyMy0uMDUwNjYsMS41SDQzdi0zWiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTI1LDM4Yy0uNTA2MjksMC0xLjAwMzIzLS4wMjUyNy0xLjUtLjA1MDY2VjQzaDNWMzcuOTQ5MzRDMjYuMDAzMjMsMzcuOTc0NzMsMjUuNTA2MjksMzgsMjUsMzhaIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgICAgICAgICAgICA8cGF0aCBkPSJNMjUsMTJjLjUwNjI5LD'+
			'AsMS4wMDMyMy4wMjUyNywxLjUuMDUwNjZWN2gtM3Y1LjA1MDY2QzIzLjk5Njc3LDEyLjAyNTI3LDI0LjQ5MzcxLDEyLDI1LDEyWiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTEyLDI1YzAtLjUwNjI5LjAyNTI3LTEuMDAzMjMuMDUwNjYtMS41SDd2M2g1LjA1MDY2QzEyLjAyNTI3LDI2LjAwMzIzLDEyLDI1LjUwNjI5LDEyLDI1WiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgICA8L2c+DQogICAgICAgICAgPGNpcmNsZSBjeD0iMjUiIGN5PSIyNSIgcj0iMTAiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQogICAgICAg'+
			'IDwvZz4NCiAgICAgIDwvZz4NCiAgICA8L2c+DQogIDwvZz4NCjwvc3ZnPg0K';
		me.__hotspotpopupsvg__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;_hotspotpopupsvg;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="__hotspot-popup-svg";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me.__hotspotpopupsvg.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me.__hotspotpopupsvg.onmouseover=function (e) {
			me.__hotspotpopupsvg__img.style.visibility='hidden';
			me.__hotspotpopupsvg__imgo.style.visibility='inherit';
		}
		me.__hotspotpopupsvg.onmouseout=function (e) {
			me.__hotspotpopupsvg__img.style.visibility='inherit';
			me.__hotspotpopupsvg__imgo.style.visibility='hidden';
			me.__hotspotpopupsvg__imga.style.visibility='hidden';
		}
		me.__hotspotpopupsvg.onmousedown=function (e) {
			me.__hotspotpopupsvg__imga.style.visibility='inherit';
			me.__hotspotpopupsvg__imgo.style.visibility='hidden';
		}
		me.__hotspotpopupsvg.onmouseup=function (e) {
			me.__hotspotpopupsvg__imga.style.visibility='hidden';
			me.__hotspotpopupsvg__imgo.style.visibility='inherit';
		}
		me.__hotspotpopupsvg.ggUpdatePosition=function (useTransition) {
		}
		me.__hotspotpopup.appendChild(me.__hotspotpopupsvg);
		me.__div = me.__hotspotpopup;
	};
	function SkinHotspotClass_hotspotpopup(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspotpopup=document.createElement('div');
		el.ggId="hotspot-popup";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : 88px;';
		hs+='position : absolute;';
		hs+='top : 25px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotpopup.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me._hotspotpopup.onclick=function (e) {
			HideHotspot(this)
			var flag=me._hotspotpopupcontainer.ggOpacitiyActive;
			if (player.transitionsDisabled) {
				me._hotspotpopupcontainer.style[domTransition]='none';
			} else {
				me._hotspotpopupcontainer.style[domTransition]='all 500ms ease-out 0ms';
			}
			if (flag) {
				me._hotspotpopupcontainer.style.opacity='0';
				me._hotspotpopupcontainer.style.visibility='hidden';
			} else {
				me._hotspotpopupcontainer.style.opacity='1';
				me._hotspotpopupcontainer.style.visibility=me._hotspotpopupcontainer.ggVisible?'inherit':'hidden';
			}
			me._hotspotpopupcontainer.ggOpacitiyActive=!flag;
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotpopup.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotpopup.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			ShowHotspot(this)
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotpopup.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspotpopup.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspotpopupsvg=document.createElement('div');
		els=me._hotspotpopupsvg__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		me._hotspotpopupsvg__img.setAttribute('src',basePath + 'images/hotspotpopupsvg.svg');
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;hotspotpopupsvg;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._hotspotpopupsvg__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		me._hotspotpopupsvg__imgo.setAttribute('src',basePath + 'images/hotspotpopupsvg__o.svg');
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;hotspotpopupsvg;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._hotspotpopupsvg__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXRhcmdldDItYzAwPC90aXRsZT4NCiAgPGcgc3R5bGU9Imlzb2xhdGlvbjogaXNvbGF0ZSI+DQogICAgPGcgaWQ9ImZpbGwiPg0KICAgICAgPGc+DQogICAgICAgIDxnPg0KICAgICAgICAgIDxnPg0KICAgICAgICAgICAgPHBhdGggZD0iTTI1LDM4QTEzLDEzLDAsMSwxLDM4LDI1LDEzLjAxNDksMTMuMDE0OSwwLDAsMSwyNSwzOFptMC0yM0ExMC'+
			'wxMCwwLDEsMCwzNSwyNSwxMC4wMTE0NSwxMC4wMTE0NSwwLDAsMCwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICA8Zz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTM3Ljk0OTM0LDIzLjVjLjAyNTM5LjQ5Njc3LjA1MDY2Ljk5MzcxLjA1MDY2LDEuNXMtLjAyNTI3LDEuMDAzMjMtLjA1MDY2LDEuNUg0M3YtM1oiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICAgIDxwYXRoIGQ9Ik0yNSwzOGMtLjUwNjI5LDAtMS4wMDMyMy0uMDI1MjctMS41LS4wNTA2NlY0M2gzVjM3Ljk0OTM0QzI2LjAwMzIzLDM3Ljk3NDczLDI1LjUwNjI5LDM4LDI1LDM4'+
			'WiIgc3R5bGU9ImZpbGw6ICMwMDY0Y2MiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTI1LDEyYy41MDYyOSwwLDEuMDAzMjMuMDI1MjcsMS41LjA1MDY2VjdoLTN2NS4wNTA2NkMyMy45OTY3NywxMi4wMjUyNywyNC40OTM3MSwxMiwyNSwxMloiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMiwyNWMwLS41MDYyOS4wMjUyNy0xLjAwMzIzLjA1MDY2LTEuNUg3djNoNS4wNTA2NkMxMi4wMjUyNywyNi4wMDMyMywxMiwyNS41MDYyOSwxMiwyNVoiIHN0eWxlPSJmaWxsOiAjMDA2NGNjIi8+DQogICAgICAgICAgICA8L2c+DQogICAgICAgICAgPC9nPg'+
			'0KICAgICAgICAgIDxjaXJjbGUgY3g9IjI1IiBjeT0iMjUiIHI9IjEwIiBzdHlsZT0iZmlsbDogIzAwNjRjYztvcGFjaXR5OiAwLjMwMDAwMDAwMDAwMDAwMDA0Ii8+DQogICAgICAgIDwvZz4NCiAgICAgICAgPGcgc3R5bGU9Im9wYWNpdHk6IDAuMzAwMDAwMDAwMDAwMDAwMDQ7bWl4LWJsZW5kLW1vZGU6IHNjcmVlbiI+DQogICAgICAgICAgPGc+DQogICAgICAgICAgICA8cGF0aCBkPSJNMjUsMzhBMTMsMTMsMCwxLDEsMzgsMjUsMTMuMDE0OSwxMy4wMTQ5LDAsMCwxLDI1LDM4Wm0wLTIzQTEwLDEwLDAsMSwwLDM1LDI1LDEwLjAxMTQ1LDEwLjAxMTQ1LDAsMCwwLDI1LDE1WiIgc3R5bGU9ImZp'+
			'bGw6ICNmZmYiLz4NCiAgICAgICAgICAgIDxnPg0KICAgICAgICAgICAgICA8cGF0aCBkPSJNMzcuOTQ5MzQsMjMuNWMuMDI1MzkuNDk2NzcuMDUwNjYuOTkzNzEuMDUwNjYsMS41cy0uMDI1MjcsMS4wMDMyMy0uMDUwNjYsMS41SDQzdi0zWiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTI1LDM4Yy0uNTA2MjksMC0xLjAwMzIzLS4wMjUyNy0xLjUtLjA1MDY2VjQzaDNWMzcuOTQ5MzRDMjYuMDAzMjMsMzcuOTc0NzMsMjUuNTA2MjksMzgsMjUsMzhaIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgICAgICAgICAgICA8cGF0aCBkPSJNMjUsMTJjLjUwNjI5LD'+
			'AsMS4wMDMyMy4wMjUyNywxLjUuMDUwNjZWN2gtM3Y1LjA1MDY2QzIzLjk5Njc3LDEyLjAyNTI3LDI0LjQ5MzcxLDEyLDI1LDEyWiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgICAgPHBhdGggZD0iTTEyLDI1YzAtLjUwNjI5LjAyNTI3LTEuMDAzMjMuMDUwNjYtMS41SDd2M2g1LjA1MDY2QzEyLjAyNTI3LDI2LjAwMzIzLDEyLDI1LjUwNjI5LDEyLDI1WiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgICAgICAgIDwvZz4NCiAgICAgICAgICA8L2c+DQogICAgICAgICAgPGNpcmNsZSBjeD0iMjUiIGN5PSIyNSIgcj0iMTAiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQogICAgICAg'+
			'IDwvZz4NCiAgICAgIDwvZz4NCiAgICA8L2c+DQogIDwvZz4NCjwvc3ZnPg0K';
		me._hotspotpopupsvg__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;hotspotpopupsvg;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="hotspot-popup-svg";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotpopupsvg.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotpopupsvg.onmouseover=function (e) {
			me._hotspotpopupsvg__img.style.visibility='hidden';
			me._hotspotpopupsvg__imgo.style.visibility='inherit';
		}
		me._hotspotpopupsvg.onmouseout=function (e) {
			me._hotspotpopupsvg__img.style.visibility='inherit';
			me._hotspotpopupsvg__imgo.style.visibility='hidden';
			me._hotspotpopupsvg__imga.style.visibility='hidden';
		}
		me._hotspotpopupsvg.onmousedown=function (e) {
			me._hotspotpopupsvg__imga.style.visibility='inherit';
			me._hotspotpopupsvg__imgo.style.visibility='hidden';
		}
		me._hotspotpopupsvg.onmouseup=function (e) {
			me._hotspotpopupsvg__imga.style.visibility='hidden';
			me._hotspotpopupsvg__imgo.style.visibility='inherit';
		}
		me._hotspotpopupsvg.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotpopup.appendChild(me._hotspotpopupsvg);
		el=me._hotspotpopupcontainer=document.createElement('div');
		el.ggId="hotspot-popup-container";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 35px;';
		hs+='left : -25px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : 28px;';
		hs+='visibility : hidden;';
		hs+='width : 155px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspotpopupcontainer.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotpopupcontainer.onclick=function (e) {
			player.openUrl("..\/..\/html\/vh"+me.hotspot.url+".html","_parent");
		}
		me._hotspotpopupcontainer.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspotpopuptext=document.createElement('div');
		els=me._hotspotpopuptext__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="hotspot-popup-text";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 100px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="text";
		el.appendChild(els);
		me._hotspotpopuptext.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotpopuptext.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotpopupcontainer.appendChild(me._hotspotpopuptext);
		el=me._hotspotpopupurl=document.createElement('div');
		els=me._hotspotpopupurl__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="hotspot-popup-url";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 100px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="text";
		el.appendChild(els);
		me._hotspotpopupurl.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspotpopupurl.ggUpdatePosition=function (useTransition) {
		}
		me._hotspotpopupcontainer.appendChild(me._hotspotpopupurl);
		me._hotspotpopup.appendChild(me._hotspotpopupcontainer);
		me._hotspotpopuptext.ggText=me.hotspot.title;
		me._hotspotpopuptext.ggTextDiv.innerHTML=me._hotspotpopuptext.ggText;
		if (me._hotspotpopuptext.ggUpdateText) {
			me._hotspotpopuptext.ggUpdateText=function() {
				var hs=me.hotspot.title;
				if (hs!=this.ggText) {
					this.ggText=hs;
					this.ggTextDiv.innerHTML=hs;
					if (this.ggUpdatePosition) this.ggUpdatePosition();
				}
			}
		}
		if (me._hotspotpopuptext.ggUpdatePosition) {
			me._hotspotpopuptext.ggUpdatePosition();
		}
		me._hotspotpopuptext.ggTextDiv.scrollTop = 0;
		me._hotspotpopupurl.ggText=me.hotspot.url;
		me._hotspotpopupurl.ggTextDiv.innerHTML=me._hotspotpopupurl.ggText;
		if (me._hotspotpopupurl.ggUpdateText) {
			me._hotspotpopupurl.ggUpdateText=function() {
				var hs=me.hotspot.url;
				if (hs!=this.ggText) {
					this.ggText=hs;
					this.ggTextDiv.innerHTML=hs;
					if (this.ggUpdatePosition) this.ggUpdatePosition();
				}
			}
		}
		if (me._hotspotpopupurl.ggUpdatePosition) {
			me._hotspotpopupurl.ggUpdatePosition();
		}
		me._hotspotpopupurl.ggTextDiv.scrollTop = 0;
		me.__div = me._hotspotpopup;
	};
	me.addSkinHotspot=function(hotspot) {
		var hsinst = null;
		if (hotspot.skinid=='hotspot-script-p2') {
			hotspot.skinid = 'hotspot-script-p2';
			hsinst = new SkinHotspotClass_hotspotscriptp2(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		} else
		if (hotspot.skinid=='hotspot-script-p') {
			hotspot.skinid = 'hotspot-script-p';
			hsinst = new SkinHotspotClass_hotspotscriptp(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		} else
		if (hotspot.skinid=='__hotspot-popup') {
			hotspot.skinid = '__hotspot-popup';
			hsinst = new SkinHotspotClass__hotspotpopup(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		} else
		{
			hotspot.skinid = 'hotspot-popup';
			hsinst = new SkinHotspotClass_hotspotpopup(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		}
		return hsinst;
	}
	me.removeSkinHotspots=function() {
		if(hotspotTemplates['hotspot-script-p2']) {
			var i;
			for(i = 0; i < hotspotTemplates['hotspot-script-p2'].length; i++) {
				hotspotTemplates['hotspot-script-p2'][i] = null;
			}
		}
		if(hotspotTemplates['hotspot-script-p']) {
			var i;
			for(i = 0; i < hotspotTemplates['hotspot-script-p'].length; i++) {
				hotspotTemplates['hotspot-script-p'][i] = null;
			}
		}
		if(hotspotTemplates['__hotspot-popup']) {
			var i;
			for(i = 0; i < hotspotTemplates['__hotspot-popup'].length; i++) {
				hotspotTemplates['__hotspot-popup'][i] = null;
			}
		}
		if(hotspotTemplates['hotspot-popup']) {
			var i;
			for(i = 0; i < hotspotTemplates['hotspot-popup'].length; i++) {
				hotspotTemplates['hotspot-popup'][i] = null;
			}
		}
		hotspotTemplates = [];
	}
	me.addSkin();
	me.skinTimerEvent();
};