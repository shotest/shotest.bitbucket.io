var Eom = !!Eom?Eom:{};
Eom.Hotspot = function(svgelm,listelm){
	var rtn = {};
	var capNomn = {};
	var hspGrp, hspCrl, hspRct;
	var idRaw, idCur, idPrv = "";
	var listBk=listelm.parent();
	svgelm.children("g").each(function(){
		hspGrp = $(this);
		idRaw = hspGrp.attr("id");
		if(idRaw && idRaw.length >= 10){
			idCur = idRaw.substring(0,10);
			if(!(idPrv && idPrv == idCur)) capNomn[idCur] = [];
			capNomn[idCur].push([idRaw]);
			idPrv = idCur;
		}
		hspRct=hspGrp.children("*:not(circle)");
		hspRct
			.hide();
	});
	listelm.children("li").each(function(){
		$(this)
			.on("mouseover",function(ev){
				var curUl = $(ev.target).closest("li");
				ShowHide(curUl.attr("id").replace(/_caps$/,""));
			})
			.addFocus();
	});
	$("body").on("mouseout",function(ev){
		if($(ev.target).closest("li[data-href].selected").length<=0)ShowHide("");
	});

	function GetGroupId(event){
		return $(event.target).closest("g").attr("id").substring(0,10);
	}
	function GetListItem(id){
		if(typeof(id)!=="string"&&!!id&&!!id.target)id=GetGroupId(id);
		return listelm.find("li#"+id.substring(0,10));
	}
	function Jump(id){
		var tmpItm=GetListItem(id);
		if(tmpItm.length>0&&tmpItm.hasClass("selected"))location.href=tmpItm.data("href");
	}
	function ShowHide(id){
		if(typeof(id)!=="string"&&!!id&&!!id.target)id=GetGroupId(id);
		var grpItem, hspItem;
		for(var g in capNomn){
			grpItem = capNomn[g];
			for(var i in grpItem){
				hspItem=grpItem[i];
				svgelm.find("g#"+hspItem+">*:not(circle)").each(function(){SetSelection($(this), GetListItem(g), g==id);});
			}
		}
	}
	function SetSelection(svgobj, listitem, show){
		if(show){
			svgobj.attr("fill-opacity", "0.4").attr("stroke-opacity", "0.4").show();
			listitem.siblings().removeClass("selected").removeClass("movable");
			setTimeout(function(){listitem.addClass("selected");},Eom.CommonDelay);
			setTimeout(function(){listitem.addClass("movable");},Eom.CommonDelay*2);
		}else{
			svgobj.attr("fill-opacity", "0.0").attr("stroke-opacity", "0.0").hide();
			listitem.removeClass("selected").removeClass("movable");
		}
	}
	function OffsetHide(){
		setTimeout(ShowHide,Eom.CommonDelay);
	}

	rtn.Show = function(id){
		ShowHide(id);
		return rtn;
	};
	rtn.HideAll = function(){
		ShowHide("");
		return rtn;
	};

	var mrqRun = Eom.Marqee($("ul#captions>li"),"span.hotspotstrip",{onclickevent:function(mrqbox){if(mrqbox.hasClass("movable"))location.href=mrqbox.data("href");}}).Start();
	return rtn;
};