var Eom = !!Eom?Eom:{};
Eom.Toc = function(breadroot,listroot,toc,title){
	var allDim=Eom.Query["dim"],curDim="";
	var dimLen=!!allDim?allDim.length:0;
	var dimMod=dimLen % 4;
	if(dimMod!=0&&dimLen>4) allDim=allDim.substring(0,dimLen-dimMod);
	var prvToc={id:"",label:title,children:toc},curToc=null;
	var brdStr=[];
	for(var i=0;i<dimLen;i+=4){
		curDim+=allDim.substring(i*4,4);
		curToc=DigNode(prvToc,curDim);
		if(!!curToc) {
			brdStr.push({label:prvToc.label,link:SwapDimention(prvToc.id)});
			prvToc=curToc;
		}else{
			break;
		}
	}
	if(brdStr.length>0){
		brdStr.reverse();
		Eom.Header.BreadClumb.Load(brdStr);
	}

	var lstBox=$("<ul/>").addClass("simplelist");
	var FRM_ITM="<li data-href=\"{1}\" class=\"{2}\"><div class=\"list\"><span class=\"liststrip\">{0}</span></div></li>";
	var lstStr="";
	var tmpNod;
	var lnkFlg;
	var icoNam;
	for(var j in prvToc.children){
		tmpNod=prvToc.children[j];
		lnkFlg=typeof(tmpNod.link)!=="undefined";
		icoNam=lnkFlg?"icon-arrow-c":"icon-arrow-x";
		lstBox.append($(Itp.Common.Format(FRM_ITM,tmpNod.label,lnkFlg?tmpNod.link:SwapDimention(tmpNod.id),icoNam)).append(Eom.CreateIcon(icoNam)));
	}
	listroot.html("").append(lstBox);
	Eom.Header.Title=prvToc.label;

	function DigNode(node,id){
		for(var i in node.children) if(node.children[i].id==id)return node.children[i];
		return null;
	}
	function SwapDimention(dimention){
		var rgxDimQry=/(\bdim=)[a-z0-9]+/;
		var curPth=location.pathname;
		var curQry=location.search;
		return curPth+((rgxDimQry.test(curQry))?curQry.replace(rgxDimQry,"$1"+dimention):(curQry.length>0?curQry+"&dim=":"?dim=")+dimention);
	}
	
	var mrqRun=Eom.Marqee(listroot.find("li"),"span.liststrip",{onclickevent:function(mrqbox){location.href=mrqbox.data("href");}}).Start();
};

