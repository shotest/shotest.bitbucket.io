var Eom = !!Eom?Eom:{};
Eom.Marqee = function(marqeeboxelm,marqeeselector,option){
	var rtn={};
	if(typeof(option)==="undefined")option={};
	var clkFlg=$.isFunction(option.onclickevent);
	if(!clkFlg)option.onclickevent=function(){};
	if(typeof(option.interval)!=="number")option.interval=10;
	if(!(typeof(option.repeatonload)==="number"))option.repeatonload=0;
	option.suppressfocus=typeof(option.suppressfocus)!=="undefined"?!!option.suppressfocus:false;
	var mrqMgr=null;
	rtn.Start = function(){
		AttachMarqee();
		return rtn;
	};
	rtn.Stop = function(){
		if(!!mrqMgr)clearTimeout(mrqMgr);
		DetachMarqee();
		return rtn;
	};
	function AttachMarqee(){
		marqeeboxelm.each(function(){
			var mrqBox=$(this),mrqSpr=mrqBox.find(marqeeselector).eq(0),mrqPrt=mrqSpr.parent();
			if(mrqSpr.length<=0) return;
			function SetRunning(ev){mrqBox.addClass("running");}
			function UnsetRunning(ev){mrqBox.removeClass("running");mrqBox.removeAttr("data-repeatonload");}
			function Click(ev){if(!(mrqBox.hasClass("marqee")&&mrqBox.hasClass("surpressclick")))option.onclickevent(mrqBox);}
			mrqBox
				.off("click.marqee")
				.on("click.marqee",Click)
				.find(marqeeselector).eq(1).remove();
			if(clkFlg)mrqBox.addFocus().enableFocus(!option.suppressfocus);
			mrqPrt.css({height:mrqSpr.cssVal("lineHeight")+mrqPrt.cssVal("paddingTop")+mrqPrt.cssVal("paddingBottom"),whiteSpace:"nowrap",position:"relative",overflow:"hidden"});
			if(mrqSpr.width()>mrqPrt.width()){
				mrqBox
					.addClass("marqee")
					.addClass("surpressclick");
				mrqSpr
					.css({position:"absolute",left:mrqPrt.cssVal("paddingLeft")})
					.after(mrqSpr.clone());
				mrqBox.find(marqeeselector).eq(1).css({left:GetTotalWidth(mrqSpr)});
				if(option.repeatonload>-1){
					mrqBox
						.on("mouseover.marqee",SetRunning)
						.on("mouseout.marqee",UnsetRunning);
				}
				if(option.repeatonload!=0){
					mrqBox
						.attr({"data-repeatonload":option.repeatonload})
						.addClass("running");
				}
			}else{
				ClearMarqee(mrqBox,mrqPrt,mrqSpr);
			}
		});
		mrqMgr = setTimeout(RunMarqee,0);
	}
	function DetachMarqee(){
		marqeeboxelm.each(function(){
			var mrqBox=$(this),mrqSpr=mrqBox.find(marqeeselector).eq(0),mrqPrt=mrqSpr.parent();
			if(mrqSpr.length<=0) return;
			ClearMarqee(mrqBox,mrqPrt,mrqSpr);
		});
	}
	function ClearMarqee(box,spritparent,sprit){
		sprit.css({position:"",left:""});
		box
			.off("mouseover.marqee")
			.off("mouseout.marqee")
			.removeAttr("data-repeatonload")
			.removeClass("running")
			.removeClass("stopped")
			.removeClass("surpressclick")
			.removeClass("marqee");
		spritparent.html(sprit[0].outerHTML);
	}
	function RunMarqee(){
		clearTimeout(mrqMgr);
		marqeeboxelm.each(function(){
			var mrqBox=$(this);
			if(mrqBox.hasClass("marqee")){
				if(mrqBox.hasClass("running")){
					mrqBox
						.removeClass("stopped")
						.find(marqeeselector).each(function(){Proceed(mrqBox,$(this));});
				}else if(!mrqBox.hasClass("stopped")){
					mrqBox
						.addClass("stopped")
						.find(marqeeselector).each(function(i){Halt(mrqBox,$(this),i);});
				}
			}
		});
		mrqMgr = setTimeout(RunMarqee,option.interval);
	}
	function Proceed(box,strip){
		var mgnMax=GetTotalWidth(strip);
		var mgnCur=strip.cssVal("left");
		if(mgnCur<=(mgnMax*-1)+(strip.parent().cssVal("paddingLeft")*2)){
			if(option.repeatonload>0){
				var attRep=box.attr("data-repeatonload");
				if(!!attRep){
					var newRep=parseInt(attRep)-1;
					if(newRep>0){
						box.attr({"data-repeatonload":newRep});
					}else{
						box.trigger("mouseout");
						return;
					}
				}
			}
			strip.css({left:mgnMax});
		}else{
			strip.css({left:mgnCur-1});
		}
		if(box.hasClass("surpressclick")&&mgnCur<strip.cssVal("marginRight")*-1)box.removeClass("surpressclick");
	}
	function Halt(box,strip,index){
		strip.css({left:(GetTotalWidth(strip)*index)+strip.parent().cssVal("paddingLeft")});
		if(!box.hasClass("surpressclick"))box.addClass("surpressclick");
	}
	function GetTotalWidth(strip){
		return strip.grossWidth()+strip.cssVal("marginRight");
	}
	$(window).on("resize.marqee",AttachMarqee);
	return rtn;
};
