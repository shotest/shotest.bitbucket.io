var Eom = !!Eom?Eom:{};
(function(){
	var qryDic=Itp.Common.GetQueryDictionary();
	var isSdl=typeof(qryDic["sdl"])!=="undefined"&&qryDic["sdl"]!=0;
	var isPsudoSdl=!isSdl&&!!window.visualViewport&&window.visualViewport.height==416&&(window.visualViewport.width==800||window.visualViewport.width==720);
	Object.defineProperty(Eom,"Query",{
		get:function(){return qryDic;}
	});
	Object.defineProperty(Eom.Query,"Count",{
		enumerable: false,
		get:function(){var i=0;for(var ky in Eom.Query)i++;return i;}
	});
	Object.defineProperty(Eom,"IsSdl",{
		get:function(){return isSdl||isPsudoSdl;}
	});
	if(Eom.IsSdl)$("html").addClass("sdl");
	if(isPsudoSdl)$("html").addClass("psudo");

	Eom.Setting=(function(){
		var hstStg=localStorage.getItem("setting.eom.toyota.jp");
		var rtn=!!hstStg?JSON.parse(hstStg):{fs:/*0*/1};
		rtn.Save=function(){
			localStorage.setItem("setting.eom.toyota.jp",JSON.stringify(rtn));
		};
		if(!hstStg)rtn.Save();
		return rtn;
	})();
	Eom.Magnify=function(level){
		$("html").removeClass("magni01").removeClass("magni02").removeClass("magni03");
		if(Eom.IsSdl){
			if(!level)level=Eom.Setting.fs;
			switch(level){
				case 1:
					$("html").addClass("magni01");
					break;
				case 2:
					$("html").addClass("magni02");
					break;
				case 3:
					$("html").addClass("magni03");
					break;
				default:
					break;
			}
		}
	};
	Eom.Magnify();

	var isMob=((navigator.userAgent.indexOf('iPhone') > -1 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > -1 || navigator.userAgent.indexOf('Android') > -1);
	var isIos=navigator.userAgent.indexOf('iPhone') > -1 || navigator.userAgent.indexOf('iPad') > -1 || navigator.userAgent.indexOf('iPod') > -1;
	var isAnd=navigator.userAgent.indexOf('Android') > -1;
	if(isIos)$("html").addClass("ios");
	if(isAnd)$("html").addClass("android");
	var dlyMsc=isIos ? 150 : 50;
	Object.defineProperty(Eom,"IsMobile",{
		get:function(){return isMob;}
	});
	Object.defineProperty(Eom,"IsIos",{
		get:function(){return isIos;}
	});
	Object.defineProperty(Eom,"IsAndroid",{
		get:function(){return isAnd;}
	});
	Object.defineProperty(Eom,"CommonDelay",{
		get:function(){return dlyMsc;}
	});

	Object.defineProperty(Eom,"Publication",{
		get:function(){
			var tmpPth=location.pathname.replace(/\/html\/[^\/]+\.html$/i,"");
			var segTxt=tmpPth.replace(/^\u002f/,"").replace(/\u002f$/,"");
			var segUrl=segTxt.split("/");
			return {
				path:tmpPth,
				fullId:segTxt.toLowerCase().split("/").reverse().join("."),
				bland:segUrl[0].toUpperCase(),
				line:decodeURIComponent(segUrl[1]).toUpperCase(),
				type:decodeURIComponent(segUrl[2]).toUpperCase(),
				year:(function(){var y=segUrl[3].substring(0,2);return (/^[89][0-9]$/.test(y)?"19":"20")+y;})(),
				month:segUrl[3].substring(3,2)+"",
				language:segUrl[5].toLowerCase()
			};
		}
	});
	var EXPLIM=60;
	var expDat=new Date();
	expDat=expDat.setDate(expDat.getDate()+EXPLIM);
	localStorage.setItem(Eom.Publication.language + ".previouspub.eom.toyota.jp", JSON.stringify({dt:expDat,pub:Eom.Publication.path+"/html/top.html"}));
	
	var PRVPAGESTR=Eom.Publication.language + ".previouspage.eom.toyota.jp";
	var prvPag=sessionStorage.getItem(PRVPAGESTR);
	Object.defineProperty(Eom,"PreviousPage",{
		get:function(){return prvPag;}
	});
	setTimeout(function(){sessionStorage.setItem(PRVPAGESTR,location.href);},100);

	Eom.CreateIcon = function(name,size){
		return $("<img/>").addClass("icon " + name + (!!size?" "+size:"")).attr({src:Eom.pathIco+name+".svg"})
	};
	var itpSfx="ishidataiseisha_appreq=1";
	Eom.ApplyLink=function(text){
		var rtn=text;
		rtn=rtn.replace(/http(s?:\/\/[0-9a-zA-Z_\.\-%\+=\/]+\?[0-9a-zA-Z_\-\+=%&]+)/ig,"<a href=\"[#http#]$1&"+itpSfx+"\" class=\"smplink\">[#http#]$1</a>");
		rtn=rtn.replace(/http(s?:\/\/[0-9a-zA-Z_\.\-%\+=\/]+)/ig,"<a href=\"[#http#]$1?"+itpSfx+"\" class=\"smplink\">[#http#]$1</a>");
		return rtn.replace(/\[#http#\]/ig,"http");
	};
})();
Object.defineProperty(Eom,"pathImg",{get:function(){return "../img/";}});
Object.defineProperty(Eom,"pathIco",{get:function(){return "../ico/";}});
Object.defineProperty(Eom,"pathMov",{get:function(){return "../movie/";}});
Object.defineProperty(Eom,"pathAud",{get:function(){return "../sound/";}});
