Eom.Modal=(function(){
	var rtn={IgnoreError:false,Duration:2000};
	var mdlCvr=null,mdlPop=null,mdlCmd=null;
	var sdlScr=null;
	rtn.showModalDialog=function(title,body,button1,button2,texttype) {
		Open(title, body);
		if(!!button1){
			AppendButton({value:button1,evhandler:!!texttype?inputResult:notifyResult},!!button2?{value:button2,evhandler:!!texttype?inputResult:Cancel}:undefined);
		}else{
			setTimeout(function(){mdlCvr.animate({opacity:0},500);mdlPop.animate({opacity:0},500,Close);},rtn.Duration);
		}
		if(!!mdlCmd&&!!texttype)mdlCmd.before($("<div class=\"mic_modaltextbox\"></div>").append($("<input/>").attr({type:(texttype=="password"||texttype=="pw"?"password":"text")}).addFocus()));
		function inputResult() {
			var txtElm=mdlPop.find("input");
			var resVal=!!txtElm&&txtElm.length>0?txtElm.val():"";
			rtn.trigger("default",resVal);
			if(Eom.IsIos){
				try{
					webkit.messageHandlers.inputResult.postMessage(resVal);
				}catch(ex){
					if(!rtn.IgnoreError)rtn.trigger("error",{type:"ios_emulator"});
				}
			}else if(Eom.IsAndroid){
				try{
					android.inputResult(resVal);
				}catch(ex){
					if(!rtn.IgnoreError)rtn.trigger("error",{type:"android_emulator"});
				}
			}else{
			
			}
			Close();
		}
		function notifyResult(event) {
			var resVal=$(event.target).data("value");
			rtn.trigger(resVal);
			if(Eom.IsIos){
				try{
					webkit.messageHandlers.dialogResult.postMessage(resVal);
				}catch(ex){
					if(!rtn.IgnoreError)rtn.trigger("error",{type:"ios_emulator"});
				}
			}else if(Eom.IsAndroid){
				try{
					android.dialogResult(resVal);
				}catch(ex){
					if(!rtn.IgnoreError)rtn.trigger("error",{type:"android_emulator"});
				}
			}else{
			
			}
			Close();
		}
		return mdlPop;
	}
	function Default(){
		Close();
		rtn.trigger("default");
	}
	function Cancel(){
		Close();
		rtn.trigger("cancel");
	}

	rtn.showModalReader=function(title,body,button1,button2){
		if($.isFunction(body)){
			Open(title);
			body(function(ev1,ev2){
				AddButton();
				mdlPop.find("div.mic_modalbody>div.bodybox>span").html(Sanitize(ev2.text,ev2.callback));
				mdlPop.find("a.smplink").addFocus();
				if(Eom.IsSdl)AddSdlScrollbar();
				if(!!sdlScr&&$.isFunction(sdlScr.Reset))sdlScr.Reset();
			});
		}else{
			Open(title,body);
			AddButton();
			if(Eom.IsSdl)AddSdlScrollbar();
		}
		function AddButton(){
			if(!!button1){
				AppendButton({value:button1,evhandler:Default},(!!button2?{value:button2,evhandler:Cancel}:undefined));
			}else{
				var clsBtn=$("<span class=\"closer\"></span>").addFocus();
				mdlPop.append(clsBtn);
				clsBtn.on("click",Close);
			}
		}
		function AddSdlScrollbar(){
			sdlScr=Eom.CreateSdlScrollbar($("<div/>").addClass("sdlscrollbarbox"));
			mdlPop.find("div.mic_modalbody").append(sdlScr);
			var bdyBox=mdlPop.find("div.mic_modalbody>div.bodybox");
			bdyBox.addClass("scrollable");
			sdlScr.Reset(bdyBox);
		}
		mdlPop.addClass("reader");
		return mdlPop;
	};
	rtn.Scroll=function(val){
		if(!!sdlScr&&$.isFunction(sdlScr.Scroll))sdlScr.Scroll(val);
	};

	function Open(title,body){
		Close();
		mdlPop=$(Itp.Common.Format("<div class=\"mic_modaldialog\"><div class=\"mic_modaltitle\"><span>{0}</span></div><div class=\"mic_modalbody\"><div class=\"bodybox\"><span>{1}</span></div></div></div>",!!title?Sanitize(title):"",!!body?Sanitize(body):""));
		mdlCvr=$("<div class=\"mic_modalcover\"></div>");
		$("body").append(mdlCvr).append(mdlPop);
		mdlPop.find("a.smplink").addFocus();
	}
	function AppendButton(button1,button2){
		mdlCmd=$("<div class=\"mic_modalbuttonbox\"></div>");
		mdlPop.append(mdlCmd);
		mdlPop.find("div.mic_modalbody").addClass("hasbutton");
		mdlCmd.append(CreateButton("default",button1.value,button1.evhandler));
		if(!!button2)mdlCmd.append(CreateButton("cancel",button2.value,button2.evhandler));
	}
	function CreateButton(name,value,evhandler){
		var rtn=$(Itp.Common.Format("<span type=\"button\" class=\"mic_modalbutton\" data-value=\"{1}\">{0}</span>",value,name));
		rtn.on("click",evhandler);
		return rtn.addFocus();
	}
	function Close(){
		if(!!mdlCvr){mdlCvr.remove();mdlCvr=null;}
		if(!!mdlPop){mdlPop.remove();mdlPop=null;}
		if(!!mdlCmd){mdlCmd.remove();mdlCmd=null;}
		if(!!sdlScr)sdlScr=null;
	};
	function Sanitize(value,overridecallback){
		var tmpStr=value.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\r\n/g,"<br/>").replace(/\r/g,"<br/>").replace(/\n/g,"<br/>");
		return $.isFunction(overridecallback)?overridecallback(tmpStr):tmpStr;
	}
	Object.defineProperty(rtn,"Visible",{
		get:function(){return $("body>.mic_modaldialog").length>0;}
	});

	if(Eom.IsSdl){
		$(function(){
			HeadUnit.Rectangle
				.bind(Eom.Header,function(){return !rtn.Visible;})
				.bind(Eom.Footer,function(){return !rtn.Visible;})
				.bind(Eom.SdlScrollbar,function(){return !rtn.Visible;})
				.bind(Eom.Main,function(){return !rtn.Visible;});
		});
	}

	var evtItm={};
	rtn.on=function(name,callback){
		if(!evtItm[name])evtItm[name]=[];
		evtItm[name].push(callback);
		return rtn;
	};
	rtn.off=function(name){
		delete evtItm[name];
	};
	rtn.trigger=function(name,arg){
		if(!!evtItm[name]){
			for(var i in evtItm[name]){
				evtItm[name][i](null,arg);
			}
		}
		return rtn;
	};

	return rtn;
})();
