var Eom = !!Eom?Eom:{};
Eom.Init=(function(){
	var rtn={};
	rtn.Header=function(){
		Eom.Header = (function(){
			var rtn2=$("header");
			var ttlBox=rtn2.find("div.titlebox");
			var ttlCol=ttlBox.children("div.title");
			var cmdRtn=ttlBox.children("div.return").addFocus();
			var cmdMnu=ttlBox.children("div.menu").addFocus();
			cmdMnu.Opener=cmdMnu.children("img.icon-menu-open");
			cmdMnu.Closer=cmdMnu.children("img.icon-menu-close");
			var brdClm=rtn2.find("ul.breadclumb");

			var mrqBrd={Start:function(){},Stop:function(){}},mrqHbg=null,mrqTtl=null;
			var ttlClk=function(){};
			function ClickTitle(){
				ttlClk();
			}
			ttlCol.Open=function(){
				brdClm.show();
				ttlCol.addClass("open");
				mrqBrd.Start();
				return ttlCol;
			};
			ttlCol.Close=function(){
				brdClm.hide();
				ttlCol.removeClass("open");
				ttlCol.trigger("mouseout");
				mrqBrd.Stop();
				return ttlCol;
			};
			ttlCol.Load = function(list){
				brdClm.html("");
				if(list.length>0){
					for(var i in list){
						brdClm.append($("<li/>").append(Eom.CreateIcon("icon-arrow-y"))).append($("<li/>").addClass("labelbox").attr({"data-href":list[i].link}).append($("<span class=\"breadstrip\"/>").html(list[i].label)));
					}
					ttlClk=function(){
						if(rtn2.BreadClumb.Visible){
							ttlCol.Close();
						}else{
							ttlCol.Open();
						}
					};
					mrqBrd = Eom.Marqee(brdClm.find("li.labelbox"),"span.breadstrip",{repeatonload:1,onclickevent:function(mrqbox){var tmpRef=mrqbox.data("href");if(!!tmpRef&&!/^#/.test(tmpRef))location.href=tmpRef;}});
					if(!!mrqTtl)mrqTtl.Stop();
					mrqTtl = Eom.Marqee(ttlCol,"span.titlestrip",{repeatonload:1,onclickevent:ClickTitle});
					mrqTtl.Start();
				}else{
					ttlClk=function(){};
					mrqTtl = Eom.Marqee(ttlCol,"span.titlestrip",{repeatonload:1});
				}
				ttlCol.trigger("load",{list:list});
				return ttlCol;
			};
			Object.defineProperty(rtn2,"BreadClumb",{
				get:function(){return ttlCol;},
			});
			Object.defineProperty(rtn2.BreadClumb,"Visible",{
				get:function(){return ttlCol.hasClass("open");},
			});

			rtn2.Menu=rtn2.find("div.menu>div.menugroup>ul");
			mrqHbg = Eom.Marqee(rtn2.find("div.menu").find("li.labelbox"),"span.menustrip",{repeatonload:1,onclickevent:function(mrqbox){var tmpRef=mrqbox.data("href");if(!!tmpRef&&!/^#/.test(tmpRef))location.href=tmpRef;}});
			function ShowMenu(){
				if(rtn2.hasClass("showmenu")) {
					rtn2.removeClass("showmenu");
					mrqHbg.Stop();
				}else{
					rtn2.addClass("showmenu");
					mrqHbg.Start();
				}
			};
			cmdMnu.on("click",ShowMenu);
			rtn2.Menu.Show=function(){
				if(!rtn2.hasClass("showmenu")) {
					rtn2.addClass("showmenu");
					mrqHbg.Start();
				}
			};
			rtn2.Menu.Hide=function(){
				if(rtn2.hasClass("showmenu")) {
					rtn2.removeClass("showmenu");
					mrqHbg.Stop();
				}
			};

			mrqTtl = Eom.Marqee(ttlCol,"span.titlestrip",{repeatonload:1});
			Object.defineProperty(rtn2,"Title",{
				get:function(){return ttlCol.find("span.titlestrip").html();},
				set:function(value){ttlCol.find("span.titlestrip").eq(0).html(value);if(!!value){mrqTtl.Start();}else{mrqTtl.Stop();}}
			});
			if(!!rtn2.Title)mrqTtl.Start();
			
			cmdRtn.on("click",function(){if(rtn2.hasClass("showmenu"))ShowMenu();else history.go(-1);});

			rtn2.find("a[href]:not([href='#'])").each(function(){$(this).addFocus()});
			return rtn2;
		})();
	};

	rtn.Footer=function(){
		Eom.Footer = (function(){
			var rtn2=$("footer");
			rtn2.BottomMenu=rtn2.find("ul.footmenu");
			rtn2.SideMenu=rtn2.find("div.sidemenu");
			rtn2.SideMenu.Main=rtn2.SideMenu.find("div.envelope>ul.main");
			rtn2.SideMenu.Sub=rtn2.SideMenu.find("div.envelope>ul.sub");
			var mrqFootMjr = Eom.Marqee(rtn2.SideMenu.Main.find("li.labelbox"),"span.menustrip",{onclickevent:function(mrqbox){var tmpRef=mrqbox.data("href");if(!!tmpRef&&!/^#/.test(tmpRef)&&(mrqbox.hasClass("selected")||mrqbox.siblings(".selected").length>0))location.href=mrqbox.data("href");},suppressfocus:true}).Start();
			var mrqFootMno = Eom.Marqee(rtn2.SideMenu.Sub.find("li.labelbox"),"span.menustrip",{onclickevent:function(mrqbox){HideMenu();var tmpRef=mrqbox.data("href");if(!!tmpRef&&!/^#/.test(tmpRef))location.href=tmpRef;},suppressfocus:true}).Start();
			rtn2.SideMenu.on("mouseover",ShowMenu);
			$("body").on("mouseover",HideMenu);
			Object.defineProperty(rtn2,"Visible",{
				get:function(){return rtn2.hasClass("selected");}
			});
			function ShowMenu(event){
				setTimeout(function(){if(!rtn2.Visible){rtn2.addClass("selected");}},Eom.CommonDelay);
			}
			function HideMenu(event){
				if(!(!!event&&$(event.target).closest("footer").length>0))setTimeout(function(){rtn2.removeClass("selected");},Eom.CommonDelay);
			}
			setTimeout(function(){rtn2.addClass("hidable");rtn2.focusable("li").each(function(){$(this).enableFocus(true);});},Eom.CommonDelay);
			rtn2.find("a[href]:not([href='#'])").each(function(){$(this).addFocus()});
			return rtn2;
		})();
	};

	rtn.Main = function(){
		Eom.Main = (function(){
			var rtn2=$("div.scrollable");
			rtn2.find("a[href]:not([href='#'])").each(function(){$(this).addFocus()});
			return rtn2
		})();

		Eom.Header.find("li").each(function(){
			$(this).on("mouseover",SetSelection);
		});
		Eom.Footer.SideMenu.Main.find("li").each(function(){
			$(this).on("mouseover",SetSelection);
		});
		Eom.Footer.SideMenu.Sub.find("li").each(function(){
			$(this).on("mouseover",SetSelection);
		});
		$("body").on("mouseover",UnsetSelection);
		function SetSelection(event){
			var curItm=$(event.target).closest("li");
			setTimeout(function(){ClearSelection();curItm.addClass("selected");},Eom.CommonDelay);
		}
		function UnsetSelection(event){
			if($(event.target).closest("li.labelbox").length<=0)setTimeout(function(){ClearSelection();},Eom.CommonDelay);
		}
		function ClearSelection(){
			Eom.Header.find("li").each(function(){$(this).removeClass("selected");});
			Eom.Footer.SideMenu.Main.find("li").each(function(){$(this).removeClass("selected");});
			Eom.Footer.SideMenu.Sub.find("li").each(function(){$(this).removeClass("selected");});
		}

		Eom.Header.BreadClumb.on("load",function(ev1,ev2){
			Eom.Main.off("mouseover.bread");
			if(ev2.list.length>0)Eom.Main.on("mouseover.bread",Eom.Header.BreadClumb.Close);
		});

		if(Eom.IsSdl){
			if(!!Eom.SdlScrollbar)Eom.SdlScrollbar.Reset(Eom.Main);
			HeadUnit.Rectangle
				.bind(Eom.Main,function(rawelm){return !Eom.Header.BreadClumb.Visible&&!Eom.Footer.Visible&&Eom.SdlScrollbar.CheckPosition(rawelm);})
				.bind(Eom.SdlScrollbar,function(rawelm){return !Eom.Header.BreadClumb.Visible;})
				.bind(Eom.Footer,function(rawelm){return !Eom.Header.BreadClumb.Visible;})
				.Start();
		}

	};
	return rtn;
})();
