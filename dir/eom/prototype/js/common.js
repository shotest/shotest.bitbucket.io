var Itp = !!Itp?Itp:{};
Itp.Common = (function(){
	jQuery.fn.extend({
		cssVal: function (cssname) {
			var jqThis = jQuery(this);
			var cssTmp = jqThis.css(cssname);
			var rtnTmp = cssTmp ? (typeof cssTmp === "string" ? parseInt(cssTmp.replace(/^(-?[0-9]+)px$/, "$1")) : cssTmp) : 0;
			return rtnTmp ? rtnTmp : 0;
		}
	});
	jQuery.fn.extend({
		grossHeight: function (countmargin) {
			var jqThis = jQuery(this);
			return jqThis.height() + jqThis.cssVal("paddingTop") + jqThis.cssVal("paddingBottom") + jqThis.cssVal("borderTopWidth") + jqThis.cssVal("borderBottomWidth") + (!!countmargin ? jqThis.cssVal("marginTop") + jqThis.cssVal("marginBottom") : 0);
		},
		grossWidth: function (countmargin) {
			var jqThis = jQuery(this);
			return jqThis.width() + jqThis.cssVal("paddingLeft") + jqThis.cssVal("paddingRight") + jqThis.cssVal("borderLeftWidth") + jqThis.cssVal("borderRightWidth") + (!!countmargin ? jqThis.cssVal("marginLeft") + jqThis.cssVal("marginRight") : 0);
		},
	});

	var rtn={};
	rtn.Format = function () {
		var s = arguments[0];
		var reg;
		for (var i = 0; i < arguments.length - 1; i++) {
			reg = new RegExp("\\{" + i + "\\}", "gm");
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
	rtn.Random = function (len) {
		var rtn2 = "";
		for (i = 0; i < len; i++) {
			var digTyp = parseInt(Math.random() * 3);
			var digIdx = parseInt(Math.random() * (digTyp == 0 ? 10 : 26));
			var digSft = digTyp == 0 ? 48 : (digTyp == 1 ? 65 : 97);
			rtn2 += String.fromCharCode(digIdx + digSft);
		}
		return rtn2;
	};
	rtn.GetQueryDictionary = function(url_with_query){
		if(typeof(url_with_query)!=="string")url_with_query=location.search;
		var qI=(url_with_query.indexOf("?")>-1?url_with_query.split("?")[1]:url_with_query).split("&");
		var rtn2={};
		var tmp;
		for(var i in qI){
			tmp=qI[i].split("=");
			if(tmp.length>1&&tmp[0]!=="")rtn2[tmp[0]]=decodeURI(tmp[1]);
		} 
		return rtn2;
	};
	rtn.UpdateQuery = function (query, url) {
		if (!url) url = location.href;
		return ReplaceQuery(query, url, true);
	};
	rtn.AppendQuery = function (query, url) {
		if (!url) url = location.href;
		return ReplaceQuery(query, url, false);
	};
	function ReplaceQuery(query, url, ignorenew) {
		if (!url) url = location.href;
		var curQry = rtn.GetQueryDictionary(url);
		var tmpQry = {};
		var rtnPth = url.split("?")[0];
		var rtnQry = "";
		for (var ky in curQry) tmpQry[ky] = typeof (query[ky]) !== "undefined" ? query[ky] : curQry[ky];
		for (var ky in query) if (!ignorenew || typeof (tmpQry[ky]) !== "undefined") tmpQry[ky] = query[ky];
		for (var ky in tmpQry) rtnQry += ky + "=" + encodeURI(tmpQry[ky]) + "&";
		return rtnPth + (rtnQry ? "?" + rtnQry.replace(/&$/, "") : "");
	}
	Object.defineProperty(rtn,"IsPortrait",{
		get:function(){return $(window).height()>$(window).width();}
	});
	return rtn;
})();
