var Eom = !!Eom?Eom:{};
Eom.Init=!!Eom.Init?Eom.Init:{};
Eom.Init.ImageFitter=function(widthmargincallback,heightmargincallback){
	Eom.ImageFitter=(function(){
		var rtn={};
		var mainW=0,mainH=0;
		rtn.GetMainDimension=function(){
			mainW=$(window).width()-widthmargincallback();
			mainH=$(window).height()-heightmargincallback();
		}
		rtn.WatchImageSize=function(elm,allowoversize){
			elm.css({visibility:"hidden"});
			if(!!elm.attr("width")&&!elm.data("orgw"))elm.attr({"data-orgw":elm.attr("width")});
			if(!!elm.attr("height")&&!elm.data("orgh"))elm.attr({"data-orgh":elm.attr("height")});
			var scrSiz={sdl:Eom.IsSdl&&!Itp.Common.IsPortrait,x:mainW};
			scrSiz.y=mainH*(scrSiz.sdl?0.7:1);
			if(elm.width()>0){
				ResizeFigure(elm,scrSiz);
			}else{
				var imgDsp=elm.css("display")||"inherit";
				elm.css({display:"none"}).on("load",function(ev){
					elm.css({display:imgDsp});
					elm.attr({"data-orgw":elm.width()});
					elm.attr({"data-orgh":elm.height()});
					ResizeFigure(elm,scrSiz,allowoversize);
				});
			}
		};
		function ResizeFigure(elm,clientsize,allowoversize){
			elm.attr({"width":elm.data("orgw")});
			elm.attr({"height":elm.data("orgh")});
			if(!!clientsize.sdl){
				setTimeout(function(){Resize(elm,clientsize);},Eom.CommonDelay);
			}else{
				Resize(elm,clientsize,allowoversize);
			}
		}
		function Resize(elm,clientsize,allowoversize){
			var maxWdt=(function(){var fW=elm.parent().width();hW=clientsize.x;return fW<hW?fW:hW;})();
			var maxHgt=(function(){var fH=elm.height();dH=clientsize.y;return fH<dH?fH:dH;})();
			var ratWdt=elm.width()/maxWdt;
			var ratHgt=elm.height()/maxHgt;
			var isVert=elm.height()>elm.width();
			var zoomRt=Itp.Common.IsPortrait?ratWdt:(ratWdt>ratHgt||isVert?ratWdt:ratHgt);
			if(zoomRt>1||allowoversize){
				elm.attr("width",elm.width()/zoomRt);
				elm.attr("height",elm.height()/zoomRt);
			}
			elm.css({visibility:""});
		}
		var slcLst={};
		rtn.bind=function(selector,allowoversize){
			if(typeof(allowoversize)==="undefined")allowoversize=false;
			if(!slcLst[selector]){
				rtn.WatchImageSize($(selector),allowoversize);
				slcLst[selector]=allowoversize;
			}
		};
		function ResizeEachImage(){
			rtn.GetMainDimension();
			for(var slc in slcLst)$(slc).each(function(){rtn.WatchImageSize($(this),slcLst[slc]);});
		}

		rtn.InitImageSize=function(elm){
			if(elm.attr("width")==0&&elm.attr("height")==0&&!!elm.data("orgw")&&!!elm.data("orgh")){
				elm.attr({"width":elm.data("orgw"),"height":elm.data("orgh")});
				elm.removeAttr("data-orgw").removeAttr("data-orgh");
			}
			return elm;
		};

		$(window).on("resize",ResizeEachImage);
		rtn.GetMainDimension();

		return rtn;
	})();
};