var Eom = !!Eom?Eom:{};
Eom.SignManager=function(tabelm){
	var rtn={};
	var strNam="sign."+Eom.Publication.fullId+".eom.toyota.jp";
	var lstTab=tabelm;
	var hstTab=sessionStorage.getItem(strNam)||lstTab.eq(0).data("tabindex");
	sessionStorage.removeItem(strNam);

	lstTab.click(function(e){
		var curElm=$(e.target);
		if(!curElm.hasClass("selected"))rtn.Select(curElm.data('tabindex'));
	});

	rtn.Select=function(tabindex){
		if(!tabindex)tabindex=lstTab.eq(0).data("tabindex");
		hstTab=tabindex;
		sessionStorage.setItem(strNam,hstTab);
		lstTab.addFocus().removeClass("selected");
		$(Itp.Common.Format("a[data-tabindex='{0}']",tabindex)).removeFocus().addClass("selected");
		$(Itp.Common.Format(".iconlist:not([id='{0}'])",tabindex)).addClass("hide");
		$('#'+tabindex).removeClass("hide");
		rtn.trigger("select");
	};

	rtn.Center=function(){
		$("div.iconbox").css({width:""});
		var prvLim,curLim,maxLim=-1;
		$("div.iconbox ul").each(function(){
			prvLim=-1;
			curLim=0;
			$(this).children("li").each(function(){
				curLim=(this.offsetLeft||0)+$(this).width()+5;
				if(curLim<prvLim)return false;
				prvLim=curLim;
			});
			if(prvLim>maxLim)maxLim=prvLim;
		});
		if(maxLim>0)$("div.iconbox").width(maxLim);
		$("div.iconbox").removeClass("invisible");
	};

	var evtItm={};
	rtn.on=function(name,callback){
		if(!evtItm[name])evtItm[name]=[];
		evtItm[name].push(callback);
		return rtn;
	};
	rtn.trigger=function(name,arg){
		if(!!evtItm[name]){
			for(var i in evtItm[name]){
				evtItm[name][i](null,arg);
			}
		}
		return rtn;
	};

	rtn.Select(hstTab);

	return rtn;
};