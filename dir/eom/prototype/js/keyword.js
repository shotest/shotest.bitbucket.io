var Eom = !!Eom?Eom:{};
Eom.KeywordSearcher=function(formelm,listelm){
	var rtn={Form:formelm,List:listelm,CountFormat:"Result count: {0}"};
	var strNam="keyword."+Eom.Publication.fullId+".eom.toyota.jp";
	var hstStg=sessionStorage.getItem(strNam);
	sessionStorage.removeItem(strNam);
	var hstDic=!!hstStg?JSON.parse(hstStg):{k:"",f:0,p:0};

	var kwdTxt=$("<input/>").attr({type:"text"}).addFocus();
	var kwdDel=$("<img/>").addClass("icon icon-circle-x tiny").attr({src:"../ico/icon-circle-x.svg"}).addFocus();
	var kwdCmd=$("<img/>").addClass("icon icon-send").attr({src:"../ico/icon-send.svg"}).addFocus();
	rtn.Form.append($("<img/>").addClass("icon icon-search").attr({src:"../ico/icon-search.svg"})).append(kwdTxt).append(kwdDel).append(kwdCmd);

	var kwdCnt=$("<div/>").addClass("rescount");
	var kwdRes=$("<ul/>");
	rtn.List.append(kwdCnt).append(kwdRes);
	var kwdSgt;
	var pagInf={sgt:{from:0,page:0},src:{from:0,page:0}};

	function Sanitize(value){
		return value.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
	}
	function Contaminate(value){
		return value.replace(/&gt;/g,">").replace(/&lt;/g,"<").replace(/&amp;/g,"&");
	}

	rtn.Init = function(keyword){
		kwdTxt.val(Contaminate(keyword));
		kwdCnt.html("").hide();
		return rtn;
	};

	if(Eom.IsSdl){
		kwdSgt={Init:function(){},Clear:function(){},hide:function(){},show:function(){}};
		rtn.Peek = function(){return rtn;};
		rtn.LoadSuggestion = function(json){return rtn;};
	}else{
		kwdSgt=$("<div/>").addClass("suggestbox");
		kwdSgt.Clear=function(){kwdSgt.html("");kwdCnt.html("").hide();};
		rtn.Form.append(kwdSgt);
		rtn.Peek = function(text){
			var txtVal=typeof(text)==="string"?text:kwdTxt.val();
			if(!!txtVal){
				var chkGtm=null;
				function WatchGtm(){
					if(!!Eom.Gtm.ElasticSearcher){
						GtmPeek();
					}else{
						chkGtm=setTimeout(WatchGtm,100);
					}
				}
				chkGtm=setTimeout(WatchGtm,100);
				function GtmPeek(){
					Eom.Gtm.ElasticSearcher.Peek(
						txtVal,
						{
							done:rtn.LoadSuggestion,
							fail:function(res){rtn.trigger("error",{code:-1,message:"ｷﾞｬｧｧｧｧｧ :';(iiﾟ∀ﾟ).;'",payload:res});}
						},
						pagInf.sgt
					);
				}
			}else{
				rtn.Clear();
			}
			return rtn;
		};
		rtn.LoadSuggestion = function(json){
			kwdSgt.Clear();
			var sgtLst=$("<ul/>");
			kwdSgt.append(sgtLst);
			if(!!json&&!!json.items&&$.isArray(json.items)){
				for(var i in json.items){
					sgtLst.append(CreateSuggestItem(json.items[i]));
				}
			}
			return rtn;
		};
		function CreateSuggestItem(item){
			return $("<li/>").attr({"data-score":item.score}).html(Sanitize(item.label)).on("click",ApplyKeyword)
		}
		function ApplyKeyword(ev){
			kwdTxt.val(Contaminate($(ev.target).html().replace(/<span(?: +[^>]+)?>/g,"").replace(/<\/span>/g,"")));
			kwdSgt.Clear();
			rtn.Search();
		}

		var prvTxt="";
		var wtcKey=null;
		function WatchKeyword(){
			clearTimeout(wtcKey);
			if(kwdTxt.val()!=prvTxt)rtn.Peek();
			prvTxt=kwdTxt.val();
			wtcKey=setTimeout(WatchKeyword,50);
		}
		kwdSgt.Init=function(){
			prvTxt=kwdTxt.val();
			wtcKey=setTimeout(WatchKeyword,50);
		};
	}

	rtn.Search = function(text){
		var txtVal=typeof(text)==="string"?text:kwdTxt.val();
		if(!!txtVal){
			var chkGtm=null;
			function WatchGtm(){
				if(!!Eom.Gtm.ElasticSearcher){
					GtmSearch();
				}else{
					chkGtm=setTimeout(WatchGtm,100);
				}
			}
			chkGtm=setTimeout(WatchGtm,100);
			function GtmSearch(){
				Eom.Gtm.ElasticSearcher.Search(
					txtVal,
					{
						done:rtn.LoadResult,
						fail:function(res){rtn.trigger("error",{code:-1,message:"_(:3 」∠)_",payload:res});}
					},
					pagInf.src
				);
			}
		}
		return rtn;
	};
	kwdCmd.on("click",rtn.Search);

	var mrqRun;
	rtn.LoadResult = function(json){
		kwdRes.html("");
		if(!!mrqRun)mrqRun.Stop();
		if(!!json&&!!json.items&&$.isArray(json.items)){
			for(var i in json.items){
				kwdRes.append(CreateResultItem(json.items[i]));
			}
			mrqRun = Eom.Marqee(kwdRes.find("li>div.search_title"),"span.anchor",{onclickevent:function(marqeebox){UpdateStorage(kwdTxt.val(),pagInf.src.page);location.href=location.origin+"/"+marqeebox.data("href");}}).Start();
		}
		kwdSgt.Clear();
		kwdTxt.blur();
		kwdCnt.html(Itp.Common.Format(rtn.CountFormat,json.total)).show();
		rtn.trigger("reload");
		return rtn;
	};
	function CreateResultItem(item){
		var rtn2=$("<li/>").attr({"data-score":item.score});
		var divTtl=$("<div/>").addClass("search_title").attr({"data-href":CleanPath(item.path)+(!!item.hash?"#"+item.hash:"")}).append($("<span/>").addClass("anchor").html(Emphasis(Sanitize(item.title))));
		rtn2.append(divTtl);
		if(!!item.desc){
			rtn2.append($("<div/>").addClass("search_body icon-accordionhead").addFocus().append(Eom.CreateIcon("icon-accordionhead","small")).append($("<div/>").addClass("bodytext").html("..."+Emphasis(Sanitize(item.desc))+"...")).on("click",ShowHide));
		}
		return rtn2;
	}
	var CleanPath=(function(bookidlist){
		var rtn=function(path){return path;};
		if(!!bookidlist){
			var ptn="";
			var idAry=bookidlist.split("|");
			for(var i in idAry)ptn+=idAry[i].replace(/vh$/,"").replace(/nv$/,"")+"|";
			ptn="(?:"+ptn.replace(/\|$/,"")+")";
			var rgx=new RegExp("/"+ptn+"([^\.\/]+\.html)");
			rtn=function(path){return path.replace(rgx,"/$1")};
		}
		return rtn;
	})($("meta[name='book_id']").attr("content"));
	function Emphasis(value){
		var rgxLst=kwdTxt.val().replace(/\s/g," ").replace(/^ +/,"").replace(/ +$/,"").split(" ");
		var rtn2=value;
		var tmpSpl,tmpJnt;
		var rgxEmp;
		for(var i in rgxLst){
			if(!!rgxLst[i]){
				rgxEmp=new RegExp("("+rgxLst[i]+")","g");
				tmpSpl=rtn2.replace(/(<span[^>]*>[^<]+?<\/span>)/g,"|$1|").split("|");
				tmpJnt=[];
				for(var j in tmpSpl)if(tmpSpl[j].length>0)tmpJnt.push(/<span>/.test(tmpSpl[j])?tmpSpl[j]:tmpSpl[j].replace(rgxEmp,"<span class=\"emp\">$1</span>"));
				rtn2=tmpJnt.join("");
			}
		}
		return rtn2;
	}
	function ShowHide(event){
		var curItm=$(event.target).closest("div.search_body");
		if(curItm.hasClass("open")){
			curItm.removeClass("open");
		}else{
			curItm.addClass("open");
		}
		rtn.trigger("showhide");
	}
	function UpdateStorage(keyword,pageinfo){
		hstDic.k=keyword||"";
		hstDic.f=!!pageinfo?pageinfo.from:0;
		hstDic.p=!!pageinfo?pageinfo.page:0;
		sessionStorage.setItem(strNam,JSON.stringify(hstDic));
	}

	rtn.Clear = function(){
		kwdTxt.val("");
		kwdSgt.Clear();
		kwdRes.html("");
	};
	kwdDel.on("click",rtn.Clear);

	$(document).on("keyup",function(ev){
		switch(ev.key.toLowerCase())
		{
			case "enter":
				rtn.Search();
				return;
			case "backspace":
			case "delete":
			if(kwdTxt.val()==""){
					rtn.Clear();
					return;
				}
			default:
				/*nothing*/
		}
		//rtn.Peek();
	});

	var evtItm={};
	rtn.on=function(name,callback){
		if(!evtItm[name])evtItm[name]=[];
		evtItm[name].push(callback);
		return rtn;
	};
	rtn.trigger=function(name,arg){
		if(!!evtItm[name]){
			for(var i in evtItm[name]){
				evtItm[name][i](null,arg);
			}
		}
		return rtn;
	};

	$("body").on("mouseover",function(ev){
		var curElm=$(ev.target);
		if(curElm.closest("div.formbox").length>0){
			kwdSgt.show();
		}else{
			kwdSgt.hide();
		}
	});
	if(!!hstDic.k)rtn.Init(hstDic.k).Search();
	kwdSgt.Init();

	return rtn;
};