jQuery.fn.extend({
	hasFocus:function(){
		return this.classList.contains("focusable");
	},
	addFocus:function(focusing){
		var jqElm=$(this);
		$(this).removeClass("focusable").addClass("focusable").on("touchforcechange.focusable",function(ev){if(ev.touches[0].force>0.5){ev.preventDefault();return false;}});
		if(focusing!==false)$(this).on("touchstart.focusable",function(ev){var curElm=$(ev.target).closest(".focusable");curElm.addClass('focusing');setTimeout(function(){curElm.removeClass('focusing');},100);});
		return this;
	},
	removeFocus:function(){
		var jqElm=$(this);
		jqElm.removeClass("focusable").off(".focusable");
		return this;
	},
	focusable:function(selector){
		if(typeof(selector)==="string"){
			var tmpSlc=selector.split(",");
			var jntSlc="";
			for(var i in tmpSlc)jntSlc+=tmpSlc[i]+".focusable"+",";
			return $(this).find(jntSlc.replace(/,$/,""));
		}else{
			return $(this).find(".focusable");
		}
	},
	enableFocus:function(flag){
		var jqElm=$(this);
		if(typeof(flag)==="undefined"){
			return jqElm.hasClass("focusable")&&!jqElm.hasClass("scrollout");
		}else{
			if(!!flag){
				if(jqElm.hasClass("focusable")&&jqElm.hasClass("scrollout"))jqElm.removeClass("scrollout");
			}else{
				if(jqElm.hasClass("focusable")&&!jqElm.hasClass("scrollout"))jqElm.addClass("scrollout");
			}
			return this;
		}
	},
});
var HeadUnit = !!HeadUnit?HeadUnit:{};
HeadUnit.Rectangle=(function(interval){
	var rtn={};
	var wtcRct=null;
	var cndRct={},elmRct={};
	function WatchRectangle(){
		clearTimeout(wtcRct);
		for(var ky in cndRct)if(!!cndRct[ky]&&$.isFunction(cndRct[ky].Check))cndRct[ky].Check();
		wtcRct=setTimeout(WatchRectangle,interval);
	}
	rtn.bind=function(element,condition){
		if(!!element&&!!element.prevObject&&$.isFunction(condition)){
			if(!element.data("hurectid"))element.data({"hurectid":Itp.Common.Random(16)});
			var rectId=element.data("hurectid");
			if(!elmRct[rectId])elmRct[rectId]=element;
			cndRct[rectId]=Watcher(rectId,condition);
		}
		return rtn;
	};
	rtn.unbind=function(element,condition){
		var rectId=element.data("hurectid");
		if(!!rectId){
			var tmpCnd=cndRct[rectId];
			if(!!tmpCnd){
				delete tmpCnd.callbacks[condition.toString()];
				if(JSON.stringify(tmpCnd.callbacks)=="{}"){
					delete cndRct[rectId];
					delete elmRct[rectId];
					element.removeAttr("data-hurectid");
				}
			}
		}
		return rtn;
	};
	function Watcher(id,callback){
		var rtn2={id:id,callbacks:!!cndRct[id]?cndRct[id].callbacks:{}};
		rtn2.callbacks[callback.toString()]=callback;
		rtn2.Check=function(){
			elmRct[rtn2.id].focusable().each(function(){$(this).enableFocus(Enable(this));});
		};
		function Enable(rawelm){
			var rtn3=true;
			for(var i in rtn2.callbacks){
				if(!rtn2.callbacks[i](rawelm)){
					rtn3=false;
					break;
				}
			}
			return rtn3;
		}
		return rtn2;
	}
	rtn.Start=function(){
		if(!wtcRct)wtcRct=setTimeout(WatchRectangle,interval);
	};
	rtn.Stop=function(){
		if(!!wtcRct)clearTimeout(wtcRct);
	};
	return rtn;
})(100);
