var Eom = !!Eom?Eom:{};
Eom.Debug=(function(){
	var rtn={};
	rtn.DrawRectangle=function(item,duration){
		if(typeof(duration)!=="number")duration=5000;
		if(typeof(item.w)==="number"&&typeof(item.h)==="number"&&typeof(item.x)==="number"&&typeof(item.y)==="number"){
			var rct=$("<div/>").addClass("rectitem");
			if(typeof(item.attr)==="object"){
				for(var ky in item.attr)if(!rct.attr(ky))rct.attr(ky,item.attr[ky]);
			}
			var css={width:item.w,height:item.h,top:item.y,left:item.x};
			if(typeof(item.css)==="object"){
				for(var ky in item.css)if(!css[ky])css[ky]=item.css[ky];
			}else{
				rct.addClass("default");
			}
			rct.css(css);
			if(!rct.attr("id"))rct.attr({id:Itp.Common.Random(16)});
			$("body").append(rct);
			setTimeout(function(){rct.fadeOut("slow",function(){rct.remove();})},duration);
			return rct;
		}else{
			return null;
		}
	};
	return rtn;
})();
