var Eom = !!Eom?Eom:{};
Eom.Slider=function(elm,labelmin,labelmax){
	var FRM_SKID="<div class=\"itpslider\"><div class=\"label min\"><span>{0}</span></div><div class=\"track\"><div class=\"skid\"></div><div class=\"thumb\"></div></div><div class=\"label max\"><span>{1}</span></div></div>";
	var sldStp=0,sldVal=0;
	var sldIdx=Itp.Common.Random(12);
	var rtn=elm;
	elm.append($(Itp.Common.Format(FRM_SKID,labelmin,labelmax)));
	elm.children("div.itpslider").addClass(sldIdx);
	var elmTrk=elm.find("div.track").addClass(sldIdx);
	var elmMin=elm.find("div.label.min").addFocus();
	var elmMax=elm.find("div.label.max").addFocus();
	var elmThb=elm.find("div.thumb").addClass(sldIdx);
	Object.defineProperty(elmThb,"Sliding",{
		get:function(){return elmThb.hasClass("sliding");},
		set:function(value){if(!!value){if(!elmThb.hasClass("sliding"))elmThb.addClass("sliding");}else{if(elmThb.hasClass("sliding"))elmThb.removeClass("sliding");}},
	});
	
	elmMin.on("click",function(ev){rtn.Value--;});
	elmMax.on("click",function(ev){rtn.Value++;});
	if(Eom.IsMobile){
		$("body")
			.on("touchstart.slider_"+sldIdx,function(ev){
				elmThb.Sliding=$(ev.target).closest("div.thumb."+sldIdx).length>0;
			})
			.on("touchmove.slider_"+sldIdx,function(ev){
				if(!elmThb.Sliding)return;
				WatchPosition(ev);
			})
			.on("touchend.slider_"+sldIdx,function(ev){
				if(!elmThb.Sliding)return;
				WatchPosition(ev);
				elmThb.Sliding=false;
			});
	}else{
		elmTrk.on("click",WatchPosition);
	}
	function WatchPosition(event){
		var curElm=$(event.target).closest("div.track."+sldIdx);
		if(curElm.length>0){
			rtn.Value=parseInt(rtn.Step*(((event.pageX||event.changedTouches[0].pageX||curElm[0].offsetLeft)-curElm[0].offsetLeft)/curElm.width()));
		}
	}
	
	function ShiftThumb(){
		elmThb.css({left:((elmTrk.width()-elmThb.width())/(rtn.Step-1))*rtn.Value});
	}
	$(window).on("resize",ShiftThumb);
	
	Object.defineProperty(rtn,"Step",{
		get:function(){return sldStp;},
		set:function(value){sldStp=value;},
	});
	Object.defineProperty(rtn,"Value",{
		get:function(){return sldVal;},
		set:function(value){if(value>-1&&value<sldStp){sldVal=value;ShiftThumb();rtn.trigger("change",sldVal);}},
	});
	rtn.Step=2;
	
	return rtn;
};
