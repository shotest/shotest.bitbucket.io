var Eom = !!Eom?Eom:{};
Eom.Page = function(tocpage,tocpagetitle){
	var topBrd={link:tocpage,label:tocpagetitle};
	var secTtl=$("section.section");
	var chpTtl=$("section.chapter");
	Eom.Header.BreadClumb.Load(
		secTtl.length>0&&chpTtl.length>0?
			[GetClumbItem($("section.section")),GetClumbItem($("section.chapter")),topBrd]:
			[]
	);
	function GetClumbItem(section){return {link:tocpage+section.children("a").eq(0).attr("name"),label:section.children("h1").eq(0).html()};}

	Eom.Page.Remarks=(function(){
		var rtn={};
		$("section.paragraph.attention").each(function(){
			Build(rtn,$(this),CreateAttentionItem);
		});
		$("section.paragraph.hint").each(function(){
			Build(rtn,$(this),CreateHintItem);
		});
		function CreateAttentionItem(elm){
			var rtn2={Title:elm.children("h2"),Items:{}};
			var rtn3;
			var nxtElm;
			var i=0;
			elm.children("h3").each(function(){
				rtn3={Title:$(this).attr({"data-id":i}).addClass("show").addFocus()};
				nxtElm=rtn3.Title.next();
				while(!!nxtElm&&nxtElm.length>0&&nxtElm[0].tagName.toLowerCase()!="h3"){
					Build(rtn3,nxtElm,function(elm){return elm});
					nxtElm=nxtElm.next();
				}
				rtn3.Title.on("click",ShowHideByEvent);
				rtn2.Items[i]=rtn3;
				i++;
			});
			return rtn2;
		}
		function CreateHintItem(elm){
			var rtn2={Title:elm.children("h2"),Items:{}};
			var rtn3;
			var curElm;
			var i=0;
			elm.children("section.note_para").each(function(){
				curElm=$(this);
				rtn3={Title:curElm.children("h3").attr({"data-id":i}).addClass("show").addFocus()};
				Build(rtn3,curElm,function(elm){return elm.children("*:not(a):not(h3)")});
				rtn3.Title.on("click",ShowHideByEvent);
				rtn2.Items[i]=rtn3;
				i++;
			});
			return rtn2;
		}
		function Build(obj,elm,setelm){
			var curIdx=elm.children("a[name]").eq(0).attr("name");
			if(!!curIdx){
				if(!obj.Items)obj.Items={};
				obj.Items[curIdx]=setelm(elm);
			}
		}
		function ShowHideAll(mode){
			mode=!!mode&&mode=="show";
			var sh=mode?
				function(t){if(!t.hasClass("show"))t.trigger("click");}:
				function(t){if(t.hasClass("show"))t.trigger("click");};
			var p;
			for(var i in rtn.Items){
				p=rtn.Items[i];
				for(var j in p.Items){
					sh(p.Items[j].Title);
				}
			}
		}
		var MIN_IDX_LEN=16;
		function Seek(id){
			if(!!rtn.Items){
				var p,sp,ssp;
				var tmpIdx;
				for(var i=MIN_IDX_LEN;i<=id.length;i=i+4){
					tmpIdx=id.substring(0,i);
					p=rtn.Items[tmpIdx];
					if(!!p){
						for(var j in p.Items){
							sp=p.Items[j];
							for(var k=i;k<=id.length;k=k+4){
								ssp=sp.Items[id.substring(0,k)];
								if(!!ssp)return [sp];
							}
						}
						return p.Items;
					}
				}
			}
			return null;
		}
		function ShowHideByEvent(event){
			if(!!rtn.Items){
				var curElm=$(event.target).closest("h3");
				if(curElm.find(".icon-accordionhead").length>0){
					var acdElm=curElm.closest("h3");
					var praElm=acdElm.closest("section.paragraph");
					var tgtIdx=praElm.children("a[name]").eq(0).attr("name");
					var p=rtn.Items[tgtIdx],sp,ssp;
					if(!!p){
						sp=p.Items[acdElm.data("id")];
						if(acdElm.hasClass("show")){
							for(var i in sp.Items){
								ssp=sp.Items[i];
								if(!ssp.hasClass("hide"))ssp.addClass("hide");
							} 
							acdElm.removeClass("show");
							rtn.trigger("hide");
						}else{
							for(var i in sp.Items){
								ssp=sp.Items[i];
								if(ssp.hasClass("hide")){
									ssp.removeClass("hide");
									if(ssp.find("img[height='0']").length>0)Eom.ImageFitter.WatchImageSize(Eom.ImageFitter.InitImageSize(ssp.find("img")));
								}
							} 
							acdElm.addClass("show");
							rtn.trigger("show");
						}
					}
				}
			}
		}
		rtn.Hide=function(id){
			var tgtNod=Seek(id);
			if(!!tgtNod){
				var nod;
				for(var i in tgtNod){
					nod=tgtNod[i];
					if(nod.Title.hasClass("show"))nod.Title.trigger("click");
				}
			}
			return rtn;
		};
		rtn.Show=function(id){
			var tgtNod=Seek(id);
			if(!!tgtNod){
				var nod;
				for(var i in tgtNod){
					nod=tgtNod[i];
					if(!nod.Title.hasClass("show"))nod.Title.trigger("click");
				}
			}
			return rtn;
		};
		rtn.HideAll=function(){
			ShowHideAll();
			return rtn;
		};
		rtn.ShowAll=function(){
			ShowHideAll("show");
			return rtn;
		};


		var evtItm={};
		rtn.on=function(name,callback){
			if(!evtItm[name])evtItm[name]=[];
			evtItm[name].push(callback);
			return rtn;
		};
		rtn.trigger=function(name,arg){
			if(!!evtItm[name]){
				for(var i in evtItm[name]){
					evtItm[name][i](null,arg);
				}
			}
			return rtn;
		};
		return rtn.HideAll();
	})();
	
	function AdjustTableImage(){
		$("div.tablecontainer.largeimage>table").each(function(){
			var tblEl=$(this);
			var colWdt=GetColumnWidth(tblEl);
			tblEl.find("tr").each(function(){
				$(this).children("td").each(function(i){FitInnerImage($(this),colWdt[i]);});
			});
		});
		function GetColumnWidth(tbl){
			var prtElm=tbl.parent();
			var prtWdt=tbl.width();
			var rtn=[];
			tbl.find("col").each(function(){rtn.push((parseFloat($(this).attr("width").replace(/%$/,""))/100)*prtWdt);});
			return rtn;
		}
		function FitInnerImage(col,width){
			width=width-col.cssVal("paddingLeft")-col.cssVal("paddingRight");
			var imgElm=col.find("img");
			if(imgElm.length<=0)return;
			if(imgElm.width()>width)imgElm.width(width);
		}
	}
	
	$(".symbolIMG").each(function(){var curElm=$(this);if(curElm.closest("div.tablecontainer").length<1)curElm.on("click.iconmagnify",WatchSymbol).children("img");});
	function WatchSymbol(event){
		var curElm=$(event.target).closest(".symbolIMG");
		if(curElm.length>0&&curElm.closest("table").length<=0){
			var imgElm=curElm.children("img.symbol:not(.zoom):not([width])");
			if(imgElm.length>0&&curElm.children("img.symbol.zoom").length<=0)MagnifySymbol(curElm,imgElm);
		}
	}
	function MagnifySymbol(parentelm,imgelm){
		var mgnElm=$(Itp.Common.Format("<img class=\"symbol zoom\" src=\"{0}\" />",imgelm.attr("src")));
		var dflHgt=imgelm.height();
		setTimeout(function(){parentelm.append(mgnElm.css({height:dflHgt*3}));},Eom.CommonDelay);
	}
	function ZoomSymbol(parentelm,imgelm){
		var mgnElm=$(Itp.Common.Format("<img class=\"symbol zoom\" src=\"{0}\" />",imgelm.attr("src")));
		var dflHgt=imgelm.height();
		setTimeout(function(){parentelm.append(mgnElm.css({opacity:0.0,height:dflHgt}).animate({opacity:1.0,height:dflHgt*3},100));},Eom.CommonDelay);
	}
	$("body").on("click.iconbelittle",function(ev){
		$("img.symbol.zoom").remove();
	});

	if(Eom.IsSdl)$("a.smplink").each(function(){$(this).attr({href:"javascript:void(0);"}).removeFocus();});

	$(window).on("resize",AdjustTableImage);
	AdjustTableImage();
};
