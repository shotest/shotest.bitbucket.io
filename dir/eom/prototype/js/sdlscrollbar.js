var Eom = !!Eom?Eom:{};
Eom.Init=!!Eom.Init?Eom.Init:{};
Eom.Init.SdlScrollbar=function(cancelcallback){
	Eom.SdlScrollbar=$("div.sdlscrollbarbox");
	if(!$("div.wrap").hasClass("hidescroll")&&Eom.IsSdl){
		Eom.SdlScrollbar=Eom.CreateSdlScrollbar(Eom.SdlScrollbar);
		Eom.SdlScrollbar.CancelRectangle=cancelcallback;
	}else{
		Eom.SdlScrollbar.Enabled=false;
		Eom.SdlScrollbar.Reset=function(){return Eom.SdlScrollbar;};
		Eom.SdlScrollbar.Hide=function(){return Eom.SdlScrollbar;};
		Eom.SdlScrollbar.Scroll=function(){return Eom.SdlScrollbar;};
		Eom.SdlScrollbar.CheckPosition=function(){return Eom.SdlScrollbar;};
	}
};
Eom.CreateSdlScrollbar=function(scrollbox){
	var rtn2=scrollbox;
	rtn2.MinTimeSpan=Eom.IsMobile?5:0.5;
	rtn2.ScrollerWeight=Eom.IsMobile?20:2;
	rtn2.ReductionRate=0.1;
	rtn2.FollowInterval=10;
	rtn2.CancelRectangle=function(){return true;};
	var boxHdr=$("<div class=\"sdlscrollbarheader\"/>");
	var boxScr=$("<div class=\"sdlscrollbar\"/>");
	var cmdTop=$("<div class=\"top\"/>");
	var cmdUpr=$("<div class=\"upper\"/>");
	var boxTrk=$("<div class=\"track\"/>");
	var boxSkd=$("<div class=\"skid\"/>");boxScr.find("div.track>div.skid");
	var boxThb=$("<div class=\"thumb\"/>");boxScr.find("div.track>div.thumb");
	var cmdLwr=$("<div class=\"lower\"/>");
	scrollbox.append(boxHdr).append(boxScr.append(cmdTop).append(cmdUpr).append(boxTrk.append(boxSkd).append(boxThb)).append(cmdLwr))
	var boxTgt=null;
	var trvThb=0;

	var Touch={X:{c:0,p:0,s:0},Y:{c:0,p:0,s:0},T:0};
	function TouchStart(event) {
		Touch.X.s = event.touches[0].pageX;
		Touch.Y.s = event.touches[0].pageY;
		Touch.X.c = Touch.X.s;
		Touch.Y.c = Touch.Y.s;
		Touch.T=event.timeStamp;
	}
	function TouchMove(event) {
		Touch.X.p = Touch.X.c;
		Touch.Y.p = Touch.Y.c;
		Touch.X.c = event.changedTouches[0].pageX;
		Touch.Y.c = event.changedTouches[0].pageY;
		Touch.T=event.timeStamp;
		rtn2.Scroll(Touch.Y.p-Touch.Y.c);
	}
	function TouchEnd(event) {
		var tmSpan=event.timeStamp-Touch.T;
		if(tmSpan>rtn2.MinTimeSpan)return;
		//var swpDir=Touch.Y.s>Touch.Y.c?1:-1;
		//var swpVlc=(Touch.Y.s-Touch.Y.c)/tmSpan;
		//var lmtFlw=rtn2.ScrollerWeight*Math.sqrt(Math.pow(swpVlc,2));
		//var curFlw=lmtFlw*rtn2.ReductionRate;
		//var scrFlw;
		//function FollowScroll(){
		//	if(curFlw!=NaN&&curFlw<Infinity&&curFlw>1){
		//		rtn2.Scroll(curFlw*swpDir);
		//		lmtFlw=lmtFlw-curFlw;
		//		curFlw=lmtFlw*rtn2.ReductionRate;
		//		scrFlw=setTimeout(FollowScroll,rtn2.FollowInterval);
		//	}else{
		//		clearTimeout(scrFlw);
		//	}
		//}
		//scrFlw=setTimeout(FollowScroll,0);
	}

	rtn2.Scroll=function(val){
		var tmpTop=typeof(val)!=="undefined"?boxTgt.scrollTop()+val:0;
		if(tmpTop>=0&&tmpTop<=boxTgt.sH)boxTgt.scrollTop(tmpTop<0?0:(tmpTop>boxTgt.sH?boxTgt.sH:tmpTop));
		return rtn2;
	};

	function SetBarHeight(){
		cmdTop.off("click.sdlscroller").removeFocus();
		cmdUpr.off("click.sdlscroller").removeFocus();
		cmdLwr.off("click.sdlscroller").removeFocus();
		var cngFlg=false;
		if(boxTgt.length>0){
			var ofsTop=boxTgt.oT-boxTgt.cssVal("marginTop");
			boxHdr.height(ofsTop>0?ofsTop:0);
			boxScr.height(rtn2.height()-boxHdr.height());
			rtn2.Enabled=boxTgt.sH>boxTgt.cH;
			if(rtn2.Enabled){
				boxThb.height(((boxTgt.cH/boxTgt.sH)*100)+"%");
				trvThb=boxSkd.height()-boxThb.height();
				cmdTop.on("click.sdlscroller",SendTop).addFocus();
				cmdUpr.on("click.sdlscroller",ScrollUp).addFocus();
				cmdLwr.on("click.sdlscroller",ScrollDown).addFocus();
				cngFlg=true;
			}
		}
		if(!cngFlg)boxThb.css({height:"",top:""});
	};
	function SendTop(){
		boxTgt.scrollTop(0);
	};
	function ScrollUp(){
		var scrTop=boxTgt.scrollTop()-(boxTgt.cH/2);
		boxTgt.scrollTop(scrTop>0?scrTop:0);
	};
	function ScrollDown(){
		var scrTop=boxTgt.scrollTop()+(boxTgt.cH/2);
		boxTgt.scrollTop(scrTop<boxTgt.sH?scrTop:boxTgt.sH);
	};

	Object.defineProperty(rtn2,"Enabled",{
		get:function(){
			return !rtn2.hasClass("disabled");
		},
		set:function(value){
			rtn2.removeClass("disabled");
			if(!value)rtn2.css({height:""}).addClass("disabled");
		}
	});

	rtn2.Reset = function(jqueryelm){
		if(!!jqueryelm&&!!jqueryelm.prevObject)Capture(jqueryelm);
		setTimeout(SetBarHeight,Eom.CommonDelay);
		setTimeout(ShiftSlider,Eom.CommonDelay);
		return rtn2;
	};
	function ShiftSlider(){
		boxThb.css({top:trvThb*(boxTgt.scrollTop()/(boxTgt.sH-boxTgt.cH))});
	}
	function Capture(jqueryelm){
		if(!!jqueryelm&&!!jqueryelm.prevObject){
			if(!!boxTgt&&!!boxTgt.prevObject)boxTgt.removeClass("sdlscroller").off(".sdlscroller");
			boxTgt=jqueryelm;
			if(typeof(boxTgt.oT)==="undefined") Object.defineProperty(boxTgt,"oT",{
				get:function(){
					return this[0].offsetTop;
				}
			});
			if(typeof(boxTgt.cH)==="undefined") Object.defineProperty(boxTgt,"cH",{
				get:function(){
					return this[0].clientHeight;
				}
			});
			if(typeof(boxTgt.sH)==="undefined") Object.defineProperty(boxTgt,"sH",{
				get:function(){
					return this[0].scrollHeight;
				}
			});
			boxTgt
				.addClass("sdlscroller")
				.on("touchstart.sdlscroller",TouchStart)
				.on("touchmove.sdlscroller",TouchMove)
				.on("touchend.sdlscroller",TouchEnd)
				.on("scroll.sdlscroller",ShiftSlider);
		}else{
			boxTgt={oT:0,cH:0,sH:0};
		}
	}
	rtn2.CheckPosition=function(rawelm){
		if(!!boxTgt){
			var scrTop=boxTgt.scrollTop()+boxHdr.height(),scrHgt=boxTgt.height();
			var chkFnc=rtn2.CancelRectangle()?CancelAll:CheckTop;
			return chkFnc(rawelm,scrTop,scrHgt);
		}else{
			return false;
		}
	}
	function CheckTop(rawelm,scrollertop,scrollerheight){
		var curElm=$(rawelm);
		if(rawelm.offsetHeight>0){
			var curTop=rawelm.offsetTop-scrollertop-curElm.cssVal("borderTopWidth");
			return curTop+curElm.grossHeight()>0&&curTop<scrollerheight;
		}else{
			return false;
		}
	}
	function CancelAll(rawelm){
		return false;
	}
	$(window).on("resize",SetBarHeight);
	rtn2.find("a[href]:not([href='#'])").each(function(){$(this).addFocus()});
	return rtn2;
}