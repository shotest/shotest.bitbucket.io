var Eom = !!Eom?Eom:{};
Eom.AlphaIndex=function(tabelm,scrollerelm,rootelm,indextree){
	var rtn=$("<ul/>").addClass("capitalgroup");
	var strNam="alphaindex."+Eom.Publication.fullId+".eom.toyota.jp";
	rootelm.append(rtn);
	var lstTab=tabelm;
	var hstStg=sessionStorage.getItem(strNam);
	sessionStorage.removeItem(strNam);
	var hstObj=!!hstStg?JSON.parse(hstStg):{t:"00",c:{},li:"",s:0};

	Object.defineProperty(rtn,"Status",{
		get:function(){return hstObj;}
	});

	lstTab.each(function(){
		$(this).on("click",function(ev){
			var curElm=$(ev.target).closest("td[data-tabindex]");
			if(!curElm.hasClass("selected"))SelectTab(curElm.data("tabindex"));
		});
	});

	function SelectTab(tabindex,recoverhistory){
		if(!tabindex)tabindex="00";
		var qryTab=lstTab.parent().children(Itp.Common.Format("[data-tabindex='{0}']",tabindex));
		var capLst=qryTab.data("capital").split("|")
		var CheckCapital=$.isArray(capLst)?
			function(cap){return capLst.indexOf(cap)>-1;}:
			function(cap){return true;};
		rtn.html("");
		for(var i in indextree)if(CheckCapital(indextree[i].cap))CreateCapitalNode(indextree[i]);
		lstTab.removeClass("selected").children("div").addFocus();
		qryTab.addClass("selected").children("div").removeFocus();
		var capDic={};
		if(!!recoverhistory){
			rtn.children("li[data-capital]").each(function(i){
				var curElm=$(this);
				capDic[i]=hstObj.c[i];
				if(hstObj.c[i]>=1)curElm.children("div.icon-accordionhead").trigger("click");
			});
			var tgtElm=rtn.find(Itp.Common.Format("span.jumper[name='{0}']",hstObj.li));
			tgtElm.trigger("mouseover");
			if(Eom.IsSdl){
				scrollerelm.scrollTop(hstObj.s);
			}else{
				window.scrollBy(0,hstObj.s);
			}
		}
		rtn.trigger("select");
		hstObj={t:tabindex+"",c:capDic,li:"",s:0};
	};

	function CreateCapitalNode(capitalitem){
		var itmIdx=rtn.children("li[data-capital]").length;
		var capBox=$("<li/>").attr({"data-capital":capitalitem.cap});
		var capLbl=$("<div/>").addClass("icon-accordionhead").addFocus().html(capitalitem.cap).prepend(Eom.CreateIcon("icon-accordionhead"));
		var prmBox=$("<ul/>").addClass("indexlist").addClass("hide");
		capLbl.on("click",ShowHide);
		rtn.append(capBox.append(capLbl).append(prmBox));
		for(var i in capitalitem.p)CreatePrimaryNode(prmBox,capitalitem.p[i]);
		return capBox;
	}
	function CreatePrimaryNode(parentnode,primalitem){
		var prmBox=CreateListItem(parentnode,primalitem.label,primalitem.link);
		var secBox=$("<ul/>").addClass("indexlist");
		prmBox.append(secBox);
		if(primalitem.s)for(var i in primalitem.s)CreateSecondaryNode(secBox,primalitem.s[i]);
		return prmBox;
	}
	function CreateSecondaryNode(parentnode,seconditem){
		var secBox=CreateListItem(parentnode,seconditem.label,seconditem.link);
		return secBox;
	}
	function CreateListItem(parentnode,label,linklist){
		var itmBox=$("<li/>").addClass("index");
		var itmLbl=$("<div/>").addClass("indexlabel").html(label);
		var lnkBox=$("<span/>").addClass("linksleeve");
		parentnode.append(itmBox.append(itmLbl).append($("<div/>").addClass("link").append(lnkBox)));
		var tmpAnc;
		for(var i in linklist){
			tmpAnc=$("<span/>")
				.attr({"data-href":linklist[i],"name":"link_"+rtn.find("span.jumper").length})
				.addClass("jumper")
				.append(Eom.CreateIcon("icon-arrow-c"))
				.addFocus();
			if(Eom.IsIos){
				tmpAnc.on("click",function(ev){DoEmphasis(ev);Jump(ev);});
			}else{
				tmpAnc.on("mouseover",Emphasis).on("click",Jump);
			}
			lnkBox.append(tmpAnc);
		}
		return itmBox;
	}
	function Emphasis(event){
		setTimeout(function(){DoEmphasis(event);},Eom.CommonDelay);
	}
	function DoEmphasis(event){
		var ancElm=$(event.target).closest("span.jumper");
		ancElm.addClass("emphasis");
	}
	function Unemphasis(event){
		var ancElm=$("span.jumper");
		ancElm.removeClass("emphasis");
	}
	function Jump(event){
		var curElm=$(event.target).closest("span.jumper");
		hstObj.li=curElm.attr("name");
		hstObj.s=Eom.IsSdl?scrollerelm.scrollTop():window.pageYOffset;
		sessionStorage.setItem(strNam,JSON.stringify(hstObj));
		location.href=curElm.data("href");
	}
	function ShowHide(event){
		var evtElm=$(event.target).closest("div.icon-accordionhead");
		var lstElm=evtElm.siblings("ul.indexlist");
		var capElm=evtElm.closest("li[data-capital]");
		var capIdx=capElm.parent().children("li[data-capital]").index(capElm);
		if(evtElm.hasClass("open")){
			lstElm.addClass("hide");
			evtElm.removeClass("open");
			hstObj.c[capIdx]=0;
		}else{
			lstElm.removeClass("hide");
			evtElm.addClass("open");
			hstObj.c[capIdx]=1
		}
		rtn.trigger("change");
	}

	$("body").on("mouseover",Unemphasis);
	SelectTab(hstObj.t,true);

	return rtn;
};