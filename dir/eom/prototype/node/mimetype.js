exports.mimetype = function(format){

const defaultEncode = 'UTF-8';
const types = {
	'.aac':  {mime:'audio/aac',encode:'binary'},
	'.abw':  {mime:'application/x-abiword'},
	'.arc':  {mime:'application/octet-stream'},
	'.avi':  {mime:'video/x-msvideo'},
	'.azw':  {mime:'application/vnd.amazon.ebook'},
	'.bin':  {mime:'application/octet-stream'},
	'.bmp':  {mime:'image/bmp',encode:'binary'},
	'.bz':   {mime:'application/x-bzip'},
	'.bz2':  {mime:'application/x-bzip2'},
	'.csh':  {mime:'application/x-csh'},
	'.css':  {mime:'text/css'},
	'.csv':  {mime:'text/csv'},
	'.doc':  {mime:'application/msword'},
	'.docx': {mime:'application/vnd.openxmlformats-officedocument.wordprocessingml.document'},
	'.eot':  {mime:'application/vnd.ms-fontobject'},
	'.epub': {mime:'application/epub+zip'},
	'.es':   {mime:'application/ecmascript'},
	'.gif':  {mime:'image/gif',encode:'binary'},
	'.htm':  {mime:'text/html'},
	'.html': {mime:'text/html'},
	'.ico':  {mime:'image/x-icon'},
	'.ics':  {mime:'text/calendar'},
	'.jar':  {mime:'application/java-archive'},
	'.jpeg': {mime:'image/jpeg',encode:'binary'},
	'.jpg':  {mime:'image/jpeg',encode:'binary'},
	'.js':   {mime:'application/javascript'},
	'.json': {mime:'application/json'},
	'.mid':  {mime:'audio/midi'},
	'.midi': {mime:'audio/midi'},
	'.midi': {mime:'audio/x-midi'},
	'.mp4': {mime:'video/mp4'},
	'.mpeg': {mime:'video/mpeg'},
	'.mpkg': {mime:'application/vnd.apple.installer+xml'},
	'.odp':  {mime:'application/vnd.oasis.opendocument.presentation'},
	'.ods':  {mime:'application/vnd.oasis.opendocument.spreadsheet'},
	'.odt':  {mime:'application/vnd.oasis.opendocument.text'},
	'.oga':  {mime:'audio/ogg'},
	'.ogv':  {mime:'video/ogg'},
	'.ogx':  {mime:'application/ogg'},
	'.otf':  {mime:'font/otf',encode:'binary'},
	'.png':  {mime:'image/png',encode:'binary'},
	'.pdf':  {mime:'application/pdf'},
	'.ppt':  {mime:'application/vnd.ms-powerpoint'},
	'.pptx': {mime:'application/vnd.openxmlformats-officedocument.presentationml.presentation'},
	'.rar':  {mime:'application/x-rar-compressed'},
	'.rtf':  {mime:'application/rtf'},
	'.sh':   {mime:'application/x-sh'},
	'.svg':  {mime:'image/svg+xml'},
	'.swf':  {mime:'application/x-shockwave-flash'},
	'.tar':  {mime:'application/x-tar'},
	'.tif':  {mime:'image/tiff'},
	'.tiff': {mime:'image/tiff'},
	'.ts':   {mime:'application/typescript'},
	'.ttf':  {mime:'font/ttf',encode:'binary'},
	'.txt':  {mime:'text/plain'},
	'.vsd':  {mime:'application/vnd.visio'},
	'.wav':  {mime:'audio/wav'},
	'.weba': {mime:'audio/webm'},
	'.webm': {mime:'video/webm'},
	'.webp': {mime:'image/webp'},
	'.woff': {mime:'font/woff',encode:'binary'},
	'.woff2':{mime:'font/woff2',encode:'binary'},
	'.xhtml':{mime:'application/xhtml+xml'},
	'.xls':  {mime:'application/vnd.ms-excel'},
	'.xlsx': {mime:'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
	'.xml':  {mime:'application/xml'},
	'.xul':  {mime:'application/vnd.mozilla.xul+xml'},
	'.zip':  {mime:'application/zip'},
	'.3gp':  {mime:'video/3gpp'},
	'.3gp':  {mime:'audio/3gpp'},
	'.3g2':  {mime:'video/3gpp2'},
	'.3g2':  {mime:'audio/3gpp2'},
	'.7z':   {mime:'application/x-7z-compressed'}
};

if(/\\|\/|:/.test(format)){
	format = (format.match(/\.[^.]+$/gim)||'')[0];
}
const result = types[(format || '.html')] || {mime:'text/plain'};
result.encode = result.encode || defaultEncode;
result.format = format;

if(result){
	return result;
}else{
	return {mime:'text/plain'};
}

}