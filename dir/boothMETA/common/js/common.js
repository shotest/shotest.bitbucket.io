//スムーススクロール
$(function () {
	$('a[href^="#"]').click(function(){
    let speed = 500;
    let href= $(this).attr("href");
    let target = $(href == "#" || href == "" ? 'html' : href);
    let position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});

//topへ戻る
$(function() {
  var btn = $('.pagetop');
  $(window).on('load scroll', function(){
    if($(this).scrollTop() > 120) {
      btn.addClass('pagetop_in');
    }else{
      btn.removeClass('pagetop_in');
    }
  });
});

//アニメーション
$(function(){
  $('.fade').each(function () {
		var elemPos = $(this).offset().top;
		var scroll = $(window).scrollTop();
		var windowHeight = $(window).height();
		if (scroll > elemPos - windowHeight + 50) {
			$(this).addClass('fade_in');
		}
	});
});
$(window).on('scroll', function () {
	$('.fade').each(function () {
		var elemPos = $(this).offset().top;
		var scroll = $(window).scrollTop();
		var windowHeight = $(window).height();
		if (scroll > elemPos - windowHeight + 50) {
			$(this).addClass('fade_in');
		}
	});
});

//accordion
$(function(){
	$('.accordion dt').on('click', function(){
		$(this).parent().toggleClass('on');
		$(this).next().slideToggle();
	});
});

//aside
// $(function() {
//   var scrollon = $('aside');
//   $(window).on('load scroll', function(){
//     if($(this).scrollTop() > 60) {
//       scrollon.addClass('scroll_on');
//     }else{
//       scrollon.removeClass('scroll_on');
//     }
//   });
// });

//modal
$(function(){
	$('.modal_btn').each(function(){
			$(this).on('click',function(){
					var target = $(this).data('target');
					var modal = document.getElementById(target);
					$('.modal').prepend('<div class="modal_bg modal_close"></div>');
					$(modal).fadeIn();
					return false;
			});
	});
	$(document).on('click', '.modal_close', function(){
			$('.modal').fadeOut();
			$('.modal_bg').fadeOut(300, function(){ $(this).remove();});
			return false;
	});
	$('.modal_close_btn').on('click', function(){
		$('.modal').fadeOut();
		$('.modal_bg').fadeOut(300, function(){ $(this).remove();});
		return false;
	});
});