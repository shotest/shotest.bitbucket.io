
jQuery(function ($) { // この中であればWordpressでも「$」が使用可能になる

  var topBtn = $('.pagetop');
  topBtn.hide();

  // ボタンの表示設定
  $(window).scroll(function () {
    if ($(this).scrollTop() > 70) {
      // 指定px以上のスクロールでボタンを表示
      topBtn.fadeIn();
    } else {
      // 画面が指定pxより上ならボタンを非表示
      topBtn.fadeOut();
    }
  });

  // ボタンをクリックしたらスクロールして上に戻る
  topBtn.click(function () {
    $('body,html').animate({
      scrollTop: 0
    }, 300, 'swing');
    return false;
  });

  //ドロワーメニュー
  $(".drawer-icon__button").click(function () {
    $(this).toggleClass("is-active");
    $(".header__drawer").toggleClass("is-active");
    $(".header__bg").toggleClass("is-active");
  });

  $(".drawer__link").click(function () {
    $(".drawer-icon__button").removeClass("is-active");
    $(".header__drawer").removeClass("is-active");
    $(".header__bg").removeClass("is-active");
  });

  $(".header__bg").click(function () {
    $(".drawer-icon__button").removeClass("is-active");
    $(".header__drawer").removeClass("is-active");
    $(".header__bg").removeClass("is-active");
  });



  // スムーススクロール (絶対パスのリンク先が現在のページであった場合でも作動)

  $(document).on('click', 'a[href*="#"]', function () {
    let time = 100;
    let header = $('header').innerHeight();
    let target = $(this.hash);
    if (!target.length) return;
    let targetY = target.offset().top - header;
    $('html,body').animate({ scrollTop: targetY }, time, 'swing');
    return false;
  });

});
