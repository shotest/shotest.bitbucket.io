jQuery(function($) {
	window.resizeSmallInlineFrame = function() {
		$('iframe', parent.document).closest('.frame').removeClass('large');
	};
	window.closeInlineFrame = function() {
		$('.frame').remove();
	};
	window.closeParentInlineFrame = function() {
		$('iframe', parent.document).closest('.frame').remove();
	};
	window.parentPageLink = function(url, target) {
		if (target === undefined) {
			target = '_self';
		}
		parent.open(url, target);
	};
});
