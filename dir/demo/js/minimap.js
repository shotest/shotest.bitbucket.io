var vbooth_sid = -1;
jQuery(function($) {
	$('.minimap_point').on('click', function(){
		var sid = $(this).data('sid');
		changeScene(sid, 'direct', true);
	});

	var context = $("#minimap_cursor")[0].getContext("2d");
	var cwidth = parseInt($("#minimap_cursor").width());
	var cheight = parseInt($("#minimap_cursor").height());
	var rad = 75;

	context.beginPath();
	context.moveTo(cwidth / 2, cheight / 2);
	context.arc( cwidth / 2, cheight / 2, Math.min(cwidth, cheight) / 2, (270 - rad / 2) * Math.PI / 180, (270 + rad / 2) * Math.PI / 180, false ) ;
	context.fillStyle = "#0000ff";
	context.fill();


	$('#screen').on('move', function(e, detail){
		var rad_offset = 0;
		var sid = detail.sceneNo;
		if(vbooth_sid != sid){
			vbooth_sid = sid;
			$('.minimap_point').attr('src', 'image/camera_s.png');

			var cobj = $('.minimap_point[data-sid="'+sid+'"]');
			cobj.attr('src', 'image/camera_s_r.png');

			var top = parseInt(cobj.css('top')) + parseInt(cobj.height()) / 2 - parseInt($("#minimap_cursor").height()) / 2;
			var left = parseInt(cobj.css('left')) + parseInt(cobj.width()) / 2 - parseInt($("#minimap_cursor").width()) / 2;

			$("#minimap_cursor").css("top", top + "px");
			$("#minimap_cursor").css("left", left + "px");
			$("#minimap_cursor").show();
		}
		$("#minimap_cursor").css({ transform: 'rotate('+(rad_offset + detail.direction)+'rad)' })
	});


});
