var vertexSource = "varying vec2 v_uv;\nvarying mat3 v_spriteMat;\nuniform float divisionX;\nuniform float divisionY;\nuniform float interval;\nuniform float utime;\nfloat ltime;\n\nvoid main(void) {\n\tltime = mod(utime / interval, divisionX * divisionY);\n\tmat3 scaleMat = mat3(\n\t\t1.0 / divisionX, 0.0, 0.0, // 横列のフレームの数分、横に縮小（フレーム4つ分縮小）\n\t\t0.0, 1.0 / divisionY, 0.0, // 縦列のフレームの数分、縦に縮小（フレーム5つ分縮小）\n\t\t0.0, 0.0, 1.0\n\t);\n\tmat3 translateMat = mat3(\n\t\t1.0, 0.0, int(mod(ltime, divisionX)), // 横に平行移動（横列の左から2つ目のフレームに平行移動）\n\t\t0.0, 1.0, int(divisionY - (ltime / divisionX)), // 縦に平行移動（縦列の下から3つ目のフレームに平行移動）\n\t\t0.0, 0.0, 1.0\n\t);\n\tv_spriteMat = translateMat * scaleMat; // UV座標をずらす用の変換行列を作成\n\tv_uv = uv;\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\n}\n";

var fragmentSource = "uniform sampler2D v_texture;\nvarying vec2 v_uv;\nvarying mat3 v_spriteMat;\n\nvoid main(void) {\n\tvec3 uv = vec3(v_uv, 1.0);\n\tuv *= v_spriteMat;\n\tvec4 smpColor = texture2D(v_texture, uv.xy / uv.z);\n\n\tgl_FragColor = smpColor;\n}\n";

var debug = 0;
var foce_load = 0;

var config;
var renderer;
var scene;
var camera;
var controls;
var nowSceneNo = 0;
var nextSceneNo = 0;
var cameraMove = 0;
var anmationStop = 0;
var changeSceneCount = 0;
var changeSceneTarget;
var changeSceneCameraPosition = THREE.Vector3;
var changeSceneTimer = 0;
var changeSceneTime = 1000;
var fadeinPanel;
var chspeed = 500;
var chmode = 'fov';
var nextDirection = '';
var mouseOver = '';
var movielist = [];
var imglist = [];
var animlist = [];

function vbooth(url){
	if(url == ''){
		url = 'config.json';
	}
	configDataLoad(url);
}

function configDataLoad(url){
	$.getJSON(url)
		.done(function(data, textStatus, jqXHR){
			config = data;
			init();
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			alert("設定ファイルを読み込めませんでした。");
//			alert(textStatus);
		});
}

function init(){
	start3D();

	window.addEventListener('resize', onResize, false );

	for(var i = 0; i < config.scene.length; i++){
		MakeSkybox(i);
	}
}

function start3D(){
	if(getParam('sno')){
		nowSceneNo = getParam('sno');
	}
	collisionDetection();
	setscreen();
	setFadeinPanel();

	setScene(nowSceneNo);
	anmationStop = 0;
	animate();
}

function animate() {
	if(anmationStop == 0 ){

		if(config.scene[nowSceneNo].movielist){
			for(var i = 0; i < config.scene[nowSceneNo].movielist.length; i++){
				var src =  config.scene[nowSceneNo].movielist[i].src
				if (movielist[src].video.readyState === movielist[src].video.HAVE_ENOUGH_DATA) {
				    movielist[src].videoImageContext.drawImage(movielist[src].video, 0, 0);
				    if(movielist[src].videoTexture) {
				        movielist[src].videoTexture.needsUpdate = true;
				    }
				}
			}
		}
		if(config.scene[nowSceneNo].animationlist){
			for(var i = 0; i < config.scene[nowSceneNo].animationlist.length; i++){
				config.scene[nowSceneNo].animationlist[i].material.uniforms['utime'].value = performance.now();
			}
		}
		if(changeSceneCount == 0){
			controls.update();
		}else if(changeSceneCount == 1){
			var timer = performance.now() - changeSceneTimer;
			if(timer > changeSceneTime / 2){
				// 次地点切替え、フェードイン開始
				changeScene2(nextSceneNo);
				changeSceneCount = 2;
				if(nextDirection == 'default'){
					var cameraDirection = new THREE.Vector3(config.scene[nextSceneNo].cameraDirection.x, config.scene[nextSceneNo].cameraDirection.y, config.scene[nextSceneNo].cameraDirection.z);
					cameraDirection.normalize();
					camera.position.set(cameraDirection.x / 100.0, cameraDirection.y / 100.0, cameraDirection.z / 100.0);
					camera.lookAt(new THREE.Vector3(0, 0, 0));
					camera.updateMatrixWorld(true); //matrixWorldを更新
				}

			}else{
				// フェードアウト中
				if(chmode == 'fov'){
					camera.fov = config.fov - 30 * (timer / (changeSceneTime / 2)); // 0.0 → 1.0
					camera.updateProjectionMatrix();
				}else if(chmode == 'move'){
					controls.enabled = false;
					var rate = timer * chspeed / 1000.0;
					camera.position.set(changeSceneTarget.x * rate, changeSceneTarget.y * rate, changeSceneTarget.z * rate);
				}

				const distance = 10; //カメラと板の距離
				const pos = new THREE.Vector3(0.0, 0.0, -distance);
				pos.applyMatrix4(camera.matrixWorld);
				camera.updateMatrixWorld(true); //matrixWorldを更新

				fadeinPanel.position.set(pos.x, pos.y, pos.z);
				fadeinPanel.setRotationFromQuaternion(camera.quaternion);
				fadeinPanel.material.opacity = timer / (changeSceneTime / 2);

			}
		}else if(changeSceneCount == 2){
			var timer = performance.now() - changeSceneTimer;
			if(timer > changeSceneTime){
				// フェードイン終了(地点変更完了)
				if(chmode == 'fov'){
					camera.fov = config.fov;
					camera.updateProjectionMatrix();
				}else if(chmode == 'move'){
					camera.position.set(changeSceneCameraPosition.x, changeSceneCameraPosition.y, changeSceneCameraPosition.z);
				}

				changeSceneCount = 0;
				controls.enabled = true;
				scene.remove(fadeinPanel);
				moveTrigger();
			}else{
				if(chmode == 'fov'){
					camera.fov = config.fov + 30 * ((changeSceneTime - timer) / (changeSceneTime / 2)); // 1.0 → 0.0
					camera.updateProjectionMatrix();
				}else if(chmode == 'move'){
					var rate = -(changeSceneTime - timer) * chspeed / 1000.0;
					camera.position.set(changeSceneTarget.x * rate, changeSceneTarget.y * rate, changeSceneTarget.z * rate);
				}


				const distance = 10; //カメラと板の距離
				const pos = new THREE.Vector3(0.0, 0.0, -distance);
				pos.applyMatrix4(camera.matrixWorld);
				camera.updateMatrixWorld(true); //matrixWorldを更新

				fadeinPanel.position.set(pos.x, pos.y, pos.z);
				fadeinPanel.setRotationFromQuaternion(camera.quaternion);
				fadeinPanel.material.opacity = (changeSceneTime - timer) / (changeSceneTime / 2);
			}
		}


		renderer.render(scene, camera);
	}
	requestAnimationFrame(animate);
}



function setscreen(){
	width  = $('#'+config.screenId).width();
	height = $('#'+config.screenId).height();

	scene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera(config.fov, width / height, 1, 3000);

//	var cameraDirection = new THREE.Vector3(config.cameraDirection.x, config.cameraDirection.y, config.cameraDirection.z);
	var cameraDirection = new THREE.Vector3(config.scene[nowSceneNo].cameraDirection.x, config.scene[nowSceneNo].cameraDirection.y, config.scene[nowSceneNo].cameraDirection.z);
	cameraDirection.normalize();
	camera.position.set(cameraDirection.x / 100.0, cameraDirection.y / 100.0, cameraDirection.z / 100.0);

	ambient = new THREE.AmbientLight(0xa0a0a0);
	scene.add(ambient);

	renderer = new THREE.WebGLRenderer({
		antialias: true,
		preserveDrawingBuffer: true,	// To capture image
		alpha: true
	});
	renderer.setSize(width, height);
//	renderer.shadowMapEnabled = true;
	renderer.shadowMapEnabled = false;
	renderer.setClearColor(0x000000, 0);
	$('#'+config.screenId).append(renderer.domElement);

	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.target.x = 0;
	controls.target.y = 0;
	controls.target.z = 0;
	controls.minDistance = 0;
	controls.maxDistance = 1000;
//	controls.maxDistance = 0.1;

	controls.addEventListener('change', function(){
		cameraMove += 1;
		moveTrigger();
	});
	controls.addEventListener('end', function(){
		setTimeout(function(){
			cameraMove = 0;
		}, 100);
	});
	animate();
}

function setFadeinPanel(){
	let curtain_opacity = 0.0;
	const curtain_color = new THREE.Color("rgb(0, 0, 0)");
	const geo_curtain = new THREE.PlaneGeometry(900.0, 900.0);
	const mat_curtain = new THREE.MeshBasicMaterial({
	    color: curtain_color,
	    transparent: true,
	    opacity: curtain_opacity
	  });
	fadeinPanel = new THREE.Mesh(geo_curtain, mat_curtain);
}

function setScene(sno){
	if(!config.scene[sno].loaded){
		config.scene[sno].movielist = [];
		config.scene[sno].animationlist = [];
		MakeSkybox(sno);
		setMarker(sno);

		config.scene[sno].loaded = true;
	}
	scene.add(config.scene[sno].skybox_mesh);
	for(var i = 0; i < config.scene[sno].marker.length; i++){
		scene.add(config.scene[sno].marker[i].mesh);
	}

	for(var i = 0; i < config.scene[sno].movielist.length; i++){
		var src = config.scene[sno].movielist[i].src;
		movielist[src].video.play();

	}

	var detail = {};
	detail.type = 'viewScene';
	detail.title = config.scene[sno].name;
	detail.sceneID = sno;
	$('#'+config.screenId).trigger('action', detail);
	moveTrigger();
}

function removeScene(sno){
	scene.remove(config.scene[sno].skybox_mesh);
	for(var i = 0; i < config.scene[nowSceneNo].marker.length; i++){
		scene.remove(config.scene[sno].marker[i].mesh);
	}

	for(var i = 0; i < config.scene[sno].movielist.length; i++){
		var src = config.scene[sno].movielist[i].src;
		movielist[src].video.pause();
	}
}

function allVideoPause(){
	for(var i = 0; i < config.scene[nowSceneNo].movielist.length; i++){
		var src = config.scene[nowSceneNo].movielist[i].src;
		movielist[src].video.pause();
	}
}
function allVideoPlay(){
	for(var i = 0; i < config.scene[nowSceneNo].movielist.length; i++){
		var src = config.scene[nowSceneNo].movielist[i].src;
		movielist[src].video.play();
	}
}

function MakeSkybox(sno){
	// skybox用のマテリアルを生成
	if(!config.scene[sno].skybox_mesh){

		if(config.map_type == 'equirectangular'){
			var geometry = new THREE.SphereGeometry(config.skyBoxLength / 2, 90, 12);
			geometry.scale(-1, 1, 1);
			var material = new THREE.MeshBasicMaterial({
				side: THREE.DoubleSide
		    });
			mesh = new THREE.Mesh(geometry, material);
			mesh.position.set(0, 0, 0);

			var loader = new THREE.TextureLoader();
			var texture = loader.load(config.scene[sno].image_equirectangular);
			mesh.material.map = texture;
			mesh.rotation.y = Math.PI / 2;
		}else{

			var cubeShader = THREE.ShaderLib["cube"];

			// Skybox用ジオメトリ生成
			var geometry = new THREE.BoxGeometry(config.skyBoxLength, config.skyBoxLength, config.skyBoxLength);
			geometry.scale(-1, 1, 1);

			// デフォルトのテクスチャを読込み
			var textureLoader = new THREE.TextureLoader(); 

			var texture0 = textureLoader.load(config.scene[sno].skybox[0]); 
			var texture1 = textureLoader.load(config.scene[sno].skybox[1]); 
			var texture2 = textureLoader.load(config.scene[sno].skybox[2]); 
			var texture3 = textureLoader.load(config.scene[sno].skybox[3]); 
			var texture4 = textureLoader.load(config.scene[sno].skybox[4]); 
			var texture5 = textureLoader.load(config.scene[sno].skybox[5]); 

			var materials = [
				new THREE.MeshBasicMaterial({ map: texture0 }), 
				new THREE.MeshBasicMaterial({ map: texture1 }), 
				new THREE.MeshBasicMaterial({ map: texture2 }), 
				new THREE.MeshBasicMaterial({ map: texture3 }), 
				new THREE.MeshBasicMaterial({ map: texture4 }), 
				new THREE.MeshBasicMaterial({ map: texture5 }) 
			]; 
	//		var faceMaterial = new THREE.MeshFaceMaterial(materials); 
			var mesh = new THREE.Mesh(geometry, materials);
		}
		config.scene[sno].skybox_mesh = mesh;
	}
}


function setMovie(sno, markerNo){
	var index = config.scene[sno].movielist.length;
	config.scene[sno].movielist[index] = {};

	var src = config.scene[sno].marker[markerNo].src;
	config.scene[sno].movielist[index].src = src;

	if(movielist[src] === undefined){
		movielist[src] = {};
		movielist[src].video = document.createElement('video');
		movielist[src].video.setAttribute('playsinline', '');
		movielist[src].video.loop = true;
		movielist[src].video.muted = true;
		movielist[src].video.src = config.scene[sno].marker[markerNo].src;
		movielist[src].video.load();

		movielist[src].videoImage = document.createElement('canvas');
		movielist[src].videoImage.width = config.scene[sno].marker[markerNo].movieWidth;
		movielist[src].videoImage.height = config.scene[sno].marker[markerNo].movieHeight;

		movielist[src].videoImageContext = movielist[src].videoImage.getContext('2d');
		movielist[src].videoImageContext.fillStyle = '#000000';
		movielist[src].videoImageContext.fillRect(0, 0, movielist[src].videoImage.width, movielist[src].videoImage.height);

//		textureとしてTHREE.Textureオブジェクトを生成
		movielist[src].videoTexture = new THREE.Texture(movielist[src].videoImage);
		movielist[src].videoTexture.minFilter = THREE.LinearFilter;
		movielist[src].videoTexture.magFilter = THREE.LinearFilter;
	}


	//生成したvideo textureをmapに指定しマテリアルを生成
	config.scene[sno].movielist[index].movieMaterial = new THREE.MeshBasicMaterial({map: movielist[src].videoTexture, opacity: config.scene[sno].marker[markerNo].opacity, transparent: true});

	return config.scene[sno].movielist[index].movieMaterial;
}


function setMarker(sceneNo){
	config.scene[sceneNo].movielist = [];
	config.scene[sceneNo].animationlist = [];

	for(let i = 0; i < config.scene[sceneNo].marker.length; i++){
		let marker = config.scene[sceneNo].marker[i];
		if(marker.type == "animation"){

			if(animlist[marker.src] === undefined){
				var loader = new THREE.TextureLoader();
				var texture = loader.load(marker.src);
				texture.minFilter = THREE.LinearFilter;
				texture.magFilter = THREE.LinearFilter;
				animlist[marker.src] = texture;
			}


			var material = new THREE.ShaderMaterial({
				transparent: true, 
				opacity: marker.opacity,
				vertexShader: vertexSource,
				fragmentShader: fragmentSource,
				uniforms: {
					divisionX: {value: marker.animationDivisionX },
					divisionY: {value: marker.animationDivisionY },
					interval: {value: marker.animationInterval },
					v_texture: {value: animlist[marker.src]},
					utime: {value: 2}
				}
			});
		}else if(marker.type == "image"){
			if(imglist[marker.src] === undefined){
				var loader = new THREE.TextureLoader();
				if(foce_load == 1){
					var texture = loader.load(marker.src+'?fm='+Math.random());
				}else{
					var texture = loader.load(marker.src);
				}
				texture.minFilter = THREE.LinearFilter;
				texture.magFilter = THREE.LinearFilter;

				imglist[marker.src] = texture;
			}
			var material = new THREE.MeshBasicMaterial( { map: imglist[marker.src], transparent: true, opacity: marker.opacity, transparent: true } );
		}else if(marker.type == "movie"){
			var material = setMovie(sceneNo, i);
		}else{
			var material = new THREE.MeshBasicMaterial( { color: marker.color, opacity: marker.opacity, transparent: true} );
		}

		if(marker.point_type == 'coord'){
			var geometry = new THREE.Geometry();

			geometry.vertices.push(new THREE.Vector3(-0.5,  0.5, 0));
			geometry.vertices.push(new THREE.Vector3( 0.5,  0.5, 0));
			geometry.vertices.push(new THREE.Vector3(-0.5, -0.5, 0));
			geometry.vertices.push(new THREE.Vector3( 0.5, -0.5, 0));
			geometry.faces.push(new THREE.Face3( 0, 1, 2));
			geometry.faces.push(new THREE.Face3( 1, 3, 2));
			geometry.faceVertexUvs[0] = [
			    [
			        new THREE.Vector2(1.0, 1.0),
			        new THREE.Vector2(0.0, 1.0),
			        new THREE.Vector2(1.0, 0.0),
			    ],
			    [
			        new THREE.Vector2(0.0, 1.0),
			        new THREE.Vector2(0.0, 0.0),
			        new THREE.Vector2(1.0, 0.0),
			    ]
			];
		}else{
			var geometry = new THREE.PlaneGeometry(1.0, 1.0);
		}

		var mesh = new THREE.Mesh( geometry, material);
		moveMarker(mesh, marker);

		mesh.name = sceneNo+'_'+i;
		config.scene[sceneNo].marker[i].mesh = mesh;
		if(marker.type == "animation"){
			config.scene[sceneNo].animationlist[config.scene[sceneNo].animationlist.length] = mesh;
		}
	}
}


function moveMarker(mesh, marker){
	if(marker.point_type == 'coord'){
		mesh.scale.x = 1;
		mesh.scale.y = 1;
		var quat = new THREE.Quaternion();
		quat.setFromEuler(new THREE.Euler(0, 0, 0));
		mesh.quaternion.copy(quat);
		mesh.position.set(0, 0, 0);

		var rate = (config.markerRadius + marker.depth) / config.markerRadius;

		var p1 = new THREE.Vector3(marker.p1.x * rate, marker.p1.y * rate, marker.p1.z * rate);
		var p2 = new THREE.Vector3(marker.p2.x * rate, marker.p2.y * rate, marker.p2.z * rate);
		var p3 = new THREE.Vector3(marker.p3.x * rate, marker.p3.y * rate, marker.p3.z * rate);
		var p4 = new THREE.Vector3(marker.p4.x * rate, marker.p4.y * rate, marker.p4.z * rate);

		var c_gravity = p1.clone();
		c_gravity.add(p2);
		c_gravity.add(p3);
		c_gravity.add(p4);
		c_gravity.multiplyScalar(0.25);

		mesh.geometry.vertices[0] = p2.sub(c_gravity);
		mesh.geometry.vertices[1] = p1.sub(c_gravity);
		mesh.geometry.vertices[2] = p3.sub(c_gravity);
		mesh.geometry.vertices[3] = p4.sub(c_gravity);

/*
		mesh.geometry.vertices[0] = new THREE.Vector3(-marker.p2.x * rate, marker.p2.y * rate, marker.p2.z * rate);
		mesh.geometry.vertices[1] = new THREE.Vector3(-marker.p1.x * rate, marker.p1.y * rate, marker.p1.z * rate);
		mesh.geometry.vertices[2] = new THREE.Vector3(-marker.p3.x * rate, marker.p3.y * rate, marker.p3.z * rate);
		mesh.geometry.vertices[3] = new THREE.Vector3(-marker.p4.x * rate, marker.p4.y * rate, marker.p4.z * rate);
*/
		mesh.position.set(c_gravity.x, c_gravity.y, c_gravity.z);
		mesh.geometry.verticesNeedUpdate = true;

	}else{
		mesh.scale.x = marker.width;
		mesh.scale.y = marker.height;

		var radius = config.markerRadius + marker.depth;
		// 仰角
		var phi = (marker.latitude * Math.PI) / 180;
		// 方位角
		var theta = ((marker.longitude - 180) * Math.PI) / 180;

		var x = -radius * Math.cos(phi) * Math.cos(theta);
		var y = radius * Math.sin(phi);
		var z = radius * Math.cos(phi) * Math.sin(theta);

		var quat1 = new THREE.Quaternion();
		var euler = new THREE.Euler(THREE.Math.degToRad(marker.rotation.x), THREE.Math.degToRad(marker.rotation.y), THREE.Math.degToRad(marker.rotation.z), 'XYZ');
		quat1.setFromEuler(euler);
		mesh.quaternion.copy(quat1);

		var quat2 = new THREE.Quaternion();
		quat2.setFromEuler(new THREE.Euler(phi, THREE.Math.degToRad(marker.longitude - 90), 0, 'ZYX'));
		mesh.quaternion.premultiply(quat2);

		mesh.position.set(x, y, z);
	}
}


function collisionDetection(){
	$(document).on('click touchend mousemove', '#'+config.screenId, function(event) {    // click
		if(cameraMove < 3){
			if (event.target == renderer.domElement){
				var width  = $('#'+config.screenId).width();
				var height = $('#'+config.screenId).height();
				//マウス座標2D変換
				var rect = event.target.getBoundingClientRect();

				var x = 0, y = 0;
				if (event.touches && event.touches[0]) {
					x = event.touches[0].clientX;
					y = event.touches[0].clientY;
				} else if (event.originalEvent && event.originalEvent.changedTouches && event.originalEvent.changedTouches[0]) {
					x = event.originalEvent.changedTouches[0].clientX;
					y = event.originalEvent.changedTouches[0].clientY;
				} else if (event.clientX && event.clientY) {
					x = event.clientX;
					y = event.clientY;
				}

				var mouse = { x: 0, y: 0 };
				mouse.x =  x - rect.left;
				mouse.y =  y - rect.top;

				//マウス座標3D変換 width（横）やheight（縦）は画面サイズ
				mouse.x =  (mouse.x / width) * 2 - 1;
				mouse.y = -(mouse.y / height) * 2 + 1;
				var raycaster = new THREE.Raycaster();
				raycaster.setFromCamera(mouse, camera);
				var intersects = raycaster.intersectObjects(scene.children);
				if(intersects.length > 0){
					if(event.type == "mousemove"){
						var flag = 0;
						var flag = 0;
						var first_hit = '';
						var first_hit_marker;
						for(i = 0; i < intersects.length; i++){
							if(intersects[i].object.name !== '' && intersects[i].object.name !== undefined){
								var tmp = intersects[i].object.name.split('_');
								let marker = config.scene[tmp[0]].marker[tmp[1]];

								if(first_hit == ''){
									first_hit = intersects[i].object.name;
									first_hit_marker = config.scene[tmp[0]].marker[tmp[1]];
								}

								if(marker.action == "link" || marker.action == "image" || marker.action == "pdf" || marker.action == "movie" || marker.action == "eval" || marker.action == "move"){

									flag = 1;
								}
							}
						}
						if(flag == 1){
							$(this).css("cursor","pointer");
						}else{
							$(this).css("cursor","auto");
						}
						if(mouseOver == ''){
							if(first_hit != ''){
								mouseOver = first_hit;
								//mover イベント発行 
								var detail = {};
								detail.type = 'mover';
								detail.tooltip = first_hit_marker.tooltip;
								detail.pageX = event.pageX;
								detail.pageY = event.pageY;
								$('#'+config.screenId).trigger('mover', detail);
							}
						}else{
							if(first_hit == ''){
								mouseOver = '';
								//mout イベント発行 
								var detail = {};
								detail.type = 'mover';
								detail.tooltip = '';
								detail.pageX = event.pageX;
								detail.pageY = event.pageY;
								$('#'+config.screenId).trigger('mout', detail);
							}else if(mouseOver != first_hit){
								mouseOver = first_hit;
								//mover イベント発行 
								var detail = {};
								detail.type = 'mover';
								detail.tooltip = first_hit_marker.tooltip;
								detail.pageX = event.pageX;
								detail.pageY = event.pageY;
								$('#'+config.screenId).trigger('mover', detail);
							}
						}

					}else{
						for(i = 0; i < intersects.length; i++){
							if(intersects[i].object.name !== '' && intersects[i].object.name !== undefined){
								changeSceneTarget = intersects[i].object.position.clone();
								changeSceneTarget.normalize();
								changeSceneCameraPosition.x = camera.position.x;
								changeSceneCameraPosition.y = camera.position.y;
								changeSceneCameraPosition.z = camera.position.z;
								doAction(intersects[i].object.name);
								break;
							}
						}
					}
				}
			}
		}
	});
}

function changeScene(sno, mode, setDirection){
	if(mode == ''){
		mode = 'fov';
	}
	if(setDirection == ''){
		setDirection = false;
	}
	chmode = mode;
	nextSceneNo = sno;
	if(setDirection == true){
		nextDirection = 'default';
	}else{
		nextDirection = '';
	}
	scene.add(fadeinPanel);
	changeSceneTimer = performance.now();
	if(!config.scene[sno].loaded){
		MakeSkybox(sno);
		setMarker(sno);
		config.scene[sno].loaded = true;
	}
	changeSceneCount = 1;
}


function changeScene2(id){
	anmationStop = 1;

	if(nowSceneNo >= 0){
		removeScene(nowSceneNo);
	}
	setScene(id);
	nowSceneNo = id;
	anmationStop = 0;
}

function doAction(prm){
	var tmp = prm.split('_');

	let marker = config.scene[tmp[0]].marker[tmp[1]];

	if(marker.action == "link"){
		var prm2 = marker.actionParameter.split(',');

		var detail = {};
		detail.type = 'link';
		detail.title = marker.title;
		detail.url = prm2[0];

		if(prm2.length == 1){
			detail.target = '_self';
		}else{
			detail.target = prm2[1];
			window.open(prm2[0], prm2[1]);
		}

		$('#'+config.screenId).trigger('action', detail);
	}else if(marker.action == "image"){
		var detail = {};
		detail.type = 'image';
		detail.title = marker.title;
		detail.url = marker.actionParameter;
		detail.description = marker.description;
		detail.descriptionTitle = marker.descriptionTitle;
		$('#'+config.screenId).trigger('action', detail);
	}else if(marker.action == "pdf"){
		var prm2 = marker.actionParameter.split(',');
		var detail = {};
		detail.type = 'pdf';
		detail.title = marker.title;
		detail.url = prm2[0];

		if(prm2.length == 1){
			detail.target = '_blank';
		}else{
			detail.target = prm2[1];
			window.open(prm2[0], prm2[1]);
		}
		$('#'+config.screenId).trigger('action', detail);
	}else if(marker.action == "movie"){
		var detail = {};
		detail.type = 'movie';
		detail.title = marker.title;
		detail.url = marker.actionParameter;
		detail.description = marker.description;
		detail.descriptionTitle = marker.descriptionTitle;
		$('#'+config.screenId).trigger('action', detail);
	}else if(marker.action == "eval"){
		var detail = {};
		detail.type = 'eval';
		detail.title = marker.title;
		detail.code = marker.actionParameter;
		$('#'+config.screenId).trigger('action', detail);
	}else if(marker.action == "move"){
		changeScene(marker.actionParameter, 'fov', false);
	}
}

function onResize(){
	var width  = $('#'+config.screenId).width();
	var height = $('#'+config.screenId).height();


	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(width, height);

	// カメラのアスペクト比を正す
	camera.aspect = width / height;
	camera.updateProjectionMatrix();

}
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function moveTrigger(){
	var detail = {};
	detail.sceneNo = nowSceneNo;

	var direction = new THREE.Vector2(-camera.position.x, -camera.position.z);
	direction.normalize();
	var zaxis = new THREE.Vector2(0, 1);
	if(camera.position.x < 0){
		detail.direction = -Math.acos(direction.dot(zaxis));
	}else{
		detail.direction = Math.acos(direction.dot(zaxis));
	}

	$('#'+config.screenId).trigger('move', detail);
}
