"use strict";

var $ = jQuery.noConflict();
var $body = $('body'),
  $header = $('#header'),
  $menuButton = $header.find('.menu'),
  $globalMenu = $header.find('.global_menu'),
  $menuNav = $globalMenu.find('.nav'),
  $subMenu = $menuNav.find('.sub').closest('li').addClass('has_sub');
var $overlay = $('.overlay'),
  overlayActiveClass = 'active',
  $overlayContents = $('.overlay__contents'),
  $overlayCaption = $('.overlay__caption'),
  $overlayInner = $('.overlay_inner'),
  overlayContentsActiveImageClass = 'image',
  $overlayImage = $('#simage'),
  overlayImageBaseSrc = 'image/loading.gif',
  overlayContentsActiveVideoClass = 'video',
  $overlayVideo = $('#smovie'),
  overlayVideoBaseSrc = 'blank.html';
  var formdata = {};

$(function() {
  vbooth('config_q.json');
  $menuButton.click(function() {
    $body.toggleClass('menu_opened');
  });
  $globalMenu.click(function() {
    $menuButton.trigger('click');
  }).find('span').click(function(e) {
    e.stopPropagation();
  }).find('a').click(function(e) {
    $menuButton.trigger('click');
    e.stopPropagation();
  });
  $subMenu.find('> span').click(function() {
    var $li = $(this).closest('li'),
      $sub = $li.find('> div.sub');

    if ($li.hasClass('opened')) {
      $li.removeClass('opened');
      $sub.height(0);
    } else {
      $li.addClass('opened');
      var height = 0;
      $sub.find('> ul').each(function(i, elem) {
        height += $(elem).height();
      });
      $sub.height(height);
    }
  }).click();
  $overlay.on('click', closesubw);
  $overlayImage.on('click', function(e) {
    e.stopPropagation();
  });
  $overlayVideo.on('click', function(e) {
    e.stopPropagation();
  });

	$('#screen').on('action', function(e, detail){
		if(detail.type == 'viewScene'){
			sendLog(detail.title, '');
		}else if(detail.type == 'link'){
			sendLog(detail.title, '');
			window.open(detail.url, detail.target);
		}else if(detail.type == 'image'){
			$overlayContents.addClass(overlayContentsActiveImageClass).removeAttr('style');
			$overlayImage.off('load');
			$overlayImage.on('load', function() {
				overlayContentsCtrl();
				$(this).off('load');
			});
			$overlayImage.attr('src', detail.url);
			$('#caption_title').html(detail.descriptionTitle);
			$('#caption').html(nl2br(detail.description));
			if(detail.descriptionTitle != "" || detail.description != ""){
				$('#caption_area').show();
			}else{
				$('#caption_area').hide();
			}
			$overlay.addClass(overlayActiveClass);
			deSelect();
			sendLog(detail.title, '');
		}else if(detail.type == 'pdf'){
			sendLog(detail.title, '');
			window.open(detail.url, detail.target);
		}else if(detail.type == 'movie'){
			$overlayContents.removeAttr('style');
			overlayContentsCtrl();
			$overlayVideo.attr('src', detail.url).attr('poster', detail.url + '.jpg');
			$('#caption_title').html(detail.descriptionTitle);
			$('#caption').html(nl2br(detail.description));
			if(detail.descriptionTitle != "" || detail.description != ""){
				$('#caption_area').show();
			}else{
				$('#caption_area').hide();
			}
			var OV = $overlayVideo.get(0);
			OV.load();
			$overlayVideo.on('canplay', function() {
				$overlayContents.addClass(overlayContentsActiveVideoClass);
				OV.play();
				overlayContentsCtrl();
			});
			$overlay.addClass(overlayActiveClass);
			allVideoPause();
			deSelect();
			sendLog(detail.title, '');
		}else if(detail.type == 'eval'){
			eval(detail.code);
		}

	});

	$.ajax({
		url:'/system/form.php?ftype=inquiry',
		type:'GET',
		dataType: "json"
	}).done(function(data){
		formdata['inquiry'] = data;
	});
	$.ajax({
		url:'/system/form.php?ftype=business',
		type:'GET',
		dataType: "json"
	}).done(function(data){
		formdata['business'] = data;
	});
	$.ajax({
		url:'/system/form.php?ftype=enquete',
		type:'GET',
		dataType: "json"
	}).done(function(data){
		formdata['enquete'] = data;
	});

});

function openform(ftype){
  closesubw();

  $('.form_complete').hide();
  $('#form_modal').find('.frame').addClass('large');
  $('#form_contents').html(formdata[ftype].html);
  if(ftype == 'business'){
    setcalender(formdata[ftype].maxdate, formdata[ftype].enable);
  }
  $('.frame_inner').scrollTop(0);
  $('#form_contents').show();
  $('#form_modal').show();
  $('.frame_inner').scrollTop(0);
}


function closesubw() {
  $('#form_modal').hide();
  closeImage();
  closeMovie();
}

function overlayContentsCtrl() {
  var ph = $overlayCaption.innerHeight(),
    wh = $overlayInner.innerHeight();
  console.log(ph, wh);

  if (ph !== "undefined" && ph > 0 && $('#caption_area').css('display') != 'none') {
    $overlayContents.css('max-height', wh - ph);
  } else {
    $overlayContents.css('margin-top', 0);
    $overlayContents.css('margin-bottom', '200px');
  }
}


function closeImage() {
  $overlay.removeClass(overlayActiveClass);
  $overlayContents.removeClass(overlayContentsActiveImageClass);
  $overlayImage.attr('src', overlayImageBaseSrc);
}


function closeMovie() {
  $overlayVideo.get(0).pause();
  $overlay.removeClass(overlayActiveClass);
  $overlayContents.removeClass(overlayContentsActiveVideoClass);
  $overlayVideo.attr('src', overlayVideoBaseSrc);
  allVideoPlay();
}

function formSend(){
	var formData = $('#form1').serialize();
	$('.form_send_btn').attr('disabled', true);
	$.ajax({
		url: '/system/form.php',
		type:'POST',
		data: formData,
		dataType: "json"
	}).done(function(data){
		$('.form_send_btn').attr('disabled', false);

		if(data.status == 'success'){
			$('#form_contents').hide();
			$('#form_complete_' + data.ftype).show();

		}else if(data.status == 'error'){
			$('#form_message').html(nl2br(data.message)).show();
			$('.frame_inner').scrollTop(0);
		}
	}).fail(function(jqXHR, textStatus, errorThrown){
		$('.form_send_btn').attr('disabled', false);
		alert("エラーが発生しました。\nしばらくしてからもう一度お試しください。");
	});

}

function nl2br(str) {
  if(str.length > 0){
    str = str.replace(/\r\n/g, "<br />");
    str = str.replace(/(\n|\r)/g, "<br />");
    return str;
  }else{
    return '';
  }
}

function setcalender(maxdate, enable){
	flatpickr('.calendar', {
		minDate: "today",
		maxDate: maxdate,
		dateFormat: "Y年m月d日",
		enable: enable,
		locale: {
			weekdays: {
				shorthand: ["日", "月", "火", "水", "木", "金", "土"],
				longhand: [
					"日曜日",
					"月曜日",
					"火曜日",
					"水曜日",
					"木曜日",
					"金曜日",
					"土曜日"
				],
			},
			months: {
				shorthand: [
					"1月",
					"2月",
					"3月",
					"4月",
					"5月",
					"6月",
					"7月",
					"8月",
					"9月",
					"10月",
					"11月",
					"12月"
				],
				longhand: [
					"1月",
					"2月",
					"3月",
					"4月",
					"5月",
					"6月",
					"7月",
					"8月",
					"9月",
					"10月",
					"11月",
					"12月"
				]
			},
			time_24hr: true,
			rangeSeparator: " から ",
			firstDayOfWeek: 0,
			amPM: ["午前", "午後"],
			yearAriaLabel: "年",
			hourAriaLabel: "時間",
			minuteAriaLabel: "分"
		}
	});
}
function deSelect() {
  if(window.getSelection){
    var selection = window.getSelection();
    selection.collapse(document.body, 0);
  }else{
    var selection = document.selection.createRange();
    selection.setEndPoint("EndToStart", selection);
    selection.select();
  }
}
