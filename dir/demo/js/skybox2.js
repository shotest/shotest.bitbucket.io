var vertexSource = "varying vec2 v_uv;\nvarying mat3 v_spriteMat;\nuniform float divisionX;\nuniform float divisionY;\nuniform float interval;\nuniform float utime;\nfloat ltime;\n\nvoid main(void) {\n\tltime = mod(utime / interval, divisionX * divisionY);\n\tmat3 scaleMat = mat3(\n\t\t1.0 / divisionX, 0.0, 0.0, // 横列のフレームの数分、横に縮小（フレーム4つ分縮小）\n\t\t0.0, 1.0 / divisionY, 0.0, // 縦列のフレームの数分、縦に縮小（フレーム5つ分縮小）\n\t\t0.0, 0.0, 1.0\n\t);\n\tmat3 translateMat = mat3(\n\t\t1.0, 0.0, int(mod(ltime, divisionX)), // 横に平行移動（横列の左から2つ目のフレームに平行移動）\n\t\t0.0, 1.0, int(divisionY - (ltime / divisionX)), // 縦に平行移動（縦列の下から3つ目のフレームに平行移動）\n\t\t0.0, 0.0, 1.0\n\t);\n\tv_spriteMat = translateMat * scaleMat; // UV座標をずらす用の変換行列を作成\n\tv_uv = uv;\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\n}\n";

var fragmentSource = "uniform sampler2D v_texture;\nvarying vec2 v_uv;\nvarying mat3 v_spriteMat;\n\nvoid main(void) {\n\tvec3 uv = vec3(v_uv, 1.0);\n\tuv *= v_spriteMat;\n\tvec4 smpColor = texture2D(v_texture, uv.xy / uv.z);\n\n\tgl_FragColor = smpColor;\n}\n";

var debug = 1;

var config;
var renderer;
var scene;
var camera;
var controls;
var nowSceneNo = 0;
var nextSceneNo = 0;
var cameraMove = 0;
var anmationStop = 0;
var changeSceneCount = 0;
var changeSceneTarget;
var changeSceneCameraPosition = THREE.Vector3;
var changeSceneTimer = 0;
var changeSceneTime = 1000;
var fadeinPanel;

jQuery(function($) {
	vartexShaderString = $('#vartexShaderString').val();
	fragmentShaderString = $('#fragmentShaderString').val();

	configDataLoad();


});

function init(){
	start3D();

//	var axes = new THREE.AxisHelper(1000);
//	scene.add(axes);
	window.addEventListener('resize', onResize, false );

	for(var i = 0; i < config.scene.length; i++){
		MakeSkybox(i);
	}

}

function configDataLoad(){
	$.getJSON('config2.json')
		.done(function(data, textStatus, jqXHR){
			config = data;
			init();
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			alert("設定ファイルを読み込めませんでした。");
//			alert(textStatus);
		});
}

function start3D(){
	collisionDetection();
	setscreen();
	setFadeinPanel();

	if(getParam('sno')){
		nowSceneNo = getParam('sno');
	}
	setScene(nowSceneNo);
//	setMarker(0);
	anmationStop = 0;
	animate();
}

function animate() {
	if(anmationStop == 0 ){

		if(config.scene[nowSceneNo].movielist){
			var movielist = config.scene[nowSceneNo].movielist;
			for(var i = 0; i < movielist.length; i++){
				if (movielist[i].video.readyState === movielist[i].video.HAVE_ENOUGH_DATA) {
				    movielist[i].videoImageContext.drawImage(movielist[i].video, 0, 0);
				    if(movielist[i].videoTexture) {
				        movielist[i].videoTexture.needsUpdate = true;
				    }
				}
			}
		}
		if(config.scene[nowSceneNo].animationlist){
			for(var i = 0; i < config.scene[nowSceneNo].animationlist.length; i++){
				config.scene[nowSceneNo].animationlist[i].material.uniforms['utime'].value = performance.now();
			}
		}
		var chspeed = 500;
		var chmode = 'fov';
		if(changeSceneCount == 0){
			controls.update();
		}else if(changeSceneCount == 1){
			var timer = performance.now() - changeSceneTimer;
			if(timer > changeSceneTime / 2){
				// 次地点切替え、フェードイン開始
				changeScene2(nextSceneNo);
				changeSceneCount = 2;
			}else{
				// フェードアウト中
				if(chmode == 'fov'){
					camera.fov = config.fov - 30 * (timer / (changeSceneTime / 2)); // 0.0 → 1.0
					camera.updateProjectionMatrix();
				}else{
					controls.enabled = false;
					var rate = timer * chspeed / 1000.0;
					camera.position.set(changeSceneTarget.x * rate, changeSceneTarget.y * rate, changeSceneTarget.z * rate);
				}

				const distance = 10; //カメラと板の距離
				const pos = new THREE.Vector3(0.0, 0.0, -distance);
				pos.applyMatrix4(camera.matrixWorld);
				camera.updateMatrixWorld(true); //matrixWorldを更新

				fadeinPanel.position.set(pos.x, pos.y, pos.z);
				fadeinPanel.setRotationFromQuaternion(camera.quaternion);
				fadeinPanel.material.opacity = timer / (changeSceneTime / 2);

			}
		}else if(changeSceneCount == 2){
			var timer = performance.now() - changeSceneTimer;
			if(timer > changeSceneTime){
				// フェードイン終了(地点変更完了)
				if(chmode == 'fov'){
					camera.fov = config.fov;
					camera.updateProjectionMatrix();
				}else{
					camera.position.set(changeSceneCameraPosition.x, changeSceneCameraPosition.y, changeSceneCameraPosition.z);
				}

				changeSceneCount = 0;
				controls.enabled = true;
				scene.remove(fadeinPanel);
			}else{
				if(chmode == 'fov'){
					camera.fov = config.fov + 30 * ((changeSceneTime - timer) / (changeSceneTime / 2)); // 1.0 → 0.0
					camera.updateProjectionMatrix();
				}else{
					var rate = -(changeSceneTime - timer) * chspeed / 1000.0;
					camera.position.set(changeSceneTarget.x * rate, changeSceneTarget.y * rate, changeSceneTarget.z * rate);
				}


				const distance = 10; //カメラと板の距離
				const pos = new THREE.Vector3(0.0, 0.0, -distance);
				pos.applyMatrix4(camera.matrixWorld);
				camera.updateMatrixWorld(true); //matrixWorldを更新

				fadeinPanel.position.set(pos.x, pos.y, pos.z);
				fadeinPanel.setRotationFromQuaternion(camera.quaternion);
				fadeinPanel.material.opacity = (changeSceneTime - timer) / (changeSceneTime / 2);
			}
		}


		renderer.render(scene, camera);
	}
	requestAnimationFrame(animate);
}



function setscreen(){
	width  = $('#'+config.screenId).width();
	height = $('#'+config.screenId).height();

	scene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera(config.fov, width / height, 1, 3000);
	var cameraDirection = new THREE.Vector3(config.cameraDirection.x, config.cameraDirection.y, config.cameraDirection.z);
	cameraDirection.normalize();
	if(debug == 1){
//		camera.position.set(300, 300, 300);
		camera.position.set(cameraDirection.x / 100.0, cameraDirection.y / 100.0, cameraDirection.z / 100.0);
	}else{
		camera.position.set(cameraDirection.x / 100.0, cameraDirection.y / 100.0, cameraDirection.z / 100.0);
	}
	ambient = new THREE.AmbientLight(0xa0a0a0);
	scene.add(ambient);

	renderer = new THREE.WebGLRenderer({
		antialias: true,
		preserveDrawingBuffer: true,	// To capture image
		alpha: true
	});
	renderer.setSize(width, height);
//	renderer.shadowMapEnabled = true;
	renderer.shadowMapEnabled = false;
	renderer.setClearColor(0x000000, 0);
	$('#'+config.screenId).append(renderer.domElement);

	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.target.x = 0;
	controls.target.y = 0;
	controls.target.z = 0;
	controls.minDistance = 0;
	controls.maxDistance = 1000;
//	controls.maxDistance = 0.1;

	controls.addEventListener('change', function(){
		cameraMove += 1;
	});
	controls.addEventListener('end', function(){
		setTimeout(function(){
			cameraMove = 0;
		}, 100);
	});
	animate();
}

function setFadeinPanel(){
	let curtain_opacity = 0.0;
	const curtain_color = new THREE.Color("rgb(0, 0, 0)");
	const geo_curtain = new THREE.PlaneGeometry(900.0, 900.0);
	const mat_curtain = new THREE.MeshBasicMaterial({
	    color: curtain_color,
	    transparent: true,
	    opacity: curtain_opacity
	  });
	fadeinPanel = new THREE.Mesh(geo_curtain, mat_curtain);
}

function setScene(sno){
	if(!config.scene[sno].loaded){
		config.scene[sno].movielist = [];
		config.scene[sno].animationlist = [];
		MakeSkybox(sno);
		setMarker(sno);

		config.scene[sno].loaded = true;
	}
	scene.add(config.scene[sno].skybox_mesh);
	for(var i = 0; i < config.scene[sno].marker.length; i++){
		scene.add(config.scene[sno].marker[i].mesh);
	}

	for(var i = 0; i < config.scene[sno].movielist.length; i++){
		config.scene[sno].movielist[i].video.play();
	}
	sendLog(config.scene[sno].name, '');

}

function removeScene(sno){
	scene.remove(config.scene[sno].skybox_mesh);
	for(var i = 0; i < config.scene[nowSceneNo].marker.length; i++){
		scene.remove(config.scene[sno].marker[i].mesh);
	}

	for(var i = 0; i < config.scene[sno].movielist.length; i++){
		config.scene[sno].movielist[i].video.pause();
	}
}

function allVideoPause(){
	for(var i = 0; i < config.scene[nowSceneNo].movielist.length; i++){
		config.scene[nowSceneNo].movielist[i].video.pause();
	}
}
function allVideoPlay(){
	for(var i = 0; i < config.scene[nowSceneNo].movielist.length; i++){
		config.scene[nowSceneNo].movielist[i].video.play();
	}
}

function MakeSkybox(sno){
	// skybox用のマテリアルを生成
	if(!config.scene[sno].skybox_mesh){
		var cubeShader = THREE.ShaderLib["cube"];

		// Skybox用ジオメトリ生成
		var geometry = new THREE.BoxGeometry(config.skyBoxLength, config.skyBoxLength, config.skyBoxLength);
		geometry.scale(-1, 1, 1);

		// デフォルトのテクスチャを読込み
		var textureLoader = new THREE.TextureLoader(); 

		var texture0 = textureLoader.load(config.scene[sno].skybox[0]); 
		var texture1 = textureLoader.load(config.scene[sno].skybox[1]); 
		var texture2 = textureLoader.load(config.scene[sno].skybox[2]); 
		var texture3 = textureLoader.load(config.scene[sno].skybox[3]); 
		var texture4 = textureLoader.load(config.scene[sno].skybox[4]); 
		var texture5 = textureLoader.load(config.scene[sno].skybox[5]); 

		var materials = [
			new THREE.MeshBasicMaterial({ map: texture0 }), 
			new THREE.MeshBasicMaterial({ map: texture1 }), 
			new THREE.MeshBasicMaterial({ map: texture2 }), 
			new THREE.MeshBasicMaterial({ map: texture3 }), 
			new THREE.MeshBasicMaterial({ map: texture4 }), 
			new THREE.MeshBasicMaterial({ map: texture5 }) 
		]; 
		var faceMaterial = new THREE.MeshFaceMaterial(materials); 
		var mesh = new THREE.Mesh(geometry, faceMaterial);
		config.scene[sno].skybox_mesh = mesh;
	}
}


function setMovie(sno, markerNo){
	var index = config.scene[sno].movielist.length;
	config.scene[sno].movielist[index] = {};

	config.scene[sno].movielist[index].video = document.createElement('video');
	config.scene[sno].movielist[index].video.setAttribute('playsinline', '');
	config.scene[sno].movielist[index].video.loop = true;
	config.scene[sno].movielist[index].video.muted = true;
	config.scene[sno].movielist[index].video.src = config.scene[sno].marker[markerNo].src;
	config.scene[sno].movielist[index].video.load();

/*
	var texture = new THREE.VideoTexture( config.scene[sno].movielist[index].video );
	texture.minFilter = THREE.LinearFilter;
	texture.magFilter = THREE.LinearFilter;
	texture.format = THREE.RGBFormat;
	config.scene[sno].movielist[index].movieMaterial = new THREE.MeshBasicMaterial({map: texture});
	return config.scene[sno].movielist[index].movieMaterial;
*/


	config.scene[sno].movielist[index].videoImage = document.createElement('canvas');
	config.scene[sno].movielist[index].videoImage.width = config.scene[sno].marker[markerNo].movieWidth;
	config.scene[sno].movielist[index].videoImage.height = config.scene[sno].marker[markerNo].movieHeight;

	config.scene[sno].movielist[index].videoImageContext = config.scene[sno].movielist[index].videoImage.getContext('2d');
	config.scene[sno].movielist[index].videoImageContext.fillStyle = '#000000';
	config.scene[sno].movielist[index].videoImageContext.fillRect(0, 0, config.scene[sno].movielist[index].videoImage.width, config.scene[sno].movielist[index].videoImage.height);

	//生成したcanvasをtextureとしてTHREE.Textureオブジェクトを生成
	config.scene[sno].movielist[index].videoTexture = new THREE.Texture(config.scene[sno].movielist[index].videoImage);
	config.scene[sno].movielist[index].videoTexture.minFilter = THREE.LinearFilter;
	config.scene[sno].movielist[index].videoTexture.magFilter = THREE.LinearFilter;

	//生成したvideo textureをmapに指定し、overdrawをtureにしてマテリアルを生成
	config.scene[sno].movielist[index].movieMaterial = new THREE.MeshBasicMaterial({map: config.scene[sno].movielist[index].videoTexture, overdraw: true, opacity: config.scene[sno].marker[markerNo].opacity, transparent: true});

	return config.scene[sno].movielist[index].movieMaterial;
}


function setMarker(sceneNo){
	config.scene[sceneNo].movielist = [];
	config.scene[sceneNo].animationlist = [];

	for(let i = 0; i < config.scene[sceneNo].marker.length; i++){
		let marker = config.scene[sceneNo].marker[i];
		if(marker.type == "animation"){
			var loader = new THREE.TextureLoader();
			var texture = loader.load(marker.src);
			var material = new THREE.ShaderMaterial({
				transparent: true, 
				opacity: marker.opacity,
				vertexShader: vertexSource,
				fragmentShader: fragmentSource,
				uniforms: {
					divisionX: {value: marker.animationDivisionX },
					divisionY: {value: marker.animationDivisionY },
					interval: {value: marker.animationInterval },
					v_texture: {value: texture},
					utime: {value: 2}
				}
			});
		}else if(marker.type == "image"){
			var loader = new THREE.TextureLoader();
			var texture = loader.load(marker.src);
			var material = new THREE.MeshBasicMaterial( { map: texture, transparent: true, opacity: marker.opacity, transparent: true } );
		}else if(marker.type == "movie"){
			var material = setMovie(sceneNo, i);
		}else{
			var material = new THREE.MeshBasicMaterial( { color: marker.color, opacity: marker.opacity, transparent: true} );
		}

		var geometry = new THREE.PlaneGeometry(1.0, 1.0);

		var mesh = new THREE.Mesh( geometry, material);
		moveMarker(mesh, marker);

		mesh.name = sceneNo+'_'+i;
		config.scene[sceneNo].marker[i].mesh = mesh;
		if(marker.type == "animation"){
			config.scene[sceneNo].animationlist[config.scene[sceneNo].animationlist.length] = mesh;
		}
	}
}


function moveMarker(mesh, marker){
	mesh.scale.x = marker.width;
	mesh.scale.y = marker.height;

	var radius = config.markerRadius + marker.depth;

	// 仰角
	var phi = (marker.latitude * Math.PI) / 180;
	// 方位角
	var theta = ((marker.longitude - 180) * Math.PI) / 180;

	var x = -radius * Math.cos(phi) * Math.cos(theta);
	var y = radius * Math.sin(phi);
	var z = radius * Math.cos(phi) * Math.sin(theta);

	mesh.position.set(x, y, z);

	var quat = getQuaternion(marker.latitude + marker.rotation.x, marker.longitude - 90 + marker.rotation.y, marker.rotation.z);
//	var quat = getQuaternion(marker.latitude, marker.longitude - 90, 0);
	mesh.quaternion.copy(quat);

}


function getQuaternion(angleX, angleY, angleZ){
	// Declare X and Y axes
	var axisX = new THREE.Vector3(1, 0, 0);
	var axisY = new THREE.Vector3(0, 1, 0);
	var axisZ = new THREE.Vector3(0, 0, 1);

	// Init quaternions that will rotate along each axis
	var quatX = new THREE.Quaternion();
	var quatY = new THREE.Quaternion();
	var quatZ = new THREE.Quaternion();

	// Set quaternions from each axis (in radians)...
	quatX.setFromAxisAngle(axisX, THREE.Math.degToRad(angleX));
	quatY.setFromAxisAngle(axisY, THREE.Math.degToRad(angleY));
	quatZ.setFromAxisAngle(axisZ, THREE.Math.degToRad(angleZ));
	quatY.multiply(quatX);
	quatZ.multiply(quatY);

	return quatZ;
}

function collisionDetection(){
	$(document).on('click touchend mousemove', '#'+config.screenId, function(event) {    // click
		if(cameraMove < 3){
			if (event.target == renderer.domElement){
				var width  = $('#'+config.screenId).width();
				var height = $('#'+config.screenId).height();
				//マウス座標2D変換
				var rect = event.target.getBoundingClientRect();

				var x = 0, y = 0;
				if (event.touches && event.touches[0]) {
					x = event.touches[0].clientX;
					y = event.touches[0].clientY;
				} else if (event.originalEvent && event.originalEvent.changedTouches && event.originalEvent.changedTouches[0]) {
					x = event.originalEvent.changedTouches[0].clientX;
					y = event.originalEvent.changedTouches[0].clientY;
				} else if (event.clientX && event.clientY) {
					x = event.clientX;
					y = event.clientY;
				}

				var mouse = { x: 0, y: 0 };
				mouse.x =  x - rect.left;
				mouse.y =  y - rect.top;

				//マウス座標3D変換 width（横）やheight（縦）は画面サイズ
				mouse.x =  (mouse.x / width) * 2 - 1;
				mouse.y = -(mouse.y / height) * 2 + 1;
				var raycaster = new THREE.Raycaster();
				raycaster.setFromCamera(mouse, camera);
				var intersects = raycaster.intersectObjects(scene.children);
				if(intersects.length > 0){
					if(event.type == "mousemove"){
						var flag = 0;
						for(i = 0; i < intersects.length; i++){
							if(intersects[i].object.name !== '' && intersects[i].object.name !== undefined){

								var tmp = intersects[i].object.name.split('_');
								let marker = config.scene[tmp[0]].marker[tmp[1]];
								if(marker.action == "link" || marker.action == "image" || marker.action == "pdf" || marker.action == "movie" || marker.action == "eval" || marker.action == "move"){

								flag = 1;
								}
							}
						}
						if(flag == 1){
							$(this).css("cursor","pointer");
						}else{
							$(this).css("cursor","auto");
						}
					}else{
						for(i = 0; i < intersects.length; i++){
							if(intersects[i].object.name !== '' && intersects[i].object.name !== undefined){
								changeSceneTarget = intersects[i].object.position.clone();
								changeSceneTarget.normalize();
								changeSceneCameraPosition.x = camera.position.x;
								changeSceneCameraPosition.y = camera.position.y;
								changeSceneCameraPosition.z = camera.position.z;
								doAction(intersects[i].object.name);
							}
						}
					}
				}
			}
		}
	});
}

function changeScene(sno){
	nextSceneNo = sno;
	scene.add(fadeinPanel);
	changeSceneTimer = performance.now();
	if(!config.scene[sno].loaded){
		MakeSkybox(sno);
		setMarker(sno);
		config.scene[sno].loaded = true;
	}
	changeSceneCount = 1;
}


function changeScene2(id){
	anmationStop = 1;

	removeScene(nowSceneNo);
	setScene(id);
	nowSceneNo = id;
	anmationStop = 0;
}

function doAction(prm){
	var tmp = prm.split('_');

	let marker = config.scene[tmp[0]].marker[tmp[1]];

	if(marker.action == "link"){
		var prm2 = marker.actionParameter.split(',');
		if(prm2.length == 1){
			window.open(prm2[0]);
		}else{
			window.open(prm2[0], prm2[1]);
		}
		sendLog(marker.title, '');

//		location.href = marker.actionParameter;
	}else if(marker.action == "image"){
		sendLog(marker.title, '');
		openImage(marker.actionParameter, marker.title);
//		$('#imageObj').attr("src", marker.actionParameter);
//		$.colorbox({inline: true, width:"90%", height:"90%", href:"#image"});
	}else if(marker.action == "pdf"){
		openPDF(marker.actionParameter, marker.title);

/*
		var prm2 = marker.actionParameter.split(',');
		if(prm2.length == 1){
			window.open(prm2[0]);
		}else{
			window.open(prm2[0], prm2[1]);
		}
*/
//		location.href = marker.actionParameter;
	}else if(marker.action == "movie"){
		openMovie(marker.actionParameter, marker.title);

//		$('#movieObj').attr("src", marker.actionParameter);
//		$.colorbox({inline: true, width:"90%", height:"90%", href:"#movie", onClosed: function(){$("#movieObj").get(0).pause();}});
//		$('#movieObj').get(0).play();
	}else if(marker.action == "eval"){
		eval(marker.actionParameter);
	}else if(marker.action == "move"){
		changeScene(marker.actionParameter);
	}
}

function onResize(){
	var width  = $('#'+config.screenId).width();
	var height = $('#'+config.screenId).height();


	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(width, height);

	// カメラのアスペクト比を正す
	camera.aspect = width / height;
	camera.updateProjectionMatrix();

}
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
