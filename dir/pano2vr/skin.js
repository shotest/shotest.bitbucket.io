// Garden Gnome Software - Skin
// Pano2VR 6.0.5/17333
// Filename: shotest_pano.ggsk
// Generated 2019-07-10T11:55:42

function pano2vrSkin(player,base) {
	var me=this;
	var skin=this;
	var flag=false;
	var hotspotTemplates={};
	var skinKeyPressed = 0;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	var hs,el,els,elo,ela,elHorScrollFg,elHorScrollBg,elVertScrollFg,elVertScrollBg,elCornerBg;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	player.setMargins(10,10,10,10);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	this.callNodeChange=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggNodeChange) {
				e.ggNodeChange();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	player.addListener('configloaded', function() { me.callNodeChange(me.divSkin); });
	player.addListener('changenode', function() { me.ggUserdata=player.userdata; me.callNodeChange(me.divSkin); });
	
	var parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		el=me._container_1=document.createElement('div');
		el.ggId="Container 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='bottom : 298px;';
		hs+='height : 50px;';
		hs+='left : 347px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 308px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_1.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._container_1.ggUpdatePosition=function (useTransition) {
		}
		el=me._default_loading_bar=document.createElement('div');
		el.ggId="default loading bar";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -20px;';
		hs+='position : absolute;';
		hs+='top : -5px;';
		hs+='visibility : inherit;';
		hs+='width : 308px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._default_loading_bar.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._default_loading_bar.ggUpdatePosition=function (useTransition) {
		}
		el=me._loading_text=document.createElement('div');
		els=me._loading_text__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="loading text";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 23px;';
		hs+='left : -314px;';
		hs+='position : absolute;';
		hs+='top : 307px;';
		hs+='visibility : inherit;';
		hs+='width : 334px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 334px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(170,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._loading_text.ggUpdateText=function() {
			var hs="<b>Loading... "+(player.getPercentLoaded()*100.0).toFixed(0)+"% ("+(player.getPercentLoaded()*100000.0).toFixed(0)+"kb \/ "+(100000.0).toFixed(0)+"kb)<\/b>";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._loading_text.ggUpdateText();
		el.appendChild(els);
		me._loading_text.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._loading_text.ggUpdatePosition=function (useTransition) {
		}
		me._default_loading_bar.appendChild(me._loading_text);
		el=me._loading_bar=document.createElement('div');
		el.ggId="loading bar";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : #aa0000;';
		hs+='border : 0px solid #000080;';
		hs+='bottom : -283px;';
		hs+='cursor : default;';
		hs+='height : 5px;';
		hs+='left : -317px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 960px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='0% 50%';
		me._loading_bar.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._loading_bar.ggUpdatePosition=function (useTransition) {
		}
		me._default_loading_bar.appendChild(me._loading_bar);
		me._container_1.appendChild(me._default_loading_bar);
		me.divSkin.appendChild(me._container_1);
		el=me._container_3=document.createElement('div');
		el.ggId="Container 3";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='bottom : 20px;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 10px;';
		hs+='visibility : inherit;';
		hs+='width : 157px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_3.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._container_3.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_2=document.createElement('div');
		el.ggId="Container 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='bottom : 0px;';
		hs+='height : 52px;';
		hs+='position : absolute;';
		hs+='right : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 156px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._container_2.ggUpdatePosition=function (useTransition) {
		}
		el=me._svg_1=document.createElement('div');
		els=me._svg_1__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLWxlZnQtZmZmPC90aXRsZT4NCiAgPGc+DQogICAgPGc+DQogICAgICA8cG9seWdvbiBwb2ludHM9IjguOTk4IDI2LjQxOCAyMy41NzkgNDEuMDg1IDI0Ljk5NyAzOS42NzUgMTAuNDA4IDI1IDguOTk4IDI2LjQxOCIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iMjQuOTk3IDEwLjMyNSAyMy41NzkgOC45MTUgOC45OTggMjMuNTgyIDEwLjQwOCAyNSAyNC45OTcgMTAuMz'+
			'I1IiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI4Ljk5OCAyMy41ODIgNy41ODggMjUgOC45OTggMjYuNDE4IDEwLjQwOCAyNSA4Ljk5OCAyMy41ODIiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQogICAgPC9nPg0KICAgIDxyZWN0IHg9IjEwIiB5PSIyNCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjIiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQogIDwvZz4NCjwvc3ZnPg0K';
		me._svg_1__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;svg_1;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._svg_1__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLWxlZnQtYzAwPC90aXRsZT4NCiAgPGc+DQogICAgPGc+DQogICAgICA8cG9seWdvbiBwb2ludHM9IjguNzk0IDI2LjQxOCAyMy4zNzUgNDEuMDg1IDI0Ljc5MyAzOS42NzUgMTAuMjA0IDI1IDguNzk0IDI2LjQxOCIgc3R5bGU9ImZpbGw6ICNjMDAiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iMjQuNzkzIDEwLjMyNSAyMy4zNzUgOC45MTUgOC43OTQgMjMuNTgyIDEwLjIwNCAyNSAyNC43OTMgMTAuMz'+
			'I1IiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI4Ljc5NCAyMy41ODIgNy4zODQgMjUgOC43OTQgMjYuNDE4IDEwLjIwNCAyNSA4Ljc5NCAyMy41ODIiIHN0eWxlPSJmaWxsOiAjYzAwIi8+DQogICAgPC9nPg0KICAgIDxyZWN0IHg9IjkuNzk2MDIiIHk9IjI0IiB3aWR0aD0iMzIiIGhlaWdodD0iMiIgc3R5bGU9ImZpbGw6ICNjMDAiLz4NCiAgPC9nPg0KPC9zdmc+DQo=';
		me._svg_1__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_1;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._svg_1__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLWxlZnQtNjY2PC90aXRsZT4NCiAgPGc+DQogICAgPGc+DQogICAgICA8cG9seWdvbiBwb2ludHM9IjkuMjAyIDI2LjQxOCAyMy43ODMgNDEuMDg1IDI1LjIwMSAzOS42NzUgMTAuNjEyIDI1IDkuMjAyIDI2LjQxOCIgc3R5bGU9ImZpbGw6ICM2NjYiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iMjUuMjAxIDEwLjMyNSAyMy43ODMgOC45MTUgOS4yMDIgMjMuNTgyIDEwLjYxMiAyNSAyNS4yMDEgMTAuMz'+
			'I1IiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI5LjIwMiAyMy41ODIgNy43OTIgMjUgOS4yMDIgMjYuNDE4IDEwLjYxMiAyNSA5LjIwMiAyMy41ODIiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogICAgPC9nPg0KICAgIDxyZWN0IHg9IjEwLjIwMzk4IiB5PSIyNCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjIiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogIDwvZz4NCjwvc3ZnPg0K';
		me._svg_1__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_1;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="Svg 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 100px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._svg_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._svg_1.onclick=function (e) {
			player.changePanLog(1,true);
		}
		me._svg_1.onmouseover=function (e) {
			me._svg_1__img.style.visibility='hidden';
			me._svg_1__imgo.style.visibility='inherit';
		}
		me._svg_1.onmouseout=function (e) {
			me._svg_1__img.style.visibility='inherit';
			me._svg_1__imgo.style.visibility='hidden';
			me._svg_1__imga.style.visibility='hidden';
		}
		me._svg_1.onmousedown=function (e) {
			me._svg_1__imga.style.visibility='inherit';
			me._svg_1__imgo.style.visibility='hidden';
		}
		me._svg_1.onmouseup=function (e) {
			me._svg_1__imga.style.visibility='hidden';
			me._svg_1__imgo.style.visibility='inherit';
		}
		me._svg_1.ggUpdatePosition=function (useTransition) {
		}
		me._container_2.appendChild(me._svg_1);
		el=me._svg_3on=document.createElement('div');
		els=me._svg_3on__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS02NjY8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTVjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MnYxLjkwNDU0Yy4wODM5Mi0uMDAwNzkuMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIyLDE1LjEwMTM4LDguMDk1MjhTMzMuMTg1NDIsMzMuMDk1MjEsMjUsMzMuMDk1MjEsOS44OTg2MiwyOS4zODgsOS44OTg2MiwyNWMwLTIuMjg4MT'+
			'UsMS44OTA5My00LjM4NTE5LDQuODUxMzgtNS44NzRWMTYuOTgyMTJDMTAuNjI5LDE4Ljc5MDU5LDcuOTkzODMsMjEuNjc3NTUsNy45OTM4MywyNWMwLDUuNjA3Myw3LjQ3MDI4LDEwLDE3LjAwNjE3LDEwczE3LjAwNjE2LTQuMzkyNywxNy4wMDYxNi0xMFMzNC41MzU4OCwxNSwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogICAgPHBvbHlnb24gcG9pbnRzPSIyOC43NSAyMy4wNzEgMjEuNjc5IDE2IDI4Ljc1IDguOTI5IDMwLjE2NCAxMC4zNDMgMjQuNTA3IDE2IDMwLjE2NCAyMS42NTcgMjguNzUgMjMuMDcxIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_3on__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;svg_3on;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._svg_3on__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS1jMDA8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTVjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MnYxLjkwNDU0Yy4wODM5Mi0uMDAwNzkuMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIyLDE1LjEwMTM4LDguMDk1MjhTMzMuMTg1NDIsMzMuMDk1MjEsMjUsMzMuMDk1MjEsOS44OTg2MiwyOS4zODgsOS44OTg2MiwyNWMwLTIuMjg4MT'+
			'UsMS44OTA5My00LjM4NTE5LDQuODUxMzgtNS44NzRWMTYuOTgyMTJDMTAuNjI5LDE4Ljc5MDU5LDcuOTkzODMsMjEuNjc3NTUsNy45OTM4MywyNWMwLDUuNjA3Myw3LjQ3MDI4LDEwLDE3LjAwNjE3LDEwczE3LjAwNjE2LTQuMzkyNywxNy4wMDYxNi0xMFMzNC41MzU4OCwxNSwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjYzAwIi8+DQogICAgPHBvbHlnb24gcG9pbnRzPSIyOC43NSAyMy4wNzEgMjEuNjc5IDE2IDI4Ljc1IDguOTI5IDMwLjE2NCAxMC4zNDMgMjQuNTA3IDE2IDMwLjE2NCAyMS42NTcgMjguNzUgMjMuMDcxIiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_3on__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_3on;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._svg_3on__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS02NjY8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTVjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MnYxLjkwNDU0Yy4wODM5Mi0uMDAwNzkuMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIyLDE1LjEwMTM4LDguMDk1MjhTMzMuMTg1NDIsMzMuMDk1MjEsMjUsMzMuMDk1MjEsOS44OTg2MiwyOS4zODgsOS44OTg2MiwyNWMwLTIuMjg4MT'+
			'UsMS44OTA5My00LjM4NTE5LDQuODUxMzgtNS44NzRWMTYuOTgyMTJDMTAuNjI5LDE4Ljc5MDU5LDcuOTkzODMsMjEuNjc3NTUsNy45OTM4MywyNWMwLDUuNjA3Myw3LjQ3MDI4LDEwLDE3LjAwNjE3LDEwczE3LjAwNjE2LTQuMzkyNywxNy4wMDYxNi0xMFMzNC41MzU4OCwxNSwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogICAgPHBvbHlnb24gcG9pbnRzPSIyOC43NSAyMy4wNzEgMjEuNjc5IDE2IDI4Ljc1IDguOTI5IDMwLjE2NCAxMC4zNDMgMjQuNTA3IDE2IDMwLjE2NCAyMS42NTcgMjguNzUgMjMuMDcxIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_3on__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_3on;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="Svg 3on";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 50px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._svg_3on.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._svg_3on.onclick=function (e) {
			player.stopAutorotate();
			me._svg_3on.style[domTransition]='none';
			me._svg_3on.style.visibility='hidden';
			me._svg_3on.ggVisible=false;
			me._svg_3off.style[domTransition]='none';
			me._svg_3off.style.visibility=(Number(me._svg_3off.style.opacity)>0||!me._svg_3off.style.opacity)?'inherit':'hidden';
			me._svg_3off.ggVisible=true;
		}
		me._svg_3on.onmouseover=function (e) {
			me._svg_3on__img.style.visibility='hidden';
			me._svg_3on__imgo.style.visibility='inherit';
		}
		me._svg_3on.onmouseout=function (e) {
			me._svg_3on__img.style.visibility='inherit';
			me._svg_3on__imgo.style.visibility='hidden';
			me._svg_3on__imga.style.visibility='hidden';
		}
		me._svg_3on.onmousedown=function (e) {
			me._svg_3on__imga.style.visibility='inherit';
			me._svg_3on__imgo.style.visibility='hidden';
		}
		me._svg_3on.onmouseup=function (e) {
			me._svg_3on__imga.style.visibility='hidden';
			me._svg_3on__imgo.style.visibility='inherit';
		}
		me._svg_3on.ggUpdatePosition=function (useTransition) {
		}
		me._container_2.appendChild(me._svg_3on);
		el=me._svg_3off=document.createElement('div');
		els=me._svg_3off__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS1mZmY8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTUuMDM1NTNjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MjF2MS45MDQ1NGMuMDgzOTItLjAwMDguMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIxLDE1LjEwMTM4LDguMDk1MjdTMzMuMTg1NDIsMzMuMTMwNzUsMjUsMzMuMTMwNzUsOS44OTg2MiwyOS40MjM1Myw5Ljg5ODYyLDI1Lj'+
			'AzNTUzYzAtMi4yODgxNCwxLjg5MDkzLTQuMzg1MTksNC44NTEzOC01Ljg3NFYxNy4wMTc2NWMtNC4xMjEsMS44MDg0Ny02Ljc1NjE3LDQuNjk1NDQtNi43NTYxNyw4LjAxNzg4LDAsNS42MDczLDcuNDcwMjgsMTAsMTcuMDA2MTcsMTBzMTcuMDA2MTYtNC4zOTI3LDE3LjAwNjE2LTEwUzM0LjUzNTg4LDE1LjAzNTUzLDI1LDE1LjAzNTUzWiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICA8cG9seWdvbiBwb2ludHM9IjI4Ljc1IDIzLjEwNyAyMS42NzkgMTYuMDM2IDI4Ljc1IDguOTY0IDMwLjE2NCAxMC4zNzkgMjQuNTA3IDE2LjAzNiAzMC4xNjQgMjEuNjkyIDI4Ljc1IDIzLjEwNyIgc3R5bGU9'+
			'ImZpbGw6ICNmZmYiLz4NCiAgPC9nPg0KPC9zdmc+DQo=';
		me._svg_3off__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;svg_3off;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._svg_3off__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS1jMDA8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTVjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MnYxLjkwNDU0Yy4wODM5Mi0uMDAwNzkuMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIyLDE1LjEwMTM4LDguMDk1MjhTMzMuMTg1NDIsMzMuMDk1MjEsMjUsMzMuMDk1MjEsOS44OTg2MiwyOS4zODgsOS44OTg2MiwyNWMwLTIuMjg4MT'+
			'UsMS44OTA5My00LjM4NTE5LDQuODUxMzgtNS44NzRWMTYuOTgyMTJDMTAuNjI5LDE4Ljc5MDU5LDcuOTkzODMsMjEuNjc3NTUsNy45OTM4MywyNWMwLDUuNjA3Myw3LjQ3MDI4LDEwLDE3LjAwNjE3LDEwczE3LjAwNjE2LTQuMzkyNywxNy4wMDYxNi0xMFMzNC41MzU4OCwxNSwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjYzAwIi8+DQogICAgPHBvbHlnb24gcG9pbnRzPSIyOC43NSAyMy4wNzEgMjEuNjc5IDE2IDI4Ljc1IDguOTI5IDMwLjE2NCAxMC4zNDMgMjQuNTA3IDE2IDMwLjE2NCAyMS42NTcgMjguNzUgMjMuMDcxIiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_3off__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_3off;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._svg_3off__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJvdGF0ZS02NjY8L3RpdGxlPg0KICA8Zz4NCiAgICA8cGF0aCBkPSJNMjUsMTVjLS4wODQ1NCwwLS4xNjU3OC4wMDY1My0uMjUuMDA3MnYxLjkwNDU0Yy4wODM5Mi0uMDAwNzkuMTY1NzctLjAwNy4yNS0uMDA3LDguMTg1NDIsMCwxNS4xMDEzOCwzLjcwNzIyLDE1LjEwMTM4LDguMDk1MjhTMzMuMTg1NDIsMzMuMDk1MjEsMjUsMzMuMDk1MjEsOS44OTg2MiwyOS4zODgsOS44OTg2MiwyNWMwLTIuMjg4MT'+
			'UsMS44OTA5My00LjM4NTE5LDQuODUxMzgtNS44NzRWMTYuOTgyMTJDMTAuNjI5LDE4Ljc5MDU5LDcuOTkzODMsMjEuNjc3NTUsNy45OTM4MywyNWMwLDUuNjA3Myw3LjQ3MDI4LDEwLDE3LjAwNjE3LDEwczE3LjAwNjE2LTQuMzkyNywxNy4wMDYxNi0xMFMzNC41MzU4OCwxNSwyNSwxNVoiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogICAgPHBvbHlnb24gcG9pbnRzPSIyOC43NSAyMy4wNzEgMjEuNjc5IDE2IDI4Ljc1IDguOTI5IDMwLjE2NCAxMC4zNDMgMjQuNTA3IDE2IDMwLjE2NCAyMS42NTcgMjguNzUgMjMuMDcxIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_3off__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_3off;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="Svg 3off";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 50px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._svg_3off.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._svg_3off.onclick=function (e) {
			player.startAutorotate("0.1");
			me._svg_3on.style[domTransition]='none';
			me._svg_3on.style.visibility=(Number(me._svg_3on.style.opacity)>0||!me._svg_3on.style.opacity)?'inherit':'hidden';
			me._svg_3on.ggVisible=true;
			me._svg_3off.style[domTransition]='none';
			me._svg_3off.style.visibility='hidden';
			me._svg_3off.ggVisible=false;
		}
		me._svg_3off.onmouseover=function (e) {
			me._svg_3off__img.style.visibility='hidden';
			me._svg_3off__imgo.style.visibility='inherit';
		}
		me._svg_3off.onmouseout=function (e) {
			me._svg_3off__img.style.visibility='inherit';
			me._svg_3off__imgo.style.visibility='hidden';
			me._svg_3off__imga.style.visibility='hidden';
		}
		me._svg_3off.onmousedown=function (e) {
			me._svg_3off__imga.style.visibility='inherit';
			me._svg_3off__imgo.style.visibility='hidden';
		}
		me._svg_3off.onmouseup=function (e) {
			me._svg_3off__imga.style.visibility='hidden';
			me._svg_3off__imgo.style.visibility='inherit';
		}
		me._svg_3off.ggUpdatePosition=function (useTransition) {
		}
		me._container_2.appendChild(me._svg_3off);
		el=me._svg_2=document.createElement('div');
		els=me._svg_2__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJpZ2h0LWZmZjwvdGl0bGU+DQogIDxnPg0KICAgIDxnPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI0MC43OTYgMjMuNTgyIDI2LjIxNSA4LjkxNSAyNC43OTcgMTAuMzI1IDM5LjM4NiAyNSA0MC43OTYgMjMuNTgyIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSIyNC43OTcgMzkuNjc1IDI2LjIxNSA0MS4wODUgNDAuNzk2IDI2LjQxOCAzOS4zODYgMjUgMjQuNzk3ID'+
			'M5LjY3NSIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iNDAuNzk2IDI2LjQxOCA0Mi4yMDYgMjUgNDAuNzk2IDIzLjU4MiAzOS4zODYgMjUgNDAuNzk2IDI2LjQxOCIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgICA8L2c+DQogICAgPHJlY3QgeD0iNy43OTQwNyIgeT0iMjQiIHdpZHRoPSIzMiIgaGVpZ2h0PSIyIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_2__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;svg_2;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._svg_2__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJpZ2h0LWMwMDwvdGl0bGU+DQogIDxnPg0KICAgIDxnPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI0MC43OTYgMjMuNTgyIDI2LjIxNSA4LjkxNSAyNC43OTcgMTAuMzI1IDM5LjM4NiAyNSA0MC43OTYgMjMuNTgyIiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSIyNC43OTcgMzkuNjc1IDI2LjIxNSA0MS4wODUgNDAuNzk2IDI2LjQxOCAzOS4zODYgMjUgMjQuNzk3ID'+
			'M5LjY3NSIgc3R5bGU9ImZpbGw6ICNjMDAiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iNDAuNzk2IDI2LjQxOCA0Mi4yMDYgMjUgNDAuNzk2IDIzLjU4MiAzOS4zODYgMjUgNDAuNzk2IDI2LjQxOCIgc3R5bGU9ImZpbGw6ICNjMDAiLz4NCiAgICA8L2c+DQogICAgPHJlY3QgeD0iNy43OTQwNyIgeT0iMjQiIHdpZHRoPSIzMiIgaGVpZ2h0PSIyIiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_2__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_2;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._svg_2__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXJpZ2h0LTY2NjwvdGl0bGU+DQogIDxnPg0KICAgIDxnPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSI0MC43OTYgMjMuNTgyIDI2LjIxNSA4LjkxNSAyNC43OTcgMTAuMzI1IDM5LjM4NiAyNSA0MC43OTYgMjMuNTgyIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICAgICAgPHBvbHlnb24gcG9pbnRzPSIyNC43OTcgMzkuNjc1IDI2LjIxNSA0MS4wODUgNDAuNzk2IDI2LjQxOCAzOS4zODYgMjUgMjQuNzk3ID'+
			'M5LjY3NSIgc3R5bGU9ImZpbGw6ICM2NjYiLz4NCiAgICAgIDxwb2x5Z29uIHBvaW50cz0iNDAuNzk2IDI2LjQxOCA0Mi4yMDYgMjUgNDAuNzk2IDIzLjU4MiAzOS4zODYgMjUgNDAuNzk2IDI2LjQxOCIgc3R5bGU9ImZpbGw6ICM2NjYiLz4NCiAgICA8L2c+DQogICAgPHJlY3QgeD0iNy43OTQwNyIgeT0iMjQiIHdpZHRoPSIzMiIgaGVpZ2h0PSIyIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._svg_2__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;svg_2;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="Svg 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._svg_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._svg_2.onclick=function (e) {
			player.changePanLog(-1,true);
		}
		me._svg_2.onmouseover=function (e) {
			me._svg_2__img.style.visibility='hidden';
			me._svg_2__imgo.style.visibility='inherit';
		}
		me._svg_2.onmouseout=function (e) {
			me._svg_2__img.style.visibility='inherit';
			me._svg_2__imgo.style.visibility='hidden';
			me._svg_2__imga.style.visibility='hidden';
		}
		me._svg_2.onmousedown=function (e) {
			me._svg_2__imga.style.visibility='inherit';
			me._svg_2__imgo.style.visibility='hidden';
		}
		me._svg_2.onmouseup=function (e) {
			me._svg_2__imga.style.visibility='hidden';
			me._svg_2__imgo.style.visibility='inherit';
		}
		me._svg_2.ggUpdatePosition=function (useTransition) {
		}
		me._container_2.appendChild(me._svg_2);
		me._container_3.appendChild(me._container_2);
		me.divSkin.appendChild(me._container_3);
		el=me._container_4=document.createElement('div');
		el.ggId="Container 4";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : 10px;';
		hs+='position : absolute;';
		hs+='top : 10px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_4.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._container_4.ggUpdatePosition=function (useTransition) {
		}
		el=me._fullscreen=document.createElement('div');
		els=me._fullscreen__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tbWF4LWZmZjwvdGl0bGU+DQogIDxwYXRoIGQ9Ik00MSwzOUg5VjExSDQxWk0xMSwzN0gzOVYxM0gxMVoiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQo8L3N2Zz4NCg==';
		me._fullscreen__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;fullscreen;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._fullscreen__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tbWF4LWMwMDwvdGl0bGU+DQogIDxwYXRoIGQ9Ik00MSwzOUg5VjExSDQxWk0xMSwzN0gzOVYxM0gxMVoiIHN0eWxlPSJmaWxsOiAjYzAwIi8+DQo8L3N2Zz4NCg==';
		me._fullscreen__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;fullscreen;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._fullscreen__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tbWF4LTY2NjwvdGl0bGU+DQogIDxwYXRoIGQ9Ik00MSwzOUg5VjExSDQxWk0xMSwzN0gzOVYxM0gxMVoiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQo8L3N2Zz4NCg==';
		me._fullscreen__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;fullscreen;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="fullscreen";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._fullscreen.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._fullscreen.onclick=function (e) {
			player.toggleFullscreen();
		}
		me._fullscreen.onmouseover=function (e) {
			me._fullscreen__img.style.visibility='hidden';
			me._fullscreen__imgo.style.visibility='inherit';
		}
		me._fullscreen.onmouseout=function (e) {
			me._fullscreen__img.style.visibility='inherit';
			me._fullscreen__imgo.style.visibility='hidden';
			me._fullscreen__imga.style.visibility='hidden';
		}
		me._fullscreen.onmousedown=function (e) {
			me._fullscreen__imga.style.visibility='inherit';
			me._fullscreen__imgo.style.visibility='hidden';
		}
		me._fullscreen.onmouseup=function (e) {
			me._fullscreen__imga.style.visibility='hidden';
			me._fullscreen__imgo.style.visibility='inherit';
		}
		me._fullscreen.ggUpdatePosition=function (useTransition) {
		}
		me._container_4.appendChild(me._fullscreen);
		el=me._restorescreen=document.createElement('div');
		els=me._restorescreen__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tcmVzdG9yZS1mZmY8L3RpdGxlPg0KICA8cGF0aCBpZD0iaWNvbi13aW4tbWluXzY2NiIgZD0iTTMxLDIxVjM3SDExVjIxSDMxbTItMkg5VjM5SDMzVjE5WiIgc3R5bGU9ImZpbGw6ICNmZmYiLz4NCiAgPHBhdGggaWQ9Imljb24td2luLW1pbl82NjYtMiIgZGF0YS1uYW1lPSJpY29uLXdpbi1taW5fNjY2IiBkPSJNMzksMTNWMjlIMzNWMTlIMTlWMTNIMzltMi0ySDE3VjIxSDMxVjMxSDQxVjExWiIgc3R5bG'+
			'U9ImZpbGw6ICNmZmYiLz4NCjwvc3ZnPg0K';
		me._restorescreen__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;restorescreen;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._restorescreen__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tcmVzdG9yZS1jMDA8L3RpdGxlPg0KICA8cGF0aCBpZD0iaWNvbi13aW4tbWluXzY2NiIgZD0iTTMxLDIxVjM3SDExVjIxSDMxbTItMkg5VjM5SDMzVjE5WiIgc3R5bGU9ImZpbGw6ICNjMDAiLz4NCiAgPHBhdGggaWQ9Imljb24td2luLW1pbl82NjYtMiIgZGF0YS1uYW1lPSJpY29uLXdpbi1taW5fNjY2IiBkPSJNMzksMTNWMjlIMzNWMTlIMTlWMTNIMzltMi0ySDE3VjIxSDMxVjMxSDQxVjExWiIgc3R5bG'+
			'U9ImZpbGw6ICNjMDAiLz4NCjwvc3ZnPg0K';
		me._restorescreen__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;restorescreen;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._restorescreen__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tcmVzdG9yZS02NjY8L3RpdGxlPg0KICA8cGF0aCBpZD0iaWNvbi13aW4tbWluXzY2NiIgZD0iTTMxLDIxVjM3SDExVjIxSDMxbTItMkg5VjM5SDMzVjE5WiIgc3R5bGU9ImZpbGw6ICM2NjYiLz4NCiAgPHBhdGggaWQ9Imljb24td2luLW1pbl82NjYtMiIgZGF0YS1uYW1lPSJpY29uLXdpbi1taW5fNjY2IiBkPSJNMzksMTNWMjlIMzNWMTlIMTlWMTNIMzltMi0ySDE3VjIxSDMxVjMxSDQxVjExWiIgc3R5bG'+
			'U9ImZpbGw6ICM2NjYiLz4NCjwvc3ZnPg0K';
		me._restorescreen__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;restorescreen;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="restorescreen";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._restorescreen.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._restorescreen.onclick=function (e) {
			player.toggleFullscreen();
		}
		me._restorescreen.onmouseover=function (e) {
			me._restorescreen__img.style.visibility='hidden';
			me._restorescreen__imgo.style.visibility='inherit';
		}
		me._restorescreen.onmouseout=function (e) {
			me._restorescreen__img.style.visibility='inherit';
			me._restorescreen__imgo.style.visibility='hidden';
			me._restorescreen__imga.style.visibility='hidden';
		}
		me._restorescreen.onmousedown=function (e) {
			me._restorescreen__imga.style.visibility='inherit';
			me._restorescreen__imgo.style.visibility='hidden';
		}
		me._restorescreen.onmouseup=function (e) {
			me._restorescreen__imga.style.visibility='hidden';
			me._restorescreen__imgo.style.visibility='inherit';
		}
		me._restorescreen.ggUpdatePosition=function (useTransition) {
		}
		me._container_4.appendChild(me._restorescreen);
		me.divSkin.appendChild(me._container_4);
		el=me._container_5=document.createElement('div');
		el.ggId="Container 5";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 10px;';
		hs+='top : 10px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_5.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._container_5.ggUpdatePosition=function (useTransition) {
		}
		el=me._close=document.createElement('div');
		els=me._close__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tY2xvc2UtZmZmPC90aXRsZT4NCiAgPGc+DQogICAgPHJlY3QgeD0iMjMuOTk4MTUiIHk9IjMuMzEyNjUiIHdpZHRoPSIxLjk5OTc5IiBoZWlnaHQ9IjQzLjM3NDciIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMC4zNTU3NSAyNC44OTQ0OSkgcm90YXRlKC00NC44MzEyNSkiIHN0eWxlPSJmaWxsOiAjZmZmIi8+DQogICAgPHJlY3QgeD0iMy4zMTA3IiB5PSIyNC4wMDAxIiB3aWR0aD0iNDMuMzc0NyIgaGVpZ2'+
			'h0PSIxLjk5OTc5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTAuMzU1NzggMjUuMDk2MjgpIHJvdGF0ZSgtNDUuMTU4MjgpIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._close__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;close;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		elo=me._close__imgo=document.createElement('img');
		elo.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tY2xvc2UtYzAwPC90aXRsZT4NCiAgPGc+DQogICAgPHJlY3QgeD0iMjMuOTk4MTUiIHk9IjMuMzEyNjUiIHdpZHRoPSIxLjk5OTc5IiBoZWlnaHQ9IjQzLjM3NDciIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMC4zNTU3NSAyNC44OTQ0OSkgcm90YXRlKC00NC44MzEyNSkiIHN0eWxlPSJmaWxsOiAjYzAwIi8+DQogICAgPHJlY3QgeD0iMy4zMTA3IiB5PSIyNC4wMDAxIiB3aWR0aD0iNDMuMzc0NyIgaGVpZ2'+
			'h0PSIxLjk5OTc5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTAuMzU1NzggMjUuMDk2MjgpIHJvdGF0ZSgtNDUuMTU4MjgpIiBzdHlsZT0iZmlsbDogI2MwMCIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._close__imgo.setAttribute('src',hs);
		elo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;close;');
		elo['ondragstart']=function() { return false; };
		el.appendChild(elo);
		ela=me._close__imga=document.createElement('img');
		ela.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT53aW4tY2xvc2UtNjY2PC90aXRsZT4NCiAgPGc+DQogICAgPHJlY3QgeD0iMjMuOTk4MTUiIHk9IjMuMzEyNjUiIHdpZHRoPSIxLjk5OTc5IiBoZWlnaHQ9IjQzLjM3NDciIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMC4zNTU3NSAyNC44OTQ0OSkgcm90YXRlKC00NC44MzEyNSkiIHN0eWxlPSJmaWxsOiAjNjY2Ii8+DQogICAgPHJlY3QgeD0iMy4zMTA3IiB5PSIyNC4wMDAxIiB3aWR0aD0iNDMuMzc0NyIgaGVpZ2'+
			'h0PSIxLjk5OTc5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTAuMzU1NzggMjUuMDk2MjgpIHJvdGF0ZSgtNDUuMTU4MjgpIiBzdHlsZT0iZmlsbDogIzY2NiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._close__imga.setAttribute('src',hs);
		ela.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;close;');
		ela['ondragstart']=function() { return false; };
		el.appendChild(ela);
		el.ggId="close";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='position : absolute;';
		hs+='right : 0px;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._close.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._close.onclick=function (e) {
			player.toggleFullscreen();
		}
		me._close.onmouseover=function (e) {
			me._close__img.style.visibility='hidden';
			me._close__imgo.style.visibility='inherit';
		}
		me._close.onmouseout=function (e) {
			me._close__img.style.visibility='inherit';
			me._close__imgo.style.visibility='hidden';
			me._close__imga.style.visibility='hidden';
		}
		me._close.onmousedown=function (e) {
			me._close__imga.style.visibility='inherit';
			me._close__imgo.style.visibility='hidden';
		}
		me._close.onmouseup=function (e) {
			me._close__imga.style.visibility='hidden';
			me._close__imgo.style.visibility='inherit';
		}
		me._close.ggUpdatePosition=function (useTransition) {
		}
		me._container_5.appendChild(me._close);
		me.divSkin.appendChild(me._container_5);
		el=me._tooltip=document.createElement('div');
		el.ggId="tooltip";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 112px;';
		hs+='left : 32px;';
		hs+='position : absolute;';
		hs+='top : 73px;';
		hs+='visibility : inherit;';
		hs+='width : 201px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._tooltip.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._tooltip.ggUpdatePosition=function (useTransition) {
		}
		el=me._rectangle_1=document.createElement('div');
		el.ggId="Rectangle 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 10px;';
		hs+='border-radius : 10px;';
		hs+='background : rgba(255,255,255,0.392157);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 86px;';
		hs+='left : -12px;';
		hs+='position : absolute;';
		hs+='top : -12px;';
		hs+='visibility : inherit;';
		hs+='width : 194px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._rectangle_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._rectangle_1.ggUpdatePosition=function (useTransition) {
		}
		me._tooltip.appendChild(me._rectangle_1);
		el=me._tooltiptext=document.createElement('div');
		els=me._tooltiptext__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="tooltip-text";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 65px;';
		hs+='left : -2px;';
		hs+='position : absolute;';
		hs+='top : -1px;';
		hs+='visibility : inherit;';
		hs+='width : 181px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 181px;';
		hs+='height: 65px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(120,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 2px 0px 2px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._tooltiptext.ggUpdateText=function() {
			var hs=player.hotspot.title;
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._tooltiptext.ggUpdateText();
		el.appendChild(els);
		me._tooltiptext.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._tooltiptext.ggUpdatePosition=function (useTransition) {
		}
		me._tooltip.appendChild(me._tooltiptext);
		me.divSkin.appendChild(me._tooltip);
		el=me._nodes=document.createElement('div');
		el.ggId="nodes";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 46px;';
		hs+='left : 20px;';
		hs+='position : absolute;';
		hs+='top : 165px;';
		hs+='visibility : inherit;';
		hs+='width : 62px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodes.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._nodes.ggUpdatePosition=function (useTransition) {
		}
		el=me._nodeimage_1=document.createElement('div');
		els=me._nodeimage_1__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_1.png');
		el.ggNodeId='node1';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_1;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 38px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 2px;';
		hs+='visibility : inherit;';
		hs+='width : 60px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_1.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_1.onclick=function (e) {
			player.openNext("{node1}","");
		}
		me._nodeimage_1.onmouseover=function (e) {
			me._nodeimage_5.style[domTransition]='none';
			me._nodeimage_5.style.visibility=(Number(me._nodeimage_5.style.opacity)>0||!me._nodeimage_5.style.opacity)?'inherit':'hidden';
			me._nodeimage_5.ggVisible=true;
		}
		me._nodeimage_1.onmouseout=function (e) {
			me._nodeimage_5.style[domTransition]='none';
			me._nodeimage_5.style.visibility='hidden';
			me._nodeimage_5.ggVisible=false;
		}
		me._nodeimage_1.ggUpdatePosition=function (useTransition) {
		}
		me._nodes.appendChild(me._nodeimage_1);
		el=me._nodeimage_2=document.createElement('div');
		els=me._nodeimage_2__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_2.png');
		el.ggNodeId='node2';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_2;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 38px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 49px;';
		hs+='visibility : inherit;';
		hs+='width : 60px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_2.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_2.onclick=function (e) {
			player.openNext("{node2}","");
		}
		me._nodeimage_2.onmouseover=function (e) {
			me._nodeimage_6.style[domTransition]='none';
			me._nodeimage_6.style.visibility=(Number(me._nodeimage_6.style.opacity)>0||!me._nodeimage_6.style.opacity)?'inherit':'hidden';
			me._nodeimage_6.ggVisible=true;
		}
		me._nodeimage_2.onmouseout=function (e) {
			me._nodeimage_6.style[domTransition]='none';
			me._nodeimage_6.style.visibility='hidden';
			me._nodeimage_6.ggVisible=false;
		}
		me._nodeimage_2.ggUpdatePosition=function (useTransition) {
		}
		me._nodes.appendChild(me._nodeimage_2);
		el=me._nodeimage_3=document.createElement('div');
		els=me._nodeimage_3__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_3.png');
		el.ggNodeId='node3';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_3;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 3";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 38px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 96px;';
		hs+='visibility : inherit;';
		hs+='width : 60px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_3.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_3.onclick=function (e) {
			player.openNext("{node3}","");
		}
		me._nodeimage_3.onmouseover=function (e) {
			me._nodeimage_7.style[domTransition]='none';
			me._nodeimage_7.style.visibility=(Number(me._nodeimage_7.style.opacity)>0||!me._nodeimage_7.style.opacity)?'inherit':'hidden';
			me._nodeimage_7.ggVisible=true;
		}
		me._nodeimage_3.onmouseout=function (e) {
			me._nodeimage_7.style[domTransition]='none';
			me._nodeimage_7.style.visibility='hidden';
			me._nodeimage_7.ggVisible=false;
		}
		me._nodeimage_3.ggUpdatePosition=function (useTransition) {
		}
		me._nodes.appendChild(me._nodeimage_3);
		el=me._nodeimage_4=document.createElement('div');
		els=me._nodeimage_4__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_4.png');
		el.ggNodeId='node4';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_4;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 4";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 38px;';
		hs+='left : 1px;';
		hs+='position : absolute;';
		hs+='top : 144px;';
		hs+='visibility : inherit;';
		hs+='width : 60px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_4.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_4.onclick=function (e) {
			player.openNext("{node4}","");
		}
		me._nodeimage_4.onmouseover=function (e) {
			me._nodeimage_8.style[domTransition]='none';
			me._nodeimage_8.style.visibility=(Number(me._nodeimage_8.style.opacity)>0||!me._nodeimage_8.style.opacity)?'inherit':'hidden';
			me._nodeimage_8.ggVisible=true;
		}
		me._nodeimage_4.onmouseout=function (e) {
			me._nodeimage_8.style[domTransition]='none';
			me._nodeimage_8.style.visibility='hidden';
			me._nodeimage_8.ggVisible=false;
		}
		me._nodeimage_4.ggUpdatePosition=function (useTransition) {
		}
		me._nodes.appendChild(me._nodeimage_4);
		me.divSkin.appendChild(me._nodes);
		el=me._nodesimage=document.createElement('div');
		el.ggId="nodesImage";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 321px;';
		hs+='left : 89px;';
		hs+='position : absolute;';
		hs+='top : 167px;';
		hs+='visibility : inherit;';
		hs+='width : 286px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodesimage.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._nodesimage.ggUpdatePosition=function (useTransition) {
		}
		el=me._nodeimage_8=document.createElement('div');
		els=me._nodeimage_8__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_8.png');
		el.ggNodeId='node4';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_8;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 8";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 179px;';
		hs+='left : 1px;';
		hs+='position : absolute;';
		hs+='top : 142px;';
		hs+='visibility : hidden;';
		hs+='width : 286px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_8.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_8.ggUpdatePosition=function (useTransition) {
		}
		me._nodesimage.appendChild(me._nodeimage_8);
		el=me._nodeimage_7=document.createElement('div');
		els=me._nodeimage_7__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_7.png');
		el.ggNodeId='node3';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_7;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 7";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 179px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 94px;';
		hs+='visibility : hidden;';
		hs+='width : 286px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_7.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_7.ggUpdatePosition=function (useTransition) {
		}
		me._nodesimage.appendChild(me._nodeimage_7);
		el=me._nodeimage_6=document.createElement('div');
		els=me._nodeimage_6__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_6.png');
		el.ggNodeId='node2';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_6;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 6";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 179px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 47px;';
		hs+='visibility : hidden;';
		hs+='width : 286px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_6.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_6.ggUpdatePosition=function (useTransition) {
		}
		me._nodesimage.appendChild(me._nodeimage_6);
		el=me._nodeimage_5=document.createElement('div');
		els=me._nodeimage_5__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + 'images/nodeimage_5.png');
		el.ggNodeId='node1';
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;nodeimage_5;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="NodeImage 5";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 179px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : hidden;';
		hs+='width : 286px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._nodeimage_5.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._nodeimage_5.ggUpdatePosition=function (useTransition) {
		}
		me._nodesimage.appendChild(me._nodeimage_5);
		me.divSkin.appendChild(me._nodesimage);
		me._container_3.style[domTransition]='none';
		me._container_3.style.visibility='hidden';
		me._container_3.ggVisible=false;
		me._container_4.style[domTransition]='none';
		me._container_4.style.visibility='hidden';
		me._container_4.ggVisible=false;
		player.addListener('sizechanged', function() {
			me.updateSize(me.divSkin);
		});
		player.addListener('imagesready', function() {
			me._container_1.style[domTransition]='none';
			me._container_1.style.visibility='hidden';
			me._container_1.ggVisible=false;
			me._container_3.style[domTransition]='none';
			me._container_3.style.visibility=(Number(me._container_3.style.opacity)>0||!me._container_3.style.opacity)?'inherit':'hidden';
			me._container_3.ggVisible=true;
			me._container_4.style[domTransition]='none';
			me._container_4.style.visibility=(Number(me._container_4.style.opacity)>0||!me._container_4.style.opacity)?'inherit':'hidden';
			me._container_4.ggVisible=true;
		});
		player.addListener('fullscreenenter', function() {
			me._fullscreen.style[domTransition]='none';
			me._fullscreen.style.visibility='hidden';
			me._fullscreen.ggVisible=false;
			me._restorescreen.style[domTransition]='none';
			me._restorescreen.style.visibility=(Number(me._restorescreen.style.opacity)>0||!me._restorescreen.style.opacity)?'inherit':'hidden';
			me._restorescreen.ggVisible=true;
		});
		player.addListener('fullscreenexit', function() {
			me._fullscreen.style[domTransition]='none';
			me._fullscreen.style.visibility=(Number(me._fullscreen.style.opacity)>0||!me._fullscreen.style.opacity)?'inherit':'hidden';
			me._fullscreen.ggVisible=true;
			me._restorescreen.style[domTransition]='none';
			me._restorescreen.style.visibility='hidden';
			me._restorescreen.ggVisible=false;
		});
	};
	this.hotspotProxyClick=function(id, url) {
	}
	this.hotspotProxyDoubleClick=function(id, url) {
	}
	me.hotspotProxyOver=function(id, url) {
	}
	me.hotspotProxyOut=function(id, url) {
	}
	player.addListener('changenode', function() {
		me.ggUserdata=player.userdata;
	});
	me.skinTimerEvent=function() {
		me.ggCurrentTime=new Date().getTime();
		me._loading_text.ggUpdateText();
		var hs='';
		if (me._loading_bar.ggParameter) {
			hs+=parameterToTransform(me._loading_bar.ggParameter) + ' ';
		}
		hs+='scale(' + (1 * player.getPercentLoaded() + 0) + ',1.0) ';
		me._loading_bar.style[domTransform]=hs;
		me._tooltiptext.ggUpdateText();
	};
	player.addListener('timer', me.skinTimerEvent);
	function SkinHotspotClass_hotspoter(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspoter=document.createElement('div');
		el.ggId="hotspoter";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : 500px;';
		hs+='position : absolute;';
		hs+='top : 300px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspoter.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me._hotspoter.onclick=function (e) {
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspoter.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspoter.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspoter.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspoter.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspoterplus=document.createElement('div');
		els=me._hotspoterplus__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXBsdXMtZmZmPC90aXRsZT4NCiAgPGc+DQogICAgPHBhdGggZD0iTTI0Ljk5ODA1LDQzYTE4LDE4LDAsMSwxLDE4LTE4QTE4LjAyMDYyLDE4LjAyMDYyLDAsMCwxLDI0Ljk5ODA1LDQzWm0wLTM0YTE2LDE2LDAsMSwwLDE2LDE2QTE2LjAxODMzLDE2LjAxODMzLDAsMCwwLDI0Ljk5ODA1LDlaIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgIDxyZWN0IHg9IjExLjk5ODA1IiB5PSIyNC4xODcxNyIgd2lkdG'+
			'g9IjI2IiBoZWlnaHQ9IjEuNjI1IiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgIDxyZWN0IHg9IjI0LjE4NTU1IiB5PSIxMS45OTk2NyIgd2lkdGg9IjEuNjI1IiBoZWlnaHQ9IjI2IiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._hotspoterplus__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;hotspoterplus;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspoter-plus";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : -24px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspoterplus.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspoterplus.onclick=function (e) {
			skin._tooltiptext.ggText="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
			skin._tooltiptext.ggTextDiv.innerHTML=skin._tooltiptext.ggText;
			if (skin._tooltiptext.ggUpdateText) {
				skin._tooltiptext.ggUpdateText=function() {
					var hs="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (skin._tooltiptext.ggUpdatePosition) {
				skin._tooltiptext.ggUpdatePosition();
			}
			skin._tooltiptext.ggTextDiv.scrollTop = 0;
			player.openUrl(me.hotspot.url,me.hotspot.target);
		}
		me._hotspoterplus.onmouseover=function (e) {
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspoterplus.style.opacity='1';
			me._hotspoterplus.style.visibility=me._hotspoterplus.ggVisible?'inherit':'hidden';
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspoterplus.ggParameter.sx=1.5;me._hotspoterplus.ggParameter.sy=1.5;
			me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._hotspoterplus.ggParameter.a="360";
			me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
			if (player.transitionsDisabled) {
				me._container_6.style[domTransition]='none';
			} else {
				me._container_6.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._container_6.style.opacity='1';
			me._container_6.style.visibility=me._container_6.ggVisible?'inherit':'hidden';
		}
		me._hotspoterplus.onmouseout=function (e) {
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspoterplus.style.opacity='0.5';
			me._hotspoterplus.style.visibility=me._hotspoterplus.ggVisible?'inherit':'hidden';
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspoterplus.ggParameter.sx=1;me._hotspoterplus.ggParameter.sy=1;
			me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
			if (player.transitionsDisabled) {
				me._hotspoterplus.style[domTransition]='none';
			} else {
				me._hotspoterplus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._hotspoterplus.ggParameter.a="0";
			me._hotspoterplus.style[domTransform]=parameterToTransform(me._hotspoterplus.ggParameter);
			if (player.transitionsDisabled) {
				me._container_6.style[domTransition]='none';
			} else {
				me._container_6.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._container_6.style.opacity='0';
			me._container_6.style.visibility='hidden';
		}
		me._hotspoterplus.ggUpdatePosition=function (useTransition) {
		}
		me._hotspoter.appendChild(me._hotspoterplus);
		el=me._container_6=document.createElement('div');
		el.ggId="Container 6";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 18px;';
		hs+='left : -70px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : 42px;';
		hs+='visibility : hidden;';
		hs+='width : 139px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_6.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_6.ggUpdatePosition=function (useTransition) {
		}
		el=me._rectangle_2=document.createElement('div');
		el.ggId="Rectangle 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 10px;';
		hs+='border-radius : 10px;';
		hs+='background : rgba(255,255,255,0.588235);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 29px;';
		hs+='left : -10px;';
		hs+='position : absolute;';
		hs+='top : -6px;';
		hs+='visibility : inherit;';
		hs+='width : 160px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._rectangle_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._rectangle_2.ggUpdatePosition=function (useTransition) {
		}
		me._container_6.appendChild(me._rectangle_2);
		el=me._text_1=document.createElement('div');
		els=me._text_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 18px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 139px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 139px;';
		hs+='height: 18px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(102,102,102,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="<b>"+me.hotspot.title+"<\/b>";
		el.appendChild(els);
		me._text_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._text_1.ggUpdatePosition=function (useTransition) {
		}
		me._container_6.appendChild(me._text_1);
		me._hotspoter.appendChild(me._container_6);
		me.ggUse3d=true;
		me.gg3dDistance=500;
		me.__div = me._hotspoter;
	};
	function SkinHotspotClass_hotspot_pano(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspot_pano=document.createElement('div');
		el.ggId="hotspot_pano";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='height : 0px;';
		hs+='left : 192px;';
		hs+='position : absolute;';
		hs+='top : 350px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspot_pano.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
		}
		me._hotspot_pano.onclick=function (e) {
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_pano.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_pano.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_pano.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_pano.ggUpdatePosition=function (useTransition) {
		}
		el=me._hotspot_panoplus=document.createElement('div');
		els=me._hotspot_panoplus__img=document.createElement('img');
		els.className='ggskin ggskin_svg';
		hs='data:image/svg+xml;base64,PHN2ZyBpZD0iZmlsbCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHZpZXdCb3g9IjAgMCA1MCA1MCI+DQogIDx0aXRsZT5jdHJsLXBsdXMtZmZmPC90aXRsZT4NCiAgPGc+DQogICAgPHBhdGggZD0iTTI0Ljk5ODA1LDQzYTE4LDE4LDAsMSwxLDE4LTE4QTE4LjAyMDYyLDE4LjAyMDYyLDAsMCwxLDI0Ljk5ODA1LDQzWm0wLTM0YTE2LDE2LDAsMSwwLDE2LDE2QTE2LjAxODMzLDE2LjAxODMzLDAsMCwwLDI0Ljk5ODA1LDlaIiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgIDxyZWN0IHg9IjExLjk5ODA1IiB5PSIyNC4xODcxNyIgd2lkdG'+
			'g9IjI2IiBoZWlnaHQ9IjEuNjI1IiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICAgIDxyZWN0IHg9IjI0LjE4NTU1IiB5PSIxMS45OTk2NyIgd2lkdGg9IjEuNjI1IiBoZWlnaHQ9IjI2IiBzdHlsZT0iZmlsbDogI2ZmZiIvPg0KICA8L2c+DQo8L3N2Zz4NCg==';
		me._hotspot_panoplus__img.setAttribute('src',hs);
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;hotspot_panoplus;');
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot_pano-plus";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_svg ";
		el.ggType='svg';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -24px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspot_panoplus.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._hotspot_panoplus.onclick=function (e) {
			skin._tooltiptext.ggText="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
			skin._tooltiptext.ggTextDiv.innerHTML=skin._tooltiptext.ggText;
			if (skin._tooltiptext.ggUpdateText) {
				skin._tooltiptext.ggUpdateText=function() {
					var hs="<b style=\"font-size:20px\">"+me.hotspot.title+"<\/b>";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (skin._tooltiptext.ggUpdatePosition) {
				skin._tooltiptext.ggUpdatePosition();
			}
			skin._tooltiptext.ggTextDiv.scrollTop = 0;
			player.openUrl(me.hotspot.url,me.hotspot.target);
		}
		me._hotspot_panoplus.onmouseover=function (e) {
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspot_panoplus.style.opacity='1';
			me._hotspot_panoplus.style.visibility=me._hotspot_panoplus.ggVisible?'inherit':'hidden';
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspot_panoplus.ggParameter.sx=1.5;me._hotspot_panoplus.ggParameter.sy=1.5;
			me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._hotspot_panoplus.ggParameter.a="360";
			me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			me._hotspot_panoimage.style[domTransition]='none';
			me._hotspot_panoimage.style.visibility=(Number(me._hotspot_panoimage.style.opacity)>0||!me._hotspot_panoimage.style.opacity)?'inherit':'hidden';
			me._hotspot_panoimage.ggVisible=true;
		}
		me._hotspot_panoplus.onmouseout=function (e) {
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspot_panoplus.style.opacity='0.5';
			me._hotspot_panoplus.style.visibility=me._hotspot_panoplus.ggVisible?'inherit':'hidden';
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._hotspot_panoplus.ggParameter.sx=1;me._hotspot_panoplus.ggParameter.sy=1;
			me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			if (player.transitionsDisabled) {
				me._hotspot_panoplus.style[domTransition]='none';
			} else {
				me._hotspot_panoplus.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._hotspot_panoplus.ggParameter.a="0";
			me._hotspot_panoplus.style[domTransform]=parameterToTransform(me._hotspot_panoplus.ggParameter);
			me._hotspot_panoimage.style[domTransition]='none';
			me._hotspot_panoimage.style.visibility='hidden';
			me._hotspot_panoimage.ggVisible=false;
		}
		me._hotspot_panoplus.ggUpdatePosition=function (useTransition) {
		}
		me._hotspot_pano.appendChild(me._hotspot_panoplus);
		el=me._hotspot_panoimage=document.createElement('div');
		els=me._hotspot_panoimage__img=document.createElement('img');
		els.className='ggskin ggskin_nodeimage';
		els.setAttribute('src',basePath + "images/hotspot_panoimage_" + nodeId + ".png");
		el.ggNodeId=nodeId;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;hotspot_panoimage;');
		els.className='ggskin ggskin_nodeimage';
		els['ondragstart']=function() { return false; };
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="hotspot_pano-image";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_nodeimage ";
		el.ggType='nodeimage';
		hs ='';
		hs+='height : 135px;';
		hs+='left : -105px;';
		hs+='position : absolute;';
		hs+='top : -177px;';
		hs+='visibility : hidden;';
		hs+='width : 215px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspot_panoimage.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return this.ggNodeId;
		}
		me._hotspot_panoimage.ggUpdatePosition=function (useTransition) {
		}
		me._hotspot_pano.appendChild(me._hotspot_panoimage);
		me.__div = me._hotspot_pano;
	};
	me.addSkinHotspot=function(hotspot) {
		var hsinst = null;
		if (hotspot.skinid=='hotspoter') {
			hotspot.skinid = 'hotspoter';
			hsinst = new SkinHotspotClass_hotspoter(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		} else
		{
			hotspot.skinid = 'hotspot_pano';
			hsinst = new SkinHotspotClass_hotspot_pano(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
		}
		return hsinst;
	}
	me.removeSkinHotspots=function() {
		if(hotspotTemplates['hotspoter']) {
			var i;
			for(i = 0; i < hotspotTemplates['hotspoter'].length; i++) {
				hotspotTemplates['hotspoter'][i] = null;
			}
		}
		if(hotspotTemplates['hotspot_pano']) {
			var i;
			for(i = 0; i < hotspotTemplates['hotspot_pano'].length; i++) {
				hotspotTemplates['hotspot_pano'][i] = null;
			}
		}
		hotspotTemplates = [];
	}
	me.addSkin();
	me.skinTimerEvent();
};