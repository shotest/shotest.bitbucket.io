(function() {
  if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(elem, startFrom) {
      var i, j, len, one, ref;
      startFrom = startFrom || 0;
      if (startFrom > this.length) {
        return -1;
      }
      ref = this;
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        one = ref[i];
        if (one === elem && startFrom <= i) {
          return i;
        } else if (one === elem && startFrom > i) {
          return -1;
        }
      }
      return -1;
    };
  }

  jQuery.expr[':'].contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
  };

  (function() {
    var getContent, getParams, getScope, getVersion, lang;
    lang = $.cookie('lang');
    getContent = function() {
      var contents_id;
      contents_id = '#new-header';
      if (angular.element($(contents_id)).scope() === void 0) {
        contents_id = '#contents-header';
      }
      return angular.element($(contents_id)).scope().content;
    };
    getParams = function() {
      var contents_id;
      contents_id = '#new-header';
      if (angular.element($(contents_id)).scope() === void 0) {
        contents_id = '#contents-header';
      }
      return angular.element($(contents_id)).scope().params;
    };
    getVersion = function(slug) {
      var content, content2, contents_id, header;
      contents_id = '#new-header';
      if (angular.element($(contents_id)).scope() === void 0) {
        contents_id = '#contents-header';
      }
      header = angular.element($(contents_id)).scope().header;
      content = _.find(header.contents, function(c) {
        return c.slug === slug;
      });
      if (content) {
        return content.version;
      } else {
        content = _.find(header.contents, function(c) {
          return c.slug === 'tech';
        });
        if (content) {
          content2 = _.find(content.child_contents, function(c) {
            return c.slug === slug;
          });
          if (content2) {
            return content2.version;
          } else {
            return void 0;
          }
        }
      }
    };
    getScope = function(selector) {
      return angular.element($(selector)).scope();
    };
    $(document).on('click', '.c-link', function() {
      var $cidInput, $el, TiStore, TiUtils, cidText, scope;
      $el = $(this);
      cidText = $el.text();
      $cidInput = $('.search__cid:eq(0) input[type="text"]');
      scope = angular.element($cidInput).scope();
      TiStore = scope.TiStore;
      TiUtils = scope.TiUtils;
      $.cookie('c_link', TiStore.acid + "," + cidText);
      TiUtils.jumpAcid(scope.version, cidText);
      return TiUtils.setCLink();
    });
    $(document).on('click', '.image--center img', function() {
      var src, tiFullImageRatio, width;
      src = $(this).attr('src');
      tiFullImageRatio = angular.element(document.body).injector().get('tiFullImageRatio');
      width = $(this).attr('width') * tiFullImageRatio;
      return window.open("/fullimage?src=" + src + "&width=" + width);
    });
    $(document).on('click', '.ti-download-html', function(e) {
      var $content, html, ti_version;
      e.preventDefault();
      ti_version = angular.element($('#header')).scope().version;
      $('#modal-loading').hide();
      $content = $('#modal-content').show();
      lang = getScope('#header').lang;
      switch (lang) {
        case 'en':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/download/ti/" + ti_version + "/" + lang + "\">Offline version ver." + ti_version + "</a> (recommended)</h1>\n  <p>Recommended web browsers.</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Safari</li>\n    <li>Firefox</li>\n    <li>Internet Explorer 8 or higher.</li>\n  </ul>\n\n  <p>Note:<br>\n  Not available on Android, iOS<br>\n  Offline version can be used without an internet connection. <br>\n  Function and appearance are similar to the online version. <br>\n  If you are using Internet Explorer, please turn off  the compatibility view.</p>\n\n  <hr style=\"margin-top:50px;\">\n\n  <h1 style=\"margin-top:50px;text-align:right;\" class=\"fa fa-file-pdf-o\"> <a href=\"/download/pdf/ti/" + ti_version + "/" + lang + "\">PDF ver." + ti_version + "</a></h1>\n</div>";
          break;
        case 'jp':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/download/ti/" + ti_version + "/" + lang + "\">オフライン版 ver." + ti_version + "</a> (推奨)</h1>\n  <p>推奨ウェブブラウザ</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Safari</li>\n    <li>Firefox</li>\n    <li>Internet Explorer 8 以上</li>\n  </ul>\n  <p>注:<br>\n  Android, iOSには対応していません。<br>\n  インターネット接続がなくても、オフライン版は使用可能です。<br>\n  機能と見かけはオンライン版と似ています。 <br>\n  Internet Explorerをご使用の場合は「互換表示」のチェックを外してください。</p>\n\n  <hr style=\"margin-top:50px;\">\n\n  <h1 style=\"margin-top:50px;text-align:right;\" class=\"fa fa-file-pdf-o\"> <a href=\"/download/pdf/ti/" + ti_version + "/" + lang + "\">PDF版 ver." + ti_version + "</a></h1>\n</div>";
          break;
        case 'tw':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/download/ti/" + ti_version + "/" + lang + "\">離線版 ver." + ti_version + "</a> （建議）</h1>\n  <p>建議使用的網頁瀏覽器。</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Safari</li>\n    <li>Firefox</li>\n    <li>Internet Explorer 8 或更高版本。</li>\n  </ul>\n  <p>注意：<br>\n  不可在 Android 和 iOS 系統上使用。<br>\n  離線版無需連接網際網路便可使用。 <br>\n  功能和外觀類似於線上版。</p>\n\n  <hr style=\"margin-top:50px;\">\n\n  <h1 style=\"margin-top:50px;text-align:right;\" class=\"fa fa-file-pdf-o\"> <a href=\"/download/pdf/ti/" + ti_version + "/" + lang + "\">PDF版 ver." + ti_version + "</a></h1>\n</div>";
          break;
        case 'ch':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/download/ti/" + ti_version + "/" + lang + "\">离线版 ver." + ti_version + "</a> （建议）</h1>\n  <p>建议使用的网页浏览器。</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Safari</li>\n    <li>Firefox</li>\n    <li>Internet Explorer 8 或更高版本。</li>\n  </ul>\n  <p>注：<br>\n  不可在 Android 和 iOS 系统上使用。<br>\n  离线版无需连接互联网便可使用。 <br>\n  功能和外观类似于在线版。</p>\n\n  <hr style=\"margin-top:50px;\">\n\n  <h1 style=\"margin-top:50px;text-align:right;\" class=\"fa fa-file-pdf-o\"> <a href=\"/download/pdf/ti/" + ti_version + "/" + lang + "\">PDF版 ver." + ti_version + "</a></h1>\n</div>";
      }
      $content.html(html);
      $('#modal').reveal();
      $('#modal').css('width', '800px');
      return $('#modal').css('margin-left', '-400px');
    });
    $(document).on('click', '.npp-download-html', function(e) {
      var $content, html, ti_version;
      e.preventDefault();
      ti_version = angular.element($('#header')).scope().version;
      $('#modal-loading').hide();
      $content = $('#modal-content').show();
      lang = getScope('#header').lang;
      switch (lang) {
        case 'en':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/offline/download/" + lang + "\" onclick=\"trackLink($(this).attr('href'))\">Offline web version</a> (recommended)</h1>\n  <p>Recommended web browsers.</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Internet Explorer 10 or higher.</li>\n  </ul>\n\n  <p>Note:<br>\n  Not available on Android, iOS<br>\n  Offline web version can be used without an internet connection. <br>\n  Function and appearance are similar to the online version. <br>\n  If you are using Internet Explorer, please turn off  the compatibility view.</p>\n\n</div>";
          break;
        case 'jp':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/offline/download/" + lang + "\" onclick=\"trackLink($(this).attr('href'))\">オフライン版</a> (推奨)</h1>\n  <p>推奨ウェブブラウザ</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Internet Explorer 10 以上</li>\n  </ul>\n  <p>注:<br>\n  Android, iOSには対応していません。<br>\n  インターネット接続がなくても、オフライン版は使用可能です。<br>\n  機能と見かけはオンライン版と似ています。 <br>\n  Internet Explorerをご使用の場合は「互換表示」のチェックを外してください。</p>\n\n</div>";
          break;
        case 'tw':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/offline/download/" + lang + "\" onclick=\"trackLink($(this).attr('href'))\">離線版</a> （建議）</h1>\n  <p>建議使用的網頁瀏覽器。</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Internet Explorer 10 或更高版本。</li>\n  </ul>\n  <p>注意：<br>\n  不可在 Android 和 iOS 系統上使用。<br>\n  離線版無需連接網際網路便可使用。 <br>\n  功能和外觀類似於線上版。</p>\n\n</div>";
          break;
        case 'ch':
          html = "<div class=\"plain-html\">\n  <h1 class=\"fa fa-download\"> <a href=\"/offline/download/" + lang + "\" onclick=\"trackLink($(this).attr('href'))\">离线版</a> （建议）</h1>\n  <p>建议使用的网页浏览器。</p>\n  <ul>\n    <li>Google Chrome</li>\n    <li>Internet Explorer 10 或更高版本。</li>\n  </ul>\n  <p>注：<br>\n  不可在 Android 和 iOS 系统上使用。<br>\n  离线版无需连接互联网便可使用。 <br>\n  功能和外观类似于在线版。</p>\n\n</div>";
      }
      $content.html(html);
      $('#modal').reveal();
      $('#modal').css('width', '800px');
      return $('#modal').css('margin-left', '-400px');
    });
    $(document).on('click', '.model-no', function() {
      var $el, $scope, content, contents_id, element, model, param, spec_version;
      $el = $(this);
      model = $el.text();
      content = getContent();
      spec_version = getVersion('spec');
      contents_id = '#new-header';
      if (angular.element($(contents_id)).scope() === void 0) {
        contents_id = '#contents-header';
      }
      lang = getScope(contents_id).lang;
      element = document.getElementById('contents-area');
      if (element === null) {
        element = document.getElementById('main');
      }
      $scope = angular.element(element).scope();
      param = {
        model_cd: model,
        type_nm: 'MTB',
        component_nm: 'Shifting Lever',
        version: spec_version,
        lang: lang,
        from: content
      };
      return $scope.modelModalOpen(param);
    });
    $(document).on('click', '.thumbnailImage .thumbnail', function() {
      var imgCaption, imgSrc;
      imgSrc = $('img', this).attr('src');
      imgCaption = $('.caption', this).text();
      $('.mainImage .thumbnail img').attr('src', imgSrc);
      return $('.mainImage .thumbnail .caption').text(imgCaption);
    });
    $(document).on('click', '.button-help', function(e) {
      var html;
      e.preventDefault();
      lang = getScope('#contents-header').lang;
      switch (lang) {
        case 'en':
          html = "<dl class=\"updatenewsdate\">\n  <dt>Aug 5, 2016</dt><dd>Added Model no. color definitions to Technical information.</dd>\n  <dt>Feb 26, 2016</dt><dd>The offline web version now includes not only Technical information but also Line-up chart and Specifications, in one package.</dd>\n</dl>";
          break;
        case 'jp':
          html = "<dl class=\"updatenewsdate\">\n  <dt>2016年8月5日</dt><dd>技術資料にモデルナンバーの色分け説明を追加しました。</dd>\n  <dt>2016年2月26日</dt><dd>オフライン版は技術資料だけでなく製品ラインナップと製品仕様もパッケージとして含まれています。</dd>\n</dl>";
          break;
        case 'tw':
          html = "<dl class=\"updatenewsdate\">\n  <dt>2016年8月5日</dt><dd>在技術資訊中添加了型號顏色定義。</dd>\n  <dt>2016年2月26日</dt><dd>離線版套件現在不僅包括技術資訊，還包括產品列表和規格。</dd>\n</dl>";
          break;
        case 'ch':
          html = "<dl class=\"updatenewsdate\">\n  <dt>2016年8月5日</dt><dd>在技术资料中增加了型号颜色定义。</dd>\n  <dt>2016年2月26日</dt><dd>离线版套装现在不仅包括技术资料，还包括产品配置表和规格资料。</dd>\n</dl>";
      }
      $('#modal-loading').hide();
      $('#modal-content').show().html(html);
      $('#modal').reveal();
      $('#modal').css('width', '800px');
      return $('#modal').css('margin-left', '-400px');
    });
    $(document).on('click', '.color-definitions-help', function(e) {
      var html;
      e.preventDefault();
      lang = getScope('#contents-header').lang;
      switch (lang) {
        case 'en':
          html = "<div class=\"modal-color-definitions\">\n  <h3>Model no. color definitions</h3>\n  <p>New models: <span class=\"new\">XX-####</span></p>\n  <p>Discontinued models: <span class=\"discon\">XX-####</span></p>\n</div>";
          break;
        case 'jp':
          html = "<div class=\"modal-color-definitions\">\n  <h3>モデルナンバーの色分け説明</h3>\n  <p>新モデル: <span class=\"new\">XX-####</span></p>\n  <p>生産終了モデル: <span class=\"discon\">XX-####</span></p>\n</div>";
          break;
        case 'tw':
          html = "<div class=\"modal-color-definitions\">\n  <h3>型號顏色定義</h3>\n  <p>新型號: <span class=\"new\">XX-####</span></p>\n  <p>停產型號: <span class=\"discon\">XX-####</span></p>\n</div>";
          break;
        case 'ch':
          html = "<div class=\"modal-color-definitions\">\n  <h3>型号颜色定义</h3>\n  <p>新型号: <span class=\"new\">XX-####</span></p>\n  <p>停产型号: <span class=\"discon\">XX-####</span></p>\n</div>";
      }
      $('#modal-loading').hide();
      $('#modal-content').show().html(html);
      $('#modal').reveal();
      $('#modal').css('width', '600px');
      return $('#modal').css('margin-left', '-300px');
    });
    $(document).on('reveal:close', '#modal', function() {
      $('#modal-loading').show();
      return $('#modal-content').html('');
    });
    return $(document).on('click', '.lc-help', function(e) {
      var contents_id, html;
      e.preventDefault();
      contents_id = '#new-header';
      if (angular.element($(contents_id)).scope() === void 0) {
        contents_id = '#contents-header';
      }
      lang = getScope(contents_id).lang;
      switch (lang) {
        case 'en':
          html = "<div style=\"width: 200px; float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><img width=\"16\" height=\"16\" src=\"images/lc/common/icon-new.png\"> <span> ... New model </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #b2e5fa;\"> </canvas> <span> ... Additional Option </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #eee;\"> </canvas> <span> ... Alternative Option </span> </li>\n  </ul>\n</div>\n<div style=\"float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px solid #22B4F1 ;\"> </canvas> <span> ... All select </span> </li>\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px dashed #22B4F1 ;\"> </canvas> <span> ... Single select </span> </li>\n  </ul>\n</div>";
          break;
        case 'jp':
          html = "<div style=\"width: 200px; float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><img width=\"16\" height=\"16\" src=\"images/lc/common/icon-new.png\"> <span> ... New model </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #b2e5fa;\"> </canvas> <span> ... Additional Option </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #eee;\"> </canvas> <span> ... Alternative Option </span> </li>\n  </ul>\n</div>\n<div style=\"float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px solid #22B4F1 ;\"> </canvas> <span> ... All select </span> </li>\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px dashed #22B4F1 ;\"> </canvas> <span> ... Single select </span> </li>\n  </ul>\n</div>";
          break;
        case 'tw':
          html = "<div style=\"width: 200px; float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><img width=\"16\" height=\"16\" src=\"images/lc/common/icon-new.png\"> <span> ... New model </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #b2e5fa;\"> </canvas> <span> ... Additional Option </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #eee;\"> </canvas> <span> ... Alternative Option </span> </li>\n  </ul>\n</div>\n<div style=\"float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px solid #22B4F1 ;\"> </canvas> <span> ... All select </span> </li>\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px dashed #22B4F1 ;\"> </canvas> <span> ... Single select </span> </li>\n  </ul>\n</div>";
          break;
        case 'ch':
          html = "<div style=\"width: 200px; float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><img width=\"16\" height=\"16\" src=\"images/lc/common/icon-new.png\"> <span> ... New model </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #b2e5fa;\"> </canvas> <span> ... Additional Option </span> </li>\n    <li><canvas width=\"16\" height=\"16\" style=\"background-color: #eee;\"> </canvas> <span> ... Alternative Option </span> </li>\n  </ul>\n</div>\n<div style=\"float: left;\">\n  <ul style=\"list-style-type: none;\">\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px solid #22B4F1 ;\"> </canvas> <span> ... All select </span> </li>\n    <li><canvas width=\"20\" height=\"20\" style=\"border: 2px dashed #22B4F1 ;\"> </canvas> <span> ... Single select </span> </li>\n  </ul>\n</div>";
      }
      $('#modal-loading').hide();
      $('#modal-content').show().html(html);
      $('#modal').reveal();
      $('#modal').css('width', '600px');
      return $('#modal').css('margin-left', '-300px');
    });
  })();

}).call(this);
