(function() {
  var options, prevLeft, scrolling, tapFlag;

  prevLeft = 0;

  scrolling = null;

  options = {
    '#header': 0,
    '.second-header': 38
  };

  $(document).on('scroll', function() {
    var currentLeft, scrollTop, selector;
    currentLeft = $(this).scrollLeft();
    if (prevLeft !== currentLeft) {
      return false;
    }
    for (selector in options) {
      $(selector).css({
        position: 'fixed',
        top: options[selector]
      });
    }
    scrollTop = $(this).scrollTop();
    clearTimeout(scrolling);
    return scrolling = setTimeout(function() {
      var results;
      results = [];
      for (selector in options) {
        results.push($(selector).css({
          position: 'absolute',
          top: scrollTop + options[selector]
        }));
      }
      return results;
    }, 200);
  });

  setInterval(function() {
    return $('#main-content, #main-content--full, #contents,#d3-target, #contents--other, #contents--nda, #contents--full, #table-of-contents, #sidebar, #ti-text-search, .ti-text-search__body, #search-result').each(function() {
      var height, offsetTop, paddingTop, windowHeight;
      windowHeight = $(window).height();
      offsetTop = $(this).offset().top;
      paddingTop = parseInt($(this).css("padding-top"), 10);
      if ($(this).is('.main-content-main')) {
        height = windowHeight - offsetTop - paddingTop - 94 - 4;
      } else {
        height = windowHeight - offsetTop - paddingTop - 4;
      }
      return $(this).height(height);
    });
  }, 1000);

  setInterval(function() {
    return $('.sidebar__menu').each(function() {
      var maxheight, offsetTop, paddingTop, windowHeight;
      windowHeight = $(window).height();
      offsetTop = $(this).offset().top;
      paddingTop = parseInt($(this).css("padding-top"), 10);
      maxheight = windowHeight - offsetTop - paddingTop - 4;
      return $(this).css('max-height', maxheight);
    });
  }, 1000);

  tapFlag = false;

  document.body.addEventListener("touchstart", function(evt) {
    if ($(".slider").length > 0) {
      if (tapFlag) {
        return evt.preventDefault();
      }
    }
  }, {
    passive: false
  });

  document.body.addEventListener("touchend", function(evt) {
    var timer;
    if ($(".slider").length > 0) {
      tapFlag = true;
      clearTimeout(timer);
      return timer = setTimeout(function() {
        return tapFlag = false;
      }, 300);
    }
  }, true);

}).call(this);
