﻿(() => {
    'use strict';
    let _W, _D, _B, _H, _M, _T, _S, _SW;
    let $W, $D, $B, $H, $M, $T, $S, $SW;
    const _ = window._ || {};

    // *************************************************************
    // _.init
    // *************************************************************
    _.init = () => {
        _.config.init();
        _.util.init();
        _.$.init();

        _.data.init();

        _.action.init();
        _.img.init();
    }

    // *************************************************************
    // _.config
    // *************************************************************
    _.config = {
        init: () => {
            const path = location.pathname.split('/')
            _.config.dir = path[1]

            _.config.Device();
            _.config.TouchDevice();
        },
        version: '1.0.0',
        path: {
            json: {
                item: 'build/data/item1.json',
                map: 'build/data/map1.json',
                noLinkList: 'build/data/nolinklist.json',
                Item: (NUM) => {
                    return 'build/data/item_' + NUM + '.json';
                },
                Map: (NUM) => {
                    return 'build/data/map_' + NUM + '.json';
                },
            },
            svg: {
                symbol: 'build/data/symbol-defs.svg',
                marks: 'fonts/marks.svg'
            }
        },

        lang: 'en',
        LangChk: (TYPE) => {
            return TYPE !== _.config.lang;
        },
        user: false,

        User: (TF) => {
            if (TF) {
                _.config.user = TF;
                return TF;
            } else {
                return _.config.user = (_.config.user ? false : true);
            }
        },
        device: '',
        Device: () => {
            _.config.device = _.util.ua();
        },
        touchDevice: '',
        TouchDevice: () => {
            _.config.touchDevice = _.util.is.touchDevice();
        },
        dir: ''
    }

    // *************************************************************
    // _.util
    // *************************************************************
    _.util = {
        init: () => { },
        ua: () => {
            const _ua = window.navigator.userAgent.toLowerCase();
            const uaList = [
                ['iphone', 'iphone',true],
                ['ipad', 'ipad', true],
                ['ipad', 'macintosh', 'ontouchend' in document],
                ['ipod', 'ipod', true],
                ['android', 'android', true],
                ['ie', 'msie|trident', true],
                ['edge', 'edge', true],
                ['chrome', 'chrome', true],
                ['firefox', 'firefox', true],
                ['safari', 'safari', true],
                ['opera', 'opera', true],
                ['else', '.', true]
            ];

            let ua = {};
            uaList.some((V) => {
                const reg = new RegExp(V[1], 'ig');
                return ua[V[0]] = V[2]? _ua.match(reg) || '' : '';
            })

            return ua;
        },
        escape: (STR) => {
            if (!STR) return;
            return STR.replace(/[<>&\'\`]/g, (match) => {
                const escape = {
                    '<': '&lt;',
                    '>': '&gt;',
                    '&': '&amp;',
                    '"': '&quot;',
                    "'": '&#39;',
                    '`': '&#x60;'
                };
                return escape[match];
            });
        },
        obj: {
            assign: function(T){
                if (Object.assign) {
                    return Object.assign(T, ...arguments);

                } else {
                    var to = Object(T);
                    for (var i = 1; i < arguments.length; i++) {
                        var nextSource = arguments[i];
                        if (nextSource === undefined || nextSource === null) {
                            continue;
                        }
                        nextSource = Object(nextSource);

                        var keysArray = Object.keys(Object(nextSource));
                        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                            var nextKey = keysArray[nextIndex];
                            var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                            if (desc !== undefined && desc.enumerable) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                    return to;
                }
            }
        },
        num: {
            fix: (NUM, DIGIT) => {
                return ('0000000' + NUM).slice(DIGIT * -1)
            },
            comma: (NUM) => {
                return String(NUM).replace(/(\d)(?=(\d{3})+$)/g, '$1,')
            },
            mean: (A, B) => {
                return (A instanceof Array && !B) ? d3.mean(A) : d3.mean([A, B]);
            },
            per: (NUM, _DIGIT) => {
                const DIGIT = _DIGIT || 1;
                if (NUM == 1) {
                    return '100';
                }
                return (NUM * 100).toFixed(DIGIT)
            }
        },
        date: (FMT) => {
            var _DATE = new Date();

            return FMT.replace(/(Y+|M+|D+|W+|w+|h+|m+|s+)/gm, function (s) {
                var len = String(s).length;
                switch (s) {
                    case String(s.match(/Y+/)): return _.util.num.fix(_DATE.getFullYear(), len);
                    case String(s.match(/M+/)): return _.util.num.fix(_DATE.getMonth() + 1, len);
                    case String(s.match(/D+/)): return _.util.num.fix(_DATE.getDate(), len);
                    case String(s.match(/h+/)): return _.util.num.fix(_DATE.getHours(), len);
                    case String(s.match(/m+/)): return _.util.num.fix(_DATE.getMinutes(), len);
                    case String(s.match(/s+/)): return _.util.num.fix(_DATE.getSeconds(), len);
                    default: break;
                }
            });
        },
        type: (T) => {
            return Object.prototype.toString.call(T);
        },
        is: {
            obj: (T) => {
                return /Object/.test(_.util.type(T));
            },
            arr: (T) => {
                return /Array/.test(_.util.type(T));
            },
            func: (T) => {
                return /Function/.test(_.util.type(T));
            },
            str: (T) => {
                return /String/.test(_.util.type(T));
            },
            num: (T) => {
                return /Number/.test(_.util.type(T));
            },
            filelist: (T) => {
                return /FileList/.test(_.util.type(T));
            },
            touchDevice: () => {
                return (
                    !!_.config.device.iphone
                    || !!_.config.device.ipad
                    || !!_.config.device.ipod
                    || !!_.config.device.android
                );
            }
        },
        ss: {
            get: (KEY) => {
                return sessionStorage.getItem(KEY);
            },
            set: (KEY, VAL) => {
                sessionStorage.getItem(KEY, VAL);
            },
            clear: () => {
                sessionStorage.clear();
            }
        },
        arr: {
            unique: (ARR) => {
                return ARR.filter((V, I, A) => {
                    return A.indexOf(V) == I;
                })
            },
            find: function (ARR, PRE) {
                if (Array.prototype.find) {
                    return ARR.find(PRE);
                } else {
                    var length = ARR.length >>> 0;
                    var thisArg = arguments[1];
                    var value;

                    for (var i = 0; i < length; i++) {
                        value = ARR[i];
                        if (PRE.call(thisArg, value, i, ARR)) {
                            return value;
                        }
                    }
                    return undefined;
                }
            }
        }

    };

    // *************************************************************
    // _.$
    // *************************************************************
    _.$ = {
        init: () => {
            $W = $(_W = window);
            $D = $(_D = document);
            $B = $(_B = 'body');
            $H = $(_H = 'header');
            $M = $(_M = '#main');
            $T = $(_T = '#d3-target');
            $S = $(_S = '#sidebar');
            $SW = $(_SW = '#switcher');
      
        },
    }


    // *************************************************************
    // _.data
    // *************************************************************
    _.data = {
        init: (_NUM) => {
            const NUM = _NUM || 1;　// 最初に表示するNUM
            Promise.resolve()
                .then(() => {
                    return _.data.get.predata(NUM);
                })
                .then(() => {
                    _.data.extract.iconId();
                    _.d3.init();
                })
        },
        map: {},
        items: {},
        noLinkList: '',
        marks: '',
        symbol: '',
        iconIds: [],
        archives: {
            map: [],
            item: []
        },

        get: {
            predata: (NUM) => {
                const promiseArr = [];
                return Promise.all(
                    (() => {
                        promiseArr.push(_.data.archives.item[NUM] ?
                            Promise.resolve(_.data.items = _.data.archives.item[NUM]) :
                            new Promise((RS, RJ) => {
                                $.getJSON(_.config.path.json.Item(NUM), (DATA) => {
                                    _.data.items = DATA;
                                    _.data.archives.item[NUM] = DATA;
                                    return RS();
                                })
                            })
                        )
                        promiseArr.push(_.data.archives.map[NUM] ?
                            Promise.resolve(_.data.map = _.data.archives.map[NUM]) :
                            new Promise((RS, RJ) => {
                                $.getJSON(_.config.path.json.Map(NUM), (DATA) => {
                                    _.data.map = DATA;
                                    _.data.archives.map[NUM] = DATA;
                                    return RS();
                                })
                            })
                        )
                        promiseArr.push(_.data.noLinkList ?
                            Promise.resolve(_.data.noLinkList) :
                            new Promise((RS, RJ) => {
                                $.getJSON(_.config.path.json.noLinkList, (DATA) => {
                                    _.data.noLinkList = DATA;
                                    return RS();
                                })
                            })
                        )
                        return promiseArr;
                    })()
                    
                ).then(() => {
                    return Promise.resolve();
                })
            },
            noLink: (_NAME) => {
                const NAME = _NAME || 'XXXXX';
                return _.data.noLinkList.indexOf((NAME.value || NAME).replace(/%[^%]+%/g,'')) >= 0
            },

        },
        extract: {
           iconId: () => {
                _.data.iconIds = _.util.arr.unique(
                    _.data.items.map(
                        (_V) => _V.icon
                    )
                ).sort();
            }
        }
    }


    // *************************************************************
    // _.d3
    // *************************************************************
    _.d3 = {
        init: () => {
            $T.hide();
            _.d3.config.init();
            _.d3.create.chart();
            _.d3.style();
            $T.fadeIn(1000, () => {
                setTimeout(() => {
                    _.d3.action.newIcon();
                    _.d3.action.update();
                }, 1)
            });
        },
        data: {
            tree: {},
            map: {},
            inPosArr: [],
            optPosArr: []
        },
        config: {
            init: () => {
                $T.empty();

                const wideFlg = _.d3.config.Wide();

                // svg-width/height
                const W = _.d3.config.W = wideFlg ? 960 : 720;
                const w = _.d3.config.w = 720;
                const h = _.d3.config.h = $T.outerHeight();
                const l = _.d3.config.l = 0;
                const c = _.d3.config.c = w / 2 + l;

            },


            grouping: true,
            Group: () => {
                const scale = _.d3.config.Scale();
                if (scale == 1) {
                    _.d3.config.grouping = true;
                } else {
                    _.d3.config.grouping = false;
                }
                return _.d3.config.grouping;
            },

            scales: [0.6,0.7,0.8,0.9,1,1.333],
            scale: 4,
            scaler: 0,
            Scale: (UPDOWN) => {
                const scales = _.d3.config.scales;
                const max = scales.length - 1;
                const min = 0;
                const dscale = _.d3.config.scale + (UPDOWN || 0);

                if (_.d3.config.scale < dscale
                ) {
                    _.d3.config.scaler = 0;
                }

                const scale = _.d3.config.scale = d3.min([max, d3.max([min, dscale])]);

                if (UPDOWN) {
                    // 4/8切り換えなしのためGrouping切替削除
                    //_.d3.config.Group();
                }
                return scales[scale];
            },

            narrows: [1,0.9, 0.8, 0.7],
            Narrow: (_NUM, REV) => {
                const NUM = +_NUM || 2;
                return REV
                    ? (1 / _.d3.config.narrows[NUM]) - 1
                    : _.d3.config.narrows[NUM];
            },

            W: 0,
            w: 0,
            h: 0,
            c: 0,
            l: 0,

            Dir: () => { return _.d3.config.direction == 'h'; },
            DirRev: () => { return _.d3.config.Dir() ? 'v' : 'h'; },
            direction: 'v', // v/h
            Wide: () => { return _.d3.config.space == 'w'; },
            space: 'n', // w/n

            pers: ['new', 'update', 'br', 'dash'],
            persReg: () => {
                return new RegExp('%(' + _.d3.config.pers.join('|') + ')[^%]*%', 'gi')
            },
            Pers: (_STR, REG) => {
                const STR = _STR || '';
                const persReg = _.d3.config.persReg();
                const pers = (String(STR).match(persReg) || []).map((V) => {
                    return V.replace(/%/g, '');
                });
                if (REG) {
                    return _.util.arr.find(pers,(V) => {
                        return String(V).match(REG);
                    });
                } else {
                    return pers;
                }
            },
            Escape: (_STR) => {
                const STR = _STR || '';
                const persReg = _.d3.config.persReg();
                return String(STR).replace(persReg, '');
            },

            animation: {
                duration: 400,
                ease: d3.easePolyOut,
            },

            box: {
                gap: {
                    x: 14,
                    y: 20,
                    y0: 20,
                    y1: 40,
                    y2: 60
                },
                stroke: {
                    w: 2
                },

                Width: (ITEM) => {

                    const grouping = _.d3.config.grouping;
                    // 4/8切り換え不要のため1固定に
                    const scale = 1;//d3.min([1,_.d3.config.Scale()]);
                    const horizon = grouping && _.d3.config.Dir();
                    const col = ITEM.num.col;
                    const row = ITEM.num.row;
                    const cols = _.d3.data.map[row];

                    const cid = ITEM.cid;
                    const group = cid && horizon;

                    const gcols = cols.filter((V) => {
                        return V.cid == cid;
                    })

                    const colTotal = d3.sum((horizon? gcols: cols).map((V) => {
                        return V.place || 1;
                    }))
                    const place = cols[col].place || 1;

                    const gap = _.d3.config.box.gap.x;
                    const frameWidth = (_.d3.config.w - 2) / scale;

                    const width = (frameWidth - gap * (colTotal - 1)) / colTotal;

                    if (place == 1) {
                        return width;
                    } else {
                        return width * place + gap * (place - 1);
                    }

                }
            },
            inbox: {
                padding: 10,
                props: [
                    'boxHeader',
                    'icon',
                    'iconText',
                    'model',
                    'header',
                    'name',
                    'names',
                    'cap',
                    'en',
                    'jp',
                    'comment',
                    'target'
                ],
                height: {
                    _m: 1.5,
                    boxHeader: 16,
                    icon: 60,
                    iconText: 14,
                    header: 18,
                    update: 24,
                    name: 18,
                    names: 18,
                    cap: 18,
                    model: 30,
                    option: 30,
                    comment: 14
                },
                position: {
                },
                symbol: {
                    new: '<tspan dx="7" dy="-0.3em" class="new" >New</tspan>',
                    New: (P,X,Y) => {
                        P.append('use')
                            .attr('viewBox', '0 0 12 12')
                            .attr('width', '12px')
                            .attr('height', '12px')
                            .attr('x', (X || '12px'))
                            .attr('y', (Y || '12px'))
                            .attr('href', '#icons-new')
                            .attr('xlink:href', '#icons-new')

                    }
                }
            }
        },
        create: {
            svg: () => {
                const opt = _.d3.config;
                const main = d3.select('#d3-target');
                const svg = main.append('svg')
                    .attr('id', 'svg')
                    .attr('width', opt.W)
                    .attr('height', opt.h)
                    .attr('xmlns:xlink', d3.namespace('xlink').space)

                const defs = svg.append('defs')
                    .attr('id', 'defs')
                    .append(() => {
                        return $(_.svg.marks).find('#shimano-marks').clone(true)[0];
                    })
                    .append(() => {
                        return $(_.svg.symbol).find('#icons').clone(true)[0];
                    })

                const frame = svg
                    .append('g')
                    .classed('svg-frame', true)

                const back = frame
                    .append('rect')
                    .classed('background', true)
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width', opt.W / d3.min([1, _.d3.config.Scale()]))
                    .attr('fill', '')
                    .attr('opacity', 0)

                const search = frame
                    .append('g')
                    .classed('search-frame', true)

                const update = frame
                    .append('g')
                    .classed('update-frame', true)

                return { svg, frame, back, search, update };
            },
            chart: () => {

                let svg, frame, back, search, update;
                let map = {};
                let firstGroup = 0;
                let lastGroup = 0;

                const rowHeight = 50;
                const rectHeight = 200;

                const opt = _.d3.config;

                const empty = d3.select('#svg').empty();
                if (empty) {
                    const obj = _.d3.create.svg();
                    svg = obj.svg;
                    frame = obj.frame;
                    back = obj.back;
                    search = obj.search;
                    update = obj.update;
                } else {
                    svg = d3.select('#svg');
                    frame = svg.select('.svg-frame');
                    back = svg.select('.background');
                    search = svg.select('.search-frame');
                    update = svg.select('.update-frame');
                }
                const DUR = opt.animation.duration;
                
                const NEW = opt.inbox.symbol.new;
                const New = (_STR) => {
                    const STR = _STR || '';
                    return STR;
                    return (STR.replace ? STR : STR.value).replace(/%new%/ig, NEW);
                };
                const Dash = (_STR) => {
                    const STR = _STR || '';
                    return /%dash%/ig.test(STR.replace ? STR : STR.value);
                };
                const Item = (ID) => {
                    return _.util.arr.find(_.data.items,(V) => {
                        const flg = (V.id || V._id) == ID;
                        return flg;
                    })
                };
                const Break = (STR) => {

                    if (_.util.is.str(STR) && /%br[hv]?%/ig.test(STR)) {

                        const br = new RegExp('%br' + opt.direction + '?%', 'ig');
                        const rev = new RegExp('%br' + opt.DirRev() + '%', 'ig');

                        return STR.replace(rev, '').split(br);
                    } else {
                        return STR;
                    }
                };
                const IsUpdate = (STR) => {
                    if (_.config.user) {
                        return '';
                    } else {
                        return /%update[^%]+%/ig.test(STR)
                    }
                };
                const UpdateNum = (STR) => {
                    const update = String(STR.match(/%update:[^%]+?%/ig));
                    return +(update.match(/\d+/)[0]);
                };
                const Inbox = (INBOX, BOX) => {

                    let id = 0;
                    let inpos = opt.inbox.padding; // + (INBOX.boxHeader ? 15 : 0);
                    let inposC = 5;
                    let columns = INBOX.columns > 100 ? INBOX.columns - 100 : 0;
                    let modelCols = [];
                    let namesCols = 0;
                    let modelCol = 0;
                    let namesCol = 0;
                    let modelColsNum = 0;
                    let modelColsStart = 0;
                    let modelColsEnd = 0;
                    let namesColsStart = 0;
                    let namesColsEnd = 0;
                    let isModelChild;
                    let modelChildPlace = 0;
                    let isUpdateChild;
                    let marginFlg = true;

                    const inbox = ((_INBOX) => {
                        const INBOX = _INBOX || {};
                        if (INBOX.did) {
                            const merge = _.util.obj.assign({},Item(INBOX.did), INBOX);
                            return JSON.parse(JSON.stringify(merge));
                        } else {
                            return JSON.parse(JSON.stringify(INBOX));
                        }
                    })(INBOX);
                    
                    const props = opt.inbox.props;
                    const height = opt.inbox.height;

                    const Top = (BOX, OPT) => {
                        if (OPT.prop == 'names') {
                            if (namesCol % namesCols == 0) {
                                namesColsStart = inpos;
                                namesColsEnd = 0;
                            } else {
                                inpos = namesColsStart;
                            }

                            if (IsUpdate(OPT.value)) {
                                inpos += 28;
                            }

                            return inpos;
                        } else {

                            if (IsUpdate(OPT.value)) {
                                inpos += 18;
                            }
                        }
                        return inpos;
                    }
                    const Center = (BOX, OPT) => {
                        const modelColsNumTemp = modelCols.slice(-1)[0];
                        let c, num = -1;



                        if (OPT.prop == 'boxHeader') {
                            c = BOX.c1;

                        //} else if (OPT.prop == 'header'){
                        //    c = BOX.c1;
                        //    inpos += height[OPT.prop];
                        //
                        } else if (!OPT.isModelChild) {
                            c = BOX.c1;

                            if (OPT.prop == 'comment') {
                                inposC += height[OPT.prop];
                            }
                            if (OPT.prop == 'update') {
                                inposC += height[OPT.prop];
                            } else {
                                inpos += height[OPT.prop];
                            }

                        } else if (namesCols > 1 && (OPT.prop == 'names' || OPT._prop == 'names')) {
                            let mCols = 0;
                            if (OPT.place) {
                                mCols = 1;
                                if (OPT.place >= modelColsNumTemp) {
                                }
                            } else {
                                mCols = modelColsNumTemp;
                            }

                            const cols = mCols * namesCols;
                            c = BOX['c' + cols];
                            num = namesCols * (modelCol % mCols) + namesCol % namesCols;

                            if (namesCol % namesCols == 0) {
                                namesColsEnd = 0;
                            }

                            inpos += height[OPT.prop];
                            namesColsEnd = namesColsEnd > inpos ? namesColsEnd : inpos;

                            if (namesCol % namesCols == (namesCols - 1) || namesCol == OPT.nums) {
                                inpos = namesColsEnd;
                            }

                        } else {

                            // model.place
                            if (OPT.place) {
                                c = BOX['c1']
                                if (OPT.place == modelColsNumTemp) {
                                }
                            } else {
                                c = BOX['c' + modelColsNumTemp];
                            }

                            num = modelCol % modelColsNumTemp


                            if (OPT._prop == 'cap') {
                               inpos += height['cap'];
                            } else {
                                inpos += height[OPT.prop];
                            }
                        }

                        if (_.util.is.arr(c)) {
                            return c[num];
                        } else {
                            return c;
                        }
                    };

                    const result = Setter(inbox);

                    result.height = d3.max([60,inpos + opt.inbox.padding]); //最小値を60に設定
                    return result;

                    //function Setter(INPUT, TYPE, NUM) {
                    function Setter(INPUT, _OPT) {

                        const OPT = _OPT || {};

                        if (INPUT.id) {
                            id = INPUT.id;
                            modelCols = [(columns||INPUT.columns || 1)];
                        }
                        if (/spread/ig.test(INPUT.textStyle)) {
                            inpos += 10;
                        }

                        modelColsNum = modelCols.slice(-1)[0];

                        namesCols = INPUT.id || /wrapper/ig.test(INPUT.boxStyle) ? 1 : INPUT.names ? (columns||INPUT.columns || 1) : namesCols;

                        if (_.util.is.str(INPUT) || _.util.is.num(INPUT) || OPT.prop == 'names') {


                            let result;
                            namesCol = namesCols > 1 ? OPT.num : 0;
                            result = {
                                value: INPUT
                                , t: (OPT.prop == 'comment') ? inposC: Top(BOX,OPT)
                                , h: height[OPT.prop]
                                , c: Center(BOX, OPT)
                                , modelCols: modelColsNum
                                , namesCols: namesCols
                                , namesCol: namesCol
                                , namesColAll: namesCol ? OPT.nums : 0
                                , textStyle: OPT.textStyle
                            };

                            const narrow = (OPT.textStyle || '').match(/narrow-[^ ]+/g) || [];
                            narrow.forEach((V) => {
                                const value = V.match(/\d$/) ? V.slice(-1) : '';
                                const key = V.replace(/\d|-/g,'');
                                result[key] = value || 2;
                            })


                            if (OPT.type == 'arr') {
                                namesCol = 0;
                            }
                            return result;

                        } else {
                            let obj = INPUT;
                            obj.textStyle = (OPT.textStyle ? OPT.textStyle + ' ' : '') + (obj.textStyle || '');
                            obj.modelCols = modelColsNum;
                            obj.namesCols = namesCols;

                            props.forEach((V) => {

                                const isModel = V == 'model';
                                const isNames = V == 'names';
                                const isComment = V == 'comment';
                                const isBoxHeader = V == 'boxHeader';
                                const isHeader = V == 'header';
                                const isUpdate = V == 'update';
                                const isTarget = V == 'target';

                                const isInCap = OPT.prop == 'cap';

                                //if (isModel && !!INPUT.place) {
                                //    modelChildPlace = INPUT.place;
                                //    OPT.place = INPUT.place;
                                //} else if (isModel) {
                                //    modelChildPlace = 1;
                                //}

                                if (isInCap && !!obj[V] && _.config.LangChk(V)) {
                                    return;
                                }

                                if (obj[V]) {
                                    marginFlg = true;

                                    switch (!!V) {
                                        case isComment:
                                            inpos += 10;
                                            break;
                                        case isHeader:
                                            inpos += 10;
                                            break;
                                        case isUpdate:
                                            inpos += 5;
                                            marginFlg = false;
                                            isUpdateChild = true;
                                            break;
                                        default: break;
                                    }

                                    obj[V] = Break(obj[V]);

                                    // 配列のとき
                                    if (_.util.is.arr(obj[V])) {
                                        obj[V] = obj[V].map((R, RI, AR) => {
                                            if (isModel) {
                                                isModelChild = true;
                                                modelCols.push(columns||INPUT.columns || 1);
                                                modelColsNum = modelCols.slice(-1)[0];

                                                if (R.place) {
                                                    inpos = modelColsEnd > inpos ? modelColsEnd : inpos;
                                                }
                                            }
                                            let result = {};
                                            let tmp = {};

                                            if (_.util.is.str(R)) {
                                                return Setter(R, {
                                                    type: 'arr'
                                                    , prop: V
                                                    , _prop: OPT.prop
                                                    , value: R
                                                    , num: RI
                                                    , nums: AR.length-1
                                                    , textStyle: obj.textStyle
                                                    , isModelChild: isModelChild
                                                    , place: R.place || OPT.place
                                                });
                                            } else {

                                                if (RI == 0 || modelCol < RI) {
                                                    tmp.modelCol = modelCol = RI;
                                                } else {
                                                    tmp.modelCol = ++modelCol;
                                                }

                                                if (isModel) {
                                                    if (modelColsNum > 1 && modelCol % modelColsNum == 0) {
                                                        modelColsStart = inpos;
                                                    } else if (!!R.place) {
                                                        tmp.modelCol = modelCol += (modelColsNum - modelCol % modelColsNum);

                                                    }

                                                    if (/add|alt|single|grey|blue|dotted/ig.test(R.boxStyle)) {
                                                        tmp.t = inpos;
                                                        inpos += 10;
                                                    }
                                                }

                                                result = Setter(R, {
                                                    type: 'arr'
                                                    , prop: V
                                                    , _prop: OPT.prop
                                                    , value: R
                                                    , modelCols: modelColsNum
                                                    , modelCol: modelCol
                                                    , num: modelCol
                                                    , nums: AR.length-1
                                                    , textStyle: obj.textStyle
                                                    , isModelChild: isModelChild
                                                    , place: R.place || OPT.place
                                                });

                                                if (isModel) {

                                                    if (/add|alt|single|grey|blue|dotted/ig.test(R.boxStyle)) {
                                                        tmp.b = inpos;
                                                        tmp.height = tmp.b - tmp.t;
                                                        inpos += 10;
                                                    }

                                                    if (!R.place && modelColsNum > 1 && modelCol % modelColsNum != (modelColsNum - 1) && RI != AR.length - 1) {
                                                        modelColsEnd = modelColsEnd > inpos ? modelColsEnd : inpos;
                                                        inpos = modelColsStart;
                                                    } else {
                                                        inpos = modelColsEnd > inpos ? modelColsEnd : inpos;
                                                    }

                                                    if (!!R.place) {
                                                        modelCol += modelColsNum - 1;
                                                    }

                                                    const narrow = (R.textStyle || '').match(/narrow-[^ ]+/g) || [];
                                                    narrow.forEach((V) => {
                                                        const value = V.match(/\d$/) ? V.slice(-1) : '';
                                                        const key = V.replace(/\d|-/g, '');
                                                        result[key] = value || 2;
                                                    })

                                                    result.centers = _.util.arr.unique(
                                                        result.names ? Array.prototype.concat.apply([], result.names.map((e) => {
                                                            return e.c;
                                                        })) : result.name ? [result.name.c] : []
                                                    )
                                                    modelCols.pop();
                                                }

                                                return _.util.obj.assign(tmp, result);
                                            }
                                        });

                                        if (isModel) {
                                            isModelChild = OPT.isModelChild;
                                        }

                                    } else {
                                        obj[V] = Setter(obj[V], {
                                            type: 'obj'
                                            , prop: V
                                            , _prop: OPT.prop 
                                            , value: obj[V]
                                            , textStyle: obj.textStyle
                                            , isModelChild: OPT.isModelChild
                                            , place: obj[V].place || OPT.place

                                        });
                                    }

                                    if (obj.model) {
                                        obj.centers = _.util.arr.unique(Array.prototype.concat.apply([], obj.model.map((E) => {
                                            return E.names ? E.names.map((e) => {
                                                return e.c;
                                            }) : E.name ? E.name.c : '';
                                        })))
                                    }

                                    if (marginFlg) {
                                        inpos += 10;
                                        marginFlg = false;
                                    }

                                    switch (!!V) {
                                        case isComment:
                                            inpos -= 15;
                                            break;
                                        case isBoxHeader:
                                            inpos += 42;
                                            break;
                                        case isUpdate:
                                            if (isModelChild) {
                                                inpos -= 10;
                                                isUpdateChild = false;
                                            }
                                            break;
                                        default:
                                    }

                                }

                            })


                            const narrow = (obj.textStyle || '').match(/narrow-[^ ]+/g) || [];
                            narrow.forEach((V) => {
                                const value = V.match(/\d$/) ? V.slice(-1) : '';
                                const key = V.replace(/\d|-/g, '');
                                obj[key] = value || 2;
                            })

                            return obj;
                        }
                    };

                };

                const Map = (QUERY, SET) => {
                    if (SET) {
                        _.d3.data.map = _.d3.data.map.map((V1) => {
                            return V1.map((V2) => {
                                const map = Map(QUERY);
                                const target = _.util.arr.find(map,(R) => {
                                    return V2.id == R.id;
                                });

                                if (target) {
                                    const obj = SET;
                                    const Key = (OBJ1, OBJ2) => {
                                        if (_.util.is.obj(OBJ1)) {
                                            for (let key in OBJ1) {
                                                OBJ2[key] = Key(OBJ1[key], OBJ2[key]);
                                            }
                                        } else {
                                            return OBJ1;
                                        }
                                        return OBJ2;
                                    }
                                    return Key(SET, target);
                                } else {
                                    return V2;
                                }
                            })
                        })
                    } else {
                        const flatMap = Array.prototype.concat.apply([], _.d3.data.map);

                        const keys = QUERY.key;
                        const Key = (OBJ) => {
                            if (!Array.isArray(keys)) {
                                return OBJ[keys];
                            } else if (keys.length > 0) {
                                return Key(OBJ[keys.shift()]);
                            } else {
                                return OBJ;
                            }
                        }

                        return flatMap.filter((V) => {
                            return Key(V) == QUERY.value;
                        })

                    }
                };

                //_.d3.data.map -------------------------------------------------------------------------
                _.d3.data.map = (() => {
                    let boxCount = 0;
                    let posY = 1;
                    let resultMap = [];

                    const map = _.d3.data.map = JSON.parse(JSON.stringify(_.data.map)).map((V1) => {
                        const arr = V1.filter((V2) => {
                            return V2.direction == _.d3.config.direction || !V2.direction
                        })
                        return arr;
                    }).filter((V3) => { return V3.length > 0;});


                    map.forEach((V1, I1) => {

                        let height = 0;
                        let itemPattern = 0;
                        let colPos = 0;

                        const arr = V1.map((V2, I2, A2) => {
                            return PropSetter(V2, I2, A2);
                        })
                        posY += height + (opt.box.gap.y * d3.min([4, (itemPattern + 1)]));

                        const arr2 = arr.map((V, I, A) => {

                            if (V.cid && _.d3.config.Dir()) {
                                const uid = V.cid;
                                const glist = arr.filter((R) => {
                                    return R.cid == uid;
                                });

                                V.cBox = {
                                    width: _.d3.config.w,
                                    height: 0
                                };

                                const elm1 = glist.shift();
                                const elm9 = glist.pop() || elm1;
                                const elm99 = arr[arr.length - 1];

                                V.cPosition = {
                                    t: elm1.position.t - opt.box.gap.y,
                                    l: elm1.position.l,
                                    b: elm1.position.b + opt.box.gap.y,
                                    r: elm9.position.r
                                }
                                V.cPosition.c = d3.mean([elm1.position.c, elm9.position.c]);
                                V.cPosition.ac = d3.mean([elm1.position.c, elm99.position.c]);
                                V.cPosition.m = d3.mean([V.cPosition.b - V.cPosition.t]);

                            }


                            if (V.gid) {
                                const uid = V.gid;
                                const glist = arr.filter((R) => {
                                    return R.gid == uid;
                                });

                                const elm1 = glist.shift();
                                const elm9 = glist.pop() || elm1;
                                const elm99 = arr[arr.length - 1];

                                V.gPosition = {
                                    t: elm1.position.t - opt.box.gap.y,
                                    l: elm1.position.l,
                                    b: elm1.position.b + opt.box.gap.y,
                                    r: elm9.position.r
                                }
                                V.gPosition.c = d3.mean([elm1.position.c, elm9.position.c]);
                                V.gPosition.ac = d3.mean([elm1.position.c, elm99.position.c]);
                                V.gPosition.m = d3.mean([V.gPosition.b - V.gPosition.t]);

                            }

                            // chunk => position.cc
                            V.position.cc = (() => {
                                if (V.chunks.length > 1) {
                                    return d3.mean(
                                        V.chunks.map((R) => {
                                            return Map({ key: 'id', value: R })[0].position.c;
                                        })
                                    )
                                } else {
                                    return V.position.c;
                                }
                            })();

                            // box.height
                            let height = 0;
                            arr.forEach((R) => {
                                height = height >= R.box.height ? height : R.box.height;
                            })
                            V.box.height = height;
                            V.position.b = V.position.t + height;

                            return V;
                        });

                        resultMap.push(arr2);

                        function PropSetter(ITEM, NUM, ARR) {

                            const item = ITEM;
                            const horizon = _.d3.config.Dir();
                            const link = horizon ? item.linkh || item.link : item.link || [];

                            item.ownC = item.cid ? true : false;
                            item.linkC = link.some((R1) => {
                                return Map({ key: 'id', value: R1 }).some((R2) => {
                                    return R2.cid;
                                });
                            })
                            item.ownG = item.gid ? true : false;
                            item.linkG = link.some((R1) => {
                                return Map({ key: 'id', value: R1 }).some((R2) => {
                                    return R2.gid;
                                });
                            })


                            item.num = {
                                row: I1,
                                col: NUM,
                                box: boxCount++
                            };
                            item.total = {
                                col: ARR.length,
                                box: Array.prototype.concat.apply([], map).length
                            }
                            item.box = {
                                width: opt.box.Width(ITEM),
                                height: 0,
                            }

                            item.box.c1 = item.box.width / 2;
                            d3.range(11)
                                .map((V, I) => {
                                    return I+2;
                                }).forEach((V)=>{
                                    const c0 = item.box.c1;
                                    const minimum = c0 / V;
                                    const cnum = 'c' + V;
                                    item.box[cnum] = [];
                                    for (let i = 0; i < V; i++) {
                                        item.box[cnum].push(minimum * (i *2+1));
                                    }
                                })

                            item.inbox = Inbox(Item(ITEM.item || ITEM.id), item.box);

                            if (height == 0) {
                                item.box.height = height = item.inbox.height;
                            } else if (item.inbox.height <= height) {
                                item.box.height = height;
                            } else {
                                item.box.height = height = item.inbox.height;
                            }


                            item.position = {
                                t: posY,
                                l: item.num.col == 0 ? opt.l + 1 : colPos + opt.box.gap.x,
                                b: posY + item.box.height
                            }
                            item.position.r = colPos = item.position.l + item.box.width;
                            item.position.c = (item.position.r - item.position.l) / 2 + item.position.l;
                            item.position.m = (item.position.b - item.position.t) / 2;

                            if (item.ounG || item.ownC) {
                                if (!firstGroup) {
                                    firstGroup = item.position.t;
                                }
                                lastGroup = item.position.b;
                            }

                            item.pattern = (() => {

                                const fromC = horizon ? item.ownC ? 2 : 0 : 0;
                                const toC = horizon ? item.linkC ? 1 : 0 : 0;
                                const linkC = fromC > 0 && toC > 0 ? 0 : fromC + toC;

                                const fromG = item.ownG ? 1 : 0 ;
                                const toG = item.linkG ? 1 : 0 ;
                                const linkG = fromG > 0 && toG > 0 ? 0 : fromG + toG;


                                const crossTO = link.length > 1 ? 1 : 0;
                                const crossFrom = (() => {
                                    let links = [];
                                    let chunks = [];
                                    const items = ARR.filter((S) => {
                                        links.push((horizon ? S.linkh || S.link : S.link || []));
                                        const chunkFlg = (horizon ? S.linkh || S.link : S.link || []).toString() == link.toString();
                                        if (chunkFlg) {
                                            chunks.push(S.id);
                                        }

                                        return chunkFlg;
                                    });

                                    links = Array.prototype.concat.apply([], links);

                                    item.chunks = chunks;

                                    if (horizon) {
                                        return _.util.arr.unique(links).length == 1 ? 0 : items.length > 1 ? 1 : 0
                                    } else {
                                        return items.length > 1 ? 1 : 0
                                    }
                                    return items.length > 1 ? 1 : 0;

                                })();

                                // 一時的にpatternは1以上に強制
                                return linkG + linkC + crossTO + crossFrom || 1;

                            })();
                            itemPattern = itemPattern < item.pattern ? item.pattern : itemPattern;

                            if (item.rid) {
                                const target = Map({ key: 'id', value: item.rid })[0];
                                const height = item.position.b - target.position.t;
                                const gap = (target._ridGap || 0) + item.position.b - target.position.b;

                                Map({ key: 'id', value:item.rid }, {
                                    position: {
                                        b: item.position.b
                                    },
                                    box: {
                                        height: height
                                    },
                                    _rid: item.id,
                                    _ridGap: gap
                                })

                                item.boxStyle += ' ghost';

                            }


                            return item;
                        }
                    })

                    return resultMap;

                })();
                const Line = (FROM, TO) => {

                    const gap = opt.box.gap.y;
                    const pat = FROM.pattern;
                    const ghost = /ghost/ig.test(FROM.boxStyle + FROM.inbox.boxStyle);

                    let xArrBase = [];
                    let yArrBase = [];

                    if (ghost) {
                        xArrBase.push(FROM.position.c);
                        yArrBase.push(FROM.position.t);
                    }

                    FROM.cPosition = FROM.cPosition || {};
                    TO.cPosition = TO.cPosition || {};
                    FROM.gPosition = FROM.gPosition || {};
                    TO.gPosition = TO.gPosition || {};

                    const xArr = (() => {
                        let patNum = pat;

                        xArrBase.push(FROM.position.c);
                        xArrBase.push(FROM.position.c);
                        switch (pat) {
                            //case 0:
                            //    xArrBase.push(FROM.position.c);
                            //    xArrBase.push(TO.position.c);
                            //    break;
                            case 1:
                                break;
                            case 2:
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                break;
                            case 3:
                                xArrBase.push(FROM.cPosition.c || TO.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(FROM.cPosition.c || TO.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                break;
                            case 4:
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || TO.gPosition.c || opt.c);
                                xArrBase.push(TO.cPosition.c || TO.gPosition.c || opt.c);
                                break;
                            case 5:
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || FROM.cPosition.c || TO.gPosition.c || FROM.gPosition.c || FROM.position.cc || opt.c);
                                xArrBase.push(TO.cPosition.c || TO.gPosition.c || opt.c);
                                xArrBase.push(TO.cPosition.c || TO.gPosition.c || opt.c);
                                xArrBase.push((TO.cPosition || TO.gPosition || TO.position).c);
                                xArrBase.push((TO.cPosition || TO.gPosition || TO.position).c);
                                break;
                        }
                        xArrBase.push(TO.position.c);
                        xArrBase.push(TO.position.c);

                        return xArrBase;

                    })();
                    const yArr = (() => {
                        let patNum = pat;
                        const commentFix = (FROM.inbox.comment ? 10 : 0)

                        yArrBase.push(FROM.position.b + commentFix);
                        switch (pat) {
                            //case 0:
                            //    yArrBase.push(FROM.position.b + commentFix);
                            //    yArrBase.push(TO.position.t);
                            //    break;
                            case 1:
                                break;
                            case 2:
                                yArrBase.push(FROM.position.b + gap);
                                yArrBase.push(FROM.position.b + gap);
                                break;
                            case 3:
                            case 4:
                                yArrBase.push(FROM.position.b + gap);
                                yArrBase.push(FROM.position.b + gap);
                                yArrBase.push(TO.position.t - gap * 2);
                                yArrBase.push(TO.position.t - gap * 2);
                                break;
                            case 5:
                                yArrBase.push(FROM.position.b + gap);
                                yArrBase.push(FROM.position.b + gap);
                                yArrBase.push(FROM.position.b + gap * 2);
                                yArrBase.push(FROM.position.b + gap * 2);
                                yArrBase.push(TO.position.t - gap * 2);
                                yArrBase.push(TO.position.t - gap * 2);
                                break;
                        }
                        yArrBase.push(TO.position.t - gap);
                        yArrBase.push(TO.position.t - gap);
                        yArrBase.push(TO.position.t);

                        return yArrBase;
                    })();

                    return d3.line()
                        .x((d, i) => {
                            return xArr[i];
                        })
                        .y((d, i) => {
                            return d;
                        })(yArr);
                };
                const Text = (INPUT, _OPT) => {
                    const OPT = _OPT || {};
                    const cls = 'break';
                    const type = OPT.type || _.util.type(INPUT).replace(/[\[\]\s](object)?/g, '');
                    const ClassNew = (T) => {
                        return /%new%/ig.test(T) ? ' icons-new' : '';
                    };
                    const Narrow = _.d3.config.Narrow;

                    let narrow = '';
                    let y = 0;
                    
                    switch (type) {
                        case 'naked':
                            return '<tspan class="' + ClassNew(INPUT) + '">' + opt.Escape(New(INPUT)) + '</tspan>';
                            break;
                        case 'Object':
                            y = OPT.boxHeader ? INPUT.t + opt.inbox.height.boxHeader * 0.5 : INPUT.t;

                            return '<tspan class="' + cls + ClassNew(INPUT.value) + '" x="' + (INPUT.c / (OPT.textStyle ? Narrow(OPT.narrow) : 1)) + '" y="' + y + '">' + opt.Escape(New(INPUT.value)) + '</tspan>';
                        case 'Array':
                            return INPUT.map((V, I, A) => {
                                y = OPT.boxHeader ? (I + 1) * opt.inbox.height.boxHeader - ((A.length - 1) * opt.inbox.height.boxHeader * 0.5) : V.t;

                                return '<tspan class="' + cls + ClassNew(V.value) + '" x="' + (V.c + (OPT.textStyle ? V.c * Narrow(OPT.narrow, 1) : 0)) + '" y="' + y + '">' + opt.Escape(New(V.value)) + '</tspan>';
                            }).join('');
                        case 'String':
                            return opt.Escape(New(INPUT));
                        default:
                            return '*** NODATA ***';
                    }
                };

                frame
                    .classed('horizon', (d) => {
                        return _.d3.config.Dir();
                    })

                // ROW --------------------------------------------------------------------------

                const ROWd = frame
                    .selectAll('.svg-row')
                    .data(_.d3.data.map, (d,i) => {
                        return /*i + '_' + */d[0].id + '_' + d.length;
                    })

                const ROWe = ROWd
                    .enter()
                    .append('g')
                    .classed('svg-row', true)


                ROWd.exit().remove();

                const ROW = ROWe
                    .merge(ROWd)

                // BOXES --------------------------------------------------------------------------

                const SBOXESe = ROWe
                    .append('g')
                    .classed('svg-boxes', true)
                    .classed('group', (d) => {
                        return d.some((V) => {
                            return V.cid;
                        })
                    })
                    .attr('class', function (d) {
                        const cls = d3.select(this).attr('class');
                        return 'group-' + d.map((V) => {
                            return V.cid;
                        })
                            .filter((V, I, AR) => {
                                return AR.indexOf(V) == I;
                            })
                            .join('') + ' ' + cls;
                    })

                ROW
                    .classed('group left', (d) => {
                        return d.some((V) => {
                            return V.cid;
                        })
                    })
                    .filter(function (d) {
                        return d3.select(this).classed('group')
                    })
                    .attr('class', function (d) {
                        const _this = d3.select(this);
                        const cls = _this.attr('class');
                        const boxes = _this.select('.svg-boxes')
                        const clsC = boxes.attr('class');
                        const group = clsC.match(/group-[^ ]+/);

                        return cls + ' ' + String(group);
                    })

                ROW
                    .filter(function (d, i) {
                        if (this.nextSibling) {
                            return / group/ig.test(
                                (this.nextSibling.classList || {}).value
                            );
                        } else {
                            return false;
                        }
                
                    })
                    .classed('before-group',true)

                const BOXd = ROW
                    .select('.svg-boxes')
                    .selectAll('.box')
                    .data((d) => {
                        return d;
                    }, (d) => {
                        return d.id;
                    })

                const BOXe = BOXd
                    .enter()
                    .append('g')
                    .attr('class', (d, i, arr) => {
                        return 'cid'+(d.cid||999);
                    })
                    .classed('box', true)
                    .classed('update-box', (d) => {
                        return d.inbox.update;
                    })
                    ///.attr('data-update', (d) => {
                    ///    return d.inbox.update || '';
                    ///})
                    .attr('data-update', function (d) {
                        const update = d.inbox.update;
                        if (!update) { return ;}
                        const updateArr = !_.util.is.arr(update) ? [update] : update;
                        const updates = updateArr.join(',');

                        return updates;
                    })
                    .attr('transform', (d) => {
                        return 'translate(' + (opt.w * -1) + ',' + d.position.t + ')';
                    })

                const HEADERe = BOXe
                    .filter((d) => {
                        return d.inbox.boxHeader;
                    })
                    .append('rect')
                    .classed('box-header', true)

                const HEADERTe = BOXe
                    .filter((d) => {
                        return d.inbox.boxHeader;
                    })
                    .append('text')
                    .classed('box-header-text', true)

                const RECTe = BOXe
                    .append('rect')
                    .classed('box-wrapper', true)

                BOXe
                    .append('text')
                    .classed('item', true)

                BOXe
                    .filter((d) => {
                        return d.inbox.icon;
                    })
                    .append('use')
                    .classed('icon', true)
                    .classed('icon-url', (d) => {
                        return d.inbox.iconUrl;
                    })
                    .attr('width', 120)
                    .attr('height', 80)
                    .attr('viewbox', '0 0 80 80')

                BOXe
                    .filter((d) => {
                        return d.inbox.iconText;
                    })
                    .append('text')
                    .classed('icon-text', true)

                BOXd
                    .exit()
                    .remove();

                const BOX = BOXe
                    .merge(BOXd)

                BOX
                    .classed('ghost', (d) => {
                        return /ghost/ig.test(d.boxStyle + d.inbox.boxStyle);
                    })
                    .classed('single', (d) => {
                        return /single|dotted/ig.test(d.boxStyle + d.inbox.boxStyle);
                    })
                
                const HEADER = BOX
                    .select('.box-header')
                    .attr('width', (d) => {
                        return d.box.width;
                    })
                    .attr('height', (d) => {
                        return 50;
                    })

                const HEADERT = BOX
                    .select('.box-header-text')
                    .classed('narrow', (d) => {
                        return !!d.inbox.narrowboxHeader;
                    })
                    .attr('data-narrow', (d) => {
                        return d.inbox.narrowboxHeader || '';
                    })
                    .attr('x', (d) => {
                        return d.box.c1;
                    })
                    .html(function (d) {
                        return Text(d.inbox.boxHeader, {
                            boxHeader: true,
                            textStyle: !!d.inbox.narrowboxHeader,
                            narrow: d.inbox.narrowboxHeader
                        })
                    })

                // BOX --------------------------------------------------------------------------

                const RECT = BOX
                    .select('.box-wrapper')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width', (d) => {
                        return d.box.width;
                    })
                    .attr('height', (d) => {
                        return d.box.height;
                    })

                BOX
                    .select('.icon')
                    .attr('x', (d, i, a) => {
                        return d.inbox.icon.c - 60;
                    })
                    .attr('y', (d, i, a) => {
                        return d.inbox.icon.t
                    })
                    .attr('xlink:href', (d) => {
                        return '#' + d.inbox.icon.value;
                    })
                    .filter((d) => {
                        return d.inbox.iconUrl;
                    })
                    .select(function (d) {
                        const use = this;
                        use.__data__ = d;
                        const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
                        g.setAttribute('class', 'icon-url-g');
                        g.appendChild(use.cloneNode(true));
                        return this.parentNode.insertBefore(g, this);
                    })
                    .on('click', (d) => {
                        location.assign(d.inbox.iconUrl);
                    })
                    .insert('rect', 'use')
                    .classed('icon-url-rect', true)
                    .attr('fill', 'transparent')
                    .attr('fill-opacity', 0)
                    .attr('x', (d) => {
                        return d.inbox.icon.c - 60;
                    })
                    .attr('y', (d) => {
                        return d.inbox.icon.t;
                    })
                    .attr('width', 120)
                    .attr('height', 70)

                BOX
                    .selectAll('.box > .icon-url')
                    .remove()

                BOX
                    .select('.icon-text')
                    .attr('x', (d, i, a) => {
                        return d.box.c1;
                    })
                    .attr('y', (d, i, a) => {
                        return (d.inbox.iconText.t || d.inbox.iconText[0].t);
                    })
                    .html((d) => {
                        return Text(d.inbox.iconText);
                    })


                // MODEL --------------------------------------------------------------------------

                const MODELd = BOX
                    .filter((d) => {
                        return d.inbox.model;
                    })
                    //.append('g')
                    //.classed('models',true)
                    .selectAll('.model')
                    .data((d) => {
                        let tmp = [];
                        const result = d.inbox.model.map((V) => {

                            (V.model || []).forEach((R) => {
                                tmp.push(_.util.obj.assign(R, { _d: d.box }));
                            });

                            return _.util.obj.assign(V, { _d: d.box })
                        });
                        result.push(...tmp);
                        return result;
                    })

                const MODELe = MODELd
                    .enter()
                    .append('g')
                    .classed('model', true)

                MODELe
                    .filter((d) => {
                        return /add|alt|single|grey|blue|dotted/ig.test(d.boxStyle);
                    })
                    .append('rect')
                    .classed('model-box', true)
                    .classed('add', (d) => {
                        return /add|blue/ig.test(d.boxStyle);
                    })
                    .classed('alt', (d) => {
                        return /alt|grey/ig.test(d.boxStyle);
                    })
                    .classed('single', (d) => {
                        return /single|dotted/ig.test(d.boxStyle);
                    })


                const MODEL_HEADERe = MODELe
                    .filter((d) => {
                        return d.header;
                    })
                    .append('text')
                    .classed('model-header', true)

                const MODEL_NAMEe = MODELe
                    .filter((d) => {
                        return d.name;
                    })
                    .append('text')
                    .classed('model-name', true)
                    //.classed('update-name', (d) => {
                    //    return !!d.update
                    //})
                    //.attr('class', function (d) {
                    //    const cls = d3.select(this).attr('class');
                    //    const updateArr = d.name.value.match(/%update[^%]+%/gi);
                    //    if (!updateArr) { return cls; }
                    //    const updates = updateArr.map((V) => {
                    //        return ' update-name' + V.replace(/%|update:/gim, '');
                    //    }).join('');
                    //
                    //    return cls + updates;
                    //
                    //})


                const MODEL_CAP = MODELe
                    .filter((d) => {
                        return d.cap;
                    })
                    .append('text')
                    .classed('cap', true)

                const MODEL = BOX
                    .selectAll('.model')

                MODEL
                    .select('.model-box')
                    .attr('x', (d) => {
                        if (!d.centers) {
                            const centers = d._d['c' + d.modelCols];
                            const center = _.util.is.arr(centers) ? centers[0] : centers;
                            const width = center * 2;
                            return width * d.modelCol + 10;
                        } else if (d.centers.length <= 1) {
                            if (d.modelCols > 1) {
                                const boxWidth = d._d.width;
                                const cellWidth = boxWidth / d.modelCols;
                                const row = d.modelCol % d.modelCols;
                                const x = cellWidth * row + 10;

                                return x;
                            } else {
                                return 10;
                            }
                        } else {
                            const gap = Math.abs(d.centers[1] - d.centers[0]) / 2 - 10;
                            return d3.min(d.centers) - gap;
                        }
                    })
                    .attr('y', (d) => {
                        return d.t;
                    })
                    .attr('width', (d) => {
                        if (!d.centers) {
                            const centers = d._d['c' + d.modelCols];
                            const center = _.util.is.arr(centers) ? centers[0] : centers;
                            const width = center * 2;
                            return width - 20;
                        } else if (d.centers.length <= 1) {
                            if (d.modelCols > 1) {

                                const boxWidth = d._d.width;
                                const cellWidth = boxWidth / d.modelCols -20;

                                return cellWidth;

                            } else {
                                return d._d.width - 20;
                            }
                        } else {
                            const gap = Math.abs(d.centers[1] - d.centers[0]) / 2 - 10;
                            return d3.max(d.centers) - d3.min(d.centers) + gap *2;
                        }
                        
                    })
                    .attr('height', (d) => {
                        return d.height;
                    })

                MODEL
                    .select('.model-header')
                    .classed('narrow', (d) => {
                        return !!d.narrowheader;
                    })
                    .attr('data-narrow', (d) => {
                        return d.narrowheader || '';
                    })
                    .attr('x', (d, i, a) => {
                        return d.header.c;
                    })
                    .attr('y', (d, i, a) => {
                        return d.header.t;
                    })
                    .html(function (d) {
                        return Text(d.header, {
                            textStyle: !!d.narrowheader,
                            narrow: d.narrowheader || ''
                        });
                    })

                MODEL
                    .select('.model-name')
                    .classed('narrow', (d) => {
                        return !!d.narrowname;
                    })
                    .attr('data-narrow', (d) => {
                        return d.narrowname || '';
                    })
                    .classed('no-link', (d) => {
                        return /no-link/ig.test(d.textStyle) || _.data.get.noLink(d.name);
                    })
                    .classed('update-name', (d) => {
                        return IsUpdate(d.name.value);
                    })
                    // updateの複数設定
                    //.attr('data-update', (d) => {
                    //    if (IsUpdate(d.name.value)) {
                    //        return UpdateNum(d.name.value);
                    //    }
                    //})
                    .attr('data-update', function (d) {
                        const updateArr = d.name.value.match(/%update[^%]+%/gi);
                        if (!updateArr) { return ;}
                        const updates = updateArr.map((V) => {
                            return V.replace(/%|update:/gim,'');
                        }).join(',');

                        return updates;
                    })
                    .attr('x', function(d, i, a){
                        return (d.name.c || d.name[0].c);
                    })
                    .attr('y', (d, i, a) => {
                        return (d.name.t||d.name[0].t);
                    })
                    .html(function(d){
                        const symbol = d.symbol ? NEW : '';
                        return Text(d.name, {
                            textStyle: !!d.narrowname,
                            narrow: d.narrowname || ''
                        }) + symbol;
                    })

                MODEL
                    .filter((d) => {
                        return d.names;
                    })
                    .selectAll('.model-names')
                    .data((d) => {
                        return d.names;
                    })
                    .enter()
                    .append('text')
                    .classed('model-names', true)

                MODEL
                    .selectAll('.model-names')
                    .classed('narrow', (d) => {
                        return !!d.narrowname;
                    })
                    .attr('data-narrow', (d) => {
                        return d.narrowname || '';
                    })
                    .classed('no-link', (d) => {
                        return /no-link/ig.test(d.textStyle) || _.data.get.noLink(d.value);
                    })
                    .classed('update-name', (d) => {
                        return IsUpdate(d.value);
                    })
                    .attr('data-update', function (d) {
                        const updateArr = d.value.match(/%update[^%]+%/gi);
                        if (!updateArr) { return ; }
                        const updates = updateArr.map((V) => {
                            return V.replace(/%|update:/gim, '');
                        }).join(',');

                        return updates;

                    })
                    //.classed('icons-new', (d) => {
                    //    return /%new%/ig.test(d.value);
                    //})
                    .attr('x', (d, i, a) => {
                        return d.c / (!!d.narrowname? _.d3.config.Narrow(d.narrowname): 1);
                    })
                    .attr('y', (d, i, a) => {
                        return d.t;
                    })
                    .html((d) => {
                        return Text(d.value, {type:'naked'});
                    })

                MODEL
                    .select('.cap')
                    .classed('narrow', (d) => {
                        return !!d.narrowcap;
                    })
                    .attr('data-narrow', (d) => {
                        return d.narrowcap || '';
                    })
                    .attr('x', (d, i, a) => {
                        return d._d.c1;
                    })
                    .attr('y', (d, i, a) => {
                        return (d.cap[_.config.lang].t || d.cap[_.config.lang][0].t);
                    })
                    .html(function(d){
                        return Text(d.cap[_.config.lang], {
                            textStyle: !!d.narrowcap,
                            narrow: d.narrowcap|| ''
                        })
                    })


                // comment -------------------------------------

                const COMMENT = BOXe
                    .filter((d) => {
                        return d.inbox.comment;
                    })
                    .append('g')
                    .classed('comment-group', true)

                COMMENT
                    .append('rect')
                    .classed('comment-wrapper', true)

                COMMENT
                    .append('text')
                    .classed('comment', true)


                BOX
                    .select('.comment-group')
                    .attr('transform', (d) => {
                        const h = (d.inbox.comment.length || 1) * opt.inbox.height.comment;
                        return 'translate(0,' + (d.box.height - h) + ')'
                    })

                BOX
                    .select('.comment-wrapper')
                    .attr('x', (d, i, a) => {
                        return 20;
                    })
                    .attr('y', (d, i, a) => {
                        //return (d.inbox.comment[0]||d.inbox.comment).t - 5;
                    })
                    .attr('width', (d) => {
                        return d.box.width - 40;
                    })
                    .attr('height', (d) => {
                        const h = (d.inbox.comment.length || 1) * opt.inbox.height.comment + 10;
                        return h;
                    })

                BOX
                    .select('.comment')
                    .attr('x', (d, i, a) => {
                        return d.box.c1;
                    })
                    .html((d) => {
                        return Text(d.inbox.comment);
                    })


                d3.selectAll('.model-name,.model-names')
                    .on('click', function (d) {
                        _.d3.action.table(d);
                    })


                BoxUpdate();
                function BoxUpdate(DUR) {
                    BOX
                        .transition()
                        .duration(_.d3.config.animation.duration)
                        .ease(_.d3.config.animation.ease)
                        .attr('transform', (d) => {
                            return 'translate(' + d.position.l + ',' + d.position.t + ')';
                        })
                }

                // LINES --------------------------------------------------------------------------

                const SLINESe = ROWe
                    .append('g')
                    .classed('svg-lines', true)

                const SLINES = ROW
                    .selectAll('.svg-lines')

                const LINESd = ROW
                    .select('.svg-lines')
                    .selectAll('.lines')
                    .data((d) => {
                        return d
                    }, (d,i) => {
                        return i + '_' + d.id;
                    })

                const LINESe = LINESd
                    .enter()
                    .append('g')
                    .classed('lines', true)

                LINESd
                    .exit()
                    .remove();

                const LINES = LINESe
                    .merge(LINESd)

                const LINEd = LINES
                    .selectAll('.line')
                    .data((d) => {
                        const arr = (_.d3.config.Dir() ? d.linkh || d.link : d.link || []);
                        const listStyle = d.linkStyle || [];

                        return arr.map((V,I) => {
                            const to = Map({ key: 'id', value: opt.Escape(V) })[0];
                            const style = opt.Pers(V).join('');

                            return {
                                from: d,
                                to: to,
                                pattern: d.pattern,
                                style: style
                            }
                        })
                    }, (d) => {
                        return 'line' + d.from.id + '_' + d.to.id;
                    })

                const LINEe = LINEd
                    .enter()
                    .append('path')
                    .attr('class', (d) => {
                        return 'line' + d.from.id + '_' + d.to.id;
                    })
                    .classed('line', true)
                    .classed('dash', (d) => {
                        return /dash/.test(d.style);
                    })

                LINEd
                    .exit()
                    .remove();


                SLINES
                    .selectAll('.line')
                    .exit()
                    .remove();

                const LINE = LINEe
                    .merge(LINEd);

                LineUpdate();
                function LineUpdate() {
                    LINE
                        .transition()
                        .duration(_.d3.config.animation.duration)
                        .ease(_.d3.config.animation.ease)
                        .attr('d', (d) => {
                            return Line(d.from, d.to)
                        })
                }

                // dominant-baseline非対応ブラウザ対応
                d3.selectAll('tspan:not(.new)').attr('dy', '0.9em')

                _.d3.config.h = _.d3.data.map[_.d3.data.map.length - 1][0].position.b + 1

                // 4/8切り換えなしのため一旦除外
                //if (_.d3.config.Dir() && _.d3.config.grouping) {
                //    d3.selectAll('.svg-row.group')
                //        .call(_.d3.action.drag(BoxUpdate, LineUpdate))
                //} else {
                //    d3.selectAll('.svg-row.group')
                //        .call(_.d3.action.dragNo())
                //}


                // ドラッグ
                //*       if (!_.config.touchDevice) {
                //*           d3.selectAll('.svg-frame')
                //*               .call(_.d3.action.drag2());
                //*       }


                _.action.zoom();
                back.attr('height', _.d3.config.h)
                back.attr('width', _.d3.config.w)//_.d3.config.W / d3.min([1, _.d3.config.Scale()]))

                d3.selectAll('.scrollable-rect').attr('width', _.d3.config.W / d3.min([1, _.d3.config.Scale()]))

                // 不要symbol削除
                d3.select('#shimano-marks')
                    .selectAll('symbol')
                    .filter(function () {
                        const id = this.id;
                        return (
                            id !== ''
                            && /s\d{3}/gim.test(id)
                            && _.data.iconIds.indexOf(id) < 0
                        );
                    })
                    .remove()
                    ;

                //_.d3.action.update();
            }
        },
        action: {
            data: {
                drag: {
                    start: {},
                    end: {},
                    top: 0
                }
            },
            newIcon: () => {
                const news = d3.selectAll('.icons-new');
                news.each((d, I, A) => {
                    const e = A[I];
                    const p = $(e).closest('.model').get(0);
                    const t = $(e).closest('text').get(0);

                    const de = d3.select(e);
                    const dp = d3.select(p);
                    const dt = d3.select(t);

                    const len = de.text().length;
                    const ext = e.getExtentOfChar(len - 1);

                    const nr = /narrow/.test(dt.attr('class'));
                    const dnr = dt.attr('data-narrow');
                    const snr = _.d3.config.Narrow(dnr);
                    const dx = +dt.attr('dx');

                    //const x = ~~(ext.width + ext.x + 7 - (nr ? dx + fullwidth * (1-snr)/2: 0));
                    const x = ~~((ext.width + ext.x) * (nr ? snr : 1) + 7) ;
                    const y = ~~(ext.y + 3);

                    _.d3.config.inbox.symbol.New(dp, x, y);

                    //de.attr('dx', '-9px');
                })
            },
            //iconUrl: () => {
            //    const iconUrls = d3.selectAll('.icon-url-g');
            //    iconUrls.each((d, I, A) => {
            //        const rect = A[I].querySelector('rect');
            //        const use = A[I].querySelector('use');
            //        const parent = A[I].parentNode;
            //
            //        //const uPos = use.getBoundingClientRect();
            //        //const pPos = parent.getBoundingClientRect();
            //
            //        const width = 120;//Math.round(uPos.width) + 1;
            //        const height = 70;//uPos.height;
            //        const x = use.getAttribute('x');//d.inbox.icon.c - (width / 2);
            //        const y = use.getAttribute('y');//uPos.y - pPos.y;
            //
            //        rect.setAttribute('width', width);
            //        rect.setAttribute('height', height);
            //        rect.setAttribute('x', x);
            //        rect.setAttribute('y', y);
            //
            //        rect.setAttribute('fill','#c00')
            //    })
            //},
            update: () => {
                if (_.config.user) {
                    return;
                }
                const Triangle = (D) => {
                    const w = 11;
                    const c = D.c;
                    const t = D.t - 7;
                    const xArr = [c, c + w, c - w, c];
                    const yArr = [t, t + (w * 1.7), t + (w * 1.7), t];

                    return d3.line()
                        .x((d, i) => {
                            return xArr[i];
                        })
                        .y((d, i) => {
                            return d;
                        })(yArr);
                };

                const boxes = d3.selectAll('.update-box');
                const names = d3.selectAll('.update-name');


                // boxes ---------------------------
                boxes.each((d, I, A) => {
                    const e = A[I];
                    const de = d3.select(e);

                    const X = (N) => { return 15 + N * 25;};
                    const y = 12 + (d.inbox.boxHeader ? 50: 0);
                    const deUpdates = de.attr('data-update').split(',');

                    deUpdates.forEach((deUpdate,i) => {

                        const UPDATE = de
                            .append('g')
                            .classed('update', true)

                        UPDATE
                            .append('text')
                            .classed('update-text', true)
                            .attr('x', X(i))
                            .attr('y', y)
                            .html((d) => {
                                return '<tspan dy="0.9em">' + deUpdate + '</tspan>';
                            })

                        UPDATE
                            .append('path')
                            .classed('update-path', true)
                            .attr('d', (d) => {
                                return Triangle({
                                    t: y, c: X(i)
                                })
                            })
                    })
                })

                // names ---------------------------
                names.each((d, I, A) => {
                    const e = A[I];
                    const p = e.parentNode;
                    const de = d3.select(e);
                    const dp = d3.select(p);

                    const nr = de.classed('narrow');
                    const snr = _.d3.config.Narrow(de.attr('data-narrow'));
                    const narrowRev = _.d3.config.Narrow(de.attr('data-narrow'), 1);

                    const len = de.text().length;
                    const ext = e.getExtentOfChar(0);

                    const width = e.getSubStringLength(0, len) * (nr ? snr : 1) + 6;
                    const height = ext.height;
                    const top = ext.y;

                    //const dx = +de.attr('dx') - width/2 * (isNarrow ? narrowRev : 1);
                    //const left = ext.x - 3 - (isNarrow ? dx : 0);
                    const left = ext.x * (nr? snr: 1) -3 ;

                    const DeX = (N) => { return left + 12 + (N * 25); };//+de.attr('x');
                    const deY = +de.attr('y') - 15;
                    const deUpdates = de.attr('data-update').split(',');

                    deUpdates.forEach((deUpdate, i) => {
                        const updateClass = 'v' + deUpdate;
                        const UPDATE = dp
                            .append('g')
                            .classed('update', true)
                            .lower()

                        UPDATE
                            .append('text')
                            .classed('update-text', true)
                            .attr('x', DeX(i))
                            .attr('y', deY)
                            .html((d) => {
                                return '<tspan dy="0.9em">' + deUpdate + '</tspan>';
                            })

                        UPDATE
                            .append('path')
                            .classed('update-path', true)
                            .attr('d', (d) => {
                                return Triangle({
                                    t: deY, c: DeX(i)
                                })
                            })

                        UPDATE
                            .append('rect', '.update-name')
                            .classed('update-rect', true)
                            .classed(updateClass, true)
                            .attr('x', left)
                            .attr('y', top)
                            .attr('width', width)
                            .attr('height', height)
                    })


                })
                d3.selectAll('.model-box').lower();
            },
            drag: (FUNC1,FUNC2) => {

                let scale = 1;
                let _this = '';
                let grouper = '';
                let target = '';
                let _trans0 = 0;
                let trans0 = 0;

                let originalMap = {};

                return d3.drag()
                    .on('start', function () {

                        scale = _.d3.config.Scale();
                        _this = d3.select(this);
                        grouper = _this.attr('class').match(/group-[^ ]+/g)[0];
                        target = d3.selectAll('.' + grouper);

                        _trans0 = _.d3.action.get.transform(_this.attr('transform')).translate[0];
                        trans0 = Math.abs(_trans0);

                        d3.select('.svg-frame').classed("dragging", true);

                        _.d3.action.data.drag.start = d3.event.sourceEvent;
                        _.d3.action.data.drag.top = $('#contents').scrollTop()

                    })
                    .on('drag', function () {
                        const start = _.d3.action.data.drag.start;
                        const d3se = d3.event.sourceEvent;

                        const position = (_this.attr('transform') || '0,0').replace(/translate|[()]/g, '').split(',');
                        const dx = d3.event.dx;
                        const dy = d3.event.dy;
                        const DY = d3se.pageY - start.pageY;

                        const _dx = (start.pageX || start.changedTouches[0].pageX) - (d3se.pageX || d3se.changedTouches[0].pageX);

                        _.d3.action.position(target, dx);

                        FUNC1();
                        FUNC2();

                    })
                    .on('end', function () {
                        const d3se = _.d3.action.data.drag.end = d3.event.sourceEvent;

                        const start = _.d3.action.data.drag.start;
                        const _dx = (start.pageX || start.changedTouches[0].pageX) - (d3se.pageX || d3se.changedTouches[0].pageX);
                        const dx = Math.abs(_dx);
                        const direction = _dx < 0 ? 'left' : 'right';

                        const gap = _.d3.config.W + _.d3.config.box.gap.x - 2;
                        const x = [0, gap]
                        let left = target.classed('left');
                        let sideFlg = 0;
                        let moveFlg = dx <= 0? 0: 1;


                        if (!moveFlg) {
                            slide();
                        } else if (!left && direction == 'left') {
                            sideFlg = 0;
                            slide();
                        } else if (left && direction == 'right') {
                            sideFlg = 1;
                            slide();
                        } else if (left && direction == 'left') {
                            sideFlg = 0;
                            moveFlg = 0;
                            slide();
                        } else if (!left && direction == 'right') {
                            sideFlg = 1;
                            moveFlg = 0;
                            slide();
                        }

                        function slide() {
                            target
                                .transition()
                                .duration(_.d3.config.animation.duration)
                                .ease(_.d3.config.animation.ease)
                                .attr('transform', 'translate(0,0)');

                            if (moveFlg) {
                                target.classed('left', (d) => {
                                    return sideFlg ? false : true;
                                })
                            }

                            _.d3.action.position(target, gap * moveFlg * (sideFlg ? -1 : 1) + _dx);
                            FUNC1();
                            FUNC2();

                            d3.select('.svg-frame').classed("dragging", false);
                        }


                    })
            },
            dragNo: () => {
                return d3.drag()
            },
            drag2: () => { // 4/8
                
                let x0, y0,trans0,scroll0;
                const target = d3.select('.svg-frame');

                return d3.drag()
                    .on('start', function () {
                        x0 = d3.event.sourceEvent.pageX;
                        y0 = d3.event.sourceEvent.pageY;

                        trans0 = _.d3.action.get.transform(target.attr('transform'));
                        scroll0 = _.action.scroll();

                        target.classed('dragging',true)
                    })
                    .on('drag', function () {

                        const x = d3.event.sourceEvent.pageX
                        const y = d3.event.sourceEvent.pageY
                        const dx = (x - x0);
                        const dy = (y - y0)*-1;

                        const trans = trans0.translate[0] + dx;

                        target.attr('transform', () => {
                            return 'translate(' + trans + ',0) scale(' + trans0.scale + ')';
                        })

                        _.action.scroll(scroll0 + dy);

                    })
                    .on('end', function () {
                        target.classed('dragging',false)
                    })

            },
            position: (NODES, DX) => {

                //_.d3.action.position(this.__data__[0].num.row, dx);
                let rows = [];
                NODES.each((V1) => {
                    V1.forEach((V2) => {
                        if (rows.indexOf(V2.num.row) < 0) {
                            rows.push(V2.num.row);
                        }
                    })
                })

                rows.forEach((row) => {
                    _.d3.data.map[row] = _.d3.data.map[row].map((V) => {
                        V.position.c += DX;
                        V.position.cc += DX;
                        V.position.l += DX;
                        V.position.r += DX;
                        V.cPosition.ac += DX;
                        V.cPosition.c += DX;
                        V.cPosition.l += DX;
                        V.cPosition.r += DX;
                        return V;
                    })
                })
            },
            height: () => {
                const height = _.d3.config.h * _.d3.config.Scale() + 1;
                d3.select('#svg').attr('height', height);

                if (!!_.config.touchDevice) {
                    const width = _.d3.config.w * _.d3.config.Scale() + 1;
                    d3.select('#svg').attr('width', width);
                }

                return;
            },
            get: {
                transform: (TRANS) => {
                    const trans = TRANS || 'translate(0,0) scale(0)'
                    const _tl = trans.match(/translate\([-\d., ]+\)/) || 'translate(0,0)';
                    const _sc = trans.match(/scale\([-\d., ]+\)/) || 'scale(0)';

                    const tl = String(_tl).match(/[-\d.]+/g).map((V) => { return Number(V);});
                    const sc = Number(String(_sc).match(/[-\d.]+/g)[0]);

                    return {
                        translate: tl,
                        scale: sc
                    }
                }
            },
            refresh: () => {
                //d3.select('#svg').remove();
                _.d3.create.chart();

            },
            table: (CODE) => {
                if (!/no-link/ig.test(CODE.style)) {
                    const str = (
                        CODE.value
                        || CODE.name.value
                        || CODE.name.map((V) => {
                            return V.value;
                        }).join('')
                    ).replace(/%[^%]+?%/g, '');
                    alert('テーブル表示 => ' + str);
                }
            },
            toggle: {
                break: () => {
                    const br = d3.selectAll('.break');
                    if (br.classed('no')) {
                        br.each(function (d) {
                            const _this = d3.select(this);
                            const x = _this.attr('data-break-x');
                            const y = _this.attr('data-break-y');
                            _this.attr('x', x)
                            _this.attr('y', y)
                            _this.classed('no', false);
                        })
                    } else {
                        br.each(function (d) {
                            const _this = d3.select(this);
                            const x = _this.attr('x');
                            const y = _this.attr('y');
                            _this.attr('data-break-x', x)
                            _this.attr('data-break-y', y)
                            _this.attr('x', null).attr('y', null);
                            _this.classed('no', true);
                        })
                    }
                }
            }
        },
        style: () => {
            const styles = [
                'display',
                'dominant-baseline',
                'fill',
                'fill-opacity',
                'font-family',
                'font-size',
                'font-style',
                'font-weight',
                'stroke',
                'stroke-linejoin',
                'stroke-dasharray',
                'stroke-linecap',
                'stroke-width',
                'text-anchor',
                'opacity',
                'transform'
            ];
            const rejList = ['auto', 'normal','none'];
            const $svg = $('#svg');
            const $all = $svg.find('g,line,rect,text,path,tspan');

            $all.each((I, V) => {
                const style = $(V).css(styles);
                for (let key in style) {
                    if (rejList.indexOf(style[key]) >= 0) {
                        delete style[key];
                    }
                }

                $(V).attr(style)
            })

        }
    }

    // *************************************************************
    // _.action
    // *************************************************************
    _.action = {
        init: () => {
            _.action.add.download();
            _.action.add.style();

            _.action.add.sidemenu.toggle();
            _.action.add.sidemenu.link();
            //_.action.add.switch();

            _.action.add.zoom.plus();
            _.action.add.zoom.minus();

        },
        search: (STR) => {

            d3.selectAll('.search-back').remove();

            const str = (STR || $('#search-text').val()).replace(/^([a-z.B]{2})/gim,'$1-?').replace(/([\\\/\(\)"'\[\]])/g, '\\$1') || '';
            const reg = new RegExp('(' + str + ')', 'gim');
            const tspans = d3.selectAll('tspan');

            if (!str) {
                tspans.each((d, I, A) => {
                    const tspan = A[I];
                    const unwrap = String(d3.select(tspan).html());
                    const unwarp2 = unwrap.replace(/<tspan[^<>]+search[^<>]+>([^<>]+)<\/tspan>/gim, '$1')
                    d3.select(tspan).html(unwarp2);
                })
                return false;
            }

            tspans.each((d, I, A) => {
                const tspan = A[I];

                const unwrap = String(d3.select(tspan).html());
                const unwarp2 = unwrap.replace(/<tspan[^<>]+search[^<>]+>([^<>]+)<\/tspan>/gim, '$1')
                d3.select(tspan).html(unwarp2);


                const inner = tspan.innerHTML;
                const nest = inner.match(/<tspan[^<>]+>[^<>]+<\/tspan>/) || [];

                let innerR = inner;
                nest.map((V, I) => {
                    innerR = innerR.replace(V, '@@' + I + '@@');
                })

                if (reg.test(innerR)) {
                    tspan.innerHTML = innerR
                        .replace(reg, '<tspan class="search">$1</tspan>')
                        .replace(/@@(\d+)@@/g, (N) => {
                            return nest[Number(RegExp.$1)];
                        });
                }
            })

            const frame = d3.select('.search-frame');
            frame.selectAll('*').remove();

            const fPos = frame._groups[0][0].getBoundingClientRect();

            const search = d3.selectAll('tspan.search');
            search.each((d, I, A) => {

                const e = A[I];
                const p = $(e).closest('text').get(0);
                const de = d3.select(e);
                const dp = d3.select(p);
                const len = str.length;
                const ext = e.getExtentOfChar(0);

                const nr = dp.classed('narrow');
                const dx = dp.attr('dx') || 0;
                const snr = _.d3.config.Narrow(dp.attr('data-narrow'));

                const width = e.getComputedTextLength() * (nr ? snr : 1);
                //const width = e.getSubStringLength(0, len);
                const height = ext.height;

                const top = ext.y;
                //const left = ext.x - (nr ? dx - width * (1 - snr)*2:0);
                const left = ext.x * (nr ? snr:1);

                const target = $(e).closest('text').get(0).parentNode;

                d3.select(target)
                    .append('rect')
                    .lower()
                    .classed('search-back', true)
                    .attr('x', left)
                    .attr('y', top)
                    .attr('width', width)
                    .attr('height', height)

                d3.selectAll('.model-box')
                    .lower()
            })

        },
        scroll: (PX) => {
            if (!PX) {
                return $('#d3-target').scrollTop();
            } else {
                $('#d3-target').scrollTop((PX));
            }
        },
        scrollLeft: (PX) => {
            if (!PX) {
                return $('#d3-target').scrollLeft();
            } else {
                $('#d3-target').scrollLeft((PX));
            }
        },
        add: {
            download: () => {
                $D.on('click', '#downloader .svg', () => {
                    _.img.convert.svg();
                })
                $D.on('click', '#downloader .pdf', () => {
                    _.img.convert.pdf();
                })
                $D.on('click', '#downloader .png', () => {
                    _.img.convert.png();
                })
            },
            style: () => {
                $D.on('click', '#download .style', () => {
                    _.d3.style();
                })
            },
            sidemenu: {
                toggle: () => {
                    console.log('toggle')
                    $('.sidebar__menu__btn').off('click.sidemenu').on('click.sidemenu', () => {
                        $M.toggleClass('noside');
                        //$SW.toggleClass('hidden');

                        if (_.d3.config.space == 'n') {

                            _.d3.config.space ='w';
                            _.d3.config.scale = 5;
                        } else {
                            _.d3.config.space = 'n';
                            _.d3.config.scale = 4;

                        }

                        _.d3.init();
                    });
                },
                link: () => {
                    $S.find('li.link').on('click', function () {
                        const _this = $(this);
                        const num = _this.data('num');
                        _.data.init(num);

                        $S.find('*').removeClass('active')
                        _this.addClass('active')
                        _this.find('li').addClass('active')

                        return false;
                    })
                }
            },
            zoom: {
                plus: () => {
                    $D.on('click', '#scale .plus', function () {
                        _.action.zoom(1);
                    })
                },
                minus: () => {
                    $D.on('click', '#scale .minus', function () {
                        _.action.zoom(-1);
                    })
                },
            }
        },
        zoom: (_UPDOWN) => {
            const UPDOWN = _UPDOWN || 0;
            
            const frame = d3.select('.svg-frame');
            const $target = $('#d3-target');
            const scale0 = _.d3.config.Scale();
            const scale1 = _.d3.config.Scale(UPDOWN);
            const scalee = scale1 / scale0;
            const trans = !!_.config.touchDevice? 0: (_.d3.config.W - _.d3.config.w * _.d3.config.Scale()) / 2;

            frame.attr('transform', 'translate(' + trans + ',0) scale(' + scale1 + ')');
            _.d3.action.height();

            if (scalee != 1) {
                _.d3.action.refresh();

                const half = $target.height() / 2;
                const scroll0 = $target.scrollTop();
                const point0 = scroll0 + half;
                const point1 = point0 * scalee;
                const scroll1 = point1 - half;

                $target.scrollTop(scroll1);

                if (!!_.config.touchDevice) {
                    const sl = Math.max(0, (_.d3.config.W - _.d3.config.w * _.d3.config.Scale()) / -2);
                    _.action.scrollLeft(sl);
                }
            }



        }
    }

    // *************************************************************
    // _.img
    // *************************************************************
    _.img = {
        init: () => {
        },
        convert: {
            svg: () => {
                _.d3.style();

                const doctype = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
                const prefix = {
                    xmlns: 'http://www.w3.org/2000/xmlns/',
                    xlink: 'http://www.w3.org/1999/xlink',
                    svg: 'http://www.w3.org/2000/svg'
                };

                const filename = 'shimano-chart_' + _.util.date('YYMMDD') + '.svg';

                const svg = $('#svg')[0];
                const svgData = new XMLSerializer().serializeToString(svg);

                const blobObject = new Blob([doctype + svgData], { 'type': 'text\/xml' })

                if (navigator.appVersion.toString().indexOf('.NET') > 0) { //IE hack
                    window.navigator.msSaveBlob(blobObject, filename)

                } else {
                    const url = window.URL.createObjectURL(blobObject)
                    const a = d3.select('body').append('a')

                    a.attr('class', 'downloadLink')
                        .attr('download', filename)
                        .attr('href', url)
                        .style('display', 'none')

                    a.node().click()

                    setTimeout(function () {
                        window.URL.revokeObjectURL(url)
                        a.remove()
                    }, 10)
                }

            }
        }
    }

    // *************************************************************
    // window._
    // *************************************************************
    window._ = _;
})();

$(() => {
    _.init();
    console.log(_)
});