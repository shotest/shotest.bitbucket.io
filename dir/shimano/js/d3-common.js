﻿'use strict';

(() => {

    let _W, _D, _B, _H, _M;
    let $W, $D, $B, $H, $T;

    const _ = window._ || {};

    // *************************************************************
    // _.init
    // *************************************************************
    _.init = () => {


        _.const.init();
        _.util.init();
        _.$.init();

        //if (_.util.ss.get('dir') !== _.const.dir) {
        //    location.assign('../');
        //}

        _.d3.init();
        _.action.init();
        _.img.init();

    }

    // *************************************************************
    // _.const
    // *************************************************************
    _.const = {
        init: () => {
            const path = location.pathname.split('/')
            _.const.dir = path[1]
        },
        path: {
            json: {
                xtr: 'build/data/xtr.json'
            }
        },
        color: {
            trans: 'transparent',
            blue: '#22b4f1',
            dblue: '#049',
            grey: '#eee',
        },
        dir: ''
    }

    // *************************************************************
    // _.util
    // *************************************************************
   _.util = {
        init: () => { },
        num: {
            fix: (NUM, DIGIT) => {
                return ('0000000' + NUM).slice(DIGIT * -1)
            },
            comma: (NUM) => {
                return String(NUM).replace(/(\d)(?=(\d{3})+$)/g, '$1,')
            },
            mean: (A, B) => {
                return (A instanceof Array && !B) ? d3.mean(A) : d3.mean([A, B]);
            },
            per: (NUM, DIGIT = 1) => {
                if (NUM == 1) {
                    return '100';
                }
                return (NUM * 100).toFixed(DIGIT)
            }
        },
        date: (FMT) => {
            var _DATE = new Date();

            return FMT.replace(/(Y+|M+|D+|W+|w+|h+|m+|s+)/gm, function (s) {
                var len = String(s).length;
                switch (s) {
                    case String(s.match(/Y+/)): return _.util.num.fix(_DATE.getFullYear(), len);
                    case String(s.match(/M+/)): return _.util.num.fix(_DATE.getMonth() + 1, len);
                    case String(s.match(/D+/)): return _.util.num.fix(_DATE.getDate(), len);
                    case String(s.match(/h+/)): return _.util.num.fix(_DATE.getHours(), len);
                    case String(s.match(/m+/)): return _.util.num.fix(_DATE.getMinutes(), len);
                    case String(s.match(/s+/)): return _.util.num.fix(_DATE.getSeconds(), len);
                    default: break;
                }
            });
        },
        time: {
            diff: (A, B) => {
                const a = _.util.is.number(A) ? A : A.getTime();
                const b = _.util.is.number(B) ? B : B.getTime();

                const c = (b - a) / 1000;

                const sec = (c % 60) % 60;
                const min = Math.floor(c / 60) % 60;
                const hour = Math.floor(c / 3600);

                return { h: hour, m: min, s: sec };
            }
        },
        is: {
            object: (T) => {
                return /Object/.test(Object.prototype.toString.call(T));
            },
            array: (T) => {
                return /Array/.test(Object.prototype.toString.call(T));
            },
            function: (T) => {
                return /Function/.test(Object.prototype.toString.call(T));
            },
            string: (T) => {
                return /String/.test(Object.prototype.toString.call(T));
            },
            number: (T) => {
                return /Number/.test(Object.prototype.toString.call(T));
            },
            filelist: (T) => {
                return /FileList/.test(Object.prototype.toString.call(T));
            },
       },
       ss: {
           get: (KEY) => {
               return sessionStorage.getItem(KEY);
           },
           set: (KEY, VAL) => {
               sessionStorage.getItem(KEY,VAL);
           },
           clear: () => {
               sessionStorage.clear();
           }
       }
    }

    // *************************************************************
    // _.$
    // *************************************************************
    _.$ = {
        init: () => {
            $W = $(_W = window);
            $D = $(_D = document);
            $B = $(_B = 'body');
            $H = $(_H = 'header');
            $T = $(_M = '#main-content');
        },
    }

    // *************************************************************
    // _.d3
    // *************************************************************
    _.d3 = {
        init: () => {

            $T.hide();

            //_.d3.get.json(_.const.path.json.xtr);
            _.d3.config.init();

            _.d3.create.chart();

            _.d3.style();

            $T.fadeIn(1000)

        },
        data: {
            tree: window.XTR_DATA,
            map: [{start:-19}],
            inPosArr: [],
            optPosArr: []
        },
        config: {
            init: () => {
                // svg-width/height
                const w = _.d3.config.w = $T.outerWidth() - 20;
                const h = _.d3.config.h = $T.outerHeight();

            },
            w: 0,
            h: 0,

            box: {
                gap: {
                    x: 14,
                    y0: 20,
                    y1: 40,
                    y2: 60
                },
                padding: 20,
                stroke: {
                    w: 2
                },


                Width: (COLS) => {
                    const wFix = COLS == 1 ? 2 : 1;
                    return (_.d3.config.w - (COLS - 1) * _.d3.config.box.gap.x) / COLS - wFix;
                },
                Pos: (COLS, NUM) => {
                    const w = _.d3.config.box.Width(COLS);
                    const gap = _.d3.config.box.gap.x;
                    return (NUM) * (w + gap) +1;
                }
            }
        },
        get: {
            json: (PATH) => {
                $.getJSON(PATH, (DATA) => {
                    _.d3.data.tree = DATA;
                })
            }
        },
        create: {
            svg: () => {
                const opt = _.d3.config;
                const main = d3.select('#main-content');
                const svg = main.append('svg')
                    .attr('id', 'svg')
                    .attr('width', opt.w)
                    .attr('height', opt.h)
                    .attr('xmlns:xlink', d3.namespace('xlink').space)


                const defs = svg.append('defs')
                    .attr('id', 'defs')
                    

                //$.get('fonts/ShimanoLC-Regular.svg', (DATA) => {
                $.get('fonts/marks.svg', (DATA) => {
                    console.log(DATA)
                    defs.select(function () {
                        return this.appendChild(DATA.querySelector('#shimano-marks'))
                    })
                })



                svg.append('style')
                    .html('@font-face {font-family: "ShimanoLC_en";src:url(#shimanolcregular) format("svg");font-weight: 400;font-style: normal}')

                return svg;
            },
            chart: () => {

                const rowHeight = 50;
                const rectHeight = 200;

                const opt = _.d3.config;
                const svg = _.d3.create.svg();
                const tree = (() => {
                    return _.d3.data.tree.map((V, I, A) => {
                        let obj = {};
                        if (I !== 0) {
                            obj.cross1 = !(
                                A[I - 1]
                                &&
                                A[I - 1].some((R) => {
                                    return R.link.length == 1;
                                })
                                &&
                                V.length == A[I - 1].length
                            )
                        }
                        if (I !== A.length - 1) {
                            obj.cross2 = !(
                                A[I + 1]
                                &&
                                V.some((R) => {
                                    return R.link.length == 1;
                                })
                                &&
                                V.length == A[I + 1].length
                            )
                        }
                        obj.arr = V.map((V2,I2,A2) => {
                            let res = V2;
                            res.w = opt.box.Width(A2.length)
                            res.c = Math.floor((res.w + opt.box.gap.x) * I2 + res.w / 2);
                            res.l = (res.w + opt.box.gap.x) * I2;
                            res.colNum = I2;
                            res.colNums = A2.length;
                            res.cross1 = obj.cross1;
                            res.cross2 = obj.cross2;
                            return res;
                        });
                        obj.rowNum = I;
                        obj.nums = obj.arr.length;
                        obj.afterNums = (_.d3.data.tree[I + 1] ? _.d3.data.tree[I + 1].length : 0);

                        return obj;
                    });
                })();
                const map = (() => {
                    tree.map((V) => {

                    })
                })();

                // ----------------------------------------------
                // ROWS
                // ----------------------------------------------

                const rowPos = (NUM, POS) => {
                    if (!_.d3.data.map[NUM]) {
                        _.d3.data.map[NUM] = {};
                    }

                    if (!(POS instanceof Object)) {
                        return _.d3.data.map[NUM];
                    }

                    let OBJ = {};
                    for (let key in (OBJ = (POS || { start: 0 }))) {
                        if (key == 'height') {
                            _.d3.data.map[NUM][key] = OBJ[key];
                        } else {
                            if (!_.d3.data.map[NUM][key]) {
                                _.d3.data.map[NUM][key] = OBJ[key];
                            } else {
                                _.d3.data.map[NUM][key] += OBJ[key];
                            }
                        }

                    }

                    return _.d3.data.map[NUM];
                };

                const ROWSd = svg
                    .append('g')
                    .classed('svg-box', true)
                    .attr('transform','translate(1,0)')
                    .selectAll('.rows')
                    .data(tree)
                
                const ROWSe = ROWSd
                    .enter()
                    .append('g')
                    .classed('rows', true)
                    .attr('class', (d,i) => {
                        return 'rows row' + i;
                    })

                const ROWS = ROWSd
                    .merge(ROWSe);

                // ----------------------------------------------
                // ROWS > BOXES/LINES
                // ----------------------------------------------
                const LINES = ROWS
                    .append('g')
                    .classed('lines', true)

                const BOXES = ROWS
                    .append('g')
                    .classed('boxes', true)



                // ----------------------------------------------
                // BOXES > BOX
                // ----------------------------------------------
                let boxNum = 0;
                const BOXd = BOXES
                    .selectAll('.box')
                    .data((d, i) => {
                        d.arr = d.arr.map((V) => {
                            V.rowNum = i;
                            V.boxNum = boxNum++;
                            V.cross2 = d.cross2;
                            return V;
                        })
                        return d.arr;
                    })
                
                const BOXe = BOXd
                    .enter()
                    .append('g')
                    .classed('box', true)
                    //.attr('transform', (d, i, a) => {
                    //    return 'translate('+opt.box.Pos(a.length,i)+',0)';
                    //})

                const BOX = BOXd
                    .merge(BOXe)


                // ----------------------------------------------
                // BOXES > BOX > contents
                // ----------------------------------------------
                const inPos = (NUM, d, H = 0) => {
                    if (!_.d3.data.inPosArr[NUM]) {
                        _.d3.data.inPosArr[NUM] = 30;
                    }
                    _.d3.data.inPosArr[NUM] += H;

                    return _.d3.data.inPosArr[NUM];
                };

                /*
                const ICON = BOX
                    .filter((d) => {
                        return d.icon;
                    })
                    .append('text')
                    .classed('icon', true)
                    .attr('x', (d, i, a) => {
                        return d.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = inPos(d.boxNum, d);
                        if (d.icon !== ' ') {
                            inPos(d.boxNum, d, 80);
                        }
                        return result;
                    })
                    .text((d) => {
                        return d.icon;
                    })
                */

                const ICON = BOX
                    .filter((d) => {
                        return d.icon;
                    })
                    .append('use')
                    .classed('icon', true)
                    .attr('x', (d, i, a) => {
                        return d.c - 50;
                    })
                    .attr('y', (d, i, a) => {
                        const result = inPos(d.boxNum, d);
                        if (d.icon !== ' ') {
                            inPos(d.boxNum, d, 80);
                        }
                        return result;
                    })
                    .attr('xlink:href', (d) => {
                        return '#' + d.icon;
                    })
                    .attr('width',100)
                    .attr('height',100)
                    .attr('viewbox','0 0 80 80')

                const ICON_T = BOX
                    .filter((d) => {
                        return d.iconText;
                    })
                    .append('text')
                    .classed('iconText', true)
                    .attr('x', (d,i,a) => {
                        return d.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = inPos(d.boxNum, d);
                        inPos(d.boxNum, d, 20);
                        return result;
                    })
                    .text((d) => {
                        return d.iconText;
                    })

                // MODEL ---------------------------------------------
                const MODELd = BOX
                    .filter((d) => {
                        return d.model;
                    })
                    .selectAll('.model')
                    .data((d) => {
                        let obj = d.model;
                        obj.map((V) => {
                            V.c = d.c;
                            V.boxNum = d.boxNum;
                            return V;
                        })
                        return obj;
                    })

                const MODELe = MODELd
                    .enter()
                    .append('g')
                    .classed('model', true)

                MODELe
                    .filter((d) => {
                        return d.mod;
                    })
                    .append('text')
                    .classed('mod', true)
                    .attr('x', (d, i, a) => {
                        return d.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = inPos(d.boxNum, d);
                        inPos(d.boxNum, d, 22);
                        return result;
                    })
                    .text((d) => {
                        return d.mod;
                    })

                MODELe
                    .selectAll('.cap')
                    .data((d) => {
                        let obj = (d.cap ? d.cap.en : []).map((V) => {
                            let obj2 = {};
                            obj2.t = V;
                            obj2.c = d.c;
                            obj2.boxNum = d.boxNum;
                            return obj2;
                        })
                        return obj;
                    })
                    .enter()
                    .append('text')
                    .classed('cap', true)
                    .attr('x', (d, i, a) => {
                        return d.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = inPos(d.boxNum, d);
                        inPos(d.boxNum, d, 14);
                        return result;
                    })
                    .text((d) => {
                        return d.t;
                    })

                // OPTIONS ---------------------------------------------
                const optPos = (NUM, d, H = 0) => {
                    if (!_.d3.data.optPosArr[NUM]) {
                        _.d3.data.optPosArr[NUM] = 10;
                    }
                    _.d3.data.optPosArr[NUM] += H;

                    return _.d3.data.optPosArr[NUM];
                };
                const OPTIONS = BOX
                    .filter((d, i, a) => {
                        return d.option;
                    })
                    .append('g')
                    .classed('opt', true)
                    .attr('transform', (d) => {
                        //inPos(d.boxNum, d, 20);
                        return 'translate(0,' + (inPos(d.boxNum) + 10) + ')';
                    })

                const OPTION = OPTIONS
                    .selectAll('.model')
                    .data((d) => {
                        let obj = d.option;
                        obj.map((V) => {
                            V.parent = d;
                            return V;
                        });
                        return obj;
                    })
                    .enter()
                    .append('g')
                    .classed('model', true)

                const OPT_MOD = OPTION
                    .append('text')
                    .classed('mod', true)
                    .attr('x', (d, i, a) => {
                        return d.parent.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = optPos(d.parent.boxNum, d.parent);
                        optPos(d.parent.boxNum, d.parent, 22);
                        return result;
                    })
                    .text((d) => {
                        return d.mod;
                    })
                    .select(function () {
                        return this.parentElement;
                    })

                    .selectAll('.cap')
                    .data((d) => {
                        let obj = (d.cap ? d.cap.en : []).map((V) => {
                            let obj2 = {};
                            obj2.t = V;
                            obj2.parent = d.parent;
                            return obj2;
                        })
                        return obj;
                    })
                    .enter()
                    .append('text')
                    .classed('cap', true)
                    .attr('x', (d, i, a) => {
                        return d.parent.c;
                    })
                    .attr('y', (d, i, a) => {
                        const result = optPos(d.parent.boxNum, d.parent);
                        optPos(d.parent.boxNum, d.parent, 14);
                        return result;
                    })
                    .text((d) => {
                        return d.t;
                    })

                const OPT_RECT = OPTIONS
                    .insert('rect','*')
                    .classed('opt_rect', true)
                    .classed('dotted', (d) => {
                        return d.optionStyle;
                    })
                    .classed('blue', (d) => {
                        return d.optionBg;
                    })
                    .attr('x', (d) => {
                        return d.l + 10;
                    })
                    .attr('y', (d) => {
                        return 0;
                    })
                    .attr('width', (d) => {
                        return d.w - 20;
                    })
                    .attr('height', (d) => {
                        const result = optPos(d.boxNum, d) + 10;
                        inPos(d.boxNum, d, result);
                        return result;
                    })

                // ----------------------------------------------
                // BOXES > BOX > RECT 
                // 内容に合わせて高さ設定
                // ----------------------------------------------
                d3.selectAll('.box')
                    .each(function (d,i){
                        const _this = d3.select(this);
                        const bnum = d.boxNum;
                        const rnum = d.rowNum;
                        const result = inPos(bnum);

                        const h = d3.max([(rowPos(rnum).height || 0), result]);
                        rowPos(rnum, { height: h });
                    })
                const RECT = BOX
                    .insert('rect','*')
                    .classed('rect', true)
                    .classed('dotted', (d) => {
                        return d.borderStyle ? true : false;
                    })
                    .attr('x', (d, i, a) => {
                        return d.l;
                    })
                    .attr('y', (d, i, a) => {
                        const result = opt.box.gap.y0;
                        //inPos(d.boxNum, result)
                        return result;
                    })
                    .attr('width', (d, i, a) => {
                        return d.w;
                    })
                    .attr('height', (d,i,a) => {
                        return rowPos(d.rowNum).height;

                    })

                // ----------------------------------------------
                // BOXES > LINES
                // ----------------------------------------------
                const linePos = (d, num) => {
                    if (num >= tree.length) {
                        return 0;
                    } else if (num) {
                        const d2 = tree[num];
                        if (d2.arr) {
                            return (d2.arr[0].w + opt.box.gap.x) * d2.colNum + d2.c;
                        }
                    }
                    return (d.w + opt.box.gap.x) * d.colNum + d.c;
                }

                d3.selectAll('.box')
                    .each(function (D, I) {
                        const _this = d3.select(this);
                        _this
                            .append('line')
                            .classed('line vert', true)
                            .attr('x1', (d) => {
                                return d.c;
                            })
                            .attr('x2', (d) => {
                                return d.c;
                            })
                            .attr('y1', (d) => {
                                return 0;
                            })
                            .attr('y2', (d) => {
                                return 20;
                            })
                        _this
                            .append('line')
                            .classed('line vert', true)
                            .attr('x1', (d) => {
                                return d.c;
                            })
                            .attr('x2', (d) => {
                                return d.c;
                            })
                            .attr('y1', (d) => {
                                return rowPos(d.rowNum).height + opt.box.padding;
                            })
                            .attr('y2', (d) => {
                                return rowPos(d.rowNum).height + opt.box.padding + 20;
                            })
                    })

                ROWS
                    .attr('transform', (d, i, a) => {
                        const result = rowPos(i).start;
                        const gap = (
                            d.cross2 ?
                                ((d.nums > 1 && d.afterNums > 1) ?
                                    opt.box.gap.y1 :
                                    opt.box.gap.y0
                                ) :
                                opt.box.gap.y0
                        );
                        const start =
                            result + rowPos(i).height
                            + opt.box.gap.y0
                            + gap

                        opt.h = rowPos(i + 1, {
                            start: start
                        }).start;

                        return 'translate(0,' + result + ')';
                    })

                const LINEh1 = LINES
                    .append('path')
                    .classed('line hori1', true)
                    .classed('hidden', (d, i) => {
                        return i == 0 || !d.cross1;
                    })
                    .attr('d', (d) => {
                        return (d3.line()
                            .x((d) => {
                                return d.c;
                            })
                            .y((d) => {
                                return 0;
                            })
                        )(d.arr)
                    })

                const LINE4 = LINES
                    .append('line')
                    .classed('line center', true)
                    .classed('hidden', (d) => {
                        return (!d.cross2 || d.afterNums <= 1 || d.nums == 1);
                    })
                    .attr('x1', (d) => {
                        return (opt.w - 3) / 2;
                    })
                    .attr('x2', (d) => {
                        return (opt.w - 3) / 2;
                    })
                    .attr('y1', (d) => {
                        return rowPos(d.rowNum).height + opt.box.padding + opt.box.gap.y0;
                    })
                    .attr('y2', (d) => {
                        return rowPos(d.rowNum).height + opt.box.padding + opt.box.gap.y1;
                    })

                const LINEh2 = LINES
                    .append('path')
                    .classed('line hori2', true)
                    .classed('hidden', (d, i) => {
                        return !d.cross2;
                    })
                    .attr('d', (d) => {
                        return (d3.line()
                            .x((d) => {
                                return d.c;
                            })
                            .y((d) => {
                                return rowPos(d.rowNum).height + opt.box.padding + opt.box.gap.y0;
                            })
                        )(d.arr)
                    })


                // ----------------------------------------------
                // ACTION
                // ----------------------------------------------
                const BTN = BOX
                    .selectAll('.mod')
                    .on('click', function(d){
                        const model = d3.select(this).text();

                        XTR_DATA_DETAIL[model]


                        $('#modal-loading').fadeOut();
                        $('#modal-content').fadeIn();
                        $('#modal').fadeIn();
                        $('.reveal-modal-bg').fadeIn();
                    })

                d3.selectAll('.close')
                    .on('click', function () {
                        $('#modal-content').fadeOut();
                        $('#modal').fadeOut();
                        $('.reveal-modal-bg').fadeOut();
                    })

                //d3.selectAll('.rows')
                //    .call(_.d3.action.drag);



                // ----------------------------------------------
                // SVG HEIGHT
                // ----------------------------------------------
                svg.attr('height', opt.h - 19);

            },
            table: () => {

            }
        },
        action: {
            drag: d3.drag()
                .on('start', function () {
                    console.log('drag-start');
                    d3.select(this).raise().classed("dragging", true);
                })
                .on('drag', function () {
                    const _this = d3.select(this);
                    const position = _this.attr('transform').replace(/translate|[()]/g,'').split(',');

                    _this.attr('transform', 'translate(' + (Number(position[0]) + d3.event.dx) + ',' + position[1]+')');
                })
                .on('end', function () {
                    d3.select(this).raise().classed("dragging", false);
                    console.log('drag-end');
                }),
            zoom: d3.zoom()
                .scaleExtent([0.1, 10])
                .on('zoom', function (TARGET) {
                    const alt = d3.event.sourceEvent.altKey;
                    d3.select(this).attr('transform', d3.event.transform);
                })

        },
        style: () => {
            // color ---------------------------------
            const $trans = 'transparent';
            const $blue = '#22b4f1';
            const $dblue = '#049';
            const $grey = '#eee';
            const $dgrey = '#333';
            const $black = '#000';


            //$('#svg style').html('@font-face {font-family: "shimano-font";src: url(#shimanolcregular) format("svg");}#svg rect.rect {fill: transparent;stroke: #22b4f1;stroke-width: 2; }#svg rect.opt_rect {fill: rgba(0, 0, 0, 0.1); }#svg rect.dotted {stroke: #22b4f1;stroke-width: 2;stroke-dasharray: 6 3; }#svg rect.blue {fill: rgba(34, 180, 241, 0.3); }#svg path.line,#svg line.line {stroke: #22b4f1;stroke-width: 4;stroke-linecap: square; }#svg path.hidden,#svg line.hidden {display: none; }#svg path.vert,#svg line.vert {stroke-linecap: butt; }#svg text {font-family: Arial;text-anchor: middle;dominant-baseline: text-before-edge;fill: #333; }#svg text.icon {font-size: 80px;font-family: "shimano-font"; }#svg text.iconText {font-size: 14px;font-style: italic;font-weight: bold; }#svg text.mod {font-size: 18px;font-weight: bold;fill: #22b4f1;cursor: pointer; }#svg text.cap {font-size: 12px; }')

            const styles = [
                'cursor',
                'display',
                'dominant-baseline',
                'fill',
                'font-family',
                'font-size',
                'font-style',
                'font-weight',
                'stroke',
                'stroke-dasharray',
                'stroke-linecap',
                'stroke-width',
                'text-anchor'
            ];
            const rejList = ['auto', 'normal'];
            const $svg = $('#svg');
            const $all = $svg.find('line,rect,text,path');

            $all.each((I, V) => {
                const style = $(V).css(styles);
                for (let key in style) {
                    if (rejList.indexOf(style[key]) >= 0) {
                        delete style[key];
                    }
                }

                $(V).attr(style)
            })

        }

    }

    // *************************************************************
    // _.action
    // *************************************************************
    _.action = {
        init: () => {
            _.action.add.download();
            _.action.add.style();
            //_.action.add.scale();
        },
        add: {
            download: () => {
                $D.on('click', '#download .svg', () => {
                    _.img.convert.svg();
                })
                $D.on('click', '#download .png', () => {
                    _.img.convert.png();
                })
            },
            style: () => {

                $D.on('click', '#download .style', () => {
                    _.d3.style();
                })
            },
            scale: () => {
                $D.on('click', '#download .scaleup', () => {
                    d3.select('.svg-box').attr('transform', function () {
                        const _this = d3.select(this);
                        console.log(d3.select(this).attr('transform'));
                        const scale = Number((d3.select(this).attr('transform').match(/scale\([\d\.]+\)/) || ['scale(1)'])[0].replace(/[a-z\(\)]/g, ''));

                        console.log(scale);
                        return 'scale(' + (scale * 1.2) + ')';
                    })
                })
                $D.on('click', '#download .scaledown', () => {
                    d3.select('.svg-box').attr('transform', function () {
                        const _this = d3.select(this);
                        console.log(d3.select(this).attr('transform'));
                        const scale = Number((d3.select(this).attr('transform').match(/scale\([\d\.]+\)/) || ['scale(1)'])[0].replace(/[a-z\(\)]/g, ''));

                        console.log(scale);
                        return 'scale(' + (scale * 0.8) + ')';
                    })
                })
            }
        }
    }

    // *************************************************************
    // _.img
    // *************************************************************
    _.img = {
        init: () => {
            //_.img.convert.svg();
            //_.img.convert.png();
        },
        convert: {
            svg: () => {

                const doctype = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
                const prefix = {
                    xmlns: 'http://www.w3.org/2000/xmlns/',
                    xlink: 'http://www.w3.org/1999/xlink',
                    svg: 'http://www.w3.org/2000/svg'
                };

                const filename = 'shimano-chart_' + _.util.date('YYMMDD') + '.svg';

                const svg = $('#svg')[0];
                const svgData = new XMLSerializer().serializeToString(svg);

                const blobObject = new Blob([doctype + svgData], { 'type': 'text\/xml' })

                if (navigator.appVersion.toString().indexOf('.NET') > 0) { //IE hack
                    window.navigator.msSaveBlob(blobObject, filename)

                } else {
                    const url = window.URL.createObjectURL(blobObject)
                    const a = d3.select('body').append('a')

                    a.attr('class', 'downloadLink')
                        .attr('download', filename)
                        .attr('href', url)
                        .style('display', 'none')

                    a.node().click()

                    setTimeout(function () {
                        window.URL.revokeObjectURL(url)
                        a.remove()
                    }, 10)
                }

            },
            png: () => {
                const svg = $('#svg')[0];
                const svgData = new XMLSerializer().serializeToString(svg);
                const svgEnc = unescape(encodeURIComponent(svgData));
                const svgBtoA = btoa(svgEnc);
                const cv = $('<canvas>')[0];
                    cv.width = _.d3.config.w;
                    cv.height = _.d3.config.h;
                const cvc = cv.getContext('2d');

                const filename = 'shimano-chart_' + _.util.date('YYMMDD') + '.png';

                const img = new Image;
                img.src = 'data:image/svg+xml;charset=utf-8;base64,' + svgBtoA;
                img.onload = () => {
                    cvc.drawImage(img, 0, 0);

                    const a = d3.select('body').append('a')

                    a.attr('class', 'downloadLink')
                        .attr('download', filename)
                        .attr('href', cv.toDataURL('image/png'))
                        .style('display', 'none')

                    a.node().click()

                    setTimeout(function () {
                        window.URL.revokeObjectURL(url)
                        a.remove()
                    }, 10)
                }


            }
        }
    }

    // *************************************************************
    // window._
    // *************************************************************
    window._ = _;
})();

$(() => {
    _.init();
    console.log(_)
});