////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// 読み込み後処理
// 
$(window).bind('load',function(){
	$('.btnOpenEditor').colorbox({
		 fixed:true
		,iframe:true
		,width:'75%'
		,height:'90%'
	});
	
	$(document).on('mouseenter', '.blockEditArea', showToolTip);
	$(document).on('mouseleave', '.blockEditArea', hideToolTip);
	
	function showToolTip(ev)
	{
		// 編集権限がある場合のみ
		var scope = angular.element(ev.target).scope();
		if (scope.header.user.content_editable != 1) {
			return false;
		}

		$(this).css('position', 'relative');
		var version    = $(this).attr('data-update_id');
		var cid        = $(this).attr('data-cid');
		var parent_cid = $(this).attr('data-parent_cid');
		
		var query = '';
		if(!!version)
		{
			query += (!!query)?'&':'?';
			query += 'version='+version;
		}
		if(!!parent_cid)
		{
			query += (!!query)?'&':'?';
			query += 'parent_cid='+parent_cid;
		}
		if(!!cid)
		{
			query += (!!query)?'&':'?';
			query += 'cid='+cid;
		}
		
		$(this).append($('#tooltipEditArea'));
		
		var editorUrl  = $('.btnOpenEditor', this).attr('default-href');
		$('.btnOpenEditor', this).attr('href', editorUrl+query);
	}
	function hideToolTip()
	{
		$('#opeEditArea').append($('#tooltipEditArea'));
	}
});

function reload()
{
	location.reload();
}

function closeEditor()
{
	$.colorbox.close();
}
