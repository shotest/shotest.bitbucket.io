﻿//-------------------------------------------------
// TOC
'use strict';

!(function (_DC) {
    var __ = _DC;

    var DURATION = 100;

    var $W = undefined,
        $D = undefined,
        $H = undefined,
        $B = undefined,
        $HB = undefined,
        $H1 = undefined,
        $H2 = undefined,
        $M = undefined,
        $N = undefined,
        $FC = undefined,
        $F = undefined,
        $T = undefined;
    var _W = undefined,
        _D = undefined,
        _H = undefined,
        _B = undefined,
        _HB = undefined,
        _H1 = undefined,
        _H2 = undefined,
        _M = undefined,
        _N = undefined,
        _FC = undefined,
        _F = undefined,
        _T = undefined;
    var TOUCH = undefined,
        TOUCH_CLICK = undefined,
        TOUCH_START = undefined,
        TOUCH_MOVE = undefined,
        TOUCH_END = undefined,
        TOUCH_ENTER = undefined,
        TOUCH_LEAVE = undefined;

    var $LISTS = undefined,
        $ITEMS = undefined,
        $INNERS = undefined,
        $LINKS = undefined,
        $BTNS = undefined;
    var _LISTS = undefined,
        _ITEMS = undefined,
        _INNERS = undefined,
        _LINKS = undefined,
        _BTNS = undefined;

    __.toc = {
        Init: function Init() {

            // init
            this.$.Init();
            this.action.Init();

            // set
            this.set.Init();

            // add
            this.add.Init();
        },

        scrollTop: 0,

        $: {
            Init: function Init() {
                $W = $(_W = window);
                $D = $(_D = document);
                $H = $(_H = 'html');
                $B = $(_B = 'body');
                $HB = $(_HB = _H + ',' + _B);
                $H1 = $(_H1 = 'header[role="global"]');
                $H2 = $(_H2 = 'header[role="manual"]');
                $M = $(_M = 'main[role="main_contents"]');
                $N = $(_N = 'nav[role="side_toc"]');
                $FC = $(_FC = 'nav[role="footer_control"]');
                $F = $(_F = 'footer');
                $T = $(_T = '#page_top a');

                $LISTS = $(_LISTS = '.toc_list');
                $ITEMS = $(_ITEMS = '.toc_list_item');
                $INNERS = $(_INNERS = '.toc_list_inner');
                $LINKS = $(_LINKS = '.toc_list_a');
                $BTNS = $(_BTNS = '.toc_list_btn');
            }
        },
        is: {
            show: false
        },
        set: {
            Init: function Init() {
                this.Scrollbar();
            },
            Scrollbar: function Scrollbar() {
                if (!__.window.get.is.SP()) {
                    new SimpleBar($('.side_toc_inner')[0]);
                }
            },
            scrollBack: {
                top: 0,
                Lock: function Lock() {
                    $HB.each(function (I, _E) {
                        var st = $(_E).scrollTop();
                        if (st !== 0) {
                            __.toc.scrollTop = st;
                        }
                    });
                    $B.css({ position: 'fixed' });
                },
                Release: function Release() {
                    $B.css({ position: 'inherit' });
                    $HB.scrollTop(__.toc.scrollTop);
                }
            }
        },
        add: {
            Init: function Init() {
                this.scroll.Inner();
                this.click.Open();
                this.click.Hide();
            },
            scroll: {
                Inner: function Inner() {

                    var $inner = $N.find('.side_toc_inner').eq(0);
                    var $list = $N.find('.toc_list.level1').eq(0);

                    $D.on('touchmove scroll', '.side_toc *', function (_event) {
                        _event.stopPropagation();
                        //alert('stopPropagation');
                    });
                }
            },
            click: {
                Open: function Open() {
                    $BTNS.on('click', function () {
                        var $inner = $(this).parent('.toc_list_a').next('.toc_list_inner');
                        var $parent = $(this).closest('li');

                        __.toc.action.OpenClose($parent);

                        return false;
                    });
                },
                Hide: function Hide() {

                    $('.side_toc_back,.side_toc_inner').on('click', function (_event) {
                        var target = _event.target;

                        console.log($(target));
                        if ($(target).hasClass('side_toc_back') || $(target).hasClass('side_toc_inner')) {
                            __.toc.action.Show();
                        }
                    });
                }
            }
        },
        action: {
            Init: function Init() {
                this.open.Actibate();
            },
            Refresh: function Refresh() {
                __.toc.set.Init();
            },
            Show: function Show() {

                var $tocBtn = $('.header_btn.toc');

                if ($tocBtn.hasClass('open')) {
                    $N.stop().animate({ right: '-100%' }, 0, 'swing');
                    $N.removeClass('open');
                    $tocBtn.removeClass('open');

                    $B.removeClass('toc_open');
                    //$HB.scrollTop($B.attr('data-toc_open'));
                    __.toc.set.scrollBack.Release();

                    __.toc.is.show = false;
                } else {
                    $N.stop().animate({ right: '0' }, 0, 'swing');
                    $N.addClass('open');
                    $tocBtn.addClass('open');

                    //$B.attr('data-toc_open', __.window.scrollTop);
                    $B.addClass('toc_open');

                    __.toc.set.scrollBack.Lock();
                    __.toc.is.show = true;
                }
            },
            OpenClose: function OpenClose($T) {
                if ($T.hasClass('open')) {
                    __.toc.action.close.Target($T);
                } else {
                    __.toc.action.open.Target($T);
                }
                return;
            },
            open: {
                Init: function Init() {},
                Actibate: function Actibate() {
                    var $active = $ITEMS.filter('.active');
                    $active.parents(_ITEMS).addClass('active open');

                    var $opened = $(_ITEMS).filter('.open');

                    __.toc.action.open.Target($opened);

                    return false;
                },
                Target: function Target($T) {

                    if ($T.length > 1) {
                        $T.each(function (I, E) {

                            var $inner = $(E).children('.toc_list_inner');

                            $inner.slideDown(DURATION);
                            $(E).addClass('open');
                        });
                    } else {

                        var $inner = $T.children('.toc_list_inner');

                        $inner.slideDown(DURATION);
                        $T.addClass('open');
                    }

                    return;
                }
            },
            close: {

                Target: function Target($T) {

                    if ($T.length > 1) {
                        $T.each(function (I, E) {

                            var $inner = $(E).children('.toc_list_inner');

                            $inner.slideUp(DURATION);
                            $(E).removeClass('open');
                        });
                    } else {

                        var $inner = $T.children('.toc_list_inner');

                        $inner.slideUp(DURATION);
                        $T.removeClass('open');
                    }

                    return;
                }
            }
        }
    };

    window.DC = __;
})(window.DC || {});

$(function () {
    DC.toc.Init();
});
//-------------------------------------------------

