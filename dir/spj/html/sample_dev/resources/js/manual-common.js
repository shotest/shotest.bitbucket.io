﻿//-------------------------------------------------
// common
'use strict';

!(function (_DC) {
    var __ = _DC;

    var LOCATION = location,
        L_HREF = LOCATION.href,
        L_PATHNAME = LOCATION.pathname,
        L_SEARCH = LOCATION.search,
        L_HASH = LOCATION.hash;

    var G_HEADER_H = 60,
        M_HEADER_H = 80,
        FOOTER_H = 50,
        MARGIN = 20,
        WIN_BREAK1 = 1366,
        WIN_BREAK2 = 1024,
        WIN_BREAK3 = 600,
        DURATION = 300;

    var $W = undefined,
        $D = undefined,
        $H = undefined,
        $B = undefined,
        $HB = undefined,
        $H1 = undefined,
        $H2 = undefined,
        $M = undefined,
        $N = undefined,
        $FC = undefined,
        $F = undefined,
        $T = undefined;
    var _W = undefined,
        _D = undefined,
        _H = undefined,
        _B = undefined,
        _HB = undefined,
        _H1 = undefined,
        _H2 = undefined,
        _M = undefined,
        _N = undefined,
        _FC = undefined,
        _F = undefined,
        _T = undefined;
    var TOUCH = undefined,
        TOUCH_CLICK = undefined,
        TOUCH_START = undefined,
        TOUCH_MOVE = undefined,
        TOUCH_END = undefined,
        TOUCH_ENTER = undefined,
        TOUCH_LEAVE = undefined;

    // -------------------------------------------------------
    // Init
    // -------------------------------------------------------
    __.Init = function () {
        console.log('// -- DC start -- //');

        this.$.Init();
        this.window.Init();
        this.document.Init();
    };

    // -------------------------------------------------------
    // util
    // -------------------------------------------------------
    __.util = {
        set: {
            header: {
                Text: function Text(_txt) {
                    $H1.find('.header_logo').html(_txt);
                }
            }
        }
    };

    // -------------------------------------------------------
    // $
    // -------------------------------------------------------
    __.$ = {
        Init: function Init() {
            $W = $(_W = window);
            $D = $(_D = document);
            $H = $(_H = 'html');
            $B = $(_B = 'body');
            $HB = $(_HB = _H + ',' + _B);
            $H1 = $(_H1 = 'header[role="global"]');
            $H2 = $(_H2 = 'header[role="manual"]');
            $M = $(_M = 'main[role="main_contents"]');
            $N = $(_N = 'nav[role="side_toc"]');
            $FC = $(_FC = 'nav[role="footer_control"]');
            $F = $(_F = 'footer');
            $T = $(_T = '#page_top a');
        }
    };

    // -------------------------------------------------------
    // window
    // -------------------------------------------------------
    __.window = {
        Init: function Init() {
            this.get.Init();
            this.set.Init();

            this.add.Init();
        },

        width: 0,
        height: 0,

        device: '',
        isPC: false,
        isSP: false,
        isTB: false,

        ontouch: false,
        userAgent: '',
        fixed: false,
        scrollTop: 0,
        scrollBottom: 0,

        add: {
            Init: function Init() {
                this.Load();
                this.Resize();
                this.Scroll();
            },
            Load: function Load() {
                if (L_HASH) {
                    //$HB.stop().scrollTop(0);
                    var $target = $(L_HASH);
                    var position = $target.offset().top - (G_HEADER_H + 10);
                    $HB.stop().animate({ scrollTop: position });
                }
            },
            Resize: function Resize() {
                $W.on('resize', function () {
                    __.window.get.Init();
                    __.window.set.Init();

                    __.document.get.Init();
                    __.document.set.Init();
                });
            },
            Scroll: function Scroll() {
                $W.on('scroll', function (E) {
                    __.window.get.Scroll();
                    __.window.set.Scroll();
                });
            }
        },
        get: {
            Init: function Init() {
                this.Width();
                this.Height();
                this.Scroll();
                this.Device();
                this.UserAgent();
                this.is.Init();
            },
            Width: function Width() {
                __.window.width = $W.width();
            },

            Height: function Height() {
                __.window.height = window.innerHeight || $W.height();
            },
            Scroll: function Scroll() {
                __.window.scrollTop = $W.scrollTop();
            },
            Device: function Device() {

                // window break
                var w = __.window.width;

                if (w > WIN_BREAK1) {
                    __.window.device = 'pc2';
                } else if (w > WIN_BREAK2) {
                    __.window.device = 'pc1';
                } else if (w > WIN_BREAK3) {
                    __.window.device = 'tb';
                } else {
                    __.window.device = 'sp';
                }

                // TOUCH
                var touch = __.window.ontouch = 'ontouchend' in document;
                TOUCH = [TOUCH_CLICK = touch ? 'touchstart' : 'click', TOUCH_START = touch ? 'touchstart' : 'mousedown', TOUCH_MOVE = touch ? 'touchmove' : 'mousemove', TOUCH_END = touch ? 'touchend' : 'mouseup', TOUCH_ENTER = touch ? 'touchstart' : 'mouseenter', TOUCH_LEAVE = touch ? 'touchend' : 'mouseleave'];
            },
            UserAgent: function UserAgent() {
                var ua = __.window.userAgent = navigator.userAgent;
            },
            is: {
                Init: function Init() {
                    this.PC();
                    this.SP();
                    this.TB();
                },
                PC: function PC() {
                    return __.window.isPC = __.window.device == 'pc2' || __.window.device == 'pc1';
                },
                SP: function SP() {
                    return __.window.isSP = __.window.device == 'sp';
                },
                TB: function TB() {
                    return __.window.isTB = __.window.device == 'tb';
                }
            }
        },
        set: {
            Init: function Init() {},
            Scroll: function Scroll() {

                var winHeight = __.window.height;
                var docHeight = __.document.height;
                var scrollTop = __.window.scrollTop;
                var scrollBottom = __.window.scrollBottom = docHeight - (scrollTop + winHeight);

                // set.Fixed
                __.document.set.Fixed();

                // set.fixed.PageTop
                if ((__.window.isSP || __.window.isTB) && scrollBottom < FOOTER_H) {
                    __.document.set.fixed.PageTop();
                } else {
                    __.document.set.fixed.PageTop('cancel');
                }
            }
        }
    };

    // -------------------------------------------------------
    // document
    // -------------------------------------------------------
    __.document = {
        Init: function Init() {
            this.get.Init();
            this.set.Init();
            this.add.Init();
        },

        height: 0,

        // Objects
        fc: {},
        toc: {
            height: 0
        },
        imagemap: false,

        get: {

            Init: function Init() {
                this.Height();
            },
            Height: function Height() {
                __.document.height = $D.height();
                __.document.fc.height = $FC.outerHeight();
            }
        },
        set: {

            Init: function Init() {
                this.Fixed(true);
                this.Height();
                this.body.Type();
                this.imagemap.Responsive();
            },
            Height: function Height() {

                // Body
                $B.outerHeight(__.window.height + (!!__.window.isPC ? 0 : FOOTER_H));

                // Toc
                var fix = undefined;
                if (__.window.isPC) {
                    fix = G_HEADER_H + (!__.window.fixed ? M_HEADER_H : 0) + FOOTER_H + MARGIN + 10;
                    __.document.toc.height = __.window.height - fix + (!__.window.fixed ? __.window.scrollTop : 0);
                } else {
                    fix = G_HEADER_H;
                    __.document.toc.height = __.window.height - fix;
                }

                $N.find('.side_toc').height(__.document.toc.height);
            },
            body: {
                Type: function Type() {
                    $B.removeClass('pc2 pc1 tb sp');
                    $B.addClass(__.window.device);
                }
            },
            imagemap: {
                Responsive: function Responsive() {
                    if (!__.document.imagemap) {
                        __.document.imagemap = true;
                        $('img[usemap]').rwdImageMaps();
                    }
                }
            },
            Fixed: function Fixed(_scroll) {

                var scrollTop = __.window.scrollTop;

                if (scrollTop > M_HEADER_H) {
                    $('*[role]').removeClass('top_area');
                    $('*[role]').addClass('fixed');

                    __.window.fixed = true;
                } else {
                    if ($('*[role]').hasClass('fixed')) {
                        $('*[role]').addClass('top_area');
                    }
                    $('*[role]').removeClass('fixed');
                    __.window.fixed = false;

                    __.document.set.Height();
                }

                if (!!_scroll) {
                    $('*[role]').addClass('loaded');
                } else {
                    $('*[role]').removeClass('loaded');
                }
                if (scrollTop <= 0) {
                    $('*[role]').addClass('reached');
                } else {
                    $('*[role]').removeClass('reached');
                }
            },
            fixed: {
                PageTop: function PageTop(_cancel) {
                    if (_cancel) {
                        $FC.removeClass('bottom');
                    } else {
                        $FC.addClass('bottom');
                    }
                }
            }
        },
        add: {

            Init: function Init() {
                this.click.header.Toc();
                this.click.footer.PageTop();
                this.click.Ankers();
                this.click.Cause();
            },
            click: {
                header: {
                    Toc: function Toc() {
                        var $btnToc = $H1.find('.header_btn.toc');
                        $btnToc.on('click', function (E) {
                            E.preventDefault();
                            var $this = $(this);
                            //__.document.set.toc.Open($this);
                            __.toc.action.Show();
                        });
                    }
                },
                footer: {
                    PageTop: function PageTop() {
                        $T.on('click', function () {
                            $(_B + ',' + _H).animate({ scrollTop: 0 }, DURATION, 'swing');
                            return false;
                        });
                    }
                },
                Ankers: function Ankers() {
                    $('a[href^="#"]:not([role])').on('click', function (E) {
                        var hash = this.hash;

                        if (hash == '') {
                            return false;
                        }

                        var $target = $(hash);
                        var posY = $target.offset().top - (G_HEADER_H + 10);

                        $('body,html').animate({ scrollTop: posY }, DURATION, 'swing');
                        return false;
                    });
                },
                Cause: function Cause() {
                    $D.on('click', '.cause', function () {
                        var $cause = $(this);
                        var $remedy = $cause.next('.remedy');

                        if ($cause.hasClass('close')) {
                            $remedy.stop().slideDown(DURATION);
                            $cause.removeClass('close');
                        } else {
                            $remedy.stop().slideUp(DURATION);
                            $cause.addClass('close');
                        }
                    });
                }
            }
        }
    };

    window.DC = __;
})(window.DC || {});

$(function () {
    DC.Init();

    console.log(DC);
});
//-------------------------------------------------

