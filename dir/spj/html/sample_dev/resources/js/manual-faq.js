﻿//-------------------------------------------------
// FAQ
'use strict';

!(function (_DC) {
    var __ = _DC;

    var DURATION = 100,
        WIN_BREAK1 = 1366,
        WIN_BREAK2 = 1024,
        WIN_BREAK3 = 600;

    var $P = undefined,
        $PREV = undefined,
        $PREV1 = undefined,
        $PREV9 = undefined,
        $NEXT = undefined,
        $NEXT1 = undefined,
        $NEXT9 = undefined,
        $PAGE = undefined;
    var $LINKS = undefined;

    __.faq = {
        Init: function Init() {
            if ($('body.faq')) {
                this.$.Init();
                this.page.Init();
            }
        },

        $: {
            Init: function Init() {
                $P = $('.pagenation');
                $PREV = $P.find('.pagenation_prev');
                $PREV1 = $PREV.find('.one');
                $PREV9 = $PREV.find('.last');
                $NEXT = $P.find('.pagenation_next');
                $NEXT1 = $NEXT.find('.one');
                $NEXT9 = $NEXT.find('.last');
                $PAGE = $P.find('.pagenation_page');
            }
        },
        add: {
            Init: function Init() {},
            window: {
                Resize: function Resize() {
                    $(window).on('resize', function () {});
                }
            }
        },
        page: {
            Init: function Init() {
                if ($('body.pagenation').length) {
                    this.set.Init();
                    this.add.Init();
                }
            },

            lot: 10,
            currentNum: 1,
            totalNum: 1,

            set: {
                Init: function Init() {
                    this.Group();
                    this.Control();
                },
                Group: function Group() {
                    var $links = $('.faq_link');

                    var totalNum = 0;
                    $links.each(function (I, E) {
                        var groupNum = Math.ceil((I + 1) / __.faq.page.lot);
                        if (groupNum == 1) {
                            $(E).addClass('page active page' + groupNum);
                        } else {
                            $(E).addClass('page page' + groupNum);
                        }

                        totalNum = groupNum;
                    });

                    for (var i = 1, l = totalNum; i <= l; i++) {
                        $('.page.page' + i).wrapAll('<div class="faq_group page' + i + '">');
                    }

                    $('.faq_group').wrapAll('<div class="faq_links">');

                    __.faq.page.totalNum = totalNum;
                },
                Control: function Control() {
                    var i = 1;
                    while (i <= __.faq.page.totalNum) {
                        var $span = $('<span>', {
                            html: i,
                            'class': 'pagenation_num page' + i
                        });
                        var $div = $('<div>').append($span);
                        $PAGE.append($div);

                        i++;
                    }
                },
                Position: function Position(_event, _slick, _current, _next) {
                    var next = _next || 0;

                    if (next == 0) {
                        $PREV.attr('data-position', 'inactive');
                        $NEXT.attr('data-position', '');
                    } else if (next + 1 == __.faq.page.totalNum) {
                        $PREV.attr('data-position', '');
                        $NEXT.attr('data-position', 'inactive');
                    } else {
                        $PREV.attr('data-position', '');
                        $NEXT.attr('data-position', '');
                    }
                }
            },
            get: {},
            add: {
                Init: function Init() {
                    this.Slick();
                    this.BeforeChange();
                    this.PrevNext();

                    __.document.get.Init();
                    __.document.set.Init();
                },
                Slick: function Slick() {
                    $LINKS = $('.faq_links');

                    var slidesToShow = Math.min(__.faq.page.totalNum, 5);

                    $LINKS.slick({
                        asNavFor: '.pagenation_page',
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        edgeFriction: 0.15,
                        infinite: false,
                        variableWidth: false,
                        respondTo: 'slider'

                    });
                    $('.pagenation_page').slick({
                        asNavFor: '.faq_links',
                        arrows: false,
                        slidesToShow: slidesToShow,
                        centerPadding: 10,
                        centerMode: true,
                        focusOnSelect: true,
                        focusOnChange: true,
                        swipeToSlide: true,
                        edgeFriction: 0.15,
                        infinite: false,
                        variableWidth: true,
                        respondTo: 'min'
                    });
                },
                BeforeChange: function BeforeChange() {
                    __.faq.page.set.Position();
                    $LINKS.on('beforeChange', __.faq.page.set.Position);
                },
                PrevNext: function PrevNext() {
                    $PREV1.on('click', function () {
                        $LINKS.slick('slickPrev');
                    });
                    $PREV9.on('click', function () {
                        $LINKS.slick('slickGoTo', 0);
                    });

                    $NEXT1.on('click', function () {
                        $LINKS.slick('slickNext');
                    });
                    $NEXT9.on('click', function () {
                        $LINKS.slick('slickGoTo', __.faq.page.totalNum - 1);
                    });
                }
            },
            change: {
                Init: function Init() {},
                CurrentGroup: function CurrentGroup() {
                    var currentNum = __.faq.page.currentNum;

                    var $group = $('.faq_group');
                    var activeClass = '.page' + currentNum;
                    var $activeGroup = $group.filter(activeClass);

                    $group.removeClass('active');
                    //$group.hide(DURATION);

                    $activeGroup.addClass('active');
                    //$activeGroup.show(DURATION);
                }
            }

        }
    };

    window.DC = __;
})(window.DC || {});

$(function () {
    DC.faq.Init();
});
//-------------------------------------------------

