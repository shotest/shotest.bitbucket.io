'use strict';
window.POM = (function (__) { 
  const _ = __;

  _.util = {
    Init: () => { },
    Ua: () => {
      const _ua = window.navigator.userAgent.toLowerCase();
      const uaList = [
        ['iphone', 'iphone',true],
        ['ipad', 'ipad', true],
        ['ipad', 'macintosh', 'ontouchend' in document],
        ['ipod', 'ipod', true],
        ['android', 'android', true],
        ['ie', 'msie|trident', true],
        ['edge', 'edge', true],
        ['chrome', 'chrome', true],
        ['firefox', 'firefox', true],
        ['safari', 'safari', true],
        ['opera', 'opera', true],
        ['else', '.', true]
      ];

      let ua = {};
      uaList.some((V) => {
        const reg = new RegExp(V[1], 'ig');
        return ua[V[0]] = V[2]? _ua.match(reg) || '' : '';
      })

      return ua;
    },
    Escape: (STR) => {
      if (!STR) return;
      return STR.replace(/[<>&\'\`]/g, (match) => {
        const escape = {
          '<': '&lt;',
          '>': '&gt;',
          '&': '&amp;',
          '"': '&quot;',
          "'": '&#39;',
          '`': '&#x60;'
        };
        return escape[match];
      });
    },
    obj: {
      Assign: function(T){
        if (Object.assign) {
          return Object.assign(T, ...arguments);

        } else {
          var to = Object(T);
          for (var i = 1; i < arguments.length; i++) {
            var nextSource = arguments[i];
            if (nextSource === undefined || nextSource === null) {
              continue;
            }
            nextSource = Object(nextSource);

            var keysArray = Object.keys(Object(nextSource));
            for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
              var nextKey = keysArray[nextIndex];
              var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
              if (desc !== undefined && desc.enumerable) {
                to[nextKey] = nextSource[nextKey];
              }
            }
          }
          return to;
        }
      }
    },
    num: {
      Fix: (NUM, DIGIT) => {
        return ('0000000' + NUM).slice(DIGIT * -1)
      },
      Comma: (NUM) => {
        return String(NUM).replace(/(\d)(?=(\d{3})+$)/g, '$1,')
      },
      Mean: (A, B) => {
        return (A instanceof Array && !B) ? d3.mean(A) : d3.mean([A, B]);
      },
      Per: (NUM, _DIGIT) => {
        const DIGIT = _DIGIT || 1;
        if (NUM == 1) {
          return '100';
        }
        return (NUM * 100).toFixed(DIGIT)
      }
    },
    Date: (FMT) => {
      var _DATE = new Date();

      return FMT.replace(/(Y+|M+|D+|W+|w+|h+|m+|s+)/gm, function (s) {
        var len = String(s).length;
        switch (s) {
          case String(s.match(/Y+/)): return _.util.num.Fix(_DATE.getFullYear(), len);
          case String(s.match(/M+/)): return _.util.num.Fix(_DATE.getMonth() + 1, len);
          case String(s.match(/D+/)): return _.util.num.Fix(_DATE.getDate(), len);
          case String(s.match(/h+/)): return _.util.num.Fix(_DATE.getHours(), len);
          case String(s.match(/m+/)): return _.util.num.Fix(_DATE.getMinutes(), len);
          case String(s.match(/s+/)): return _.util.num.Fix(_DATE.getSeconds(), len);
          default: break;
        }
      });
    },
    Type: (T) => {
      return Object.prototype.toString.call(T);
    },
    is: {
      Obj: (T) => {
        return /Object/.test(_.util.Type(T));
      },
      Arr: (T) => {
        return /Array/.test(_.util.Type(T));
      },
      Func: (T) => {
        return /Function/.test(_.util.Type(T));
      },
      Str: (T) => {
        return /String/.test(_.util.Type(T));
      },
      Num: (T) => {
        return /Number/.test(_.util.Type(T));
      },
      Filelist: (T) => {
        return /FileList/.test(_.util.Type(T));
      },
      TouchDevice: () => {
        return (
          !!_.config.device.iphone
          || !!_.config.device.ipad
          || !!_.config.device.ipod
          || !!_.config.device.android
        );
      }
    },
    ss: {
      Get: (KEY) => {
        return sessionStorage.getItem(KEY);
      },
      Set: (KEY, VAL) => {
        sessionStorage.getItem(KEY, VAL);
      },
      Clear: () => {
        sessionStorage.clear();
      }
    },
    arr: {
      Unique: (ARR) => {
        return ARR.filter((V, I, A) => {
          return A.indexOf(V) == I;
        })
      },
      Find: function (ARR, PRE) {
        if (Array.prototype.find) {
          return ARR.find(PRE);
        } else {
          var length = ARR.length >>> 0;
          var thisArg = arguments[1];
          var value;

          for (var i = 0; i < length; i++) {
            value = ARR[i];
            if (PRE.call(thisArg, value, i, ARR)) {
              return value;
            }
          }
          return undefined;
        }
      }
    }

  };

  return _;

})(window.POM || {});