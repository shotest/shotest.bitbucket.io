'use strict';
window.POM = (function (__) { 
  const _ = __;

  const
      MODAL_NAME = '_modal' //デフォは_modalを呼び出します
    , EXTENSION = 'html'
    , DOT_EXTENSION = '.' + EXTENSION
    , ID_DATA_KEY = 'id' //デフォはdata-idがついた要素をdata-idの値をつけてmodalします
    , PASSWORD_TOGGLE_CLASS = 'visible'
    ;

  const
    URL_ARR = location.href.split('/')
    , URL_FILENAME = URL_ARR.pop()
    , URL_PATH = URL_ARR.join('/') + '/'
    , URL_MODAL_FILENAME = URL_FILENAME.replace(DOT_EXTENSION,MODAL_NAME + DOT_EXTENSION)
    , URL_MODAL = new URL(URL_PATH + URL_MODAL_FILENAME)
    ;
  
  const
      _body = 'body'
    , _password = 'input[name="password"]'
    , _togglePassword = '.password-toggle'
    , _tablePager = '.table-navigation-page'
    , _dataId = '[data-id]'
    , _formEndDefault = '[value="2999-12-31"]'
    , _modal = '.modal'
    , _modalFrame = '.modal-frame iframe'
    , _modalBack = '.modal-back'
    , _modalClose = _modalBack + ',.modal-close'
    , _copyButton = '.symbol-content_copy'
    , _cancelButton = '.symbol-cancel'
    ;

  let
      $body
    , $password
    , $togglePassword
    , $tablePager
    , $dataId
    , $formEndDefault
    , $modal
    , $modalFrame
    , $modalBack
    , $modalClose
    , $copyButton
    , $cancelButton
    ;

  // ---------------------------------------
  // Init
  // ---------------------------------------
  _.Init = function () {
    _.$.Init();
    _.action.Init();
    _.modal.Init();
  };
  
  // ---------------------------------------
  // $
  // ---------------------------------------
  _.$ = {
    Init: function () { 
      //URL_MODAL = new URL(location.href);
      $body = $(_body);
      $password = $(_password);
      $togglePassword = $(_togglePassword);
      $tablePager = $(_tablePager);
      $dataId = $(_dataId);
      $formEndDefault = $(_formEndDefault);
      $modal = $(_modal);
      $modalFrame = $(_modalFrame);
      $modalBack = $(_modalBack);
      $modalClose = $(_modalClose);
      $copyButton = $(_copyButton);
      $cancelButton = $(_cancelButton);
    }

  };

  // ---------------------------------------
  // Action
  // ---------------------------------------
  _.action = {
    Init: function () {
      this.add.form.Visible();
      this.add.form.Page();
      this.add.form.Copy();
      //this.add.user.Edit();
      //this.add.order.Info();
      this.add.Modal();

      this.set.form.Today();
    },
    add: {
      form: {
        Visible: function () {
          $togglePassword.on('click', function () { 
            const $this = $(this);
            const $target = $this.siblings().filter(_password);

            if ($this.hasClass(PASSWORD_TOGGLE_CLASS)) {
              $this.removeClass(PASSWORD_TOGGLE_CLASS);
              $target.attr('type','password');
            } else {
              $this.addClass(PASSWORD_TOGGLE_CLASS);
              $target.attr('type','text');
            }
          })
        },
        Page: function () { 
          $tablePager.on('change', function (_e) {
            const page = _e.currentTarget.value;
            
            // ページ処理？
            alert(page);

          });
        },
        Copy: function () { 
          $copyButton.on('click', function () { 
            const $this = $(this);
            const $target = $this.siblings().filter('[value]');
            const value = $target[0].value;
            
              navigator.clipboard.writeText(value).then(
                function() {
                  alert('コピーしました。')
                },
                function() {
                  alert('コピーできませんでした。')
                },
              );

            return false;
          })
        }
      },
      Modal: function () {
        $dataId.on('click', function () { 
            const $this = $(this);
            const key = $this.data(ID_DATA_KEY);

            URL_MODAL.searchParams.set(ID_DATA_KEY, key);
            _.modal.Show(URL_MODAL.toString())
          })  
      },
    },
    set: {
      form: {
        Today: function () { 
          $formEndDefault.attr('value', _.util.Date('YYYY-MM-DD'));
        }
      }
    }
  };

  // ---------------------------------------
  // Modal
  // ---------------------------------------
  _.modal = {
    Init: function () { 
      _.modal.set.Close();
      _.modal.set.Cancel();
    },
    is: {
      get modal() {
        return $body.hasClass('modal');
      }
    },
    set: {
      Close: function () { 
        $modalClose.on('click', function(){
          _.modal.Close();
        })
      },
      Cancel: function () { 
        if (!_.modal.is.modal) {
          return false;
        }
        $cancelButton.on('click', function () { 
          $('.modal-close', window.parent.document).trigger('click');
        })
      } 
    },
    Show: function (_href,_query) { 
      const href = _href || MODAL_NAME_EXT;
      const query = _query || '';

      $modalFrame.attr('src', href);

      $modal.show();
    },
    Close: function () {
      $modal.hide();
    },
    Cancel: function () { 

    }
  };

  return _;

})(window.POM || {});

$(POM.Init);